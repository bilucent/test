/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package Infrastructure;

import clone.Clone;
import com.rits.cloning.Cloner;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import resources.Parent;
import routing.Routing;
import statistics.Stats;
import utilities.Print;

/**
 *
 * @author Bijan
 */
public class Node {
    /** total variable per node. */
    private int mNode_dimension;
    /** general variables per node. */
    private int mNoF_Node_general_info;
    /** integer ID of the node. */
    private int mNode_id;
    /** node is network, DC, Wireless, etc */
    private String mNode_type;
    /**  node cost. */
    private String mNode_cost;
    /**  node delay??. */
    private String mNode_delay;
    /**  geographical info */
    private String mX;
    /**  geographical info */
    private String mY;
    /**  geographical info */
    private String mZ;    
    
    private Parent NR_OBJ;
    //here I have a list for stats objects for differnet types
    private List<Stats> All_stats;    
    
    /**  node resources, key is dimension, value is serialised resources */
    private HashMap<String,String> mNodeResources;
    /**  node in a virtualized network */
	/** the routing object of this node(in its VN). */
    private Routing mRouting;    
    boolean Debug;
    Print P = new Print();
    
    //private List<Resources> mRerouces;
    //initiialise the node
    public Node(boolean debug,int Node_dimension,int NoF_Node_general_info,int Node_id,
            String Node_type,String Node_cost,String Node_delay,String x,
            String y, String z,HashMap resources,
            Parent nr_obj){
        
        this.mNode_id = Node_id;
        this.mNoF_Node_general_info = NoF_Node_general_info;
        this.mNode_cost = Node_cost;
        this.mNode_delay = Node_delay;
        this.mNode_dimension =  Node_dimension;
        this.mX = x;
        this.mY = y;
        this.mZ = z;
        this.mNode_type = Node_type;
        this.mRouting = new Routing(this);//from Joans: I wont use my routing object now
        this.mNodeResources = resources;
        this.Debug = debug;
        
        this.NR_OBJ = nr_obj;
        // keep record of the allocations
        this.All_stats  = new ArrayList<>();
    }
    public Node(Node copy){
        
        Cloner cloner=new Cloner();
        this.mNode_id = copy.mNode_id;
        this.mNoF_Node_general_info = copy.mNoF_Node_general_info;
        this.mNode_cost = copy.mNode_cost;
        this.mNode_delay = copy.mNode_delay;
        this.mNode_dimension =  copy.mNode_dimension ;
        this.mX = copy.mX;
        this.mY = copy.mY;
        this.mZ = copy.mZ;
        this.mNode_type = copy.mNode_type;
//        this.mRouting = new Routing(this);//from Joans: I wont use my routing object now
        this.mRouting =  new Routing(this);//from Joans: I wont use my routing object now
        this.mNodeResources = Clone.CloneHashStrStr(copy.mNodeResources); ;
//        this.mNodeResources = cloner.deepClone(copy.mNodeResources); ;
        this.Debug = copy.Debug;
        
        this.NR_OBJ = new Parent(copy.get_ParentClass_resrouces());
//        this.NR_OBJ = (copy.get_ParentClass_resrouces());
        // keep record of the allocations
        this.All_stats  = copy.All_stats ;
    }

    public Parent get_ParentClass_resrouces()
    {
        return NR_OBJ;
    } 
    
    public int get_resrouces_dimension()
    {
        return mNodeResources.size();
    }
    // getters and setters
    public int getmID() {
            return mNode_id;
    }
    public String getmNode_Type() {
            return mNode_type;
    }
    
    public Routing getmRouting() {
            return mRouting;
    }
    
    public void setRouting(Routing routing)
    {
        this.mRouting = routing;
    }
    
    //each node holds it access routes to all other nodes
    public int returnNumberOfRoutes_all(Routing routing)
    {
        return mRouting.getmRoutingTable().get_number_of_routes_all();
    }    

    
    public void setLayerResrouce(String res, int layer)
    {
        mNodeResources.put("D"+layer, res);
    }
    
    //Breaking/deviding resrouces to 1/deivde piece
    public void cutResrouces(int divide)
    {
        for(int i =0; i < mNodeResources.size();i++)
        {
            String res = mNodeResources.get("D"+i);
            res = res.substring(0,(int) (res.length()/divide));
            setLayerResrouce(res,i);
        }
    }  
    
    
        /*
         * this class will be used to save the information per node as simulation goes on
         */
        public void update_stats(String req_type, int alloc)
        {
            if(check_if_reqType_exists(req_type)>-1)
            {
                this.All_stats.get(alloc).add_to_allo_list(alloc);
                this.All_stats.get(alloc).incremenet_counter();
            }
            else
            {
                Stats stat = new Stats(req_type);
                stat.add_to_allo_list(alloc);
                stat.incremenet_counter();
                this.All_stats.add(stat);
            }

        }
        
        
        //check if this type of request has been used before
        public int check_if_reqType_exists(String req_type)
        {
            int exists = -1;
            if(this.All_stats.size()>0)
            {
                for(int i  = 0; i < this.All_stats.size();i++)
                {
                    String current_type = this.All_stats.get(i).givemethe_associated_reqtype();
                    if(req_type.matches(current_type))
                    {
                        exists = i;
                        
                    }
                    
                }
                return exists;
            }
            else return exists;
            
        }
        public void reset_node()
        {
           List<String> Res_inf =  this.NR_OBJ.getm_resinfo();
           Parent lr_obj = new Parent(Debug, Res_inf);
           this.NR_OBJ = new Parent(lr_obj);
           
        }            
        public void show_resrouces(int vod_id)
        {
            P.println(" node id : " + this.mNode_id);
            
            Parent p = get_ParentClass_resrouces();
            P.println("  the node parent resources: " + p.getme_List_all_layered_resources().toString());
            
            
//            for(int i = 0; i < this.mLinkResources.size(); i++)
//            {
//                P.println("  VON ID : "+ vod_id + " resources of D"+ i + "" + this.mLinkResources);
//            }        
        }    
}
