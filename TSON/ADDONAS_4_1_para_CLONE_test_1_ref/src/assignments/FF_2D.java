/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package assignments;

import java.util.ArrayList;
import utilities.Print;

/**
 *
 * @author Bijan
 */
public class FF_2D implements Assignment_base 
{

    Print p = new Print();
    
    public int[] run_assignment(int First_value,int Second_value,  int[] First_array , int[] Second_array, int First_delay, int Second_delay) 
    {
        ArrayList D0_slots = new ArrayList();
        int [] temp_arr = new int[First_array.length];
        for(int i = First_delay; i < First_array.length; i++)
        {
            if(First_value>0){
                if(First_array[i]==0)temp_arr[i] = 1;
                D0_slots.add(i);
                First_value--;
            }
            else break;
        }
       
        p.print_1D(" FF_2D the input array: ", First_array);
        p.print_1D(" FF_2D the result array: ", temp_arr);
        
        //now the next (second) dimension
        run_assignment(D0_slots, Second_value, Second_array, Second_delay);
        
        
        return temp_arr;    
    }
    public int[] run_assignment(ArrayList list, int Fisrt_value,int[] First_array ,int First_delay) 
    {
        ArrayList D0_slots = new ArrayList();
        int [] temp_arr = new int[First_array.length];
        for(int i = First_delay; i < First_array.length; i++)
        {
            if(Fisrt_value>0){
                if(First_array[i]==0)temp_arr[i] = 1;
                D0_slots.add(i);
                Fisrt_value--;
            }
            else break;
        }
        p.print_1D(" FF_2D the input First_array: ", First_array);
        p.print_1D(" FF_2D the result array: ", temp_arr);
        
        return temp_arr; 
    }    

    @Override
    public int[] run_assignment(int value, int[] array) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public int[] run_assignment(int First_value, int[] First_array, int First_delay) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public int[] run_assignment(ArrayList First_indeces, ArrayList Second_indeces, int Fisrt_value, int[] First_array, int First_delay) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }



    
     
}
