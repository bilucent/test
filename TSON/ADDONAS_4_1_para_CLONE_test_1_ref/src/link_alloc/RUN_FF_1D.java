///*
// * To change this template, choose Tools | Templates
// * and open the template in the editor.
// */
//package link_alloc;
//
//import Infrastructure.Link;
//import Infrastructure.Network;
//import Infrastructure.Node;
////import Infrastructure.ResourcesTable;
//import RequestTypes.Req_BW;
//import RequestTypes.Request_base;
//import assignments.Assignment_alg_return;
//import assignments.Assignment_base;
//import java.util.Collections;
//import java.util.HashMap;
//import java.util.List;
//import routing.Route;
//import routing.RoutingTable;
//import utilities.Print;
//import utilities.Converters;
//
///**
// *
// * @author Bijan
// */
//public class RUN_FF_1D extends Link_algortihm_base{
//    
//    Print P;
//    Converters  C; 
//
//    public RUN_FF_1D(Network mNetwork, int numRoutes) {
//        this.mNetwork = mNetwork;
//        this.mNumKRoutes = numRoutes;
//        this.P = new Print();
//        this.C = new Converters();
//    }
//
//    
//    public void process_request_init(Req_BW req, String process_type){
//        List nodes_list = mNetwork.getmListNodes();
//        List links_list = mNetwork.getmListLinks();
//        
//        Node node = (Node) nodes_list.get(req.returnSrc());
//        P.println("in process request initi method " );
//        node.getmRouting().print_routes();
//        Route route = node.getmRouting().getmRoutingTable().getRoute(req.returnsDest(), 0);
//        //Route route = node.getmRouting().getmRoutingTable().getShortestRoute(req.returnsDest());
//        P.println("src: " + req.returnBW());
//        String req_type = req.returnAlgorithmType();
//        if(req_type=="basic")
//        {
//            if(process_type == "delay")
//            {
//               P.println(" the algorithm run successfuly? "+ process_basic_bw(req, route));
//            }
//        }
//        else 
//        {
//            P.println(" no such request type");
//            System.exit(23);
//        }
//        
//        
//    }
//    private boolean process_basic_bw(Request_base req, Route route){
//
//        List links = mNetwork.getmListLinks();
//        HashMap link_adj = mNetwork.return_link_str2num_hash_list();
//        List<String> hops = route.return_hops_in_route();
//        int cumulative_delay =0;
//        int num_hops = hops.size();
//        P.println(" numner of hops in FF link: " + num_hops);
//        
//        for (int i = 0; i < num_hops; i++){
//            
//            int link_number = (int) link_adj.get(hops.get(i));
//             
//            Link link = (Link) links.get(link_number);
//            //int delay = 8;
//            P.println(" link.getmLength: " + link.getmLength());
//            int delay = (int) Double.parseDouble(link.getmLength());
//            P.println(" link delay: " + delay);
//            HashMap resources = link.get_resrouces();
//            P.println(" the resource_1D from link: resources.size()" + resources.size());
//            String resource_1D = (String) resources.get("D0");
//            P.println(" the resource_1D from link:" + resource_1D);
//            int array[] = C.convert_string_to_1Darray(resource_1D);
//            P.print_1D(" converted str resource to array: " , array);
//            //1 is FF_1D
//            Assignment_base algorithm = Assignment_alg_return.return_alg(1);
//            
//            // the first hop has no delay, bascially
//            if(i>0) cumulative_delay = delay + cumulative_delay;
//            
//            algorithm.run_assignment(req.returnBW(),array,cumulative_delay);
//        }
//        
//        
//        return false;
//    }
//
//}
