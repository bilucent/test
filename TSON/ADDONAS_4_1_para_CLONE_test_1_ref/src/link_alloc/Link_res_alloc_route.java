/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package link_alloc;

import Infrastructure.Link;
import Infrastructure.Network;
import Infrastructure.ResourceOperations;
import java.util.ArrayList;
import java.util.List;
import resources.Parent;
import routing.Route;
import utilities.Converters;
import utilities.Maths;
import utilities.Print;

/**
 *
 * @author Bijan
 */
public class Link_res_alloc_route {
    
    Route Sub_route;
    String alloc_policy;
    List<Link> links_list;
    List<Parent> link_res_family;
    List<List> combof_all_layered_resoruces;
    List<List> all_layered_resoruces;
    List<List> final_allLayers_combined;
    Network mNetwork;
    int resource_layers =0;
    
    int hop_number_with_index_of_min_level_res = 0;
    public Link_res_alloc_route(Network network,Route sub_route)
    {
        this.mNetwork = network;
        this.Sub_route = sub_route;
        this.links_list = mNetwork.return_links_in_route(sub_route);
        this.combof_all_layered_resoruces = new ArrayList<>();
        this.all_layered_resoruces = new ArrayList<>();
        this.final_allLayers_combined = new ArrayList<>();
        this.link_res_family = new ArrayList<>();
        for(int i =0; i < links_list.size(); i ++)
        {
            // get all the parent calsses
            this.link_res_family.add(links_list.get(i).get_ParentClass_resrouces());
            //Print.println(" \t th resources per hop in the Link_res_alloc_route class " + links_list.get(i).get_ParentClass_resrouces().getme_List_all_layered_resources().toString());

            //get all the layered lists
            this.all_layered_resoruces.add(link_res_family.get(i).getme_List_all_layered_resources());
        }
        
        
  
        this.cal_route_wide_levleof_res_layer();
        //this.sort_layercombined_inRoute_res_family();
        this.final_combination();
        
        
    }
    
    public void cal_route_wide_levleof_res_layer()
    {
        int link_with_min_res_level;
        int[] res_levels = new int[links_list.size()];
        for(int i =0; i < links_list.size(); i ++)
        {
            res_levels[i] = link_res_family.get(i).return_res_number_layers();
        }
        int min = Maths.find_min(res_levels);
        int min_index = Maths.find_min_return_index(res_levels);
        this.resource_layers = min;
        this.hop_number_with_index_of_min_level_res = min_index;
        
        //Print.println("  in cal route leevl minimum level ... " + min +   " and the index is " + min_index);
 
    }    
    public List<String> res_info_structure()
    {
        List<Link> links = mNetwork.return_links_in_route(Sub_route);
        Link l =links.get(hop_number_with_index_of_min_level_res);
        return l.get_ParentClass_resrouces().getm_resinfo();
    }
    
    
    public int getm_route_wide_levleof_res_layer()
    {
 
        return this.resource_layers;
    }
    public int getm_number_hops()
    {
        return Sub_route.getmLength();
    }

    public  List<List> sort_layercombined_inRoute_res_family()
    {
        List<List> sorted_layers = new ArrayList<>(); 
        for(int i = 0; i <this.resource_layers; i++ )
        {
            List<List> layers = new ArrayList<>();
            for(int j = 0; j <this.all_layered_resoruces.size(); j++ )
            {
                // ecch of the added layered_hops still are a list of a number of strings
                //Print.println(" in sorting thinig" + this.all_layered_resoruces.get(j).get(i));
                layers.add((List)this.all_layered_resoruces.get(j).get(i));
            }
            //for the numbe rof hops, I have sortedlayer-->lists-->list of strings
            sorted_layers.add(layers);
            //Print.println(" subroute sorting thinig" + Sub_route.toString());
            
        }
        //Print.println(" layers are organised in lists" + sorted_layers );
        return sorted_layers;
    }
        
    public void final_combination()
    {
        //List<List> final_allLayers_combined = new ArrayList<>();
        
        List<List> sorted = sort_layercombined_inRoute_res_family();
        
        // through the layered_hops
        for(int i = 0; i <sorted.size(); i++ )
        {
            List<String> final_layers = new ArrayList<>();
            List<List> layered_hops = sorted.get(i);
            //Print.println("  layered_hops " + layered_hops);
            
            
            /*
             * note: here we have an in to out approach,
             * meaning, we are skimming through the memebrs first, and then the 
             * arrays, so instead of first choosing an array, and then p[laying
             * with the memebrs, we are choosing memerbs across all array
             * there fore J and K counter are reversely put
             */
                    
            
            //through each layer--> hops
            for(int j = 0; j <layered_hops.get(0).size(); j++ )
            {
                
                List<String> hops_cells = new ArrayList();
                String combined_cell = "";
                //get the cell, in each hop
                for(int k = 0; k <layered_hops.size();k++ )
                {
                    List<String> in_each_cell_list = layered_hops.get(k);
                    //Print.println("  in_each_cell_list " + in_each_cell_list);
                    hops_cells.add(in_each_cell_list.get(j));
                    
                }
                combined_cell = res_combiner(hops_cells);
                //Print.println(" \ngood loock at tthis combined_cell " + combined_cell);
                final_layers.add(combined_cell);
            }   
            final_allLayers_combined.add(final_layers);
        }
        
        //Print.println(" \n The links are combined now and to be used for allocations " + final_allLayers_combined);
    }
    public List<List> return_final_combination()
    {
       
        return final_allLayers_combined;
    }
        
    public String res_combiner(List<String> res){
        
        int [] layer_arr = new int[res.get(0).length()];
        for(int i = 0; i <res.size(); i++ )
        {    
            int [] this_layer = Converters.convert_string_to_1Darray(res.get(i));
            int []combined_arr = ResourceOperations.combine_arrays(layer_arr,this_layer,0);
            layer_arr = combined_arr;
        }
        

        return Converters.convert_array_to_string(layer_arr);
        
    }
    
    
     public int [] allocated_add_existing(int [] existing_array, int [] new_array)
     {
         int [] final_array = new int [existing_array.length];
         int i = 0;
         while((i )<existing_array.length)
         {
             final_array[i] = existing_array[i] + new_array[i];
             i++;
         }
         
         return final_array;
     } 
      
}
