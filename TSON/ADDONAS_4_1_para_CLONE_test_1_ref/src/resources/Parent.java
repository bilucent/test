/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package resources;

import Infrastructure.ResourceOperations;
import clone.Clone;
import com.rits.cloning.Cloner;
import java.util.*;
import java.util.logging.Level;
import java.util.logging.Logger;
import utilities.Converters;
import utilities.Print;

/**
 *
 * @author Bijan
 */
public class Parent{

    
    List<String> keys;
    HashMap<String, String> Res_hash;
    HashMap<String, Child> Children;
    String resource;
    List<Child> DOWNclasses; 
    List<List> List_all_layered_resources;
    
    // can be from node or link
    List<String> Res_info;
    int V;
    int H;
    variables v; 
    String layer_resource; 
    int H_counter  =0;
    boolean Debug =false;
    
    public Parent(boolean debug,List<String> res_info)
    {
        this.v = new variables(res_info);
        this.Debug = debug;
        this.List_all_layered_resources = new ArrayList<>();
        for(int i =0; i < res_info.size(); i ++)
        {
            List<String> layered_resources = new ArrayList<>();
            List_all_layered_resources.add(layered_resources);
        }
                    //Print.println("  IN PARENT CONSTRUCT List_all_layered_resources : " +  List_all_layered_resources.toString() );

        this.Res_info = res_info;
        this.keys = new ArrayList<>();
        this.Children = new HashMap<>();
        this.Res_hash = new HashMap<>();
        
        this.DOWNclasses = new ArrayList<>();
               
        //this.H = v.getm_H();
        this.V = v.getm_V();
        
        this.layer_resource = v.getMResList_in_Layer(this.V);
        this.resource = layer_resource;
        //Print.println("  in while , \t get horiz all " + v.getm_layered_Horiz_member(V));
        
        this.H = v.getm_layered_Horiz_member(V);
        
        // so here, add a string to the layer 
        this.List_all_layered_resources.get(V).add(layer_resource);
        this.Res_hash.put(V +":" + H, layer_resource);
        this.keys.add(V +":" + H);
        this.v.increase_layered_Horiz_member(V);
//        
//        Print.println("  V: " + v.getm_V()  + " \t H: " + v.getm_H() + 
//                " resource :" + resource + " \tresrouce.length: " + resource.length());
//        Print.println("  V: " + v.getm_V()  + " \t H: " + v.getm_H() + 
//                " resource :" + resource + " \tresrouce.length: " + resource.length());

        try {
            run();
        } catch (CloneNotSupportedException ex) {
            Logger.getLogger(Parent.class.getName()).log(Level.SEVERE, null, ex);
        }

    }
   public Parent(Parent copy)
   {
        this.Children = copy.Children;
        this.DOWNclasses = copy.DOWNclasses;
        this.Debug = copy.Debug;
        this.H = copy.H;
        this.H_counter = copy.H_counter;
        
        Cloner cloner=new Cloner();
        //Link  l_clone = cloner.deepClone(l);         
//        this.List_all_layered_resources = cloner.deepClone(copy.getme_List_all_layered_resources());
        this.List_all_layered_resources = Clone.CloneListList(copy.List_all_layered_resources);
        //this.List_all_layered_resources = copy.getme_List_all_layered_resources() ;
        this.Res_hash = Clone.CloneHashStrStr(copy.Res_hash) ;
        this.V = cloner.deepClone(copy.V);
        this.keys = cloner.deepClone(copy.keys);
        this.layer_resource =cloner.deepClone(copy.layer_resource);
        this.resource  = cloner.deepClone(copy.resource);
        this.v = cloner.deepClone(copy.v) ;
        this.Res_info = cloner.deepClone(copy.getm_resinfo());
        this.Children = copy.Children;

       
   }
    
    
    public void run() throws CloneNotSupportedException
    {

        while((H_counter < layer_resource.length()))
        { 
            //Print.println("  in while , \t get horiz all " + v.getm_layered_Horiz_member(V));

            v.set_V_(this.V);
            while((v.getm_V()  + 1< v.max_vertical_levels) )
            {                          
                //Print.println("  in while , \t V: " + v.getm_V() + " \tH: " + v.getm_H() );
                v.increase_V();
                //Print.println(" \tintro node V: " +v.getm_V()  + " \t H_counter: " + H_counter + " resource :" + this.layer_resource);

                //DOWNclasses.add(new Child(Children,v, H_counter));
                Child child = new Child(List_all_layered_resources,
                        Children,Res_hash, v, H_counter);
                DOWNclasses.add(child);
                int H_layered = v.getm_layered_Horiz_member(V);
                Children.put(V +":" + H_layered, child);
            }
            this.H_counter++;
            //Print.println("  in while , \t get horiz all " + v.getm_layered_Horiz_member(V));
            //v.increase_layered_Horiz_member(V);
        }
    }
    
    public void set_List_all_layered_resources(List<List>  updated_resrouces)
    {
        this.List_all_layered_resources = updated_resrouces;
    }
    
    public List<String>  getm_resinfo()
    {
       return Res_info;
    }
    
    public int getmeRootInfo()
    {
        return DOWNclasses.size();
    }
    
    public int getmeRootChildrenNumber()
    {
        return DOWNclasses.size();
    }  
    
    public HashMap<String, String>  getmeResHash()
    {
        return Res_hash;
    } 
    
    /*
     * return the list of lists of the string resources
     */
    public List<List>  getme_List_all_layered_resources()
    {
        return List_all_layered_resources;
    }
    
    /*
     * show how many obects(string) exist in each layer of the rsrouces
     */
    public void printLayers_List_all_layered_resources()
    {
        for(int i = 0; i < List_all_layered_resources.size(); i++)
        {
           Print.println(" List resources size in layer " + i + " -->" + List_all_layered_resources.get(i).size()); 
        }
        
    }
    
    
    /*
     * the function below return the list of the resrouces in a layer
     */
    
    public String getm_one_cell_inonce_layer_resources(int i, int j)
    {

        return (String) this.List_all_layered_resources.get(i).get(j);
    }    
    
    /*
     * the function below return the list of the resrouces in a layer
     */
    
    public List<String> getm_one_layers_resources(int i)
    {

        return this.List_all_layered_resources.get(i);
    }    
    
    
    /*
     * the function below calcualte the available resoruces in each layer
     */
    
    public List<Integer> getm_Agg_layers_resources()
    {
        //List<List> detailed_res = getm_detailed_resources_();
        List<Integer> agg_availabilities = new ArrayList<>();
        for(int i = 0; i < List_all_layered_resources.size(); i++)
        {
            
            List<String> layer_components_list = List_all_layered_resources.get(i);
            int aggregator = 0;
            for(int j = 0; j < layer_components_list.size(); j++)
            {
                String str = layer_components_list.get(j);
                if(Debug)
                Print.println(" String to be 0counted " + str);
                int zeroes = ResourceOperations.count_zeros(str);
                if(Debug)
                Print.println(" number of zeros counted " + zeroes
                         + " in string :" + str);
                aggregator = aggregator+ zeroes;
            }
            agg_availabilities.add(aggregator);
        }
        return agg_availabilities;
    }
    /*
     * this function displays the aggregated layers
     */
    public void getm_Agg_layers_resources_demonstrator()
    {
        List<Integer> l = getm_Agg_layers_resources();
        for(int i = 0 ; i < l.size(); i++)
        {
            Print.println(" the aggregate res in layer " 
                    + i + " is : " + l.get(i));
        }
    }

    /*
     * 
     */
    
    /*
     * the function below calcualte the available resoruces in each layer
     */
    
    public List<Integer> getm_used_agg_layers_resources()
    {
        List<Integer> capacity = give_me_all_capacity_per_layer();
        List<Integer> availabilities = getm_Agg_layers_resources();
        if(availabilities.size()!= capacity.size())
        {
            Print.println("  Res_info.toString() : " +  Res_info.toString() );
            Print.println("  List_all_layered_resources: " +  List_all_layered_resources.toString() );
            Print.println(" problem with calculating the used and existoing availabitlities and such,  availabilities.size():"+ availabilities.size() + "  and capacity.size(): " + capacity.size() );
            System.exit(8320);
        }
        
        List<Integer> used_in_layers = new ArrayList<>();
            for(int i = 0 ; i < capacity.size(); i++)
            {

                used_in_layers.add(i,capacity.get(i) - availabilities.get(i));
            }
            
            return used_in_layers;

    }
    
    
    /*
     * this function displays the aggregated layers
     */
    public void show_used_agg_layers_resources_demonstrator()
    {
        List<Integer> l = getm_used_agg_layers_resources();
        for(int i = 0 ; i < l.size(); i++)
        {
            Print.println(" the aggregate res in layer " 
                    + i + " is : " + l.get(i));
        }
    }
    
    public List<Integer> give_me_all_capacity_per_layer()
    {
        List<Integer> capacity = new ArrayList<>();
        //capacity.add(Res_info.get(0).length());
        int upto_this_layer =0;
        for(int i = 0 ; i < Res_info.size(); i++)
        {
            if(i==0)
            {
                upto_this_layer = Res_info.get(i).length();
                capacity.add(upto_this_layer);
            }
            else
            {
                upto_this_layer = upto_this_layer * Res_info.get(i).length();
                capacity.add(upto_this_layer);
            }
        }
        
        return capacity;
    }

    /*
     * 
     */    
    public List<List> getm_detailed_resources_()
    {
        List<List> detailed_availabilities = new ArrayList<>();
        for(int i = 0; i < List_all_layered_resources.size(); i++)
        {
            
            List layer_components_list = List_all_layered_resources.get(i);
            
            List<String> resources = new ArrayList();
            for(int j = 0; j < layer_components_list.size(); j++)
            {
                String str = (String) layer_components_list.get(j);
                resources.add(str);
            }
            detailed_availabilities.add(resources);
        }
        return List_all_layered_resources; 
    }

    public void getm_detailed_resources_demonstrator()
    {
        List<List> detailed_res = getm_detailed_resources_();
        for(int i = 0; i < detailed_res.size(); i++)
        {
            List<Integer> layered_resrouces = detailed_res.get(i);
            for(int j = 0; j < layered_resrouces.size(); j++)
            {
                Print.println(" in layer: " + i + "  member: "
                        +j+"  is -->" + layered_resrouces.get(j));
            }
        }
    }
    /*
     * this method gives all the availabilities per each member/componenet
     */
    
    public List<List> getm_detailed_resources_availabiilties()
    {
        List<List> detailed_availabilities = new ArrayList<>();
        for(int i = 0; i < List_all_layered_resources.size(); i++)
        {
            
            List<String> one_layer_components_list = List_all_layered_resources.get(i);
            
            List<Integer> availabilities = new ArrayList();
            for(int j = 0; j < one_layer_components_list.size(); j++)
            {
                String str = one_layer_components_list.get(j);
                if(Debug)
                Print.println(" String to be 0counted " + str);
                int zeroes = ResourceOperations.count_zeros(str);
                if(Debug)
                Print.println(" number of zeros counted " + zeroes
                         + " in string :" + str);
 
                availabilities.add(zeroes);
            }
            detailed_availabilities.add(availabilities);
        }
        return detailed_availabilities; 
    }

    public void getm_detailed_resources_availabiilties_demonstrator()
    {
        List<List> detailed_res = getm_detailed_resources_availabiilties();
        for(int i = 0; i < detailed_res.size(); i++)
        {
            List<Integer> layered_resrouces = detailed_res.get(i);
            for(int j = 0; j < layered_resrouces.size(); j++)
            {
                Print.println(" in layer: " + i + "  member: "
                        +j+"  is -->" + layered_resrouces.get(j));
            }
        }
    }    
    /*
     * all the resrouces in the reshash map are printed
     */
    public void  printResHash()
    {
        Collection resValues = Res_hash.values();
        Set keys = Res_hash.keySet();
        Object[] KeysArray = keys.toArray();
        int counter =0;
        while(counter < Res_hash.size())
        {
           String k =  KeysArray[counter].toString();
           Print.println(" key: " + KeysArray[counter].toString() +" "
                   + "\t Res: " + Res_hash.get( k)); 
           counter++;
        }
        
    }
    
    public int return_res_number_layers()
    {
        
       return List_all_layered_resources.size();
    }
    
    public static int returnResourcesInTreeLayers(List<String> reslayer, int layer)
    {
        List<Integer> resLayerInt = new ArrayList<>();
        for(int i =0; i< reslayer.size(); i++)
        {
            resLayerInt.add(reslayer.get(i).length());
        }
        List<Integer> resLayerIntAgg = new ArrayList<>();
        int aggfactor = 1;
        for(int i =0; i< reslayer.size(); i++)
        {
            resLayerIntAgg.add(resLayerInt.get(i)*aggfactor);
            if(i<resLayerInt.size()-1)
            aggfactor = resLayerIntAgg.get(i);
            
        } 
        
//        Print.println(" the layer sizes calcualted " +  resLayerIntAgg.toString());
        return resLayerIntAgg.get(layer);
    } 
    
}
