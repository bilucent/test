/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package utilities;

import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author br12367
 */
public class Maths {
    public static Print P = new Print();
        
    public static int find_max(int array[]){
//        P.print(" \n in find max  ");
        int max=0;
        for (int i = 0;i < array.length;i++)
        {
//            P.print(" \nin array[i] =  " + array[i] + "and max is: " + max);
            if (max<array[i])max = array[i];
        }
        
        return max;
    }
        
    public static int find_min(int array[]){
        int min=array[0];
        for (int i = 0;i < array.length;i++)
        {
            //P.print(" in find min  " + array[i]);
            if (min>array[i])min = array[i];
        }
        
        return min;
    }   
    
    public static int find_min_return_index(int array[]){
        int index = 0;
        int min=array[0];
        for (int i = 0;i < array.length;i++)
        {
            //P.print(" in find min  " + array[i]);
            if (min>array[i])
            {
                min = array[i];
                index = i;
            }
        }
        
        return index;
    }  
    
    public static int[] bubbleSort(int[] array) {
        boolean swapped = true;
        int j = 0;
        int tmp;
        while (swapped) {
            swapped = false;
            j++;
            for (int i = 0; i < array.length - j; i++) {
                if (array[i] > array[i + 1]) {
                    tmp = array[i];
                    array[i] = array[i + 1];
                    array[i + 1] = tmp;
                    swapped = true;
                }
            }
        }
        return array;
    }
    public static double[] bubbleSort(double[] array) {
        boolean swapped = true;
        int j = 0;
        double tmp;
        while (swapped) {
            swapped = false;
            j++;
            for (int i = 0; i < array.length - j; i++) {
                if (array[i] > array[i + 1]) {
                    tmp = array[i];
                    array[i] = array[i + 1];
                    array[i + 1] = tmp;
                    swapped = true;
                }
            }
        }
        return array;
    }
    
 
}
