/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package utilities;

/**
 *
 * @author Bijan
 */


import java.io.*;
import java.util.*;
import org.apache.poi.hssf.usermodel.HSSFCell;
import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.poifs.filesystem.POIFSFileSystem;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;

public class ExcelWriter {
    List<String>  sheet_labels = new ArrayList<>();
    public ExcelWriter()
    {
        
    }
    public void writer(double blocking, double admission, double throughput_type,
            double total_accepted_aa,double total_blocked_aa, double total_throughput_aa, double total_used_aa,double total_cap_aa, double total_troughput_aa,int L0_util_aa, int L1_util_aa, int L2_util_aa,
            double total_accepted_bb,double total_blocked_bb, double total_throughput_bb, double total_used_bb,double total_cap_bb, double total_troughput_bb,int L0_util_bb, int L1_util_bb, int L2_util_bb,
            double total_accepted_cc,double total_blocked_cc, double total_throughput_cc, double total_used_cc,double total_cap_cc, double total_troughput_cc,int L0_util_cc, int L1_util_cc, int L2_util_cc,
            double total_accepted_dd,double total_blocked_dd, double total_throughput_dd, double total_used_dd,double total_cap_dd, double total_troughput_dd,int L0_util_dd, int L1_util_dd, int L2_util_dd,
            double total_accepted_ee,double total_blocked_ee, double total_throughput_ee, double total_used_ee,double total_cap_ee, double total_troughput_ee,int L0_util_ee, int L1_util_ee, int L2_util_ee,
            
            double total_used_L0,double total_cap_L0, double total_troughput_L0,
            double total_used_L1,double total_cap_L1, double total_troughput_L1,
            double total_used_L2,double total_cap_L2, double total_troughput_L2,
            
            double avg_route_length,
            double avg_route_length_aa,
            double avg_route_length_bb,
            double avg_route_length_cc,
            double avg_route_length_dd,
            double avg_route_length_ee,
            int excel_horiz, String line,String file_address) throws FileNotFoundException, IOException
    {
        //String file_address = "C:\\Users\\Bijan\\Dropbox\\Under_Work\\ADDONAS_1_5_1_2\\JAVAEXCEL\\new.xls";
        HSSFWorkbook workbook; 
        HSSFSheet sheet;
//        if(line.matches("1scd")&&excel_horiz == 0 )
//        {
//             workbook = new HSSFWorkbook();
//             sheet = workbook.createSheet("excel_horiz");
//
//        }
//        else
        {
                FileInputStream file = new FileInputStream(new File(file_address));
                workbook = new HSSFWorkbook(file);
                String excel_sheet_label = "series"+excel_horiz;
                Print.println("excel_sheet_label: "+ excel_sheet_label);
                Print.println("sheet_labels: "+ sheet_labels.toString());
                if(sheet_labels.contains(excel_sheet_label))
                {
                    
                    sheet = workbook.getSheet(excel_sheet_label);
                }
                else
                {
                    Print.println("make excel_sheet_label: "+ excel_sheet_label);
                    sheet_labels.add(excel_sheet_label);
                    Print.println("sheet_labels: "+ sheet_labels.toString());

                    sheet = workbook.createSheet(excel_sheet_label);
                }
//                if(!(sheet = workbook.createSheet("excel_horiz")))
                        
                //if(excel_horiz==0)
                {

                    HashMap<String, Object[]> data = new HashMap<>();

                    data.put("0", new Object[] {"admission","blocking", "throughput",
                        "total aa admitted","total aa blocked","total aa throughput", "total aa used","total aa cap","total aa util","L0 aa util","L1 aa util","L2 aa util",
                        "total bb admitted","total bb blocked","total bb throughput", "total bb used","total bb cap","total bb util","L0 bb util","L1 bb util","L2 bb util",
                        "total cc admitted","total cc blocked","total cc throughput", "total cc used","total cc cap","total cc util","L0 cc util","L1 cc util","L2 cc util",
                        "total dd admitted","total dd blocked","total dd throughput", "total dd used","total dd cap","total dd util","L0 dd util","L1 dd util","L2 dd util",                        
                        "total ee admitted","total ee blocked","total ee throughput", "total ee used","total ee cap","total ee util","L0 ee util","L1 ee util","L2 ee util",

                        "total L0 used","total L0 cap","total L0 util",
                        "total L1 used","total L1 cap","total L1 util",
                        "total L2 used","total L2 cap","total L2 util",
                            
                        " avg route lengths",
                        " avg route lengths_aa",
                        " avg route lengths_bb",
                        " avg route lengths_cc",
                        " avg route lengths_dd",
                        " avg route lengths_ee"                   
                    });
            //        data.put("2", new Object[] {1d, "John", 1500000d});
            //        data.put("3", new Object[] {2d, "Sam", 800000d});
                    data.put(line, new Object[] {blocking, admission, throughput_type,
                        total_accepted_aa, total_blocked_aa, total_throughput_aa,  total_used_aa, total_cap_aa,  total_troughput_aa,  L0_util_aa,  L1_util_aa,  L2_util_aa,
                        total_accepted_bb, total_blocked_bb, total_throughput_bb,  total_used_bb, total_cap_bb,  total_troughput_bb,  L0_util_bb,  L1_util_bb,  L2_util_bb,
                        total_accepted_cc, total_blocked_cc, total_throughput_cc,  total_used_cc, total_cap_cc,  total_troughput_cc,  L0_util_cc,  L1_util_cc,  L2_util_cc,
                        total_accepted_dd, total_blocked_dd, total_throughput_dd,  total_used_dd, total_cap_dd,  total_troughput_dd,  L0_util_dd,  L1_util_dd,  L2_util_dd,
                        total_accepted_ee, total_blocked_ee, total_throughput_ee,  total_used_ee, total_cap_ee,  total_troughput_ee,  L0_util_ee,  L1_util_ee,  L2_util_ee,
                        
                        total_used_L0, total_cap_L0,  total_troughput_L0,
                        total_used_L1, total_cap_L1,  total_troughput_L1,
                        total_used_L2, total_cap_L2,  total_troughput_L2,
                          
                        avg_route_length,
                        avg_route_length_aa,
                        avg_route_length_bb,
                        avg_route_length_cc,
                        avg_route_length_dd,
                        avg_route_length_ee           
                    });

                    Set<String> keyset = data.keySet();
                    int rownum = 0;
                    for (String key : keyset) {
                        //Row row = sheet.createRow(Integer.parseInt(key));
                        Row row = sheet.createRow(Integer.parseInt(key));
                        Object [] objArr = data.get(key); 

                        ///I added this bit to move in the excel file running more simulations
//                        int cellnum = excel_horiz;
                        int cellnum = 0;

                        for (Object obj : objArr) {
                            Cell cell = row.createCell(cellnum++);
                            if(obj instanceof Date) 
                                cell.setCellValue((Date)obj);
                            else if(obj instanceof Boolean)
                                cell.setCellValue((Boolean)obj);
                            else if(obj instanceof String)
                                cell.setCellValue((String)obj);
                            else if(obj instanceof Double)
                                cell.setCellValue((Double)obj);
                            else if(obj instanceof Integer)
                                cell.setCellValue((Integer)obj);
                        }
                    }

                    try {
                        FileOutputStream out = 
                                new FileOutputStream(new File(file_address));
                        workbook.write(out);
                        out.close();
                        System.out.println("Excel written successfully..");

                    } catch (FileNotFoundException e) {
                        e.printStackTrace();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }                    
                }
                //else
    if(1==0){
                    

                        HashMap<String, Object[]> data = new HashMap<>();

                    data.put("0", new Object[] {"admission","blocking", "throughput",
                        "total aa admitted","total aa blocked","total aa throughput", "total aa used","total aa cap","total aa util","L0 aa util","L1 aa util","L2 aa util",
                        "total bb admitted","total bb blocked","total bb throughput", "total bb used","total bb cap","total bb util","L0 bb util","L1 bb util","L2 bb util",
                        "total cc admitted","total cc blocked","total cc throughput", "total cc used","total cc cap","total cc util","L0 cc util","L1 cc util","L2 cc util",
                        "total dd admitted","total dd blocked","total dd throughput", "total dd used","total dd cap","total dd util","L0 dd util","L1 dd util","L2 dd util",                        
                        "total ee admitted","total ee blocked","total ee throughput", "total ee used","total ee cap","total ee util","L0 ee util","L1 ee util","L2 ee util",

                        "total L0 used","total L0 cap","total L0 util",
                        "total L1 used","total L1 cap","total L1 util",
                        "total L2 used","total L2 cap","total L2 util",
                            
                        " avg route lengths",
                        " avg route lengths_aa",
                        " avg route lengths_bb",
                        " avg route lengths_cc",
                        " avg route lengths_dd",
                        " avg route lengths_ee"                   
                    });
                //        data.put("2", new Object[] {1d, "John", 1500000d});
                //        data.put("3", new Object[] {2d, "Sam", 800000d});
                    data.put(line, new Object[] {blocking, admission, throughput_type,
                        total_accepted_aa, total_blocked_aa, total_throughput_aa,  total_used_aa, total_cap_aa,  total_troughput_aa,  L0_util_aa,  L1_util_aa,  L2_util_aa,
                        total_accepted_bb, total_blocked_bb, total_throughput_bb,  total_used_bb, total_cap_bb,  total_troughput_bb,  L0_util_bb,  L1_util_bb,  L2_util_bb,
                        total_accepted_cc, total_blocked_cc, total_throughput_cc,  total_used_cc, total_cap_cc,  total_troughput_cc,  L0_util_cc,  L1_util_cc,  L2_util_cc,
                        total_accepted_dd, total_blocked_dd, total_throughput_dd,  total_used_dd, total_cap_dd,  total_troughput_dd,  L0_util_dd,  L1_util_dd,  L2_util_dd,
                        total_accepted_ee, total_blocked_ee, total_throughput_ee,  total_used_ee, total_cap_ee,  total_troughput_ee,  L0_util_ee,  L1_util_ee,  L2_util_ee,
                        
                        total_used_L0, total_cap_L0,  total_troughput_L0,
                        total_used_L1, total_cap_L1,  total_troughput_L1,
                        total_used_L2, total_cap_L2,  total_troughput_L2,
                          
                        avg_route_length,
                        avg_route_length_aa,
                        avg_route_length_bb,
                        avg_route_length_cc,
                        avg_route_length_dd,
                        avg_route_length_ee           
                    });

                        Set<String> keyset = data.keySet();
                        int rownum = 0;
                        for (String key : keyset) {
                            //Row row = sheet.createRow(Integer.parseInt(key));
                            Row row = sheet.getRow(Integer.parseInt(key));
                            Object [] objArr = data.get(key); 

                            ///I added this bit to move in the excel file running more simulations
                            //int cellnum = excel_horiz;
                            int cellnum = 0;
                            Print.println("  cellnum " + cellnum);

                            for (Object obj : objArr) {
                                Cell cell = row.createCell(cellnum++);
                                if(obj instanceof Date) 
                                    cell.setCellValue((Date)obj);
                                else if(obj instanceof Boolean)
                                    cell.setCellValue((Boolean)obj);
                                else if(obj instanceof String)
                                    cell.setCellValue((String)obj);
                                else if(obj instanceof Double)
                                    cell.setCellValue((Double)obj);
                                else if(obj instanceof Integer)
                                    cell.setCellValue((Integer)obj);
                            }
                        }

                        try {
                            FileOutputStream out = 
                                    new FileOutputStream(new File(file_address));
                            workbook.write(out);
                            out.close();
                            System.out.println("Excel written successfully..");

                        } catch (FileNotFoundException e) {
                            e.printStackTrace();
                        } catch (IOException e) {
                            e.printStackTrace();
                        }                    
                    
                }

        }
        
//
//        HashMap<String, Object[]> data = new HashMap<>();
//
//        data.put("0", new Object[] {"admission","blocking", "throughput",
//            "total aa used","total aa cap","total aa util",
//            "total bb used","total bb cap","total bb util"," avg route lengths"});
////        data.put("2", new Object[] {1d, "John", 1500000d});
////        data.put("3", new Object[] {2d, "Sam", 800000d});
//        data.put(line, new Object[] {blocking, admission, throughput_type,total_used_aa, total_cap_aa, 
//            total_troughput_aa, total_usedbb, total_capbb,  total_troughputbb,avg_route_length});
//
//        Set<String> keyset = data.keySet();
//        int rownum = 0;
//        for (String key : keyset) {
//            //Row row = sheet.createRow(Integer.parseInt(key));
//            Row row = sheet.createRow(Integer.parseInt(key));
//            Object [] objArr = data.get(key); 
//            
//            ///I added this bit to move in the excel file running more simulations
//            int cellnum = excel_horiz;
//            
//            for (Object obj : objArr) {
//                Cell cell = row.createCell(cellnum++);
//                if(obj instanceof Date) 
//                    cell.setCellValue((Date)obj);
//                else if(obj instanceof Boolean)
//                    cell.setCellValue((Boolean)obj);
//                else if(obj instanceof String)
//                    cell.setCellValue((String)obj);
//                else if(obj instanceof Double)
//                    cell.setCellValue((Double)obj);
//            }
//        }
//
//        try {
//            FileOutputStream out = 
//                    new FileOutputStream(new File(file_address));
//            workbook.write(out);
//            out.close();
//            System.out.println("Excel written successfully..");
//
//        } catch (FileNotFoundException e) {
//            e.printStackTrace();
//        } catch (IOException e) {
//            e.printStackTrace();
//        }
    }
} 