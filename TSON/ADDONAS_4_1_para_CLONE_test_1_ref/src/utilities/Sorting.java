package utilities;

import java.util.ArrayList;
import java.util.List;
import routing.Route;


/**
 * This class implements the cocktail sorting algorithm to order
 * a set of objects based on their weight value. It makes use
 * of the Comparator class.
 * @author Joan Triay
 *
 */
public class Sorting {
	/**Bijan: the method is modifed, to get routes , and a weight list 
         * so the sorting of the routes list happens according to the weight list
         * 
	 * This method is used for sorting a given list of comparator entries
	 * according to their weight in increasing order.
	 * @param list the list of entries to order.
	 */
	public static final void sortByWeightIncreasing_route(List<Route> route_list, List<Integer> avail_list){
		cocktailSortIncreasing_route(route_list, avail_list);
	}
	
	
	/**Bijan: the method is modifed, to get routes , and a weight list 
         * so the sorting of the routes list happens according to the weight list
         * 
	 * according to their weight in decreasing order.
	 * @param list the list of entries to order.
	 */
	public static final void sortByWeightDecreasing_route(List<Route> route_list, List<Integer> avail_list){
		cocktailSortDecreasing_route(route_list, avail_list);
	}    
	/**
	 * This method is used for sorting a given list of comparator entries
	 * according to their weight in increasing order.
	 * @param list the list of entries to order.
	 */
	public static final void sortByWeightIncreasing(List list){
		cocktailSortIncreasing(list);
	}
	
	
	/**
	 * This method is used for sorting a given list of comparator entries
	 * according to their weight in decreasing order.
	 * @param list the list of entries to order.
	 */
	public static final void sortByWeightDecreasing(List<Comparator> list){
		cocktailSortDecreasing(list);
	}
	
	/**
	 * This method swaps two elements in a list.
	 * @param list The list over which to swap the two elements.
	 * @param first The first element to swap.
	 * @param second The second element to swap.
	 */
	private static void swapElements(List<Comparator> list, int first, int second){
		Comparator _fC = list.get(first);
		list.remove(first);
		list.add(second, _fC);
	}
	
	/**
	 * A method used for running a cocktail sort in a list of elements in
	 * increasing order.
	 * @param list The list to sort.
	 */
	private static void cocktailSortIncreasing(List<Comparator> list){
		int _listLen = list.size();
		int _bottom = 0;
		int _top = _listLen - 1;
		boolean _swapped = true;
		while(_swapped == true){
			_swapped = false;
			for(int i=_bottom; i<_top; i++){
				if(list.get(i).weight > list.get(i+1).weight){
					swapElements(list, i, i+1);
					_swapped = true;
				}
			}
			_top = _top -1;
			for(int i=_top; i>_bottom; i--){
				if(list.get(i).weight < list.get(i-1).weight){
					swapElements(list, i, i-1);
				}
			}
			_bottom = _bottom + 1;
		}
	}
	
	/**
	 * A method used to run a cocktail sort in a list of elements in
	 * decreasing order.
	 * @param list The list to sort.
	 */
	private static void cocktailSortDecreasing(List<Comparator> list){
		int _listLen = list.size();
		int _bottom = 0;
		int _top = _listLen - 1;
		boolean _swapped = true;
		while(_swapped == true){
			_swapped = false;
			for(int i=_bottom; i<_top; i++){
				if(list.get(i).weight < list.get(i+1).weight){
					swapElements(list, i, i+1);
					_swapped = true;
				}
			}
			_top = _top -1;
			for(int i=_top; i>_bottom; i--){
				if(list.get(i).weight > list.get(i-1).weight){
					swapElements(list, i, i-1);
				}
			}
			_bottom = _bottom + 1;
		}
	}
        
	/**
	 * This method swaps two elements in a list.
	 * @param list The list over which to swap the two elements.
	 * @param first The first element to swap.
	 * @param second The second element to swap.
	 */
	private static void swapElements_route(List<Route> list, int first, int second){
		Route _fC = list.get(first);
		list.remove(first);
		list.add(second, _fC);
	}        
	private static void swapElements_int(List<Integer> list, int first, int second){
		int _fC = list.get(first);
		list.remove(first);
		list.add(second, _fC);
	}          
	/**
	 * A method used for running a cocktail sort in a list of elements in
	 * increasing order.
	 * @param list The list to sort.
	 */
	private static List<Route> cocktailSortIncreasing_route(List<Route> route_list, List<Integer> avail_list){
                
		int _listLen = route_list.size();
                int avail_list_size = avail_list.size();
                
                if(_listLen!=avail_list_size)
                {
                    Print.println("  problem in sorting class, unequal sizes of lists to be compared");
                    System.exit(32312);
                }
		int _bottom = 0;
		int _top = _listLen - 1;
		boolean _swapped = true;
		while(_swapped == true){
			_swapped = false;
			for(int i=_bottom; i<_top; i++){
				if(avail_list.get(i) > avail_list.get(i+1)){
                                        swapElements_int(avail_list, i, i+1);
					swapElements_route(route_list, i, i+1);
					_swapped = true;
				}
			}
			_top = _top -1;
			for(int i=_top; i>_bottom; i--){
				if(avail_list.get(i) < avail_list.get(i-1)){
                                        swapElements_int(avail_list, i, i-1);
					swapElements_route(route_list, i, i-1);
				}
			}
			_bottom = _bottom + 1;
		}
                
                List<Route> processed_route_list = route_list;
//                Print.println(" processed_route_list \t\t\t\t\t"+ processed_route_list.toString());
                return processed_route_list;
	}
	
	/**
	 * A method used to run a cocktail sort in a list of elements in
	 * decreasing order.
	 * @param list The list to sort.
	 */
	private static void cocktailSortDecreasing_route(List<Route> route_list, List<Integer> avail_list){
		int _listLen = route_list.size();
                int avail_list_size = avail_list.size();
                
                if(_listLen!=avail_list_size)
                {
                    Print.println("  problem in sorting class, unequal sizes of lists to be compared");
                    System.exit(32312);
                }
		int _bottom = 0;
		int _top = _listLen - 1;
		boolean _swapped = true;
		while(_swapped == true){
			_swapped = false;
			for(int i=_bottom; i<_top; i++){
				if(avail_list.get(i) < avail_list.get(i+1)){
					swapElements_route(route_list, i, i+1);
                                        swapElements_int(avail_list, i, i+1);
					_swapped = true;
				}
			}
			_top = _top -1;
			for(int i=_top; i>_bottom; i--){
				if(avail_list.get(i) > avail_list.get(i-1)){
                                        swapElements_int(avail_list, i, i-1);
					swapElements_route(route_list, i, i-1);
				}
			}
			_bottom = _bottom + 1;
		}
	}        
        
}
