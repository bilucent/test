/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package utilities;

import Infrastructure.Link;
import Infrastructure.Node;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Set;

/**
 *
 * @author Bijan
 */
public class Converters {
        
    public static int[] convert_Int_List_to_1Darray( List<Integer> list)
    {
        int [] array = new int[list.size()];
        for(int i = 0; i < list.size() ; i++)
        {
                array[i] = list.get(i);
        }
        return array;
    }      
    
    
    public static int[] convert_string_to_1Darray( String str)
    {
        //Print.println("str in convert sgring to 1D int[]  " + str);
        int [] array = new int[str.length()];
        for(int i = 0; i < str.length() ; i++)
        {
                array[i] = Character.digit(str.charAt(i), 10);
        }
        return array;
    }  


    public static String[] convert_string_to_Stringarray( String str)
    {
       //this commented bit did not work, generated an extra space member!
       /*
        String try_str = str;
        try_str.trim();
        try_str.replaceAll("\\W","");
        
        String [] array = try_str.split("");
        P.print_1D(" show me the split string ???? ", array);
        return array;
       
       * 
       */
        
        //this is a beutiful method instead
        //String try_str = str;
        String [] array = new String[str.length()];
        //Print.print(" array.length is: "+ array.length);
        for(int i = 0; i < array.length ; i++)
        {
                array[i] = Character.toString(str.charAt(i));
        }        
        //Print.print_1D(" show me the split string ???? ", array);
        return array;
    }  
    public static void convert_string_to_MDarray(int divisions, String str)
    {
        int [] array = new int[divisions];
        for(int i = 0; i < divisions ; i++)
        {
                array[i] = Character.digit(str.charAt(i), 10);
        }
    }    
    
    public static String make_string_of_x(String x, int length){
        String String_value ="";
        //double value = Double.parseDouble(input);
        for(int i = 0; i< length; i++){
            String_value= String_value+x;
        }
        return String_value;
    }
    public static String convert_array_to_string(int[] input){

        StringBuilder String_value = new StringBuilder();
        for (int member : input) 
        {
            String_value.append(member);
        }        

        return String_value.toString();
    }    
    public static  int[] convert_string_to_2D(String chain)
    {
        int[] intArray = new int[chain.length()];

        for (int i = 0; i <chain.length(); i++) {
                intArray[i] = Character.digit(chain.charAt(i), 10);
        }
        //Print.print_1D(" the chain is converted-->", intArray);
        
    return intArray;    
    } 
    
    
    //to be used form kaing resrouces object out of resrouce hash in inputReader
    public List<Integer> convert_hash2List_int(HashMap<String,String> res_hash)
    {
        List <Integer> res_abstract_inf  = new ArrayList<>();
        for(int i = 0; i < res_hash.size(); i++)
        {
            res_abstract_inf.add(res_hash.get("D"+i).length());
        }
        return res_abstract_inf; 
    }
    
    //to be used form kaing resrouces object out of resrouce hash in inputReader
    public List<String> convert_hash2List_str(HashMap<String,String> res_hash)
    {
        List <String> res_abstract_inf  = new ArrayList<>();
        for(int i = 0; i < res_hash.size(); i++)
        {
            res_abstract_inf.add(res_hash.get("D"+i));
        }
        return res_abstract_inf; 
    }    

    //to be used form kaing resrouces object out of resrouce hash in inputReader
    public static int[] convert_Arr_list_to_int_arr(ArrayList<Integer> arrl)
    {
        int array[]  = new int[arrl.size()];
        for(int i = 0; i < arrl.size(); i++)
        {
            array[i] = arrl.get(i);
        }
        return array; 
    } 
    
    //to be used form kaing resrouces object out of resrouce hash in inputReader
    public static List<Integer> convert_int_arr_to_Arr_list(int[] array)
    {
        List <Integer> list  = new ArrayList<>();
        for(int i = 0; i < array.length; i++)
        {
            list.add(array[i]);
        }
        return list; 
    }    
    
    public static int[] key_set_obj_to_int_array(HashMap hash)
    {
            Set<Integer> keySet = hash.keySet();
            Object[] toArray = keySet.toArray();
            int [] intarr = new int [toArray.length];
            for (int i = 0; i < intarr.length; i++){
                intarr[i] = (int) toArray[i];
            }
            return intarr;
    }

    public static String[] key_set_obj_to_str_array(HashMap hash)
    {
            Set<Integer> keySet = hash.keySet();
            Object[] toArray = keySet.toArray();
            String [] strarr = new String [toArray.length];
            for (int i = 0; i < strarr.length; i++){
                strarr[i] = toArray[i].toString();
            }
            return strarr;
    }
        
    public static List<Node> deepcloneNodeList(List<Node> nodeobjects){
        List<Node> nodes = new ArrayList();
        
        for(int i = 0; i < nodeobjects.size(); i ++)
        {
            nodes.add(new Node(nodeobjects.get(i)));
        }
        return nodes;
        
    }
    
    public static List<Link> deepcloneLinkList(List<Link> linkobjects){
        List<Link> link = new ArrayList();
        
        for(int i = 0; i < linkobjects.size(); i ++)
        {
            link.add(new Link(linkobjects.get(i)));
        }
        return link;
        
    }
    
}
