/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package alloc;

import Infrastructure.Link;
import Infrastructure.Network;
import Infrastructure.Node;
import Infrastructure.ResourceOperations;
import Record.Link_Rec_Obj;
import Record.Node_Rec_Obj;
import Record.Rec_Link_Allocs;
import Record.Rec_Node_Allocs;
import RequestTypes.Req_IT_NET_timed;
import RequestTypes.Request_base;
import static alloc.RUN_Heuristic_Star.static_IDI;
import com.rits.cloning.Cloner;
//import com.sun.org.apache.bcel.internal.generic.L2D;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import node_alloc.Node_res_alloc_node;
//import org.apache.commons.math.linear.Array2DRowFieldMatrix;
import routing.Route;
import routing.RouteProcess;
import statistics.Stats_collector_Star;
import utilities.Converters;
import utilities.Maths;
import utilities.Matrix;
import utilities.Print;

/**
 *
 * @author Bijan
 */
public class GetStarNet {
    String Route_select;
    int Spec_time_map;
    int SubL_max;
    int SubL_min;
    int Lambda_min;
    int Lambda_max;
    int Flex_min;      
    int Flex_max;  
    Node node;
//    Req_NET_timed request;
    Request_base request;
    int VON_ID;
    int Star_ID;
    String Star_Req_type;
    int Con_ID;
    int Con_time;
    String Req_type;
    Route Route;
    
    boolean Debug;
    HashMap<String,Rec_Link_Allocs>  Link_record_hash;
    HashMap<Integer,Rec_Node_Allocs>  Node_record_hash;
    
    
       
    Network mNetwork;
    
    private Route currentRoute;

    public GetStarNet(boolean debug,Network mnetwork, Node n,Request_base req, 
            int Von_ID, int con_time, int star_id, String star_type, int con_id,
            HashMap<String, Rec_Link_Allocs> linkrecord_hash
            ,HashMap<Integer, Rec_Node_Allocs> noderecord_hash
            , int spec_time_map, 
            int subl_min,int subl_max,int lambda_min,int lambda_max, int flex_max,int flex_min,
            String route_select)
    {
        this.node = n;
        this.VON_ID = Von_ID;
        this.request = req;
        this.Debug = debug;
        
        /*
         * instead of uodating a clone, I make a new list
         * if successful, I start adding them one by one
         */
//        this.Link_record_hash = linkrecord_hash;
//        this.Node_record_hash = noderecord_hash;

        
        this.Con_time = con_time;
        this.Star_ID = star_id;
        this.Con_ID = con_id;
        this.Star_Req_type = star_type;
        this.Req_type = req.returnConnectionReqType();
        this.mNetwork = mnetwork;
        this.Lambda_min = lambda_min;
        this.Lambda_max = lambda_max;
        this.SubL_max = subl_max;
        this.SubL_min = subl_min;
        this.Flex_max = flex_max;
        this.Flex_min = flex_min;
        this.Spec_time_map = spec_time_map;
        this.Route_select = route_select;
        this.Link_record_hash = linkrecord_hash;
        this.Node_record_hash = noderecord_hash;
//        init_recordList();
        
//        Print.println("  VON ID : "+ VON_ID +" &&&****  Star_ID: " + this.Star_ID +" &&&****  Con_ID: " + this.Con_ID+ " ******  ");        
//        Print.println("  VON ID  in starnet get link ids : "+  mNetwork.getmLinkIDS().toString()+ " ******  ");        
        
//        Print.println("  double check the node after sending it in  " +  node.getmRouting().getmRoutingTable().get_number_of_routes_all() );

        
    }
    private void fillRoutes_dest(Network mNetwork, int k, int dest){
//            Print.println(" number of shortest paths to be allocated to the nodes: " +k);
            List<Node> listNodes = mNetwork.getmListNodes();
            Node node = listNodes.get(request.returnSrc());
            node.getmRouting().searchKShortestPaths_one_Dest(mNetwork, k, dest);
            
    }   
//    public void init_recordList()
//    {
//        
//        
//        
//        List<Link> link_list = mNetwork.getmListLinks();
//        List<Node> node_list = mNetwork.getmListNodes();
//        
//        //for each link, make a recording object
//        for(int i = 0; i < link_list.size(); i++)
//        {
//            Link_record_hash.put(link_list.get(i).getmID(), new Rec_Link_Allocs
//                    (Debug,link_list.get(i).getmID(), link_list.get(i).getmNumber(), 
//                    link_list.get(i).get_ParentClass_resrouces().getm_resinfo()));
//        }
//        //and for each node
//        for(int i = 0; i < node_list.size(); i++)
//        {
//            Node_record_hash.put(node_list.get(i).getmID(), new Rec_Node_Allocs
//                    (Debug, node_list.get(i).getmID(), 
//                    node_list.get(i).get_ParentClass_resrouces().getm_resinfo()));
//        }
//    }    
    
    
    public boolean run_routing(Stats_collector_Star SC, int number_of_routes_to_try, 
            //int Blocked, int Allowed,
            int totalreroute,
            int nodereroute,
            int linkreroute,
            HashMap<String, Integer> nodereroutehash,
            HashMap<String, Integer> linkreroutehash
            )
    {
            int src = request.returnSrc();
            int dest = request.returnsDest();
            List  requested_network_resource =  request.returnNETREQ();          
            int request_dimension = requested_network_resource.size() -1;
            String req_algorithm_type = request.returnAlgorithmType();   
            
            boolean re_route = true;
            int route_count = 0;
            Route route = null;

            /*
             * the routing mechanisms are deployed here. I will deploy a separate case
             * here, for selecting multiple routes. the request should reflect on 
             * the virtual connectivities.
             * and then it should be broken into set of routings.
             */
            if(Route_select.matches("RS_NORM"))
            {
                /*
                * here i fil the routes, for as much as needed, with new class of reouting table
                */
//                mNetwork.createNetworkGraph();
                
                Print.println(" ITREQ  " + request.returnITREQ());
                
//                Matrix NetMat = mNetwork.return_network_matrix();
//                int mat_array[][] = NetMat.getDataInt();
//                Print.print_2D(" get the new network matrix: ", mat_array);     
                
//                Print.print_2D("network created  again... ", mNetwork.return_network_matrix().getDataInt());
//                Print.println("network nodes again... "+ mNetwork.getmListNodes().toString());
//                node.getmRouting().getmRoutingTable().delRouteS(dest);
//                fillRoutes_dest(mNetwork, number_of_routes_to_try, dest);
                int available_routes = node.getmRouting().get_number_of_routes_dest(dest);
                Print.println("  the number of routes in the stock: " + available_routes);
                /*
                */  
                while(route_count<available_routes && re_route)
                {
                    currentRoute = null;

                    if(Debug){
                        Print.println("  VON ID : "+ VON_ID +" routes avaialble for node " + node.getmID());
                        node.getmRouting().print_routes();
                    }

                    Print.println("  ****************  Routing start  ********************");


                    //get the first available route in the list
                    try{
                            route = node.getmRouting().getmRoutingTable().getRoute(dest, route_count);
                            Print.println("  the route is : " + route.toString());
                    }
                    
                    
                    catch(NullPointerException e){
                            Print.println("  VON ID : "+ VON_ID +" Warning no route exists for node+"+ node.getmID());
                            System.exit(3773);
                    }
                    if (route!=null)
                    {
                        Print.println("  \t****************  Route found  ********************" );
                        Print.println("  \t****************  Link Alloc start  ********************" );
                        LinkAllocPacked LAP = routeBreak_resAlloc(route);
                        Print.println("  \t****************  routeBreak_resAlloc done  ********************" );
                      
                        
//                        Print.println(" LAP? "  +   LAP.getAlloc_combinedRes_Link().getm_Resrouces_tb_updated().toString());
             
                        if(LAP == null)
                        {
                            Print.println("  \t****************  Link Alloc NULL  ********************" );
                            
                            re_route = true;
                            totalreroute++;
                            linkreroute++;
                            update_linkreroute_hash(linkreroutehash);

                            
                            
                        }
                        else 
                            re_route = false;
                      
                        if(re_route)
                        {
                            Print.println("  \t****************  Link Alloc failed ********************" );                            
                            Print.println("  \t****************  ReRoute the Request ********************" );                            
                            Print.println("  \troute_count--> " + route_count );
                            route_count++;
                            SC.add_to_reroute_type(Star_Req_type);
                            SC.add_to_reroute_type_link(Star_Req_type);                            
                        }
                        else
                        {
                            Print.println("  \troute_count--> " + route_count );
                            Print.println("  \t****************  Link Allocation OK  ********************");
                            Print.println("  \t****************  Node alloc start  ********************");
                            
                            /*
                            * node resrouce allocation should happen here
                            * after the route has resources on the links,
                            * the node resource allocation will take place
                            * this bit is not right, but it will do the job for the time being
                            * my aim is, to have the aggregated resources, all swiped for 
                            * now what I do, I use one hop res link info, for all th ehops.
                            * I have to use the link res info per hop
                            */

                            Alloc_combinedRes_Node NARsrc =     nodeAllocLateUpdate(src);
//                            Alloc_combinedRes_Node NARdest =     nodeAllocLateUpdate(dest);
                            List LNAR = new ArrayList();
                            LNAR.add(NARsrc);
//                            LNAR.add(NARdest);
                            
//                            if((NARsrc == null || !NARsrc.return_success_status())
//                                    ||
//                                    (NARdest == null || !NARdest.return_success_status()))
//                            {                            
                            if((NARsrc == null || !NARsrc.return_success_status()))
                            {                            
                                Print.println("  \t****************  Node Alloc failed ********************" );                            
                                Print.println("  \t****************  ReRoute the Request ********************" );                            
                                Print.println("  \troute_count--> " + route_count );
                                route_count++;
                                re_route = true;
                                SC.add_to_reroute_type(Star_Req_type);
                                SC.add_to_reroute_type_node(Star_Req_type);

                                totalreroute++;
                                nodereroute++;
                                update_nodereroute_hash(nodereroutehash);
                                                            
                            }
//                            else if(NARsrc.return_success_status()
//                                    && 
//                                    NARdest.return_success_status())
//                            {
                            else if(NARsrc.return_success_status())
                            {
                                Print.println("  \t****************  Node Allocation OK  ********************");
                                Print.println("  \t****************  --> The Allocation set was OK, return to process  ********************");
                                updateNodeLinkAllocsSet(LAP, LNAR);
                                Print.println("  \t****************  node resrouce to be updated ******************** with node id: " + src);                          
//                                Print.println("  \t****************  NARsrc.getm_Resrouces_tb_updated(); ******************** with node id: " + 
//                                         NARsrc.getm_Resrouces_tb_updated().toString());                          
                                                               
//                                show_records("  now printing the instant record. ");
                                
                                currentRoute = route;
                                return true;

                            }                            
                        }                                            
                    }
                    else 
                    {
                        Print.println("  ****************NO ROUTE  REJECT THE REQUEST  ********************");

                        /*
                        * the philosofy is, if I finish all the routes, and still get to his
                        * point, it means the allcations where not successful
                        */
                        return false;                                               
                    }
                }
//                Print.println("  ****************NO ALLOCATION  REJECT THE REQUEST  ********************");
//
//                /*
//                * the philosofy is, if I finish all the routes, and still get to his
//                * point, it means the allcations where not successful
//                */
//                return false;   
            }   
            else if(Route_select.matches("RS_NORM_AVA"))
            {
                /*
                * here i fil the routes, for as much as needed, with new class of reouting table
                */
                mNetwork.createNetworkGraph();
                
//                Matrix NetMat = mNetwork.return_network_matrix();
//                int mat_array[][] = NetMat.getDataInt();
//                Print.print_2D(" network matrix: ", mat_array);
                                
//                Print.print_2D("network created  again... ", mNetwork.return_network_matrix().getDataInt());
//                Print.println("network nodes again... "+ mNetwork.getmListNodes().toString());
                node.getmRouting().getmRoutingTable().delRouteS(dest);
                fillRoutes_dest(mNetwork, number_of_routes_to_try, dest);
                int available_routes = node.getmRouting().get_number_of_routes_dest(dest);
                Print.println("  the number of routes in the stock");
                /*
                */  

                List<Route> routes = node.getmRouting().getmRoutingTable().get_dest_routes_list(dest);
                RouteProcess RP = new RouteProcess(Debug,mNetwork, routes);           

                while(route_count<available_routes && re_route)
                {
                    try
                    {
                            route = RP.getm_least_used_processed_by_availabilites(routes, request_dimension);
                            Print.println("  the route is : " + route.toString());
                    }

                    catch(NullPointerException e){
                        Print.println("  VON ID : "+ VON_ID +" Warning no route exists for node+"+ node.getmID());
                        System.exit(3773);
                    }  
                    if (route!=null)
                    {
                        Print.println("  \t****************  Route found  ********************" );
                        Print.println("  \t****************  Link Alloc start  ********************" );
                        LinkAllocPacked LAP = routeBreak_resAlloc(route);
                        
                        if(LAP == null)
                            re_route = true;
                        else 
                            re_route = false;
                      
                        if(re_route)
                        {
                            Print.println("  \t****************  Link Alloc failed ********************" );                            
                            Print.println("  \t****************  ReRoute the Request ********************" );                            
                            Print.println("  \troute_count--> " + route_count );
                            route_count++;
                        }
                        else
                        {
                            Print.println("  \troute_count--> " + route_count );
                            Print.println("  \t****************  Link Allocation OK  ********************");
                            Print.println("  \t****************  Node alloc start  ********************");
                            
                            /*
                            * node resrouce allocation should happen here
                            * after the route has resources on the links,
                            * the node resource allocation will take place
                            * this bit is not right, but it will do the job for the time being
                            * my aim is, to have the aggregated resources, all swiped for 
                            * now what I do, I use one hop res link info, for all th ehops.
                            * I have to use the link res info per hop
                            */

                            Alloc_combinedRes_Node NARsrc =     nodeAllocLateUpdate(src);
                            Alloc_combinedRes_Node NARdest =     nodeAllocLateUpdate(dest);
                            List LNAR = new ArrayList();
                            LNAR.add(NARsrc);
                            LNAR.add(NARdest);
                            
                            if((NARsrc == null || !NARsrc.return_success_status())
                                    &&
                                    (NARdest == null || !NARdest.return_success_status()))
                            {                            
                                Print.println("  \t****************  Node Alloc failed ********************" );                            
                                Print.println("  \t****************  ReRoute the Request ********************" );                            
                                
                                route_count++;
                                Print.println("  \troute_count--> " + route_count );
                                re_route = true;
                            }
                            else if(NARsrc.return_success_status()
                                    && 
                                    NARdest.return_success_status())
                            {
                                Print.println("  \t****************  Node Allocation OK  ********************");
                                Print.println("  \t****************  --> The Allocation set was OK, return to process  ********************");
                                updateNodeLinkAllocsSet(LAP, LNAR);
                                return true;

                            }                     
                        }                                            
                    }
                    else 
                    {
                        Print.println("  ****************NO ROUTE  REJECT THE REQUEST  ********************");

                        /*
                        * the philosofy is, if I finish all the routes, and still get to his
                        * point, it means the allcations where not successful
                        */
                        return false;                                               
                    }

                }   

                Print.println("  ****************NO ALLOCATION  REJECT THE REQUEST  ********************");

                /*
                * the philosofy is, if I finish all the routes, and still get to his
                * point, it means the allcations where not successful
                */
                return false;   
                      
            }   
            else if(Route_select.matches("RS_FILTER_HOP_FL_S"))
            {
                List<String> Grid_nodes_types = new ArrayList<>();
                List<String> Flex_nodes_types = new ArrayList<>();
//                Grid_nodes_types.add("G");
//                Flex_nodes_types.add("F");
                Grid_nodes_types.add("FL");
                Flex_nodes_types.add("S");

                List nodes_ids_ofGrid = mNetwork.getNodesIds_of_types(Grid_nodes_types);
                List nodes_ids_ofFlex = mNetwork.getNodesIds_of_types(Flex_nodes_types);

                if(Req_type.matches("aa"))
                {
                    /*
                    * here i fill the routes, for as much as needed, with new class of reouting table
                    */
                    mNetwork.createNetworkGraph_filteredNodesOut(nodes_ids_ofFlex);
                    
//                    Matrix NetMat = mNetwork.return_network_matrix();
//                    int mat_array[][] = NetMat.getDataInt();
//                    Print.print_2D(" get the new network matrix: ", mat_array);                    

                    node.getmRouting().getmRoutingTable().delRouteS(dest);
                    fillRoutes_dest(mNetwork, number_of_routes_to_try, dest);
                    
                    int available_routes = node.getmRouting().get_number_of_routes_dest(dest);
                    Print.println("  the number of routes in the stock: " + available_routes + "  and number_of_routes_to_try:" + number_of_routes_to_try);
                    if(Debug)
                    {
                        Print.println("  VON ID : "+ VON_ID +" routes avaialble for node " + node.getmID());
                        node.getmRouting().print_routes();
                    }   
                    /*
                        */  
                    while(route_count<available_routes && re_route)
                    {

                        try{
                                route = node.getmRouting().getmRoutingTable().getRoute(dest, route_count);
                                Print.println("  the route is : " + route.toString());
                        }

                        catch(NullPointerException e){
                            Print.println("  VON ID : "+ VON_ID +" Warning no route exists for node+"+ node.getmID());
                            System.exit(3773);
                        }  
                    if (route!=null)
                    {
                        Print.println("  \t****************  Route found  ********************" );
                        Print.println("  \t****************  Link Alloc start  ********************" );
                        LinkAllocPacked LAP = routeBreak_resAlloc(route);
                        
                        if(LAP == null)
                            re_route = true;
                        else 
                            re_route = false;
                      
                        if(re_route)
                        {
                            Print.println("  \t****************  Link Alloc failed ********************" );                            
                            Print.println("  \t****************  ReRoute the Request ********************" );                            
                            Print.println("  \troute_count--> " + route_count );
                            route_count++;
                        }
                        else
                        {
                            Print.println("  \troute_count--> " + route_count );
                            Print.println("  \t****************  Link Allocation OK  ********************");
                            Print.println("  \t****************  Node alloc start  ********************");
                            
                            /*
                            * node resrouce allocation should happen here
                            * after the route has resources on the links,
                            * the node resource allocation will take place
                            * this bit is not right, but it will do the job for the time being
                            * my aim is, to have the aggregated resources, all swiped for 
                            * now what I do, I use one hop res link info, for all th ehops.
                            * I have to use the link res info per hop
                            */

                            Alloc_combinedRes_Node NARsrc =     nodeAllocLateUpdate(src);
                            Alloc_combinedRes_Node NARdest =     nodeAllocLateUpdate(dest);
                            List LNAR = new ArrayList();
                            LNAR.add(NARsrc);
                            LNAR.add(NARdest);
                            
                            if((NARsrc == null || !NARsrc.return_success_status())
                                    &&
                                    (NARdest == null || !NARdest.return_success_status()))
                            {                            
                                Print.println("  \t****************  Node Alloc failed ********************" );                            
                                Print.println("  \t****************  ReRoute the Request ********************" );                            
                                Print.println("  \troute_count--> " + route_count );
                                route_count++;
                                re_route = true;
                            }
                            else if(NARsrc.return_success_status()
                                    && 
                                    NARdest.return_success_status())
                            {
                                Print.println("  \t****************  Node Allocation OK  ********************");
                                Print.println("  \t****************  --> The Allocation set was OK, return to process  ********************");
                                updateNodeLinkAllocsSet(LAP, LNAR);
                                return true;

                            }                           
                        }                                            
                    }
                        else 
                        {
                            Print.println("  ****************NO ROUTE  REJECT THE REQUEST  ********************");

                            /*
                            * the philosofy is, if I finish all the routes, and still get to his
                            * point, it means the allcations where not successful
                            */
                            return false;                                               
                        }

                    }   


                    Print.println("  ****************NO ALLOCATION  REJECT THE REQUEST  ********************");

                    /*
                    * the philosofy is, if I finish all the routes, and still get to his
                    * point, it means the allcations where not successful
                    */
                    return false;   

                }
                else if(Req_type.matches("bb")||Req_type.matches("cc"))
                {
                        /*
                  * here i fil the routes, for as much as needed, with new class of reouting table
                  */
                  mNetwork.createNetworkGraph();
                  
//                    Matrix NetMat = mNetwork.return_network_matrix();
//                    int mat_array[][] = NetMat.getDataInt();
//                    Print.print_2D(" get the new network matrix: ", mat_array);
                                
  //                Print.print_2D("network created  again... ", mNetwork.return_network_matrix().getDataInt());
  //                Print.println("network nodes again... "+ mNetwork.getmListNodes().toString());
                  node.getmRouting().getmRoutingTable().delRouteS(dest);
                  fillRoutes_dest(mNetwork, number_of_routes_to_try, dest);
                  int available_routes = node.getmRouting().get_number_of_routes_dest(dest);
                  Print.println("  the number of routes in the stock");
                  /*
                  */  
                  while(route_count<available_routes && re_route)
                  {

                      if(Debug){
                          Print.println("  VON ID : "+ VON_ID +" routes avaialble for node " + node.getmID());
                          node.getmRouting().print_routes();
                      }


                      //get the first available route in the list
                      try{
                              route = node.getmRouting().getmRoutingTable().getRoute(dest, route_count);
                              Print.println("  the route is : " + route.toString());
                      }
                      catch(NullPointerException e){
                              Print.println("  VON ID : "+ VON_ID +" Warning no route exists for node+"+ node.getmID());
                              System.exit(3773);
                      }
                    if (route!=null)
                    {
                        Print.println("  \t****************  Route found  ********************" );
                        Print.println("  \t****************  Link Alloc start  ********************" );
                        LinkAllocPacked LAP = routeBreak_resAlloc(route);
                        
                        if(LAP == null)
                            re_route = true;
                        else 
                            re_route = false;
                      
                        if(re_route)
                        {
                            Print.println("  \t****************  Link Alloc failed ********************" );                            
                            Print.println("  \t****************  ReRoute the Request ********************" );                            
                            Print.println("  \troute_count--> " + route_count );
                            route_count++;
                        }
                        else
                        {
                            Print.println("  \troute_count--> " + route_count );
                            Print.println("  \t****************  Link Allocation OK  ********************");
                            Print.println("  \t****************  Node alloc start  ********************");
                            
                            /*
                            * node resrouce allocation should happen here
                            * after the route has resources on the links,
                            * the node resource allocation will take place
                            * this bit is not right, but it will do the job for the time being
                            * my aim is, to have the aggregated resources, all swiped for 
                            * now what I do, I use one hop res link info, for all th ehops.
                            * I have to use the link res info per hop
                            */

                            Alloc_combinedRes_Node NARsrc =     nodeAllocLateUpdate(src);
                            Alloc_combinedRes_Node NARdest =     nodeAllocLateUpdate(dest);
                            List LNAR = new ArrayList();
                            LNAR.add(NARsrc);
                            LNAR.add(NARdest);
                            
                            if((NARsrc == null || !NARsrc.return_success_status())
                                    &&
                                    (NARdest == null || !NARdest.return_success_status()))
                            {                            
                                Print.println("  \t****************  Node Alloc failed ********************" );                            
                                Print.println("  \t****************  ReRoute the Request ********************" );                            
                                Print.println("  \troute_count--> " + route_count );
                                route_count++;
                                re_route = true;
                            }
                            else if(NARsrc.return_success_status()
                                    && 
                                    NARdest.return_success_status())
                            {
                                Print.println("  \t****************  Node Allocation OK  ********************");
                                Print.println("  \t****************  --> The Allocation set was OK, return to process  ********************");
                                updateNodeLinkAllocsSet(LAP, LNAR);
                                return true;

                            }                      
                        }                                            
                    }
                      else 
                      {
                          Print.println("  ****************NO ROUTE  REJECT THE REQUEST  ********************");

                          /*
                          * the philosofy is, if I finish all the routes, and still get to his
                          * point, it means the allcations where not successful
                          */
                          return false;                                               
                      }
                  }
                  Print.println("  ****************NO ALLOCATION  REJECT THE REQUEST  ********************");

                  /*
                  * the philosofy is, if I finish all the routes, and still get to his
                  * point, it means the allcations where not successful
                  */
                  return false;   
                }
                else if(Req_type.matches("dd")||Req_type.matches("ee"))
                {
                    /*
                    * here i fil the routes, for as much as needed, with new class of reouting table
                    */
                    mNetwork.createNetworkGraph_filteredNodesOut(nodes_ids_ofGrid);
                    node.getmRouting().getmRoutingTable().delRouteS(dest);
                    fillRoutes_dest(mNetwork, number_of_routes_to_try, dest);
                    Print.println("  nodes_ids_ofGRID" + nodes_ids_ofGrid.toString());
                    int available_routes = node.getmRouting().get_number_of_routes_dest(dest);
                    Print.println("  the number of routes in the stock: " + available_routes + "  and number_of_routes_to_try:" + number_of_routes_to_try);
                    
                    if(Debug)
                    {
                        Print.println("  VON ID : "+ VON_ID +" routes avaialble for node " + node.getmID());
                        node.getmRouting().print_routes();
                    }                                
                    /*
                    */  
                    while(route_count<available_routes && re_route)
                    {
                        try{
                                route = node.getmRouting().getmRoutingTable().getRoute(dest, route_count);
                                Print.println("  the route is : " + route.toString());
                        }

                        catch(NullPointerException e){
                            Print.println("  VON ID : "+ VON_ID +" Warning no route exists for node+"+ node.getmID());
                            System.exit(3773);
                        } 
                        if (route!=null)
                        {
                            LinkAllocPacked LAP = routeBreak_resAlloc(route);

                            if(LAP == null)
                                re_route = true;
                            else 
                                re_route = false;
                            
                            if(re_route)
                            {
                                Print.println("  route_count--> " + route_count );
                                route_count++;
                            }
                            else
                            {
                                Print.println("  route_count--> " + route_count );
                                Print.println("  ****************  ACCEPT THE REQUEST  ********************");
                                return true;
                            }                                            
                        }
                        else 
                        {
                            Print.println("  ****************NO ROUTE  REJECT THE REQUEST  ********************");

                            /*
                            * the philosofy is, if I finish all the routes, and still get to his
                            * point, it means the allcations where not successful
                            */
                            return false;                                               
                        }

                    }   
                    Print.println("  ****************NO ALLOCATION  REJECT THE REQUEST  ********************");

                    /*
                    * the philosofy is, if I finish all the routes, and still get to his
                    * point, it means the allcations where not successful
                    */
                    return false;   
                }

            }
            else if(Route_select.matches("RS_FILTER_HOP_F_LS"))
            {
                List<String> Grid_nodes_types = new ArrayList<>();
                List<String> Flex_nodes_types = new ArrayList<>();
//                Grid_nodes_types.add("G");
//                Flex_nodes_types.add("F");
                Grid_nodes_types.add("F");
                Flex_nodes_types.add("LS");

                List nodes_ids_ofGrid = mNetwork.getNodesIds_of_types(Grid_nodes_types);
                List nodes_ids_ofFlex = mNetwork.getNodesIds_of_types(Flex_nodes_types);

                //if(Star_Req_type.matches("aa")||Star_Req_type.matches("bb")||Star_Req_type.matches("cc"))
                if(Req_type.matches("aa"))
                {
                    /*
                    * here i fill the routes, for as much as needed, with new class of reouting table
                    */
                    mNetwork.createNetworkGraph_filteredNodesOut(nodes_ids_ofFlex);
                    node.getmRouting().getmRoutingTable().delRouteS(dest);
                    fillRoutes_dest(mNetwork, number_of_routes_to_try, dest);
                    int available_routes = node.getmRouting().get_number_of_routes_dest(dest);
                    Print.println("  the number of routes in the stock: " + available_routes + "  and number_of_routes_to_try:" + number_of_routes_to_try);
                    
                    if(Debug)
                    {
                        Print.println("  VON ID : "+ VON_ID +" routes avaialble for node " + node.getmID());
                        node.getmRouting().print_routes();
                    }   
                    /*
                        */  
                    while(route_count<available_routes && re_route)
                    {

                        try{
                                route = node.getmRouting().getmRoutingTable().getRoute(dest, route_count);
                                Print.println("  the route is : " + route.toString());
                        }

                        catch(NullPointerException e){
                            Print.println("  VON ID : "+ VON_ID +" Warning no route exists for node+"+ node.getmID());
                            System.exit(3773);
                        }  
                    if (route!=null)
                    {
                        Print.println("  \t****************  Route found  ********************" );
                        Print.println("  \t****************  Link Alloc start  ********************" );
                        LinkAllocPacked LAP = routeBreak_resAlloc(route);
                        
                        if(LAP == null)
                            re_route = true;
                        else 
                            re_route = false;
                      
                        if(re_route)
                        {
                            Print.println("  \t****************  Link Alloc failed ********************" );                            
                            Print.println("  \t****************  ReRoute the Request ********************" );                            
                            Print.println("  \troute_count--> " + route_count );
                            route_count++;
                        }
                        else
                        {
                            Print.println("  \troute_count--> " + route_count );
                            Print.println("  \t****************  Link Allocation OK  ********************");
                            Print.println("  \t****************  Node alloc start  ********************");
                            
                            /*
                            * node resrouce allocation should happen here
                            * after the route has resources on the links,
                            * the node resource allocation will take place
                            * this bit is not right, but it will do the job for the time being
                            * my aim is, to have the aggregated resources, all swiped for 
                            * now what I do, I use one hop res link info, for all th ehops.
                            * I have to use the link res info per hop
                            */

                            Alloc_combinedRes_Node NARsrc =     nodeAllocLateUpdate(src);
                            Alloc_combinedRes_Node NARdest =     nodeAllocLateUpdate(dest);
                            List LNAR = new ArrayList();
                            LNAR.add(NARsrc);
                            LNAR.add(NARdest);
                            
                            if((NARsrc == null || !NARsrc.return_success_status())
                                    &&
                                    (NARdest == null || !NARdest.return_success_status()))
                            {                            
                                Print.println("  \t****************  Node Alloc failed ********************" );                            
                                Print.println("  \t****************  ReRoute the Request ********************" );                            
                                Print.println("  \troute_count--> " + route_count );
                                route_count++;
                                re_route = true;
                            }
                            else if(NARsrc.return_success_status()
                                    && 
                                    NARdest.return_success_status())
                            {
                                Print.println("  \t****************  Node Allocation OK  ********************");
                                Print.println("  \t****************  --> The Allocation set was OK, return to process  ********************");
                                updateNodeLinkAllocsSet(LAP, LNAR);
                                return true;

                            }                          
                        }                                            
                    }
                        else 
                        {
                            Print.println("  ****************NO ROUTE  REJECT THE REQUEST  ********************");

                            /*
                            * the philosofy is, if I finish all the routes, and still get to his
                            * point, it means the allcations where not successful
                            */
                            return false;                                               
                        }

                    }   


                    Print.println("  ****************NO ALLOCATION  REJECT THE REQUEST  ********************");

                    /*
                    * the philosofy is, if I finish all the routes, and still get to his
                    * point, it means the allcations where not successful
                    */
                    return false;   

                }
                else if(Req_type.matches("bb")||Req_type.matches("cc"))
                {
                        /*
                  * here i fil the routes, for as much as needed, with new class of reouting table
                  */
                  mNetwork.createNetworkGraph();
                  
//                  Matrix NetMat = mNetwork.return_network_matrix();
//                  int mat_array[][] = NetMat.getDataInt();
//                Print.print_2D(" network matrix: ", mat_array);
  //                Print.print_2D("network created  again... ", mNetwork.return_network_matrix().getDataInt());
  //                Print.println("network nodes again... "+ mNetwork.getmListNodes().toString());
                  node.getmRouting().getmRoutingTable().delRouteS(dest);
                  fillRoutes_dest(mNetwork, number_of_routes_to_try, dest);
                  int available_routes = node.getmRouting().get_number_of_routes_dest(dest);
                  Print.println("  the number of routes in the stock");
                  /*
                  */  
                  while(route_count<available_routes && re_route)
                  {

                      if(Debug){
                          Print.println("  VON ID : "+ VON_ID +" routes avaialble for node " + node.getmID());
                          node.getmRouting().print_routes();
                      }


                      //get the first available route in the list
                      try{
                              route = node.getmRouting().getmRoutingTable().getRoute(dest, route_count);
                              Print.println("  the route is : " + route.toString());

                      }
                      catch(NullPointerException e){
                              Print.println("  VON ID : "+ VON_ID +" Warning no route exists for node+"+ node.getmID());
                              System.exit(3773);
                      }
                    if (route!=null)
                    {
                        Print.println("  \t****************  Route found  ********************" );
                        Print.println("  \t****************  Link Alloc start  ********************" );
                        LinkAllocPacked LAP = routeBreak_resAlloc(route);
                        
                        if(LAP == null)
                            re_route = true;
                        else 
                            re_route = false;
                      
                        if(re_route)
                        {
                            Print.println("  \t****************  Link Alloc failed ********************" );                            
                            Print.println("  \t****************  ReRoute the Request ********************" );                            
                            Print.println("  \troute_count--> " + route_count );
                            route_count++;
                        }
                        else
                        {
                            Print.println("  \troute_count--> " + route_count );
                            Print.println("  \t****************  Link Allocation OK  ********************");
                            Print.println("  \t****************  Node alloc start  ********************");
                            
                            /*
                            * node resrouce allocation should happen here
                            * after the route has resources on the links,
                            * the node resource allocation will take place
                            * this bit is not right, but it will do the job for the time being
                            * my aim is, to have the aggregated resources, all swiped for 
                            * now what I do, I use one hop res link info, for all th ehops.
                            * I have to use the link res info per hop
                            */

                            Alloc_combinedRes_Node NARsrc =     nodeAllocLateUpdate(src);
                            Alloc_combinedRes_Node NARdest =     nodeAllocLateUpdate(dest);
                            List LNAR = new ArrayList();
                            LNAR.add(NARsrc);
                            LNAR.add(NARdest);
                            
                            if((NARsrc == null || !NARsrc.return_success_status())
                                    &&
                                    (NARdest == null || !NARdest.return_success_status()))
                            {                            
                                Print.println("  \t****************  Node Alloc failed ********************" );                            
                                Print.println("  \t****************  ReRoute the Request ********************" );                            
                                Print.println("  \troute_count--> " + route_count );
                                route_count++;
                                re_route = true;
                            }
                            else if(NARsrc.return_success_status()
                                    && 
                                    NARdest.return_success_status())
                            {
                                Print.println("  \t****************  Node Allocation OK  ********************");
                                Print.println("  \t****************  --> The Allocation set was OK, return to process  ********************");
                                updateNodeLinkAllocsSet(LAP, LNAR);
                                return true;

                            }                      
                        }                                            
                    }
                      else 
                      {
                          Print.println("  ****************NO ROUTE  REJECT THE REQUEST  ********************");

                          /*
                          * the philosofy is, if I finish all the routes, and still get to his
                          * point, it means the allcations where not successful
                          */
                          return false;                                               
                      }
                  }
                  Print.println("  ****************NO ALLOCATION  REJECT THE REQUEST  ********************");

                  /*
                  * the philosofy is, if I finish all the routes, and still get to his
                  * point, it means the allcations where not successful
                  */
                  return false;   
                }
                else if(Req_type.matches("dd")||Req_type.matches("ee"))
                {
                    /*
                    * here i fil the routes, for as much as needed, with new class of reouting table
                    */
                    mNetwork.createNetworkGraph_filteredNodesOut(nodes_ids_ofGrid);
                    node.getmRouting().getmRoutingTable().delRouteS(dest);
                    fillRoutes_dest(mNetwork, number_of_routes_to_try, dest);
                    int available_routes = node.getmRouting().get_number_of_routes_dest(dest);
                    Print.println("  the number of routes in the stock: " + available_routes + "  and number_of_routes_to_try:" + number_of_routes_to_try);
                    
                    Print.println("  nodes_ids_ofGRID" + nodes_ids_ofGrid.toString());

                    if(Debug)
                    {
                        Print.println("  VON ID : "+ VON_ID +" routes avaialble for node " + node.getmID());
                        node.getmRouting().print_routes();
                    }                                
                    /*
                    */  
                    while(route_count<available_routes && re_route)
                    {
                        try{
                                route = node.getmRouting().getmRoutingTable().getRoute(dest, route_count);
                                Print.println("  the route is : " + route.toString());
                        }

                        catch(NullPointerException e){
                            Print.println("  VON ID : "+ VON_ID +" Warning no route exists for node+"+ node.getmID());
                            System.exit(3773);
                        } 
                    if (route!=null)
                    {
                        Print.println("  \t****************  Route found  ********************" );
                        Print.println("  \t****************  Link Alloc start  ********************" );
                        LinkAllocPacked LAP = routeBreak_resAlloc(route);
                        
                        if(LAP == null)
                            re_route = true;
                        else 
                            re_route = false;
                      
                        if(re_route)
                        {
                            Print.println("  \t****************  Link Alloc failed ********************" );                            
                            Print.println("  \t****************  ReRoute the Request ********************" );                            
                            Print.println("  \troute_count--> " + route_count );
                            route_count++;
                        }
                        else
                        {
                            Print.println("  \troute_count--> " + route_count );
                            Print.println("  \t****************  Link Allocation OK  ********************");
                            Print.println("  \t****************  Node alloc start  ********************");
                            
                            /*
                            * node resrouce allocation should happen here
                            * after the route has resources on the links,
                            * the node resource allocation will take place
                            * this bit is not right, but it will do the job for the time being
                            * my aim is, to have the aggregated resources, all swiped for 
                            * now what I do, I use one hop res link info, for all th ehops.
                            * I have to use the link res info per hop
                            */

                            Alloc_combinedRes_Node NARsrc =     nodeAllocLateUpdate(src);
                            Alloc_combinedRes_Node NARdest =     nodeAllocLateUpdate(dest);
                            List LNAR = new ArrayList();
                            LNAR.add(NARsrc);
                            LNAR.add(NARdest);
                            
                            if((NARsrc == null || !NARsrc.return_success_status())
                                    &&
                                    (NARdest == null || !NARdest.return_success_status()))
                            {                            
                                Print.println("  \t****************  Node Alloc failed ********************" );                            
                                Print.println("  \t****************  ReRoute the Request ********************" );                            
                                Print.println("  \troute_count--> " + route_count );
                                route_count++;
                                re_route = true;
                            }
                            else if(NARsrc.return_success_status()
                                    && 
                                    NARdest.return_success_status())
                            {
                                Print.println("  \t****************  Node Allocation OK  ********************");
                                Print.println("  \t****************  --> The Allocation set was OK, return to process  ********************");
                                updateNodeLinkAllocsSet(LAP, LNAR);
                                return true;

                            }                       
                        }                                            
                    }
                        else 
                        {
                            Print.println("  ****************NO ROUTE  REJECT THE REQUEST  ********************");

                            /*
                            * the philosofy is, if I finish all the routes, and still get to his
                            * point, it means the allcations where not successful
                            */
                            return false;                                               
                        }

                    }   
                    Print.println("  ****************NO ALLOCATION  REJECT THE REQUEST  ********************");

                    /*
                    * the philosofy is, if I finish all the routes, and still get to his
                    * point, it means the allcations where not successful
                    */
                    return false;   
                }

            }
  
            
            else if(Route_select.matches("RS_FILTER_AVA_FL_S"))
            {
                List<String> Grid_nodes_types = new ArrayList<>();
                List<String> Flex_nodes_types = new ArrayList<>();
                Grid_nodes_types.add("FL");
                Flex_nodes_types.add("S");

                List nodes_ids_ofGrid = mNetwork.getNodesIds_of_types(Grid_nodes_types);
                List nodes_ids_ofFlex = mNetwork.getNodesIds_of_types(Flex_nodes_types);

                //Route route = null;
                //List<Route> proccesed_routes = new ArrayList<>();
                if(Req_type.matches("aa"))
                {
                    /*
                    * here i fil the routes, for as much as needed, with new class of reouting table
                    */
                    mNetwork.createNetworkGraph_filteredNodesOut(nodes_ids_ofFlex);
                    node.getmRouting().getmRoutingTable().delRouteS(dest);
                    fillRoutes_dest(mNetwork, number_of_routes_to_try, dest);
                    if(Debug)
                    {
                        Print.println("  VON ID : "+ VON_ID +" routes avaialble for node " + node.getmID());
                        node.getmRouting().print_routes();
                    }   
                    /*
                    */  
                    List<Route> routes = node.getmRouting().getmRoutingTable().get_dest_routes_list(dest);
                    RouteProcess RP = new RouteProcess(Debug,mNetwork, routes);           
                    int available_routes = node.getmRouting().get_number_of_routes_dest(dest);
                    Print.println("  the number of routes in the stock: " + available_routes + "  and number_of_routes_to_try:" + number_of_routes_to_try);
                    
                    while(route_count<available_routes && re_route)
                    {
                        try
                        {
                                route = RP.getm_least_used_processed_by_availabilites(routes, request_dimension);
                                Print.println("  the route is : " + route.toString());

                        }

                        catch(NullPointerException e){
                            Print.println("  VON ID : "+ VON_ID +" Warning no route exists for node+"+ node.getmID());
                            System.exit(3773);
                        }  
                    if (route!=null)
                    {
                        Print.println("  \t****************  Route found  ********************" );
                        Print.println("  \t****************  Link Alloc start  ********************" );
                        LinkAllocPacked LAP = routeBreak_resAlloc(route);
                        
                        if(LAP == null)
                            re_route = true;
                        else 
                            re_route = false;
                      
                        if(re_route)
                        {
                            Print.println("  \t****************  Link Alloc failed ********************" );                            
                            Print.println("  \t****************  ReRoute the Request ********************" );                            
                            Print.println("  \troute_count--> " + route_count );
                            route_count++;
                        }
                        else
                        {
                            Print.println("  \troute_count--> " + route_count );
                            Print.println("  \t****************  Link Allocation OK  ********************");
                            Print.println("  \t****************  Node alloc start  ********************");
                            
                            /*
                            * node resrouce allocation should happen here
                            * after the route has resources on the links,
                            * the node resource allocation will take place
                            * this bit is not right, but it will do the job for the time being
                            * my aim is, to have the aggregated resources, all swiped for 
                            * now what I do, I use one hop res link info, for all th ehops.
                            * I have to use the link res info per hop
                            */

                            Alloc_combinedRes_Node NARsrc =     nodeAllocLateUpdate(src);
                            Alloc_combinedRes_Node NARdest =     nodeAllocLateUpdate(dest);
                            List LNAR = new ArrayList();
                            LNAR.add(NARsrc);
                            LNAR.add(NARdest);
                            
                            if((NARsrc == null || !NARsrc.return_success_status())
                                    &&
                                    (NARdest == null || !NARdest.return_success_status()))
                            {                            
                                Print.println("  \t****************  Node Alloc failed ********************" );                            
                                Print.println("  \t****************  ReRoute the Request ********************" );                            
                                Print.println("  \troute_count--> " + route_count );
                                route_count++;
                                re_route = true;
                            }
                            else if(NARsrc.return_success_status()
                                    && 
                                    NARdest.return_success_status())
                            {
                                Print.println("  \t****************  Node Allocation OK  ********************");
                                Print.println("  \t****************  --> The Allocation set was OK, return to process  ********************");
                                updateNodeLinkAllocsSet(LAP, LNAR);
                                return true;

                            }                         
                        }                                            
                    }
                        else 
                        {
                            Print.println("  ****************NO ROUTE  REJECT THE REQUEST  ********************");

                            /*
                            * the philosofy is, if I finish all the routes, and still get to his
                            * point, it means the allcations where not successful
                            */
                            return false;                                               
                        }

                    }   

                    Print.println("  ****************NO ALLOCATION  REJECT THE REQUEST  ********************");

                    /*
                    * the philosofy is, if I finish all the routes, and still get to his
                    * point, it means the allcations where not successful
                    */
                    return false;   
                    
                }
                if(Req_type.matches("bb")||Req_type.matches("cc"))
                {
                    /*
                   * here i fil the routes, for as much as needed, with new class of reouting table
                   */
                   mNetwork.createNetworkGraph();
                   
//                   Matrix NetMat = mNetwork.return_network_matrix();
//                  int mat_array[][] = NetMat.getDataInt();
//                Print.print_2D(" network matrix: ", mat_array);
                  
   //                Print.print_2D("network created  again... ", mNetwork.return_network_matrix().getDataInt());
   //                Print.println("network nodes again... "+ mNetwork.getmListNodes().toString());
                   node.getmRouting().getmRoutingTable().delRouteS(dest);
                   fillRoutes_dest(mNetwork, number_of_routes_to_try, dest);
                   int available_routes = node.getmRouting().get_number_of_routes_dest(dest);
                   Print.println("  the number of routes in the stock");
                   /*
                   */  

                   List<Route> routes = node.getmRouting().getmRoutingTable().get_dest_routes_list(dest);
                   RouteProcess RP = new RouteProcess(Debug,mNetwork, routes);           

                   while(route_count<available_routes && re_route)
                   {
                       try
                       {
                               route = RP.getm_least_used_processed_by_availabilites(routes, request_dimension);
                               Print.println("  the route is : " + route.toString());

                       }

                       catch(NullPointerException e){
                           Print.println("  VON ID : "+ VON_ID +" Warning no route exists for node+"+ node.getmID());
                           System.exit(3773);
                       }  
                    if (route!=null)
                    {
                        Print.println("  \t****************  Route found  ********************" );
                        Print.println("  \t****************  Link Alloc start  ********************" );
                        LinkAllocPacked LAP = routeBreak_resAlloc(route);
                        
                        if(LAP == null)
                            re_route = true;
                        else 
                            re_route = false;
                      
                        if(re_route)
                        {
                            Print.println("  \t****************  Link Alloc failed ********************" );                            
                            Print.println("  \t****************  ReRoute the Request ********************" );                            
                            Print.println("  \troute_count--> " + route_count );
                            route_count++;
                        }
                        else
                        {
                            Print.println("  \troute_count--> " + route_count );
                            Print.println("  \t****************  Link Allocation OK  ********************");
                            Print.println("  \t****************  Node alloc start  ********************");
                            
                            /*
                            * node resrouce allocation should happen here
                            * after the route has resources on the links,
                            * the node resource allocation will take place
                            * this bit is not right, but it will do the job for the time being
                            * my aim is, to have the aggregated resources, all swiped for 
                            * now what I do, I use one hop res link info, for all th ehops.
                            * I have to use the link res info per hop
                            */

                            Alloc_combinedRes_Node NARsrc =     nodeAllocLateUpdate(src);
                            Alloc_combinedRes_Node NARdest =     nodeAllocLateUpdate(dest);
                            List LNAR = new ArrayList();
                            LNAR.add(NARsrc);
                            LNAR.add(NARdest);
                            
                            if((NARsrc == null || !NARsrc.return_success_status())
                                    &&
                                    (NARdest == null || !NARdest.return_success_status()))
                            {                            
                                Print.println("  \t****************  Node Alloc failed ********************" );                            
                                Print.println("  \t****************  ReRoute the Request ********************" );                            
                                Print.println("  \troute_count--> " + route_count );
                                route_count++;
                                re_route = true;
                            }
                            else if(NARsrc.return_success_status()
                                    && 
                                    NARdest.return_success_status())
                            {
                                Print.println("  \t****************  Node Allocation OK  ********************");
                                Print.println("  \t****************  --> The Allocation set was OK, return to process  ********************");
                                updateNodeLinkAllocsSet(LAP, LNAR);
                                return true;

                            }                      
                        }                                            
                    }
                       else 
                       {
                           Print.println("  ****************NO ROUTE  REJECT THE REQUEST  ********************");

                           /*
                           * the philosofy is, if I finish all the routes, and still get to his
                           * point, it means the allcations where not successful
                           */
                           return false;                                               
                       }

                   }   

                   Print.println("  ****************NO ALLOCATION  REJECT THE REQUEST  ********************");

                   /*
                   * the philosofy is, if I finish all the routes, and still get to his
                   * point, it means the allcations where not successful
                   */
                   return false;   
                    
                }
                else if(Req_type.matches("dd")||Req_type.matches("ee"))                
//                else if(Star_Req_type.matches("dd")||Star_Req_type.matches("ee"))
                {
                    /*
                    * here i fil the routes, for as much as needed, with new class of reouting table
                    */
                    mNetwork.createNetworkGraph_filteredNodesOut(nodes_ids_ofGrid);
                    node.getmRouting().getmRoutingTable().delRouteS(dest);
                    fillRoutes_dest(mNetwork, number_of_routes_to_try, dest);

                    if(Debug)
                    {
                        Print.println("  VON ID : "+ VON_ID +" routes avaialble for node " + node.getmID());
                        node.getmRouting().print_routes();
                    }                                
                    /*
                    */  
                    List<Route> routes = node.getmRouting().getmRoutingTable().get_dest_routes_list(dest);
                    RouteProcess RP = new RouteProcess(Debug,mNetwork, routes);   
                    int available_routes = node.getmRouting().get_number_of_routes_dest(dest);
                    Print.println("  the number of routes in the stock: " + available_routes + "  and number_of_routes_to_try:" + number_of_routes_to_try);
                    
                    while(route_count<available_routes && re_route)
                    {
                        try
                        {
                                route = RP.getm_least_used_processed_by_availabilites(routes, request_dimension);
                                Print.println("  the route is : " + route.toString());

                        }

                        catch(NullPointerException e){
                            Print.println("  VON ID : "+ VON_ID +" Warning no route exists for node+"+ node.getmID());
                            System.exit(3773);
                        }
                    if (route!=null)
                    {
                        Print.println("  \t****************  Route found  ********************" );
                        Print.println("  \t****************  Link Alloc start  ********************" );
                        LinkAllocPacked LAP = routeBreak_resAlloc(route);
                        
                        if(LAP == null)
                            re_route = true;
                        else 
                            re_route = false;
                      
                        if(re_route)
                        {
                            Print.println("  \t****************  Link Alloc failed ********************" );                            
                            Print.println("  \t****************  ReRoute the Request ********************" );                            
                            Print.println("  \troute_count--> " + route_count );
                            route_count++;
                        }
                        else
                        {
                            Print.println("  \troute_count--> " + route_count );
                            Print.println("  \t****************  Link Allocation OK  ********************");
                            Print.println("  \t****************  Node alloc start  ********************");
                            
                            /*
                            * node resrouce allocation should happen here
                            * after the route has resources on the links,
                            * the node resource allocation will take place
                            * this bit is not right, but it will do the job for the time being
                            * my aim is, to have the aggregated resources, all swiped for 
                            * now what I do, I use one hop res link info, for all th ehops.
                            * I have to use the link res info per hop
                            */

                            Alloc_combinedRes_Node NARsrc =     nodeAllocLateUpdate(src);
                            Alloc_combinedRes_Node NARdest =     nodeAllocLateUpdate(dest);
                            List LNAR = new ArrayList();
                            LNAR.add(NARsrc);
                            LNAR.add(NARdest);
                            
                            if((NARsrc == null || !NARsrc.return_success_status())
                                    &&
                                    (NARdest == null || !NARdest.return_success_status()))
                            {                            
                                Print.println("  \t****************  Node Alloc failed ********************" );                            
                                Print.println("  \t****************  ReRoute the Request ********************" );                            
                                Print.println("  \troute_count--> " + route_count );
                                route_count++;
                                re_route = true;
                            }
                            else if(NARsrc.return_success_status()
                                    && 
                                    NARdest.return_success_status())
                            {
                                Print.println("  \t****************  Node Allocation OK  ********************");
                                Print.println("  \t****************  --> The Allocation set was OK, return to process  ********************");
                                updateNodeLinkAllocsSet(LAP, LNAR);
                                return true;

                            }                         
                        }                                            
                    }
                        else 
                        {
                            Print.println("  ****************NO ROUTE  REJECT THE REQUEST  ********************");

                            /*
                            * the philosofy is, if I finish all the routes, and still get to his
                            * point, it means the allcations where not successful
                            */
                            return false;                                               
                        }
                    }   


                    Print.println("  ****************NO ALLOCATION  REJECT THE REQUEST  ********************");

                    /*
                    * the philosofy is, if I finish all the routes, and still get to his
                    * point, it means the allcations where not successful
                    */
                    return false;                                

                }
            }
            else if(Route_select.matches("RS_FILTER_AVA_F_LS"))
            {
                List<String> Grid_nodes_types = new ArrayList<>();
                List<String> Flex_nodes_types = new ArrayList<>();
                Grid_nodes_types.add("F");
                Flex_nodes_types.add("LS");

                List nodes_ids_ofGrid = mNetwork.getNodesIds_of_types(Grid_nodes_types);
                List nodes_ids_ofFlex = mNetwork.getNodesIds_of_types(Flex_nodes_types);

                //Route route = null;
                //List<Route> proccesed_routes = new ArrayList<>();
                if(Req_type.matches("aa"))
                {
                    /*
                    * here i fil the routes, for as much as needed, with new class of reouting table
                    */
                    mNetwork.createNetworkGraph_filteredNodesOut(nodes_ids_ofFlex);
                    node.getmRouting().getmRoutingTable().delRouteS(dest);
                    fillRoutes_dest(mNetwork, number_of_routes_to_try, dest);
                    if(Debug)
                    {
                        Print.println("  VON ID : "+ VON_ID +" routes avaialble for node " + node.getmID());
                        node.getmRouting().print_routes();
                    }   
                    /*
                    */  
                    List<Route> routes = node.getmRouting().getmRoutingTable().get_dest_routes_list(dest);
                    RouteProcess RP = new RouteProcess(Debug,mNetwork, routes);  
                    int available_routes = node.getmRouting().get_number_of_routes_dest(dest);
                    Print.println("  the number of routes in the stock: " + available_routes + "  and number_of_routes_to_try:" + number_of_routes_to_try);
                    
                    while(route_count<available_routes && re_route)
                    {
                        try
                        {
                                route = RP.getm_least_used_processed_by_availabilites(routes, request_dimension);
                                Print.println("  the route is : " + route.toString());

                        }

                        catch(NullPointerException e){
                            Print.println("  VON ID : "+ VON_ID +" Warning no route exists for node+"+ node.getmID());
                            System.exit(3773);
                        }  
                    if (route!=null)
                    {
                        Print.println("  \t****************  Route found  ********************" );
                        Print.println("  \t****************  Link Alloc start  ********************" );
                        LinkAllocPacked LAP = routeBreak_resAlloc(route);
                        
                        if(LAP == null)
                            re_route = true;
                        else 
                            re_route = false;
                      
                        if(re_route)
                        {
                            Print.println("  \t****************  Link Alloc failed ********************" );                            
                            Print.println("  \t****************  ReRoute the Request ********************" );                            
                            Print.println("  \troute_count--> " + route_count );
                            route_count++;
                        }
                        else
                        {
                            Print.println("  \troute_count--> " + route_count );
                            Print.println("  \t****************  Link Allocation OK  ********************");
                            Print.println("  \t****************  Node alloc start  ********************");
                            
                            /*
                            * node resrouce allocation should happen here
                            * after the route has resources on the links,
                            * the node resource allocation will take place
                            * this bit is not right, but it will do the job for the time being
                            * my aim is, to have the aggregated resources, all swiped for 
                            * now what I do, I use one hop res link info, for all th ehops.
                            * I have to use the link res info per hop
                            */

                            Alloc_combinedRes_Node NARsrc =     nodeAllocLateUpdate(src);
                            Alloc_combinedRes_Node NARdest =     nodeAllocLateUpdate(dest);
                            List LNAR = new ArrayList();
                            LNAR.add(NARsrc);
                            LNAR.add(NARdest);
                            
                            if((NARsrc == null || !NARsrc.return_success_status())
                                    &&
                                    (NARdest == null || !NARdest.return_success_status()))
                            {                            
                                Print.println("  \t****************  Node Alloc failed ********************" );                            
                                Print.println("  \t****************  ReRoute the Request ********************" );                            
                                Print.println("  \troute_count--> " + route_count );
                                route_count++;
                                re_route = true;
                            }
                            else if(NARsrc.return_success_status()
                                    && 
                                    NARdest.return_success_status())
                            {
                                Print.println("  \t****************  Node Allocation OK  ********************");
                                Print.println("  \t****************  --> The Allocation set was OK, return to process  ********************");
                                updateNodeLinkAllocsSet(LAP, LNAR);
                                return true;

                            }                        
                        }                                            
                    }
                        else 
                        {
                            Print.println("  ****************NO ROUTE  REJECT THE REQUEST  ********************");

                            /*
                            * the philosofy is, if I finish all the routes, and still get to his
                            * point, it means the allcations where not successful
                            */
                            return false;                                               
                        }

                    }   

                    Print.println("  ****************NO ALLOCATION  REJECT THE REQUEST  ********************");

                    /*
                    * the philosofy is, if I finish all the routes, and still get to his
                    * point, it means the allcations where not successful
                    */
                    return false;   
                    
                }
               if(Req_type.matches("bb")||Req_type.matches("cc"))
                {
                    /*
                   * here i fil the routes, for as much as needed, with new class of reouting table
                   */
                   mNetwork.createNetworkGraph();
                   
//                   Matrix NetMat = mNetwork.return_network_matrix();
//                  int mat_array[][] = NetMat.getDataInt();
//                Print.print_2D(" network matrix: ", mat_array);
   //                Print.print_2D("network created  again... ", mNetwork.return_network_matrix().getDataInt());
   //                Print.println("network nodes again... "+ mNetwork.getmListNodes().toString());
                   node.getmRouting().getmRoutingTable().delRouteS(dest);
                   fillRoutes_dest(mNetwork, number_of_routes_to_try, dest);
                   int available_routes = node.getmRouting().get_number_of_routes_dest(dest);
                   Print.println("  the number of routes in the stock");
                   /*
                   */  

                   List<Route> routes = node.getmRouting().getmRoutingTable().get_dest_routes_list(dest);
                   RouteProcess RP = new RouteProcess(Debug,mNetwork, routes);           

                   while(route_count<available_routes && re_route)
                   {
                       try
                       {
                               route = RP.getm_least_used_processed_by_availabilites(routes, request_dimension);
                               Print.println("  the route is : " + route.toString());

                       }

                       catch(NullPointerException e){
                           Print.println("  VON ID : "+ VON_ID +" Warning no route exists for node+"+ node.getmID());
                           System.exit(3773);
                       }  
                    if (route!=null)
                    {
                        Print.println("  \t****************  Route found  ********************" );
                        Print.println("  \t****************  Link Alloc start  ********************" );
                        LinkAllocPacked LAP = routeBreak_resAlloc(route);
                        
                        if(LAP == null)
                            re_route = true;
                        else 
                            re_route = false;
                      
                        if(re_route)
                        {
                            Print.println("  \t****************  Link Alloc failed ********************" );                            
                            Print.println("  \t****************  ReRoute the Request ********************" );                            
                            Print.println("  \troute_count--> " + route_count );
                            route_count++;
                        }
                        else
                        {
                            Print.println("  \troute_count--> " + route_count );
                            Print.println("  \t****************  Link Allocation OK  ********************");
                            Print.println("  \t****************  Node alloc start  ********************");
                            
                            /*
                            * node resrouce allocation should happen here
                            * after the route has resources on the links,
                            * the node resource allocation will take place
                            * this bit is not right, but it will do the job for the time being
                            * my aim is, to have the aggregated resources, all swiped for 
                            * now what I do, I use one hop res link info, for all th ehops.
                            * I have to use the link res info per hop
                            */

                            Alloc_combinedRes_Node NARsrc =     nodeAllocLateUpdate(src);
                            Alloc_combinedRes_Node NARdest =     nodeAllocLateUpdate(dest);
                            List LNAR = new ArrayList();
                            LNAR.add(NARsrc);
                            LNAR.add(NARdest);
                            
                            if((NARsrc == null || !NARsrc.return_success_status())
                                    &&
                                    (NARdest == null || !NARdest.return_success_status()))
                            {                            
                                Print.println("  \t****************  Node Alloc failed ********************" );                            
                                Print.println("  \t****************  ReRoute the Request ********************" );                            
                                Print.println("  \troute_count--> " + route_count );
                                route_count++;
                                re_route = true;
                            }
                            else if(NARsrc.return_success_status()
                                    && 
                                    NARdest.return_success_status())
                            {
                                Print.println("  \t****************  Node Allocation OK  ********************");
                                Print.println("  \t****************  --> The Allocation set was OK, return to process  ********************");
                                updateNodeLinkAllocsSet(LAP, LNAR);
                                return true;

                            }                       
                        }                                            
                    }
                       else 
                       {
                           Print.println("  ****************NO ROUTE  REJECT THE REQUEST  ********************");

                           /*
                           * the philosofy is, if I finish all the routes, and still get to his
                           * point, it means the allcations where not successful
                           */
                           return false;                                               
                       }

                   }   

                   Print.println("  ****************NO ALLOCATION  REJECT THE REQUEST  ********************");

                   /*
                   * the philosofy is, if I finish all the routes, and still get to his
                   * point, it means the allcations where not successful
                   */
                   return false;   
                    
                }                
                else if(Req_type.matches("dd")||Req_type.matches("ee"))
                {
                    /*
                    * here i fil the routes, for as much as needed, with new class of reouting table
                    */
                    mNetwork.createNetworkGraph_filteredNodesOut(nodes_ids_ofGrid);
                    node.getmRouting().getmRoutingTable().delRouteS(dest);
                    fillRoutes_dest(mNetwork, number_of_routes_to_try, dest);

                    if(Debug)
                    {
                        Print.println("  VON ID : "+ VON_ID +" routes avaialble for node " + node.getmID());
                        node.getmRouting().print_routes();
                    }                                
                    /*
                    */  
                    List<Route> routes = node.getmRouting().getmRoutingTable().get_dest_routes_list(dest);
                    RouteProcess RP = new RouteProcess(Debug,mNetwork, routes);   
                    int available_routes = node.getmRouting().get_number_of_routes_dest(dest);
                    Print.println("  the number of routes in the stock: " + available_routes + "  and number_of_routes_to_try:" + number_of_routes_to_try);
                    
                    while(route_count<available_routes && re_route)
                    {
                        try
                        {
                                route = RP.getm_least_used_processed_by_availabilites(routes, request_dimension);
                                Print.println("  the route is : " + route.toString());

                        }

                        catch(NullPointerException e){
                            Print.println("  VON ID : "+ VON_ID +" Warning no route exists for node+"+ node.getmID());
                            System.exit(3773);
                        }
                    if (route!=null)
                    {
                        Print.println("  \t****************  Route found  ********************" );
                        Print.println("  \t****************  Link Alloc start  ********************" );
                        LinkAllocPacked LAP = routeBreak_resAlloc(route);
                        
                        if(LAP == null)
                            re_route = true;
                        else 
                            re_route = false;
                      
                        if(re_route)
                        {
                            Print.println("  \t****************  Link Alloc failed ********************" );                            
                            Print.println("  \t****************  ReRoute the Request ********************" );                            
                            Print.println("  \troute_count--> " + route_count );
                            route_count++;
                        }
                        else
                        {
                            Print.println("  \troute_count--> " + route_count );
                            Print.println("  \t****************  Link Allocation OK  ********************");
                            Print.println("  \t****************  Node alloc start  ********************");
                            
                            /*
                            * node resrouce allocation should happen here
                            * after the route has resources on the links,
                            * the node resource allocation will take place
                            * this bit is not right, but it will do the job for the time being
                            * my aim is, to have the aggregated resources, all swiped for 
                            * now what I do, I use one hop res link info, for all th ehops.
                            * I have to use the link res info per hop
                            */

                            Alloc_combinedRes_Node NARsrc =     nodeAllocLateUpdate(src);
                            Alloc_combinedRes_Node NARdest =     nodeAllocLateUpdate(dest);
                            List LNAR = new ArrayList();
                            LNAR.add(NARsrc);
                            LNAR.add(NARdest);
                            
                            if((NARsrc == null || !NARsrc.return_success_status())
                                    &&
                                    (NARdest == null || !NARdest.return_success_status()))
                            {                            
                                Print.println("  \t****************  Node Alloc failed ********************" );                            
                                Print.println("  \t****************  ReRoute the Request ********************" );                            
                                Print.println("  \troute_count--> " + route_count );
                                route_count++;
                                re_route = true;
                            }
                            else if(NARsrc.return_success_status()
                                    && 
                                    NARdest.return_success_status())
                            {
                                Print.println("  \t****************  Node Allocation OK  ********************");
                                Print.println("  \t****************  --> The Allocation set was OK, return to process  ********************");
                                updateNodeLinkAllocsSet(LAP, LNAR);
                                return true;

                            }                        
                        }                                            
                    }
                        else 
                        {
                            Print.println("  ****************NO ROUTE  REJECT THE REQUEST  ********************");

                            /*
                            * the philosofy is, if I finish all the routes, and still get to his
                            * point, it means the allcations where not successful
                            */
                            return false;                                               
                        }
                    }   


                    Print.println("  ****************NO ALLOCATION  REJECT THE REQUEST  ********************");

                    /*
                    * the philosofy is, if I finish all the routes, and still get to his
                    * point, it means the allcations where not successful
                    */
                    return false;                                

                }
            }
            else 
            {
                Print.println("  problem in findng routing state " );
                System.exit(5552555);                        
            }

            return false;
    }
    
    /*
     * all the allocations happen here.
     * a calculated path is broken in to pieces, and then each
     * part of them is checked for allocation
     * initially it was only for link res allocs.
     * I have added node information, which to be enabked
     * requires algorithms in the *alloc* 
     */
    public LinkAllocPacked routeBreak_resAlloc(Route route)
    {
        
        
        LinkAllocPacked LAP; 
        /*
         * note: here I have added some node allocation stuff, which they are n
         * not working and are only definitions.
         * the use for this is if I am pasing each and every node in the
         * route to check the availabilities, just like the link coutner parts
         * 
         * since I dont need it for OFC, I will add ideentical method but with
         * node oonly operationsbelow.
         */
        boolean re_route = false;
        boolean continue_res_alloc_on_subroutes = true;
        
            if(Debug)
            Print.println(" original route : "+ route.toString());
            List<Route> rots = route_breaker(route);
            if(Debug)
            Print.println(" number of sub_routes  : "+ rots.size());

            //first, remove the expired connections
            //--> I am now clearing all the links, in the heuristics class

            //swipeTheRecords(route.return_hops_in_route());

            /*
            * in each route, I process the sub routes -->
            * if one of the sub routes fail in allocation, the whole process needs
            * to be stopped
            */ 
            
            /*
             * for node resrouce allocation, probably here is the right place
             * 
             */
            for(int i = 0; i < rots.size()&&continue_res_alloc_on_subroutes; i ++)
            {

                
                    
                    // get the resources for each subroute
                /*
                 * problem if I set the nodes to true. probably, since the nodes 
                 * along iwth the links on any sub path should have the same 
                 * (because of the aggregation of resources). so there can be the
                 * problem0
                 */
                    NodeLink_res_aloc_route GR = new NodeLink_res_aloc_route(mNetwork, rots.get(i), false);
                    
                    
                    //List<String>  res_info = GR.res_info_structure();
                    List<List> link_res_tobe_alloc = GR.return_final_links_combination();
//                    List<List> node_res_tobe_alloc = GR.return_final_nodes_combination();
                    //Print.println(" after final combination,ready for alloc " + link_res_tobe_alloc.toString());
                    /*
                        * this bit is not right, but it will do the job for the time being
                        * my aim is, to have the aggregated resources, all swiped for 
                        * now what I do, I use one hop res link info, for all th ehops.
                        * I have to use the link res info per hop
                        */
                    String hop_id = rots.get(i).return_hops_in_route().get(0);
                    int Link_id = mNetwork.get_link_num_from_id(hop_id);
                    Link  l = mNetwork.getmListLinks().get(Link_id);
                    if(Debug)
                    Print.println("  routebreak Res_info_link: " );
                    
                    List<String> Res_info_link =  l.get_ParentClass_resrouces().getm_resinfo();
                    
                    if(Debug)
                    Print.println("  routebreak Res_info_link: " + Res_info_link.toString());
                    
                    List<List> cloned_link_res = spread_all_over_combined(link_res_tobe_alloc,Res_info_link);
                    

//                    Print.println("  in LinkAllocPacked routebreak, get the detailed res availabilities: "
//                            + l.get_ParentClass_resrouces().getm_detailed_resources_availabiilties());
//                    Print.println("  in LinkAllocPacked routebreak, get the detailed AGGREGATE res availabilities: "
//                            + l.get_ParentClass_resrouces().getm_Agg_layers_resources());                    
//                    
                    /*
                        * this bit is not right, borrowd from the links stuff
                        */                    
                    
//                    int Node_id = rots.get(i).return_nodesIDs_in_route()[0];
//                    Node  n = mNetwork.getmListNodes().get(Node_id);
//                    List<String> Res_info_node =  n.get_ParentClass_resrouces().getm_resinfo();                    
//                    List<List> cloned_node_res = spread_all_over_combined(node_res_tobe_alloc,Res_info_node);                    
//                    

//                    Print.println("  \nlink_res_tobe_alloc " + link_res_tobe_alloc.toString());
//                    Print.println("  \nRes_info_link " + Res_info_link.toString());
//                    Print.println("  \nnode_res_tobe_alloc " + node_res_tobe_alloc.toString());
//                    Print.println("  \nnode_Res_info_node " + Res_info_node.toString());
//                    
                   /*
                    * make a new standalone node res allocaiton. this is differtnet form 
                    * links, it should allocate adhoc selection of the node.
                    * so I pass the required resources, on the selected nodes,
                    * and they should be allocated
                    */                         
                    
//                    Print.println(" cloned!! cloned_link_res " + cloned_link_res.toString());
//                    Print.println(" cloned!! cloned_node_res " + cloned_node_res.toString());
                    
//                    if(this.check_for_IT_NET_availabilities(request, cloned_link_res, cloned_node_res))
                    if(this.check_for_NET_availabilities(request, cloned_link_res))
                    {
                        //Res_Parent RP = new Res_Parent(Debug, res_info, link_res_tobe_alloc, request);
                        //List<List> cloned_link_res = spread_all(rots.get(i).return_hops_in_route());
                        //List<List> cloned_link_res = link_res_tobe_alloc;
                        //Print.println(" after final combination, a copy of the resources are made " + cloned_link_res.toString());

                        //Print.println(" cloned!! ready for alloc " + cloned_link_res.toString());
    //                                Alloc_combinedRes_Link NAR = new Alloc_combinedRes_Link(Debug,  cloned_link_res, request, Res_info_link);
                        
                        
                        
                        /*
                         * the node information are passed in to the ALloc.. 
                         * class below. However in there, I need to apply algorithms ,
                         * which only is applied to the link resrouces.
                         *
                         */
                        Alloc_combinedRes_Link AR = new Alloc_combinedRes_Link(Debug, request, 
                                cloned_link_res, Res_info_link, 
//                                cloned_node_res, Res_info_node, 
                                Spec_time_map, 
                                SubL_min,SubL_max,
                                Lambda_min,
                                Lambda_max, 
                                Flex_max, 
                                Flex_min
                                );


                        if(AR.return_success_status()){
                            Print.println("++++++++++++++++++++++++++++++++++++SUCCESSFUL ALLOCATION in routeBreak_resAlloc " );
                            
                            List<List> alloc = AR.getm_Resrouces_tb_updated();
                            LAP = new LinkAllocPacked(route, AR);
                           
//                            update_link_resrouces(rots.get(i).return_hops_in_route(), alloc,AR.get_allocation_object());
                            this.Route = route;
                            re_route= false;
                            continue_res_alloc_on_subroutes = true;
//                            put_it_in_records(rots.get(i), Link_record_hash, alloc, AR.get_allocation_object());
                            //return re_route;
                                return LAP;
                        }
                        else {
                            Print.println("++++++++++++++++++++++++++++++++++++NO SUCCESS in ALLOCATION in routeBreak_resAlloc " );
                            
                            // here the failure is during the allocaitons
                            continue_res_alloc_on_subroutes = false;
                            re_route = true;
                            return null;
                        }
                    }
                    else
                    {
                        //her ethe failure is before getting into the allcoaitons

                        Print.println("  not enough resources WITH HOP ID" + hop_id );
                        continue_res_alloc_on_subroutes = false;
                        re_route = true;
                        return null;
                    }
                
            

            }
            Print.println(" bad route I suppose " );

        /*
         * this return should be positive, and meaning allocation
         */
        return null;
    }
    
    
    /*
     * the twin of routebreak alloc
     * I have implemented the node allocation, individually here, so I can allocate 
     * resources more freely.
     */
//    public boolean nodeAllocRoute(int Node_id)
//    {
//        /*
//         * note: here I have added some node allocation stuff, which they are n
//         * not working and are only definitions.
//         * the use for this is if I am pasing each and every node in the
//         * route to check the availabilities, just like the link coutner parts
//         * 
//         * since I dont need it for OFC, I will add ideentical method but with
//         * node oonly operationsbelow.
//         */
//        boolean re_route = false;
//        boolean continue_res_alloc_on_subroutes = true;
//        
//
//            if(Debug){}
////            Print.println(" number of sub_routes  : "+ rots.size());
//
//            //first, remove the expired connections
//            //--> I am now clearing all the links, in the heuristics class
//
//            //swipeTheRecords(route.return_hops_in_route());
//
//            /*
//            * in each route, I process the sub routes -->
//            * if one of the sub routes fail in allocation, the whole process needs
//            * to be stopped
//            */ 
//            
//            /*
//             * for node resrouce allocation, probably here is the right place
//             * 
//             */            
//
//                    
//                    // get the resources for each subroute
//                    Node_res_alloc_node GR = new Node_res_alloc_node(mNetwork, Node_id);
//                    //List<String>  res_info = GR.res_info_structure();
//                    List<List> node_res_tobe_alloc = GR.return_final_nodes_combination();
//                    //Print.println(" after final combination,ready for alloc " + link_res_tobe_alloc.toString());
//                    /*
//                        * this bit is not right, but it will do the job for the time being
//                        * my aim is, to have the aggregated resources, all swiped for 
//                        * now what I do, I use one hop res link info, for all th ehops.
//                        * I have to use the link res info per hop
//                        */
//
//                    /*
//                        * this bit is not right, borrowd from the links stuff
//                        */                    
//                    
//                    Node  n = mNetwork.getmListNodes().get(Node_id);
//                    List<String> Res_info_node =  n.get_ParentClass_resrouces().getm_resinfo();    
//                    Print.println("  \n  Res_info_node" + Res_info_node.toString());
//                    
//                    
//                    
//                    List<List> cloned_node_res = spread_all_over_combined(node_res_tobe_alloc,Res_info_node);                    
//                    
//
//                    Print.println("  \nnode_res_tobe_alloc " + node_res_tobe_alloc.toString());
//                    Print.println("  \nnode_Res_info_node " + Res_info_node.toString());
//                    
//                   /*
//                    * make a new standalone node res allocaiton. this is differtnet form 
//                    * links, it should allocate adhoc selection of the node.
//                    * so I pass the required resources, on the selected nodes,
//                    * and they should be allocated
//                    */                         
//                    
//                    
//                    if(this.check_for_NET_availabilities(request, cloned_node_res))
//                    {
//                        //Res_Parent RP = new Res_Parent(Debug, res_info, link_res_tobe_alloc, request);
//                        //List<List> cloned_link_res = spread_all(rots.get(i).return_hops_in_route());
//                        //List<List> cloned_link_res = link_res_tobe_alloc;
//                        //Print.println(" after final combination, a copy of the resources are made " + cloned_link_res.toString());
//
//                        //Print.println(" cloned!! ready for alloc " + cloned_link_res.toString());
//    //                                Alloc_combinedRes_Link NAR = new Alloc_combinedRes_Link(Debug,  cloned_link_res, request, Res_info_link);
//                        Alloc_combinedRes_Node AR = new Alloc_combinedRes_Node
//                                (
//                                Debug, request, 
//                                cloned_node_res, Res_info_node, 
//                                Spec_time_map, 
//                                SubL_min,
//                                SubL_max,
//                                Lambda_min,
//                                Lambda_max, 
//                                Flex_max, 
//                                Flex_min
//                                );
//
//
//                        if(AR.return_success_status()){
//                            List<List> alloc = AR.getm_Resrouces_tb_updated();
//                            update_single_node_resrouces(Node_id, alloc,AR.get_allocation_object());
//                            re_route= false;
//                            continue_res_alloc_on_subroutes = true;
//                            put_it_in_recordsnodes(Node_id, Link_record_hash, alloc, AR.get_allocation_object());
////                            put_it_in_records(rots.get(i), Link_record_hash, alloc, NAR.get_allocation_object());
//                            //return re_route;
//
//                        }
//                        else {
//                            // here the failure is during the allocaitons
//                            continue_res_alloc_on_subroutes = false;
//                            re_route = true;
//                            return re_route;
//                        }
//                    }
//                    else
//                    {
//                        //her ethe failure is before getting into the allcoaitons
//
//                        Print.println("  not enough resources " );
//                        continue_res_alloc_on_subroutes = false;
//                        re_route = true;
//                        return re_route;
//                    }
//                
//            
//
//            
// 
//        /*
//         * this return should be positive, and meaning allocation
//         */
//        return re_route;
//    }
//            
    public Alloc_combinedRes_Node nodeAllocLateUpdate(int Node_id)
    {
        /*
         * note: here I have added some node allocation stuff, which they are n
         * not working and are only definitions.
         * the use for this is if I am pasing each and every node in the
         * route to check the availabilities, just like the link coutner parts
         * 
         * since I dont need it for OFC, I will add ideentical method but with
         * node oonly operationsbelow.
         */
        boolean re_route = false;
        boolean continue_res_alloc_on_subroutes = true;
        

        if(Debug){}

            /*
            * in each route, I process the sub routes -->
            * if one of the sub routes fail in allocation, the whole process needs
            * to be stopped
            */ 

        // get the resources for each subroute
        Node_res_alloc_node GR = new Node_res_alloc_node(mNetwork, Node_id);
        //List<String>  res_info = GR.res_info_structure();
        List<List> node_res_tobe_alloc = GR.return_final_nodes_combination();
        //Print.println(" after final combination,ready for alloc " + link_res_tobe_alloc.toString());
        /*
            * this bit is not right, but it will do the job for the time being
            * my aim is, to have the aggregated resources, all swiped for 
            * now what I do, I use one hop res link info, for all th ehops.
            * I have to use the link res info per hop
            */

        /*
            * this bit is not right, borrowd from the links stuff
            */                    

        Node  n = mNetwork.getmListNodes().get(Node_id);
        List<String> Res_info_node =  n.get_ParentClass_resrouces().getm_resinfo();
//        Print.println("  \nnode_res_tobe_alloc " + node_res_tobe_alloc.toString());
//        Print.println("  \nnode_Res_info_node " + Res_info_node.toString());

        List<List> cloned_node_res = spread_all_over_combined(node_res_tobe_alloc,Res_info_node);                    

        if(Debug){
                Print.println("  in nodealloc, get the detailed res availabilities: "
                        + n.get_ParentClass_resrouces().getm_detailed_resources_availabiilties());
                Print.println("  in nodealloc, get the detailed AGGREGATE res availabilities: "
                        + n.get_ParentClass_resrouces().getm_Agg_layers_resources());                    
        }


       /*
        * make a new standalone node res allocaiton. this is differtnet form 
        * links, it should allocate adhoc selection of the node.
        * so I pass the required resources, on the selected nodes,
        * and they should be allocated
        */                         
        if(Debug)    
        {
            Print.println("  in nodealloc, get the detailed res availabilities: "
                            +cloned_node_res.toString());
            Print.println("  in nodealloc, get the Res_info_node: "
                            +Res_info_node.toString());
        }
        Alloc_combinedRes_Node NAR = null;
        if(this.check_for_Node_availabilities(request, cloned_node_res))
        {
            //Res_Parent RP = new Res_Parent(Debug, res_info, link_res_tobe_alloc, request);
            //List<List> cloned_link_res = spread_all(rots.get(i).return_hops_in_route());
            //List<List> cloned_link_res = link_res_tobe_alloc;
            //Print.println(" after final combination, a copy of the resources are made " + cloned_link_res.toString());

            //Print.println(" cloned!! ready for alloc " + cloned_link_res.toString());
//                                Alloc_combinedRes_Link NAR = new Alloc_combinedRes_Link(Debug,  cloned_link_res, request, Res_info_link);
//            Print.println(" request.returnITREQ().toString() " + request.returnITREQ().toString());
            NAR = new Alloc_combinedRes_Node
                    (
                    Debug, request, 
                    cloned_node_res, Res_info_node, 
                    Spec_time_map, 
                    SubL_min,
                    SubL_max,
                    Lambda_min,
                    Lambda_max, 
                    Flex_max, 
                    Flex_min,
                    Node_id,
                    Spec_time_map
                    );


            if(NAR.return_success_status()){
                Print.println("++++++++++++++++++++++++++++++++++++SUCCESSFUL in ALLOCATION in NodeALloc " );
                
                List<List> alloc = NAR.getm_Resrouces_tb_updated();
//                update_single_node_resrouces(Node_id, alloc,NAR.get_allocation_object());
                re_route= false;
                continue_res_alloc_on_subroutes = true;
//                put_it_in_recordsnodes(Node_id, Link_record_hash, alloc, NAR.get_allocation_object());
//                            put_it_in_records(rots.get(i), Link_record_hash, alloc, NAR.get_allocation_object());
                //return re_route;
                return NAR;
            }
            else {
                Print.println("++++++++++++++++++++++++++++++++++++NO SUCCESS in ALLOCATION in NodeALloc " );
                
                // here the failure is during the allocaitons
                continue_res_alloc_on_subroutes = false;
                re_route = true;
                return NAR;
            }
        }
        else
        {
            //her ethe failure is before getting into the allcoaitons

            Print.println("  ++++++++++++++++++++++++++++++++++++Not enough node resources WITH NODE ID: " + Node_id );
            continue_res_alloc_on_subroutes = false;
            re_route = true;
            return NAR;
        }

        /*
         * this return should be positive, and meaning allocation
         */
//        return NAR;
    }
            

    public Route get_route()
    {
        return this.Route;
    }
    
    
    /*
     * this class updates the allocations over the links of the path
     */        
     public void put_it_in_records(Route sub_route, HashMap linkResources, List<List> alloc, Alg_alloc_object aloc_obj)
     {
         int number_of_hops_call = 0;
         List<String> route_hops = sub_route.return_hops_in_route();
         
//                     Print.println( " \n getstarnbb sub_route  : "+ sub_route.toString());        

         
         for (int i = 0; i < route_hops.size(); i++){                       
            
             
//            Print.println( " \nnumber_of_hops_call  : "+ ++number_of_hops_call);        

             String hop_id = route_hops.get(i);
             Link_Rec_Obj aloc_instance = new Link_Rec_Obj(this.Req_type, 
                    this.Star_ID,this.Con_time ,request.returnDurationTime() , 
                    alloc, request.returnNETREQ().size() - 1, sub_route,hop_id, aloc_obj.get_cell_positions());  
//            Print.println( " \nput ot om records  : "+ alloc.toString());        
            if(Debug)
            {
                Print.println( " VON ID : "+ VON_ID +" -------ADDED-to-Recs L I N K-----------  ");        
                Print.println("  VON ID : "+ VON_ID +" the (sub)route used:  " + sub_route.toString()); 
                Print.println("  VON ID : "+ VON_ID +" hop_id in put in records:  " + hop_id);    
                Print.println("  VON ID : "+ VON_ID +" Request type: " + this.Req_type + " con id: " + this.Star_ID + "\tcon time: " + this.Con_time + "\tlink res.size:" + linkResources.size());
                aloc_instance.display();
            }
//            Print.println(" hop_id  " + hop_id);                        
//            int hop = mNetwork.get_link_num_from_id(hop_id);
//            Print.println("  Link resources " + mNetwork.getmListLinks().get(hop)
//                    .get_ParentClass_resrouces().getme_List_all_layered_resources());    
//            Print.println( " Just allocated and to be recorded : "+ alloc.toString());        
//            this.Node_record_hash.get(nodeid).add_to_node((100* this.Star_ID) + (10*this.Con_ID)+static_IDI++,aloc_instance);
//            this.Node_record_hash.get(nodeid).add_to_node((100* this.Star_ID) + (10*this.Con_ID)+static_IDI++,aloc_instance);
              this.Link_record_hash.get(hop_id).add_to_link((100* this.Star_ID) + (10*this.Con_ID)+static_IDI++,aloc_instance);

//            this.Link_record_hash.get(hop_id).add_to_link(this.Star_ID ,aloc_instance);
         }
     } 
     /*
      * identical of the link recording of the allocation, so later on I can
      * check them for expiry
      */
     public void put_it_in_recordsnodes(int nodeid, 
             HashMap nodeResources,
             List<List> alloc, Alg_alloc_object aloc_obj)
     {
         
            
            Node_Rec_Obj aloc_instance = new Node_Rec_Obj(this.Req_type, 
                    this.Con_ID,this.Con_time ,request.returnDurationTime() , 
                    alloc, request.returnITREQ().size() - 1, nodeid, aloc_obj.get_cell_positions());  
//            Print.println( " put ot om records  : "+ alloc.toString());        
            if(Debug)
            {
                Print.println( " VON ID : "+ VON_ID +" -------ADDED-to-Recs N O D E-----------  ");        
                Print.println("  VON ID : "+ VON_ID +" hop_id in put in records:  " + nodeid);    
                Print.println("  VON ID : "+ VON_ID +" Request type: " + this.Req_type + " con id: "
                        + this.Star_ID + "\tcon time: " + this.Con_time + "\tlink res.size:" + nodeResources.size());
                aloc_instance.display();
            }
//            Print.println(" node id  " + nodeid);            
//            Print.println("  Nodes resources " + mNetwork.getmListNodes().
//                    get(nodeid).get_ParentClass_resrouces().getme_List_all_layered_resources());    
//            Print.println( " Just allocated and to be recorded : "+ alloc.toString());        

            
//            Print.println( " -------ADDED-to-Recs N O D E-----------  " + this.Node_record_hash.get(nodeid));
//            Print.println( " contains?? "+this.Node_record_hash.containsKey(nodeid));
//            Print.println( " aloc_instance.toString() "+aloc_instance.return_noderes_allocresrouces_family().toString());
//            aloc_instance.toString();
            this.Node_record_hash.get(nodeid).add_to_node((100* this.Star_ID) + (10*this.Con_ID)+static_IDI++,aloc_instance);
//            this.Node_record_hash.get(nodeid).add_to_node(this.Con_ID ,aloc_instance);


            
     } 
     
     /*
      * for testing purposes inly
      */
    public List<List> customised_link_res( List<List> ref_list)
    {
        if(Debug)
        Print.println(" make the customisation on the resrouces");
        replace_string_in_list("011", 2,2,ref_list);
        return ref_list;
        
    }
    
     public List<List> replace_string_in_list(String str, int i, int j, List<List> ref_list)
    {
        
        List<String> layer = ref_list.remove(i);
        String ex_cell = layer.remove(j);
        layer.add(j,str);
        ref_list.add(i, layer);
        return ref_list;
    }   


//    
//    public List<List> spread_all(List<String> route_hops)
//    {
//        List<List> cured_resources = new ArrayList<>();
//        for (int i = 0; i < route_hops.size(); i++){                       
//            //Link_Rec_Obj aloc_instance = new Link_Rec_Obj(this.req_type, Con_id,Con_time , Con_duration, linkResources);            
//            String hop_id = route_hops.get(i);
//            int Link_id = mNetwork.get_link_num_from_id(hop_id);
//            Link  n = mNetwork.getmListLinks().get(Link_id);
//            //Print.println("  \n link resrouces beffore spread : " + n.);
//            Cloner cloner=new Cloner();
//            //Link  l_clone = cloner.deepClone(n);
//            Link  l_clone = new Link(n);
//            
//            
//            cured_resources = spread_downwards(l_clone);
//            if(Debug)
//            Print.println("  \nresult of downwards resource spread : " + cured_resources);
//            cured_resources = spread_upwards(l_clone);
//            if(Debug)
//            Print.println("  result of upwards resource spread : " + cured_resources);
//        }
//        return cured_resources;
//    }
//    public List<List> spread_downwards(Link n)
//    {
//         List<List> existing_resources = n.get_ParentClass_resrouces().getme_List_all_layered_resources();
//         List<String> resinfo = n.get_ParentClass_resrouces().getm_resinfo();
//         int depth = existing_resources.size();
//         List<List> com_node_resources = new ArrayList<>();
//         com_node_resources.add(existing_resources.get(0));
////         Print.println("  existing_resources : " + existing_resources.toString());
////         Print.println("  resinfo : " + resinfo.toString());
//         for(int i = 0; i < depth - 1; i++ )
//                 {
//                     List<String> ex_layers_list = existing_resources.get(i);
//                     List<String> ex_next_layers_list = existing_resources.get(i+1);
//                     List<String> new_next_layers_list = new ArrayList<>();
//                     for(int j = 0; j < ex_layers_list.size(); j++ )
//                     {
//                         String ex_cell_str = ex_layers_list.get(j);
////                         Print.println(" ex_cell_str " + ex_cell_str);
//                         String[] splt_ex_cell_str = ex_cell_str.split("");
//                         
//                         // the k starts from one ecause of stupid split method behaviour
//                         //Print.println(" splt_ex_cell_str[ size" + splt_ex_cell_str.length);
//                         for (int k = 1; k < splt_ex_cell_str.length; k++)
//                         {
//                             //Print.println(" splt_ex_cell_str[k] " + splt_ex_cell_str[k]);
//                             String com = "";
//                             if(splt_ex_cell_str[k].matches("1"))
//                             {
//                                 com = ResourceOperations.create_resource_string_ones(resinfo.get(i+1).length());
//                             }
//                             else if(splt_ex_cell_str[k].matches("0"))
//                             {
//                                 
//                                 int Desired_cell_address = j * resinfo.get(i).length() + k - 1;
//                                 //Print.println(" J " + j + " \t [k] " + k + " Desired_cell_address: " + Desired_cell_address);
//                                 String ex_cell_str_next_line = ex_next_layers_list.get(Desired_cell_address);
//                                 com = ex_cell_str_next_line;
//                             }
//                             else
//                             {
//                                 System.exit(99320);
//                             }
//                             new_next_layers_list.add(com);
//                         }
//                     }
////                     Print.println("\ni: " + i );
////                     Print.println("  new_next_layers_list get i " + new_next_layers_list);
////                     Print.println("  existing_resources get i   " + existing_resources.get(i));
//                     com_node_resources.add(new_next_layers_list);
//                     existing_resources.remove(i+1);
////                     Print.println("i: " + i + "  existing_resources " + existing_resources.toString());
//                     existing_resources.add(i+1, new_next_layers_list);
////                     Print.println("i: " + i + "  existing_resources " + existing_resources.toString());
//                 }
//                 
//        return com_node_resources;
//    }       
//    public List<List> spread_upwards(Link n)
//    {
//         List<List> existing_resources = n.get_ParentClass_resrouces().getme_List_all_layered_resources();
//         List<List> com_node_resources = new ArrayList<>();
//         com_node_resources.add(existing_resources.get(existing_resources.size()-1));
//         Print.println(" get me the link ID  " + n.getmID());
//         for(int i = existing_resources.size()-1; i > 0; i-- )
//                 {
//                     List<String> ex_layers_list = existing_resources.get(i);
//                     List<String> ex_previous_layers_list = existing_resources.get(i - 1);
//                     List<String> new_previous_layers_list = existing_resources.get(i - 1);
//                     //HashMap<Integer, String>  bbij  =  new HashMap();
////                     Print.println(" \n\niiii  " + i);
//                     for(int j = 0; j < ex_layers_list.size(); j++ )
//                     {
//                         String ex_cell_str = ex_layers_list.get(j);
//                         String[] splt_ex_cell_str = ex_cell_str.split("");
//                         // the k starts from one ecause of stupid split method behaviour
//                          
//                         for (int k = 1; k < splt_ex_cell_str.length; k++)
//                         {
//                             //Print.println(" \t give me J" + j + " \t\tsplt_ex_cell_str[k] " + splt_ex_cell_str[k]);
//                             
//                             //String com = "";
//                             if(splt_ex_cell_str[k].matches("1"))
//                             {
//                                 int prev_list_lenght = ex_previous_layers_list.get(0).length();
//                                 int prev_cell_position = j/prev_list_lenght;
//                                 //Print.println(" prev_cell_position " + prev_cell_position ); 
//                                 //int prev_char_position = j - prev_cell_position*prev_list_lenght;
//                                 int prev_char_position = j%prev_list_lenght;
//                                 //Print.println(" prev_char_position " + prev_char_position ); 
//
//                                 
//                                 String ex_perv_cell = ex_previous_layers_list.get(prev_cell_position);
//                                 int[] ex_perv_cell_arr = Converters.convert_string_to_1Darray(ex_perv_cell);
//                                 ex_perv_cell_arr[prev_char_position] = 1;
//                                 String new_perv_cell = Converters.convert_array_to_string(ex_perv_cell_arr);
//                                 
//                                 if(Debug){
//                                 Print.println(" ex_previous_layers_list " + ex_previous_layers_list.toString() );
//                                 Print.println(" ex_layers_list " + ex_layers_list.toString() ); 
//                                 Print.println(" new_perv_cell " + new_perv_cell ); 
//                                 Print.println(" prev_list_lenght " + prev_list_lenght + 
//                                         " \t prev_cell_position " + prev_cell_position + 
//                                         " \t prev_char_position: " + prev_char_position);        
//                                 }
//                                 //now replace it 
//                                 new_previous_layers_list.remove(prev_cell_position);
//                                 new_previous_layers_list.add(prev_cell_position, new_perv_cell);
//                                 
//                                 ex_previous_layers_list = new_previous_layers_list;
//                                 existing_resources.remove(i - 1);
//                                 existing_resources.add(i-1, ex_previous_layers_list);
//                                //com = ResourceOperations.create_resource_string_ones(resinfo.get(i-1).length());
//                                // com = com + "1";
////                                 Print.println(" com in case of 1  " + com);
//                                 
//                                 //if you get 1, the whole thing in upper layer is useless
//                                //bbij.put(j / resinfo.get(i-1).length(), com);
//                                //new_previous_layers_list = convert_hash_to_list(bbij);
////                                Print.println(" J " + j + " \t [k] " + k + " new_previous_layers_list: " + new_previous_layers_list);
////                                Print.println("\n\n ");
//                                 //j++;
//                                 
//                             }
//                             else if(splt_ex_cell_str[k].matches("0"))
//                             {
//                                 //j++;
////                                 int Desired_cell_address = j / resinfo.get(i-1).length();
//////                                 Print.println(" J " + j + " \t [k] " + k + " Desired_cell_address: " + Desired_cell_address);
////                                 
////                                 String ex_cell_str_prev_line = ex_previous_layers_list.get(Desired_cell_address);
////                                 com = ex_cell_str_prev_line;
//                             }
//                             else
//                             {
//                                 System.exit(99321);
//                             }
//                             //bbij.put(j / resinfo.get(i-1).length(), com);
//                            // new_previous_layers_list = convert_hash_to_list(bbij);
//                             //rint.println(" B bbij.toString()  " + bbij.toString());
//                             //new_previous_layers_list.add(com);
//                             
//                             
//                         }//Print.println("  bbij.toString()  " + bbij.toString());
//                     }
//
////                     Print.println("\ni: " + i );
////                     Print.println("  new_previous_layers_list get i " + new_previous_layers_list);
////                     Print.println("i: " + i + "  1existing_resources " + existing_resources.toString());                     
//                       com_node_resources.add(new_previous_layers_list);
////                     existing_resources.remove(i-1);
//////                     Print.println("i: " + i + "  2existing_resources " + existing_resources.toString());
////                     existing_resources.add(i-1, new_previous_layers_list);
//                     //Print.println("i - 1: " + (i -1) + "  3existing_resources " + existing_resources.toString());                     
//                     
//                 }
//         
//        //com_link_resources.add(existing_resources.get(existing_resources.size() - 1)); 
//        return existing_resources;         
//        //return com_node_resources;
//    }  
//    
    public List<String> convert_hash_to_list(HashMap<Integer, String> HM)
    {
//        Print.println(" HM.toString(): " + HM.toString());
        List<String> str_list = new ArrayList<>();
        for(int i =0; i < HM.size(); i++ )
        {
            str_list.add(HM.get(i));
        }
        return str_list;
    }
    public List<List> trim_alloc_res(Link l, List<List> new_linkResources)
    {
         //List<List> existing_resources = n.get_ParentClass_resrouces().getme_List_all_layered_resources();
         List<List> com_link_resources = new ArrayList<>();
//         Print.println("  new_linkResources.size: " + new_linkResources.size());
//         Print.println("  request.size: " + request.returnNETREQ().size());
         // go through the layers
                 for(int i = 0; i < new_linkResources.size(); i++ )
                 {
                     //Print.println(" \t  i in tirm is : " + i);
                     List<String> ex_layers_list = new_linkResources.get(i);
//                     Print.println(" \t  ex_layers_list is : " + ex_layers_list.toString());
                     
                     List<String> com_layer_resources = new ArrayList<>();
                     if(i>=request.returnNETREQ().size()-1){
                         com_layer_resources = ex_layers_list;
//                         Print.println(" \t  com_layer_resources is : " + com_layer_resources.toString());
                     }
                     else
                     {
                         
                        for(int j = 0; j < ex_layers_list.size(); j++ )
                        {
                            String ex_cell_str = ex_layers_list.get(j);
                            String com = ResourceOperations.create_resource_string_zeroes(ex_cell_str.length());

                            //here I copy the last row of resrouces, the rest should be zero though
                            if(i>=new_linkResources.size() -1)
                                com  = ex_cell_str;

                            com_layer_resources.add(com);
                        }
                     }
                     com_link_resources.add(com_layer_resources);
                 }
                 
        return com_link_resources;
    }    
    
    /*
     * this method is supposed to get all the link resources, combine them,
     * and the do the allocation, in the sub path
     */
    
//     public void update_link_resrouces(List<String> route_hops, List<List> linkResources, Alg_alloc_object alloc_obj)
     public void update_link_resrouces(List<String> route_hops, Alloc_combinedRes_Link ACL)
     {
         for (int i = 0; i < route_hops.size(); i++){                       
            //Link_Rec_Obj aloc_instance = new Link_Rec_Obj(this.req_type, Con_id,Con_time , Con_duration, linkResources);            
            String hop_id = route_hops.get(i);
            int Link_id = mNetwork.get_link_num_from_id(hop_id);
            Link  l = mNetwork.getmListLinks().get(Link_id);
            if(Debug){
                Print.println( "  VON ID : "+ VON_ID +" -------ADDED-----------  ");        
                Print.println("  VON ID : "+ VON_ID +" hop_id in put in records:   " + hop_id);                        
                //Print.println("  VON ID : "+ VON_ID +" Request type: " + this.req_type + " con id: " + this.Con_id + "\tcon time: " + this.Con_time + "\tlink res.size:" + linkResources.size());
            }
            //int res_size = n.get_ParentClass_resrouces().return_res_number_layers();
//            Print.println("  VON ID :" + VON_ID + " \t th resources before adding up" + n.get_ParentClass_resrouces().getme_List_all_layered_resources().toString());
//            Print.println("  VON ID :" + VON_ID + " \t th to be added with this" + linkResources.toString());
            //List<List>  trimmed_node_resource = trim_alloc_res(n, linkResources);
            List<List>  trimmed_link_resource = ACL.getm_Resrouces_tb_updated();
//            Print.println("  VON ID :" + VON_ID + " \t th trimmed_link_resourcep      " + trimmed_link_resource.toString());
            
            //this method was used when I had a tree, now I use a modified version of it
            //List<List> com_node_resources = combine_with_recs(n, trimmed_node_resource);
//            Print.println("  com_link_resources      " + 
//                    l.get_ParentClass_resrouces().getme_List_all_layered_resources());
//                        
            List<List> com_link_resources = combine_with_recs_out_family(l, trimmed_link_resource);
            
            l.get_ParentClass_resrouces().set_List_all_layered_resources(com_link_resources);
            l.update_stats(Req_type, ACL.get_allocation_object().get_load());
//            Print.println("  VON ID :" + VON_ID + " \t th resources after adding up " + n.get_ParentClass_resrouces().getme_List_all_layered_resources().toString());
            //this.Link_record_hash.get(hop_id).add_to_link(aloc_instance);
         }
     }
     
     /*
      * please note that this is only for  nodes, twin of the link alloc above
      * I can make it to allocate for allocation for any nodes i want perhaps,
      * however teh goal here is to allocate only for the end points
      */
//     public void update_single_node_resrouces(int nodeid, List<List> nodeResources, Alg_alloc_object alloc_obj)i
     public void update_single_node_resrouces(Alloc_combinedRes_Node ACN)
     {
            //Link_Rec_Obj aloc_instance = new Link_Rec_Obj(this.req_type, Con_id,Con_time , Con_duration, nodeResources);            
            Node  n = mNetwork.getNode(ACN.getNodeID());
            if(Debug){
                Print.println( "  VON ID : "+ VON_ID +" -------ADDED-----------  ");        
                Print.println("  VON ID : "+ VON_ID +" hop_id in put in records:   " + ACN.getNodeID());                        
                //Print.println("  VON ID : "+ VON_ID +" Request type: " + this.req_type + " con id: " + this.Con_id + "\tcon time: " + this.Con_time + "\tlink res.size:" + nodeResources.size());
            }

            //I have no idea what I meant!!
            List<List>  trimmed_node_resource = ACN.getm_Resrouces_tb_updated();
//            Print.println("  VON ID :" + VON_ID + " \t th trimmed_node_resource      " + trimmed_node_resource.toString());
                
//            Print.println( "  ---   in com_node_resources  in getstarnet-------  ");        

            List<List> com_node_resources = combine_with_recs_out_family_node(n, trimmed_node_resource);
            
             
//            Print.println("  \ncom_node_resources      " + 
//                    n.get_ParentClass_resrouces().getme_List_all_layered_resources());
//            
            
            
            n.get_ParentClass_resrouces().set_List_all_layered_resources(com_node_resources);
            
            /*
             * the allocaiton stats need to be rebiult
             */
//            n.update_stats(Star_Req_type, alloc_obj.get_load());

         
     }
    
     //public List<List> trim
     /*
      * here the 
      */
     public List<List> combine_with_recs_out_family(Link l, List<List> new_linkResources)
     {
         List<List> existing_resources = l.get_ParentClass_resrouces().getme_List_all_layered_resources();
         List<List> com_link_resources = new ArrayList<>();
         if(Debug)
         {
            Print.println(" in combine existing_resources  " + existing_resources.toString());
            Print.println(" in combine new_linkResources  " + new_linkResources.toString());
         }
         // go through the layers
         
         int[] layer_sized = {existing_resources.size(),new_linkResources.size()};  
         int min_layer = Maths.find_min(layer_sized);
        for(int i = 0; i < min_layer; i++ )
        {
            List<String> ex_layers_list = existing_resources.get(i);
            List<String> new_layers_list = new_linkResources.get(i);
            List<String> com_layer_resources = new ArrayList<>();
            for(int j = 0; j < ex_layers_list.size(); j++ )
            {
                String ex_cell_str = ex_layers_list.get(j);
                String new_cell_str = new_layers_list.get(j);
                int [] ex_cell_arr = Converters.convert_string_to_1Darray(ex_cell_str);
                int [] new_cell_arr = Converters.convert_string_to_1Darray(new_cell_str);
                int delay = 0;
                int []combined  = ResourceOperations.
                        combine_arrays(ex_cell_arr, new_cell_arr, delay);
        //                         Print.print_1D("  the ex_cell_arr in combining procedure to checked out ", ex_cell_arr);
        //                         Print.print_1D("  the new_cell_arr in combining procedure to checked out ", new_cell_arr);

                String com = Converters.convert_array_to_string(combined);
                com_layer_resources.add(com);
            }
            com_link_resources.add(com_layer_resources);
        }
        if(existing_resources.size()>new_linkResources.size())
        {
            for(int i = min_layer; i < existing_resources.size(); i++ )
            {
                com_link_resources.add(existing_resources.get(i));
            }
            
        }
                 
        return com_link_resources;
    }      
     /*
      * I am not sure what the function belwo was supposed to do, while I have the function above?
      * anyway, I made a twin of the above function for node allocs
      */
     
     //public List<List> combine_with_recs(Link n, List<List> new_linkResources)
//    {
//         List<List> existing_resources = n.get_ParentClass_resrouces().getme_List_all_layered_resources();
//         List<List> com_node_resources = new ArrayList<>();
//         
//         // go through the layers
//                 for(int i = 0; i < new_linkResources.size(); i++ )
//                 {
//                     List<String> ex_layers_list = existing_resources.get(i);
//                     List<String> new_layers_list = new_linkResources.get(i);
//                     List<String> com_layer_resources = new ArrayList<>();
//                     for(int j = 0; j < ex_layers_list.size(); j++ )
//                     {
//                         String ex_cell_str = ex_layers_list.get(j);
//                         String new_cell_str = new_layers_list.get(j);
//                         int [] ex_cell_arr = Converters.convert_string_to_1Darray(ex_cell_str);
//                         int [] new_cell_arr = Converters.convert_string_to_1Darray(new_cell_str);
//                         int delay = 0;
//                         int []combined  = ResourceOperations.
//                                 combine_arrays(ex_cell_arr, new_cell_arr, delay);
//                         String com = Converters.convert_array_to_string(combined);
//                         com_layer_resources.add(com);
//                     }
//                     com_node_resources.add(com_layer_resources);
//                 }
//                 
//        return com_node_resources;
//    }
     

     /*
      * the allocation in the node heirarchy,twon of the link method.
      * 
      */
     public List<List> combine_with_recs_out_family_node(Node n, List<List> new_nodeResources)
     {
         List<List> existing_resources = n.get_ParentClass_resrouces().getme_List_all_layered_resources();
         List<List> com_node_resources = new ArrayList<>();
         if(Debug)
         {
            Print.println(" in combine existing_resources  " + existing_resources.toString());
            Print.println(" in combine new_linkResources  " + new_nodeResources.toString());
         }
         // go through the layers
         
         
         int[] layer_sized = {existing_resources.size(),new_nodeResources.size()};  
         int min_layer = Maths.find_min(layer_sized);
        for(int i = 0; i < min_layer; i++ )
        {
            List<String> ex_layers_list = existing_resources.get(i);
            List<String> new_layers_list = new_nodeResources.get(i);
            List<String> com_layer_resources = new ArrayList<>();
            for(int j = 0; j < ex_layers_list.size(); j++ )
            {
                String ex_cell_str = ex_layers_list.get(j);
                String new_cell_str = new_layers_list.get(j);
                int [] ex_cell_arr = Converters.convert_string_to_1Darray(ex_cell_str);
                int [] new_cell_arr = Converters.convert_string_to_1Darray(new_cell_str);
                int delay = 0;
                int []combined  = ResourceOperations.
                        combine_arrays(ex_cell_arr, new_cell_arr, delay);
        //                         Print.print_1D("  the ex_cell_arr in combining procedure to checked out ", ex_cell_arr);
        //                         Print.print_1D("  the new_cell_arr in combining procedure to checked out ", new_cell_arr);

                String com = Converters.convert_array_to_string(combined);
                com_layer_resources.add(com);
            }
            com_node_resources.add(com_layer_resources);
        }
        if(existing_resources.size()>new_nodeResources.size())
        {
            for(int i = min_layer; i < existing_resources.size(); i++ )
            {
                com_node_resources.add(existing_resources.get(i));
            }
            
        }
                 
        return com_node_resources;
    }         
     
     
     
     
     
    /*
     * the function below calcualte the available resoruces in each layer Node only
     */
    private boolean check_for_NET_availabilities(Request_base req, 
            List<List> all_layered_resources_combined)
    {
        List<Integer> availabilities = getm_Agg_link_layers_resources(all_layered_resources_combined);
        List<Integer> net_reqs = req.returnNETREQ();
        boolean go_alloc = true;
        if(Debug)
        Print.println(" IN check_for_NET_availabilities: \nall_layered_resources_combined:  "
                + all_layered_resources_combined.toString());
        for(int i = 0 ; i < req.returnNETREQ().size(); i++)
        {
            if(Debug)
            Print.println(" availabilities.get(i):" + availabilities.get(i) + " \t net_reqs.get(i)" + net_reqs.get(i));
            if(availabilities.get(i)<net_reqs.get(i))
                go_alloc= false;
        }
        return go_alloc;
    }
    
    /*
     * the function below calcualte the available resoruces in each layer
     */
    private boolean check_for_IT_NET_availabilities(Request_base req, 
            List<List> all_link_layered_resources_combined,
            List<List> all_node_layered_resources_combined)
    {
        List<Integer> link_availabilities = getm_Agg_link_layers_resources(all_link_layered_resources_combined);
        List<Integer> node_availabilities = getm_Agg_node_layers_resources(all_node_layered_resources_combined);
        List<Integer> net_reqs = req.returnNETREQ();
        List<Integer> it_reqs = req.returnITREQ();
        boolean go_alloc = true;
        if(Debug)
        {
            Print.println("\nall_link_layered_resources_combined:  "
                    + all_link_layered_resources_combined.toString());
            Print.println("\nall_node_layered_resources_combined:  "
                    + all_node_layered_resources_combined.toString());
            Print.println("\n net_reqs:  "
                    + net_reqs.toString());
            Print.println("\n it_reqs:  "
                    + it_reqs.toString());
        }
        for(int i = 0 ; i < net_reqs.size(); i++)
        {
            if(Debug)
            Print.println(" link availabilities.get(i):" + link_availabilities.get(i) + " \t net_reqs.get(i)" + net_reqs.get(i));
            if(link_availabilities.get(i)<net_reqs.get(i))
                go_alloc= false;
        }
        for(int i = 0 ; i < it_reqs.size(); i++)
        {
            if(Debug)
            Print.println(" node availabilities.get(i):" + node_availabilities.get(i) + " \t net_reqs.get(i)" + it_reqs.get(i));
            if(node_availabilities.get(i)<it_reqs.get(i))
                go_alloc= false;
        }
        return go_alloc;
    }
    private boolean check_for_Node_availabilities(Request_base req, 
            List<List> all_node_layered_resources_combined)
    {
        List<Integer> node_availabilities = getm_Agg_node_layers_resources(all_node_layered_resources_combined);
        List<Integer> net_reqs = req.returnNETREQ();
        List<Integer> it_reqs = req.returnITREQ();
        boolean go_alloc = true;
        if(Debug)
        {
            Print.println("\nall_node_layered_resources_combined:  "
                    + all_node_layered_resources_combined.toString());
            Print.println("\n net_reqs:  "
                    + net_reqs.toString());
            Print.println("\n it_reqs:  "
                    + it_reqs.toString());
            }

        for(int i = 0 ; i < it_reqs.size(); i++)
        {
            if(Debug)
            Print.println(" node availabilities.get(i):" + node_availabilities.get(i) + " \t net_reqs.get(i)" + it_reqs.get(i));
            if(node_availabilities.get(i)<it_reqs.get(i))
                go_alloc= false;
        }
        return go_alloc;
    }
    
    
    
    
    private List<Integer> getm_Agg_link_layers_resources(List<List> List_all_layered_resources)
    {
        //List<List> detailed_res = getm_detailed_resources_();
        List<Integer> agg_availabilities = new ArrayList<>();
        for(int i = 0; i < List_all_layered_resources.size(); i++)
        {
            
            List<String> layer_components_list = List_all_layered_resources.get(i);
            int aggregator = 0;
            for(int j = 0; j < layer_components_list.size(); j++)
            {
                String str = layer_components_list.get(j);
                if(Debug)
                Print.println(" String to be 0counted " + str);
                int zeroes = ResourceOperations.count_zeros(str);
                if(Debug)
                Print.println(" number of zeros counted " + zeroes
                         + " in string :" + str);
                aggregator = aggregator+ zeroes;
            }
            agg_availabilities.add(aggregator);
        }
        return agg_availabilities;
    }       
    private List<Integer> getm_Agg_node_layers_resources(List<List> List_all_layered_resources)
    {
        //List<List> detailed_res = getm_detailed_resources_();
        List<Integer> agg_availabilities = new ArrayList<>();
        for(int i = 0; i < List_all_layered_resources.size(); i++)
        {
            
            List<String> layer_components_list = List_all_layered_resources.get(i);
            int aggregator = 0;
            for(int j = 0; j < layer_components_list.size(); j++)
            {
                String str = layer_components_list.get(j);
                if(Debug)
                Print.println(" String to be 0counted " + str);
                int zeroes = ResourceOperations.count_zeros(str);
                if(Debug)
                Print.println(" number of zeros counted " + zeroes
                         + " in string :" + str);
                aggregator = aggregator+ zeroes;
            }
            agg_availabilities.add(aggregator);
        }
        return agg_availabilities;
    }       
//     public void swipeTheRecords(List<String> route_hops)
//     {
//         
//         for (int i = 0; i < route_hops.size(); i++){
//            this.Link_record_hash.get(route_hops.get(i)).delete_expired_linkres(Con_time);
//         }     
//     }
//     public void swipeAllLinksForTheRecords(List<Link> links)
//     {
//         
//         for (int i = 0; i < links.size(); i++){
//            this.Link_record_hash.get(links.get(i).getmID()).delete_expired_linkres(Con_time);
//         }     
//     }      
     
     public List<Route> route_breaker(Route route)
     {

         int[] path = route.getmPath();
         
         List<Route> sub_routes = new ArrayList();
         String[] route_arr = mNetwork.return_route_links_string_arr(route.return_hops_in_route());
         String[] str_arr;
         //String type = route_arr[0].split("")[0];
         ArrayList<Integer> S_path = new ArrayList();
         if(route_arr.length>0)
         {
            for(int i = 0; i < route_arr.length; i ++)
            {                 
               //Print.println(" i: "+ i + " route_arr[i].split()[1]  " + route_arr[i].split("")[1]);
                //Print.print_1D("  route split " , route_arr[i].split(""));
                if(route_arr[i].split("")[1].matches("A"))
                {             


                   if(S_path.size()>0)
                   { 
                       S_path.add(path[i]);
                       int[] p_arr = Converters.convert_Arr_list_to_int_arr(S_path); 
                       if(Debug)
                       Print.print_1D(" route number  " + sub_routes.size() , p_arr);
                       sub_routes.add(new Route(p_arr));
                       S_path = new ArrayList<>();
                   }
                   int[] p_arr = {path[i],path[i+1]};
                   if(Debug)
                   Print.print_1D(" route number  " + sub_routes.size() , p_arr);
                   sub_routes.add(new Route(p_arr));

                }      
                else if(route_arr[i].split("")[1].matches("S"))
                {

                    S_path.add(path[i]);
                    if(i ==route_arr.length -1)
                    {
                        S_path.add(path[i+1]);
                       int[] p_arr = Converters.convert_Arr_list_to_int_arr(S_path);
                       if(Debug)
                       Print.print_1D(" route number  " + sub_routes.size() , p_arr);
                       sub_routes.add(new Route(p_arr));
                       S_path = new ArrayList<>();                     
                    }
                }
                else
                {
                    Print.println(" problem in route indentification " );
                    System.exit(1134);
                }


            }
        }
         return sub_routes;
     }        
     public void diplay_routes_in_list(List<Route> route_list)
     {
         if(Debug)
         Print.println("  print the broken routes ");
         for(int i = 0; i < route_list.size(); i++)
         {
             Print.println(route_list.get(i).toString());
         }
         
     }
     

    
    public List<List> spread_all_over_combined(List<List> cloned_res, List<String> resinfo)
    {
         

            Cloner cloner=new Cloner();
            List<List> cured_resources = cloner.deepClone(cloned_res);
           
            
            cured_resources = spread_downwards_over_combined(cured_resources, resinfo);
            if(Debug)
            Print.println("  \nresult of downwards resource spread : " + cured_resources);
            cured_resources = spread_upwards_over_combined(cured_resources, resinfo);
            if(Debug)
            Print.println("  result of upwards resource spread : " + cured_resources);
        
        return cured_resources;
    }
    public List<List> spread_downwards_over_combined(List<List> existing_resources, List<String> resinfo)
    {
//        Print.println(" existing_resources: : " + existing_resources.toString());

         int depth = existing_resources.size();
         List<List> com_link_resources = new ArrayList<>();
         com_link_resources.add(existing_resources.get(0));

         for(int i = 0; i < depth - 1; i++ )
                 {
                     List<String> ex_layers_list = existing_resources.get(i);
                     List<String> ex_next_layers_list = existing_resources.get(i+1);
                     List<String> new_next_layers_list = new ArrayList<>();
                     for(int j = 0; j < ex_layers_list.size(); j++ )
                     {
                         String ex_cell_str = ex_layers_list.get(j);
//                         Print.println(" ex_cell_str " + ex_cell_str);
                         String[] splt_ex_cell_str = ex_cell_str.split("");
                         
                         // the k starts from one ecause of stupid split method behaviour
                         //Print.println(" splt_ex_cell_str[ size" + splt_ex_cell_str.length);
                         for (int k = 1; k < splt_ex_cell_str.length; k++)
                         {
                             //Print.println(" splt_ex_cell_str[k] " + splt_ex_cell_str[k]);
                             String com = "";
                             if(splt_ex_cell_str[k].matches("1"))
                             {
                                 com = ResourceOperations.create_resource_string_ones(resinfo.get(i+1).length());
                             }
                             else if(splt_ex_cell_str[k].matches("0"))
                             {
                                 
                                 int Desired_cell_address = j * resinfo.get(i).length() + k - 1;
                                 //Print.println(" J " + j + " \t [k] " + k + " Desired_cell_address: " + Desired_cell_address);
                                 String ex_cell_str_next_line = ex_next_layers_list.get(Desired_cell_address);
                                 com = ex_cell_str_next_line;
                             }
                             else
                             {
                                 System.exit(99320);
                             }
                             new_next_layers_list.add(com);
                         }
                     }
//                     Print.println("\ni: " + i );
//                     Print.println("  new_next_layers_list get i " + new_next_layers_list);
//                     Print.println("  existing_resources get i   " + existing_resources.get(i));
                     com_link_resources.add(new_next_layers_list);
                     existing_resources.remove(i+1);
//                     Print.println("i: " + i + "  existing_resources " + existing_resources.toString());
                     existing_resources.add(i+1, new_next_layers_list);
//                     Print.println("i: " + i + "  existing_resources " + existing_resources.toString());
                 }
                 
        return com_link_resources;
    }       
    public List<List> spread_upwards_over_combined(List<List> existing_resources, List<String> resinfo)
    {
         List<List> com_link_resources = new ArrayList<>();
         com_link_resources.add(existing_resources.get(existing_resources.size()-1));
         for(int i = existing_resources.size()-1; i > 0; i-- )
                 {
                     List<String> ex_layers_list = existing_resources.get(i);
                     List<String> ex_previous_layers_list = existing_resources.get(i - 1);
                     List<String> new_previous_layers_list = existing_resources.get(i - 1);
                     //HashMap<Integer, String>  bbij  =  new HashMap();
//                     Print.println(" \n\niiii  " + i);
                     for(int j = 0; j < ex_layers_list.size(); j++ )
                     {
                         String ex_cell_str = ex_layers_list.get(j);
                         String[] splt_ex_cell_str = ex_cell_str.split("");
                         // the k starts from one ecause of stupid split method behaviour
                          
                         for (int k = 1; k < splt_ex_cell_str.length; k++)
                         {
                             //Print.println(" \t give me J" + j + " \t\tsplt_ex_cell_str[k] " + splt_ex_cell_str[k]);
                             
                             //String com = "";
                             if(splt_ex_cell_str[k].matches("1"))
                             {
                                 int prev_list_lenght = ex_previous_layers_list.get(0).length();
                                 int prev_cell_position = j/prev_list_lenght;
                                 //Print.println(" prev_cell_position " + prev_cell_position ); 
                                 //int prev_char_position = j - prev_cell_position*prev_list_lenght;
                                 int prev_char_position = j%prev_list_lenght;
                                 //Print.println(" prev_char_position " + prev_char_position ); 

                                 
                                 String ex_perv_cell = ex_previous_layers_list.get(prev_cell_position);
                                 int[] ex_perv_cell_arr = Converters.convert_string_to_1Darray(ex_perv_cell);
                                 ex_perv_cell_arr[prev_char_position] = 1;
                                 String new_perv_cell = Converters.convert_array_to_string(ex_perv_cell_arr);
                                 
                                 if(Debug){
                                    Print.println(" ex_previous_layers_list " + ex_previous_layers_list.toString() );
                                    Print.println(" ex_layers_list " + ex_layers_list.toString() ); 
                                    Print.println(" new_perv_cell " + new_perv_cell ); 
                                    Print.println(" prev_list_lenght " + prev_list_lenght + 
                                            " \t prev_cell_position " + prev_cell_position + 
                                            " \t prev_char_position: " + prev_char_position);        
                                 }
                                 //now replace it 
                                 new_previous_layers_list.remove(prev_cell_position);
                                 new_previous_layers_list.add(prev_cell_position, new_perv_cell);
                                 
                                 ex_previous_layers_list = new_previous_layers_list;
                                 existing_resources.remove(i - 1);
                                 existing_resources.add(i-1, ex_previous_layers_list);

                             }
                             else if(splt_ex_cell_str[k].matches("0"))
                             {
                                 //j++;
//                                 int Desired_cell_address = j / resinfo.get(i-1).length();
////                                 Print.println(" J " + j + " \t [k] " + k + " Desired_cell_address: " + Desired_cell_address);
//                                 
//                                 String ex_cell_str_prev_line = ex_previous_layers_list.get(Desired_cell_address);
//                                 com = ex_cell_str_prev_line;
                             }
                             else
                             {
                                 System.exit(99321);
                             }

                         }//Print.println("  bbij.toString()  " + bbij.toString());
                     }
    
                       com_link_resources.add(new_previous_layers_list);
              
                     
                 }
         
        //com_link_resources.add(existing_resources.get(existing_resources.size() - 1)); 
        return existing_resources;         
        //return com_node_resources;
    }       
     
    /*
     * I have link alloc pack, which has the allocaiton sets for the sub routes.
     * and then there is a node allocation object
     */
    public void updateNodeLinkAllocsSet(LinkAllocPacked LAP, List<Alloc_combinedRes_Node> LACN){
        
        
        update_link_resrouces(LAP.getSubRoute().return_hops_in_route(),
                LAP.getAlloc_combinedRes_Link());
        put_it_in_records(LAP.getSubRoute(), Link_record_hash, LAP.getAlloc_combinedRes_Link().getm_Resrouces_tb_updated() ,
                LAP.getAlloc_combinedRes_Link().get_allocation_object());
                        
//        Print.println(" ++++++++++++++++++++++++++++++++ ALLOCATED Route "+ LAP.getSubRoute().return_hops_in_route().toString()
//                +  " RESOURCES --> " +   LAP.getAlloc_combinedRes_Link().getm_Resrouces_tb_updated().toString());

//             Print.println(" LinkAllocPacked.size " + LAP.getAlloc_combinedRes_Link(). );
        
        
                        
        int number_of_node_calls = 0;
//        Print.println(" LACN size " + LACN.size() ); 
         
        for(Alloc_combinedRes_Node ACN: LACN)        
        {
//             Print.println(" updateNodeLinkAllocsSet :ACN.getm_Resrouces_tb_updated() " + 
//                     ACN.getm_Resrouces_tb_updated().toString());            
            
//             Print.println(" ACN.getNodeID() " + 
//                     ACN.getNodeID());
//             Print.println(" number_of_node_calls) " + 
//                     number_of_node_calls);
//             Print.println(" ++++++++++++++++++++++++++++++++ ALLOCATED NODE "+ ACN.getNodeID() +  " RESOURCES --> " + 
//                     ACN.getm_Resrouces_tb_updated().toString());
            update_single_node_resrouces(ACN);
            put_it_in_recordsnodes(ACN.getNodeID(),
                    Node_record_hash, 
                    ACN.getm_Resrouces_tb_updated(),
                    ACN.get_allocation_object());
            
            
//            
//             Print.println(" test in ACN.getNodeID() " + 
//                     Node_record_hash.get(ACN.getNodeID()).);
        }
        

        
        
        
    }
    
    public Route getCurrentRoute()
    {
        return currentRoute;
    }
    public void update_linkreroute_hash(HashMap<String, Integer> hm)
    {
            if(!hm.containsKey(Req_type))
            {
                //int cap = addmission_per_req_type_hash_link13.get(req_type);
                hm.put(Req_type, 1);
            }
            else
            {
                int existing_b = hm.get(Req_type);
                hm.put(Req_type, existing_b + 1);

            }         
    }
    public void update_nodereroute_hash(HashMap<String, Integer> hm)
    {
            if(!hm.containsKey(Req_type))
            {
                //int cap = addmission_per_req_type_hash_link13.get(req_type);
                hm.put(Req_type, 1);
            }
            else
            {
                int existing_b = hm.get(Req_type);
                hm.put(Req_type, existing_b + 1);

            }         
    }
    
    
//    public HashMap<Integer,Rec_Node_Allocs> return_NodeHash()
//    {
//        return Node_record_hash; 
//    }
//    
//    public HashMap<String,Rec_Link_Allocs> return_LinkHash()
//    {
//        return Link_record_hash; 
//    }
    public void show_records (String message)
    {
//            Print.println(" <<<<<<<<< Show records in getsstarnet  -->  ");
    
        String [] keys_arr = Converters.key_set_obj_to_str_array(Link_record_hash);
        for(int i  = 0; i < keys_arr.length; i ++)
        {
//            Print.println(" <<<<<<<<< Link id  -->  "+ keys_arr[i] );
            Rec_Link_Allocs link_rec  = Link_record_hash.get(keys_arr[i]);
            link_rec.printResHash();
//            P.println(" <<<<<<<<< link fuckers  -->  "+ link_rec.return_all_layer_util_of_link_allocs().toString() ); 
                        
        }
        int [] keys_arr_node = Converters.key_set_obj_to_int_array(Node_record_hash);
            
        
//        Print.println(" <<<<<<<<< Record_hash_node.size() -->  "+ Node_record_hash.size());
        
        
        for(int i  = 0; i < keys_arr_node.length; i ++)
        {
            Print.println(" <<<<<<<<< node id  -->  "+ keys_arr_node[i] );
            Rec_Node_Allocs node_rec  = Node_record_hash.get(keys_arr_node[i]);
            node_rec.printResHash();
//            P.println(" <<<<<<<<< node fuckers  -->  "+ node_rec.return_all_layer_util_of_node_allocs().toString() ); 
            
//            node_rec.printResHash();
        }
        

    }    
}
