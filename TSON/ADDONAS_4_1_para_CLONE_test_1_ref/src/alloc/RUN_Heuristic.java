/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package alloc;

import Infrastructure.*;
import Record.Link_Rec_Obj;
import Record.Node_Rec_Obj;
import Record.Rec_Link_Allocs;
import Record.Rec_Node_Allocs;
import RequestTypes.Req_BW;
import RequestTypes.Req_IT_NET_timed;
import RequestTypes.Request_base;
import RequestTypes.Request_base;
import RequestTypes.Req_IT_NET_timed_startnet;
import alloc.GetRoute;
import assignments.Assignment_alg_return;
import assignments.Assignment_base;
import com.mysql.jdbc.exceptions.DeadlockTimeoutRollbackMarker;
//import com.sun.org.apache.bcel.internal.generic.AALOAD;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Set;
import routing.Route;
import routing.RoutingTable;
import statistics.Stats_collector;
import utilities.Print;
import utilities.Converters;

/**
 *
 * @author Bijan
 */
public class RUN_Heuristic extends Heuristic_base{
    
    String Route_select;
    int Spec_time_map;
    int SubL_max;
    int SubL_min;
    int Lambda_min;
    int Lambda_max;
    int Flex_min;      
    int Flex_max;  
    static int Blocked;
    static int Allowed;
    
    int Added_Link_Con;
    int Deleted_Link_Con;
    
    //int Con_duration;
    Print P = new Print();
    Converters  C; 
    int Con_time;
    int Con_id;
    boolean Debug;
    int VON_ID;
    HashMap<String,Rec_Link_Allocs> Record_hash_link;
    HashMap<Integer,Rec_Node_Allocs> Record_hash_node;
    String Req_type;
    boolean STARNET;
//    Request_base Request;
    Request_base Request;
    Stats_collector SC;
    /*
     * in constructure the class is made witht its corresponding network in it
     */
    public RUN_Heuristic(boolean debug, int VON_id, Network mNetwork, 
            int numRoutes, Stats_collector sc, HashMap<String,
                    Rec_Link_Allocs> link_record_hash, 
            HashMap<Integer,Rec_Node_Allocs> node_record_hash, 
            int spec_time_map, int subl_min, int subl_max, int lambda_min,
            int lambda_max, int flex_max, int flex_min, String route_select,
            boolean Starnet) {
        this.STARNET = Starnet;
        this.mNetwork = mNetwork;
        this.mNumKRoutes = numRoutes;
        this.P = new Print();
        this.C = new Converters();
        this.VON_ID = VON_id;
        this.Blocked = 0;
        this.Allowed  = 0;
        //this.Con_duration  = duration;
        this.Debug = debug;
        this.Record_hash_link = link_record_hash;
        this.Record_hash_node = node_record_hash;
        init_recordList();
        //req_type = "";
        this.Lambda_min = lambda_min;
        this.Lambda_max = lambda_max;
        this.SubL_max = subl_max;
        this.SubL_min = subl_min;
        this.Flex_max = flex_max;
        this.Flex_min = flex_min;
        
        
        
        
        this.Spec_time_map = spec_time_map;    
        this.Route_select = route_select;
        this.SC = sc;
//        P.println("  VON ID : "+ VON_ID +" link resources:::: &&&*************************************  ");
//
//        show_all_link_resources(mNetwork);
        
    }

    /*
     * this function is the gate, gets the list of reqs, so to process them
     */
    public void init_process_request(List<Request_base> req_list, int con_id, int time)
    {
        List nodes_list = mNetwork.getmListNodes();
        //List req_list = req_list;
        int num_reqs = req_list.size();
        
        this.Con_time = time;
        this.Con_id = con_id;
        
        this.Request = req_list.get(Con_id);
        //P.println("num_reqs: " + num_reqs );

        this.Req_type = this.Request.returnConnectionReqType();
        P.println("\n\n  VON ID : "+ VON_ID +" &&&******RUN_Heuristic  ************  ");
        P.println("  VON ID : "+ VON_ID +" &&&*************************************  ");
        P.println("  VON ID : "+ VON_ID +" &&&****  REQUEST NUMBER : " + this.Con_id+ " ******  ");

        if(!STARNET)
        {
            P.println("  VON ID : "+ VON_ID +" &&&****  SRC  : " + this.Request.returnSrc()+ " ******  ");
            P.println("  VON ID : "+ VON_ID +" &&&****  DEST  : " + this.Request.returnsDest()+ " ******  ");
        }
        if(STARNET)
        {
            Req_IT_NET_timed_startnet tempreq = (Req_IT_NET_timed_startnet)Request;
            P.println("  VON ID : "+ VON_ID +" &&&****  SRC&DEST  : " + tempreq.returnSrcdest().toString()+ " ******  ");            
        }
        P.println("  VON ID : "+ VON_ID +" &&&****  REQ TIME : " + this.Con_time+ " ******  ");        
        P.println("  VON ID : "+ VON_ID +" &&&****  REQ TYPE : " + this.Req_type+ " ******  ");        
        P.println("  VON ID : "+ VON_ID +" &&&****  EXP TIME : " +
                Request.returnStopTime() + " ******  ");        
        P.println("  VON ID : "+ VON_ID +" &&&*************************************  ");
//        P.println("  VON ID : "+ VON_ID +" link resources:::: &&&*************************************  ");
//
//        show_all_link_resources(mNetwork);
        if(Debug)
        this.Request.displayAttributes();
        

        //if((Request_base)req_list.get(this.Con_id)!= null)
        if(this.Con_id<req_list.size())
        {
            if(STARNET)
            {
//                start_process_request_Star(nodes_list,(Req_IT_NET_timed_startnet)req_list.get(this.Con_id));
            }
            else
            {
                start_process_request_GR(nodes_list,(Request_base)req_list.get(this.Con_id));
            }
        
        }
        else 
        {
            P.println("  VON ID : "+ VON_ID +" **** No requests left in the list!! cleaning the records **** ");
            swipeAllTheRecords();
            //swipeAllLinksForTheRecords(mNetwork.getmListLinks());
        }
    }
    public void fin()
    {
        show_all_link_resources(mNetwork);

        P.println("  VON ID : "+ VON_ID +" &&&******FINISHING THIS INSTANCE ****************  ");
        P.println("  VON ID : "+ VON_ID +" &&&****  BLOCKED: " + this.Blocked+ " * + ALLOWED: " 
                + this.Allowed+"*****  ");
        P.println("  VON ID : "+ VON_ID +" &&&**********************************************  ");
    
    }
    public void cleanUpTheRecords()
    {

        swipeAllTheRecords();
    
    }
    
    /*
     * there are two lists, one for nodes, one for links
     */
    public void init_recordList()
    {
        
        
        
        List<Link> link_list = mNetwork.getmListLinks();
        List<Node> node_list = mNetwork.getmListNodes();
        
        //for each link, make a recording object
        for(int i = 0; i < link_list.size(); i++)
        {
            Record_hash_link.put(link_list.get(i).getmID(), new Rec_Link_Allocs
                    (Debug,link_list.get(i).getmID(), link_list.get(i).getmNumber(), 
                    link_list.get(i).get_ParentClass_resrouces().getm_resinfo()));
        }
        //and for each node
        for(int i = 0; i < node_list.size(); i++)
        {
            Record_hash_node.put(node_list.get(i).getmID(), new Rec_Node_Allocs
                    (Debug, node_list.get(i).getmID(), 
                    node_list.get(i).get_ParentClass_resrouces().getm_resinfo()));
        }
    }
        
    public void start_process_request_GR(List nodes_list ,Request_base req)
    {
                Print.println(" ITREQ  " + req.returnITREQ());
                int src = req.returnSrc();
                int dest = req.returnsDest();
                List  requested_network_resource =  req.returnNETREQ();          
                int request_dimension = requested_network_resource.size();
                String req_algorithm_type = req.returnAlgorithmType();  
                //this.Req_type = req.returnConnectionReqType();
                if(Debug)
                P.println("  VON ID : ****** "+ VON_ID +" \t Start_process_request req number " + Con_id + 
                        "  with request category " + Req_type  + "\n\tsrc : " + src
                        + "\tdest : " + dest + "  req has  "+ request_dimension+
                        " layers" + "\t and needs req_ALG_type: " + req_algorithm_type + "\n*****");
                
                //get the node information for retreiving routing table 
                Node node = (Node) nodes_list.get(src);
                if(Debug){
                    P.println("  VON ID : "+ VON_ID +" routes avaialble for node " + node.getmID());
                    node.getmRouting().print_routes();
                }

                P.println(" +++++++++++++++++++++++++++++++++++++++++: " );
                //if(Debug)
                //show_records();
                //P.println(" +++++++++++++++++++++++++++++++++++++++++: " );
                
                
                //I changed my code, so prior to getting to the routing, I clear up 
                //the resource database, rather than clearing a a selected route
                //because my routing now is based on availabilities, not only the
                // number of the paths
                swipeAllTheRecords();
                GetRoute getroute = new GetRoute(Debug, mNetwork, node, req, VON_ID, Con_time, Con_id, 
                        Record_hash_link,
                        Record_hash_node,
                        Spec_time_map, 
                        SubL_min,SubL_max,Lambda_min,Lambda_max, Flex_max, Flex_min,
                        Route_select);
                
                int number_route_to_try = mNumKRoutes;
                
                //node resource allocation should happen in here
                /*
                 * the run_routing function bascailly, starts a chain of actions
                 * finding a route, which can be differnet algorithms
                 * then 
                 */
                boolean what_happened = getroute.run_routing(number_route_to_try, this.Blocked,this.Allowed);
                
                
                if(what_happened)
                {
                    this.Allowed++;
                    this.SC.add_to_admitted_list_link_type(req);
                    this.SC.add_to_route_list(getroute.get_route());
                    this.SC.add_to_route_list_per_req(getroute.get_route(), Req_type);
                    P.println(" +++++++++++++++++++++++++++++++++++++++++: " );
                }
                else
                {
                    this.Blocked++;
                    this.SC.add_to_blocking_list_link_type(req);
                }
                
                //P.println(" <<<<<<<<<<<<<<<< the run_routing is done: this is what happened: "  + what_happened);
                //P.println(" <<<<<<<<<<<<< plus the detailed resoruces of the network");
                //show_all_link_resources(mNetwork);
                //this.SC.demonstrate_linkRec_objects();
                
                

                     
    } 
//    public void start_process_request_Star(List nodes_list ,Req_IT_NET_timed_startnet reqs)
//    {
//        Print.println(" Star Net not GR  ");
//        
//        //create a copy of the network
//        Network tempnet = new Network(mNetwork);
//        
//        tempnet.createNetworkGraph();
//        tempnet.create_link_hash();
//        HashMap tmp_recnode = clone_nodeHashRecords(Record_hash_node);
//        HashMap tmp_reclink = clone_linkHashRecords(Record_hash_link);
////        HashMap tmp_recnode = (Record_hash_node);
////        HashMap tmp_reclink = (Record_hash_link);
//        Print.println("  \n****************  Process the star request  ********************" + ++Allowed);
//        Print.println("  ****************  Temp network resources/node and link hashes created  ********************" + ++Allowed);        
////        Print.println(" tempnet node sizes  " + tempnet.getmListNodes().size());
////        Print.println(" mNetwork node sizes  " + mNetwork.getmListNodes().size());
////        Print.println(" show the records ");
//
////        show_all_node_resources(mNetwork);
////        show_records();
//        boolean totalwhat_happened = true;
//        Print.println("  ****************  request permuataions created, now processing each  ********************" + ++Allowed);
//        List<Req_IT_NET_timed> createSingleReqPermutes = createSingleReqPermutes(reqs);
//        for(int i = 0 ; i <createSingleReqPermutes.size(); i ++)
//        {
//                Req_IT_NET_timed  RINT  = createSingleReqPermutes.get(i);
//                Print.println(" ITNETREQ  " + RINT.returnITREQ());
//                
////                int src = RINT.returnSrc();
////                int dest = RINT.returnsDest();
////                List  requested_network_resource =  RINT.returnNETREQ();          
////                int request_dimension = requested_network_resource.size();
////                String req_algorithm_type = RINT.returnAlgorithmType();  
////                //this.Req_type = req.returnConnectionReqType();
////                //if(Debug)
////                P.println("  VON ID : ****** "+ VON_ID +" \t Start_process_request req number " + Con_id + 
////                        "  with request category " + Req_type  + "\n\tsrc : " + src
////                        + "\tdest : " + dest + "  req has  "+ request_dimension+
////                        " layers" + "\t and needs req_ALG_type: " + req_algorithm_type + "\n*****");
////                
//                //get the node information for retreiving routing table 
//                
//                int src = RINT.returnSrc();
//                Node node = (Node) nodes_list.get(src);
//                if(Debug){
//                    P.println("  VON ID : "+ VON_ID +" routes avaialble for node " + node.getmID());
//                    node.getmRouting().print_routes();
//                }
//
//                //if(Debug)
//                //show_records();
//                //P.println(" +++++++++++++++++++++++++++++++++++++++++: " );
//                
//                
//                //I changed my code, so prior to getting to the routing, I clear up 
//                //the resource database, rather than clearing a a selected route
//                //because my routing now is based on availabilities, not only the
//                // number of the paths
//                swipeAllTheRecords();
//                Print.println("  ****************  databases cleaned, no allocation for the request  ********************" + ++Allowed);
//                GetStarNet getstar = new GetStarNet(Debug, tempnet, node, RINT, VON_ID, Con_time, Con_id, 
//                        tmp_reclink,
//                        tmp_recnode,
//                        Spec_time_map, 
//                        SubL_min,SubL_max,Lambda_min,Lambda_max, Flex_max, Flex_min,
//                        Route_select);
//                
//                int number_route_to_try = mNumKRoutes;
//                
//                //node resource allocation should happen in here
//                /*
//                 * the run_routing function bascailly, starts a chain of actions
//                 * finding a route, which can be differnet algorithms
//                 * then 
//                 */
//                Print.println("  ****************  Allocation finished  ********************" + ++Allowed);
//                
//                boolean what_happened = getstar.run_routing(number_route_to_try, this.Blocked,this.Allowed);
//                if(!what_happened)
//                {
//                    totalwhat_happened = false;
//                    Print.println("  ****************  Star Allocation will fail as one of the links failed  ********************" + ++Allowed);
//                    
//                    break;
//                }
//                
//       }        
//        
//        if(totalwhat_happened)
//        {
//            P.println(" +++++++++++++++++++ Allowed ++++++++++++++++++++++: " );
//            P.println(" +++++++++++++++++++ update the databases ++++++++++++++++++++++: " );
//            
////         show_all_node_resources(tempnet);
////         show_all_link_resources(tempnet);
//           
//            mNetwork = tempnet;
//            Record_hash_link = tmp_reclink;
//            Record_hash_node = tmp_recnode;
//            this.Allowed++;
////            this.SC.add_to_admitted_list_link_type(req);
////            this.SC.add_to_route_list(getroute.get_route());
////            this.SC.add_to_route_list_per_req(getstar.get_route(), Req_type);
//            P.println(" +++++++++++++++++++ Request done ++++++++++++++++++++++: " );
//        }
//        else
//        {
//            this.Blocked++;
////            this.SC.add_to_blocking_list_link_type(RINT);
//            P.println(" +++++++++++++++++++ Blocked ++++++++++++++++++++++: " );
//            P.println(" +++++++++++++++++++ Request done ++++++++++++++++++++++: " );
//            
//        }
//
//        //P.println(" <<<<<<<<<<<<<<<< the run_routing is done: this is what happened: "  + what_happened);
//        //P.println(" <<<<<<<<<<<<< plus the detailed resoruces of the network");
//        //show_all_link_resources(mNetwork);
//        //this.SC.demonstrate_linkRec_objects();
//
//        
//
//                     
//    } 
     public void swipeAllTheRecords()
     {
         
         for (int i = 0; i < mNetwork.getmListLinks().size(); i++){
             Link l = mNetwork.getmListLinks().get(i);
             String hop_id = l.getmID();
            this.Record_hash_link.get(hop_id).delete_expired_linkres(mNetwork,Con_time);
            
         } 
         
        //and for each node
         
        for(int i = 0; i < mNetwork.getmListNodes().size(); i++)
        {
             Node n = mNetwork.getmListNodes().get(i);
             int node_id = n.getmID();
            this.Record_hash_node.get(node_id).delete_expired_noderes(mNetwork,Con_time);

        }         
         
     }    
    
    /*
     * I have the hash map of all the links with the recording objects.
     * Get all those objects.
     * inside each object, then I have the allocated objetes,
     * each alloc aobject contains the res list....
     */
    public void show_records ()
    {
        String [] keys_arr = Converters.key_set_obj_to_str_array(Record_hash_link);
        for(int i  = 0; i < keys_arr.length; i ++)
        {
            P.println(" <<<<<<<<< Link id  -->  "+ keys_arr[i] );
            Rec_Link_Allocs link_rec  = Record_hash_link.get(keys_arr[i]);
            link_rec.printResHash();
        }
        String [] keys_arr_node = Converters.key_set_obj_to_str_array(Record_hash_node);
            
        
        P.println(" <<<<<<<<< Record_hash_node.size() -->  "+ Record_hash_node.size());
        
        
        for(int i  = 0; i < keys_arr_node.length; i ++)
        {
            P.println(" <<<<<<<<< node id  -->  "+ keys_arr_node[i] );
            Rec_Node_Allocs node_rec  = Record_hash_node.get(keys_arr_node[i]);
            node_rec.printResHash();
        }
        

    }
    
//     private boolean process_and_algcall(Route route,String[] req_type_split,
//             List requests,List<Boolean> Delays)
//     {
//         
//        List links = mNetwork.getmListLinks();
//        HashMap link_adj = mNetwork.return_link_str2num_hash_list();
//        List<String> hops = route.return_hops_in_route();
//        int cumulative_delay =0;
//        int num_hops = hops.size();
//        P.println("  VON ID : "+ VON_ID +" number of hops in FF link: " + num_hops);
//        
//        for (int i = 0; i < num_hops; i++){
//            
//            int link_number = (int) link_adj.get(hops.get(i));
//             
//            Link link = (Link) links.get(link_number);
//            //int delay = 8;
//            P.println("  VON ID : "+ VON_ID +" link.getmLength: " + link.getmLength());
//            int delay = (int) Double.parseDouble(link.getmLength());
//            P.println("  VON ID : "+ VON_ID +" link delay: " + delay);
//            HashMap<Integer,String> resources = link.get_resrouces();
//            P.println("  VON ID : "+ VON_ID +" link resources size: " + resources.size());
//            
//            //now process differnet layers
//            for(int u = 0; u < req_type_split.length;u++)
//            {
//                P.println("  VON ID : "+ VON_ID +" ^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^  ");
//                String res_str = resources.get("D"+u);
//                P.println("  VON ID : "+ VON_ID +" resoruce string retrived " + res_str + "  with number D" + u);
//                int [] IntArray_resoruce = C.convert_string_to_1Darray(res_str);
//                int value  = (int) requests.get(u);
//                Boolean if_delay = Delays.get(u);
//                if(!if_delay){delay=0;}
//                P.println("  VON ID : "+ VON_ID +" req_type " + req_type_split[u] );
//                String alg_type=  req_type_split[u];
//                if(alg_type.matches("S"))
//                {
//                    P.println("  VON ID : "+ VON_ID +"  the dimensions layer:" + u+ "   value:" + value 
//                            +"   delay: " + delay);
//                    P.print_1D("  VON ID : "+ VON_ID +"    IntArray_resoruce: ", IntArray_resoruce);
//                    
//                    run_FFC_assignment(value, IntArray_resoruce , delay);
//                }
//                else if(alg_type.matches("M"))
//                {
//                    P.println("  VON ID : "+ VON_ID +"  the dimensions layer:" + u+ "   value:" + value 
//                            +"   delay: " + delay);
//                    P.print_1D("  VON ID : "+ VON_ID +"   IntArray_resoruce: ", IntArray_resoruce);                   
//                    run_FF_assignment(value, IntArray_resoruce , delay);                   
//                }
//                else 
//                {
//                    P.println("  VON ID : "+ VON_ID +"  no algorithm   selected.. so exit");
//                    System.exit(553);
//                }
//            }
//        }
//       return true;
//     }
//    
     
     
     /*
      * final bit of allocation
      */
     
//     private boolean assignTheAllocation(Route route,
//             List<int[]> allocations_across_layers,
//             List<Boolean> Delays_if_apply)
//     {
//         
//        List links = mNetwork.getmListLinks();
//        HashMap link_adj = mNetwork.return_link_str2num_hash_list();
//        List<String> route_hops = route.return_hops_in_route();
//        int num_hops = route_hops.size();
//        HashMap<String,int[]> linkResourcesRecord = new HashMap<>();
//               
//        for (int i = 0; i < num_hops; i++){
//            
//            int link_number = (int) link_adj.get(route_hops.get(i));
//            
//            Link link = (Link) links.get(link_number);
//            
//            
//            int delay = (int) Double.parseDouble(link.getmLength());
//            //P.println(" link delay: " + delay);
//            HashMap<String,String> resources = link.get_resrouces();
//            //P.println(" link resources size: " + resources.size());
//            
//            HashMap<String,String> linkResources = resources;
//            //now process differnet layers
//            //for(int u =0; u<allocations_across_layers.size();u++)
//            for(int u =0; u<allocations_across_layers.size() && 
//                    !(u>link.get_resrouces_dimension());u++)
//            {
//                
//                int [] res= allocations_across_layers.get(u);
//                int applied_delay = delay;
//                
//                if (Delays_if_apply.get(u)==false){
//                    applied_delay = 0;
//                }
//                
//                int[] array_shifted_res = shift_allocation(res, applied_delay);
//
//                String old_res = resources.get("D"+u);
//                
//
//                int [] existing_alloc = C.convert_string_to_1Darray(old_res);
//                
//                int[] allocted_plus_previous = allocated_add_existing(existing_alloc, array_shifted_res);
//                                
//                
//                String new_res = C.convert_array_to_string(res);
//                String new_res_shifted = C.convert_array_to_string(array_shifted_res);
//                String string_final_res = C.convert_array_to_string(allocted_plus_previous);
//                P.println("  VON ID : "+ VON_ID +  "  Con ID : "+ Con_id +" \n%assignTheAllocation%  allocations in hop   :"
//                        + "" + i + "\twith linkId:  " + link.getmID() + "  allocations in layer   :"+u 
//                        +" \n \t Link old_res---> BEFORE :         " + old_res+ ""
//                        +" \n \t Link final_res---> FINAL:         " + string_final_res);
//                linkResources.put("D"+u, string_final_res);
//                linkResourcesRecord.put("D"+u, array_shifted_res);
//                
//            }
//            
//            link.set_resrouces(linkResources);
//        }
//        //put_it_in_records(route_hops, linkResourcesRecord);
//        return true;
//     }
     
//     public void put_it_in_records(List<String> route_hops, HashMap linkResources)
//     {
//         for (int i = 0; i < route_hops.size(); i++){                       
//            Link_Rec_Obj aloc_instance = new Link_Rec_Obj(this.req_type, Con_id,Con_time , Con_duration, linkResources);            
//            String hop_id = route_hops.get(i);
//            if(Debug){
//                P.println( "  VON ID : "+ VON_ID +" -------ADDED-----------  ");        
//                P.println("  VON ID : "+ VON_ID +" hop_id in put in records:  " + hop_id);                        
//                P.println("  VON ID : "+ VON_ID +" Request type: " + this.req_type + " con id: " + this.Con_id + "\tcon time: " + this.Con_time + "\tlink res.size:" + linkResources.size());
//            }
//            this.Record_hash_link.get(hop_id).add_to_link(aloc_instance);
//         }
//     }
     
    
     
     public int [] allocated_add_existing(int [] existing_array, int [] new_array)
     {
         int [] final_array = new int [existing_array.length];
         int i = 0;
         while((i )<existing_array.length)
         {
             final_array[i] = existing_array[i] + new_array[i];
             i++;
         }
         
         return final_array;
     }


     
     public int [] shift_allocation(int [] allocation, int delay)
     {
         int [] shifted_array = new int [allocation.length];
         int i = 0;
         while((i + delay)<allocation.length)
         {
             shifted_array[i + delay] = allocation[i];
             i++;
         }
         
         return shifted_array;
     }
     
     
     /* badly defined method
      * This method called after the start process, and since there exists a route
      * for the asked connection, it can process the request
      * 
      * this method basiclly considres the hop delays and so in the next step,
      * and calls a synch allocation method. thi smeans, the route proces should 
      * over aggregated links availablitiles,
      * it wont be over separate links
      *
      */
//     private boolean process_combine_algcall(Route route,String[] req_type_split,
//             List requests,List<Boolean> Delays_if_apply)
//     {
//         
//        List links = mNetwork.getmListLinks();
//        HashMap link_adj = mNetwork.return_link_str2num_hash_list();
//        List<String> hops = route.return_hops_in_route();
//        
//        //get the delays values per hop
//        int cumulative_delay =0;
//        List<Integer> Delays_per_hop = new ArrayList();
//       // P.println("  VON ID : "+ VON_ID +" in process_combine_algcall route.getmLength: " + route.getmLength());
//        for (int i = 0; i < route.getmLength(); i++){
//            
//            int link_number = (int) link_adj.get(hops.get(i));
//             
//            Link link = (Link) links.get(link_number);
//            //int delay = 8;
//            //P.println("  VON ID : "+ VON_ID +" in process_combine_algcall link.getmLength: " + link.getmLength());
//            int delay = (int) Double.parseDouble(link.getmLength());
//            if(i>0){
//                cumulative_delay = + delay;
//                Delays_per_hop.add(cumulative_delay);            
//            }
//            else
//            {
//                //for the firs hop, no delays are considered
//                Delays_per_hop.add(0);
//            }
//
//        }
//        //now I all the synchronous allocation of the resources
//         return use_alg_synch(route, req_type_split, requests, link_adj, Delays_per_hop, Delays_if_apply);
//
//     }
     
     /*
      * using this method, first links availabilities are aggregated on a route
      * whcih is set of a whole process, to convert layeres of resrouces in differnet
      * links to be summed up
      */
     
//     public boolean use_alg_synch(Route route, String[] req_type_split,List requests,
//             HashMap link_adj,List<Integer> Delays_per_hop,
//             List<Boolean> Delays_if_apply)
//     {
//
//         
//        ResourceOperations ro = new ResourceOperations(Debug, mNetwork,link_adj);
//        List<int []> combined_resoruces_in_layers = ro.route_link_resource_combiner
//                (route,req_type_split.length,Delays_per_hop,Delays_if_apply);
//
//        
//        List<int[]> allocs = new ArrayList();
//         //here, I reiterate calling the algortihms, for different layers
//        //so I have the combined and aggregated tables for each layer
//        
//        //Here, I am usign the maximum layers I can have in my nodes
//        int [] allocation = null;
//        for(int u = 0; u < req_type_split.length;u++)
//        {
//
//
//            // get the layers resrouces in form of array
//            int [] IntArray_resoruce = (int[]) combined_resoruces_in_layers.get(u);
//            
//            // get the slots per layer request
//            int value  = (int) requests.get(u);
//            
//            //get the algortihm type to be deployed
//            String alg_type=  req_type_split[u];
//
//
//            //deal with the delays that you nned to introduce for alllcaition
//            //this has been considered in funding the slots already
//            //bear in mind, if one link, you have no delays, so the
//            //object is empty
//            int tempdelay =0;
//            
//            //please note, in synchronous assignmnet, we dont need to
//            // consider the delays. However, to actually record the allocation,
//            // I need to consider the delay.  
//
//            allocation = new int[IntArray_resoruce.length];
//
//
//            //allocation = run_FFC_assignment(value, IntArray_resoruce , tempdelay);
//            
//            if(alg_type.matches("S"))
//            {
//
//                allocation = run_FFC_assignment(value, IntArray_resoruce , tempdelay);
//            }
//            else if(alg_type.matches("M"))
//            {
//                
//                allocation = run_FF_assignment(value, IntArray_resoruce , tempdelay);                   
//            }
//            else 
//            {
//                P.println("  VON ID : "+ VON_ID +"  Warning no algorithm   selected.. so exit");
//                System.exit(553);
//            }
//        
//            if(allocation== null)
//            {
//                P.println("  VON ID : "+ VON_ID +"  unsuccessful allocation so BLOCKED ");
//                return false;
//            }
//            else{
//
//                allocs.add(allocation);
//            }
//        }
//        assignTheAllocation(route, allocs, Delays_if_apply);
//        return true;
//    }    
     /*
      * using this method, first links availabilities are aggregated on a route
      * whcih is set of a whole process, to convert layeres of resrouces in differnet
      * links to be summed up
      */
     
//     public boolean approach_to_allocations(Route route, String[] req_type_split,List requests,
//             HashMap link_adj,List<Integer> Delays_per_hop,
//             List<Boolean> Delays_if_apply)
//     {
//
//         
//        ResourceOperations ro = new ResourceOperations(Debug, mNetwork,link_adj);
//        List<int []> combined_resoruces_in_layers = ro.route_link_resource_combiner
//                (route,req_type_split.length,Delays_per_hop,Delays_if_apply);
//
//        
//        List<int[]> allocs = new ArrayList();
//         //here, I reiterate calling the algortihms, for different layers
//        //so I have the combined and aggregated tables for each layer
//        
//        //Here, I am usign the maximum layers I can have in my nodes
//        int [] allocation = null;
//        for(int u = 0; u < req_type_split.length;u++)
//        {
//
//
//            // get the layers resrouces in form of array
//            int [] IntArray_resoruce = (int[]) combined_resoruces_in_layers.get(u);
//            
//            // get the slots per layer request
//            int value  = (int) requests.get(u);
//            
//            //get the algortihm type to be deployed
//            String alg_type=  req_type_split[u];
//
//
//            //deal with the delays that you nned to introduce for alllcaition
//            //this has been considered in funding the slots already
//            //bear in mind, if one link, you have no delays, so the
//            //object is empty
//            int tempdelay =0;
//            
//            //please note, in synchronous assignmnet, we dont need to
//            // consider the delays. However, to actually record the allocation,
//            // I need to consider the delay.  
//
//            allocation = new int[IntArray_resoruce.length];
//
//
//            //allocation = run_FFC_assignment(value, IntArray_resoruce , tempdelay);
//            
//            if(alg_type.matches("S"))
//            {
//
//                allocation = run_FFC_assignment(value, IntArray_resoruce , tempdelay);
//            }
//            else if(alg_type.matches("M"))
//            {
//                
//                allocation = run_FF_assignment(value, IntArray_resoruce , tempdelay);                   
//            }
//            else 
//            {
//                P.println("  VON ID : "+ VON_ID +"  Warning no algorithm   selected.. so exit");
//                System.exit(553);
//            }
//        
//            if(allocation== null)
//            {
//                P.println("  VON ID : "+ VON_ID +"  unsuccessful allocation so BLOCKED ");
//                return false;
//            }
//            else{
//
//                allocs.add(allocation);
//            }
//        }
//        assignTheAllocation(route, allocs, Delays_if_apply);
//        return true;
//    }    

 
    public int[] run_FF_assignment(int First_value,int[] First_array ,int First_delay) 
    {
        int[] assignmnet = new int[First_array.length];
        int load = First_value;
        ArrayList D0_slots = new ArrayList();
        int [] temp_arr = new int[First_array.length];
        
        //for(int i = First_delay; i < First_array.length; i++)
        int i = First_delay;
        if(Debug){
            P.println("  VON ID : "+ VON_ID +"First_dealy = i " 
                    + First_delay);  
            P.println("  VON ID : "+ VON_ID +"First_array.length:" 
                + First_array.length);  
        }
        while(load>0 && (i <First_array.length))
        {
                //P.println("  load: " + load + "\t i: " + i+ "\tassignmnet[i]: " + assignmnet[i]);            
                if(First_array[i]==0)
                {
                    temp_arr[i] = 1;
                    assignmnet[i] = 1;
                    //P.println("  load: " + load + "\t i: " + i + "\tassignmnet[i]: " + assignmnet[i]);
                    i++;
                    load--;

                }
                else 
                {
                    i++;
                }
        }
        if(load>0)
        {
           assignmnet = null;
           return assignmnet;
        }
        else{
            if(Debug){
                P.print_1D("  VON ID : "+ VON_ID +" FF_2D the input First_array: ", First_array);
                P.print_1D("  VON ID : "+ VON_ID +" FF_2D the result assignmnet: ", assignmnet);
            }
            return assignmnet; 
        }
    } 
    
    
    public int[] run_FFC_assignment(int First_value,int[] First_array ,int First_delay) 
    {
        int[] assignmnet = new int[First_array.length];
        int load = First_value;
        boolean assigned = false;
        ArrayList D0_slots = new ArrayList();
        int [] temp_arr = new int[First_array.length];
        int counter = 0;
        int startSlot = -1;
        
        int i = First_delay;
        if(Debug){
        P.println("  VON ID : "+ VON_ID +"  First_delay:" +First_delay+  "\tFirst_array.length:" 
                + First_array.length);   
        }
        while(load>0 && (i <First_array.length))
        {
            /*
             * P.println("\ni is:" +i + "    in FFC, give the counter:"
             * + counter + "\t and the load requestd:" + load);
             */
                if(First_array[i]==0)
                {
                    counter++;
                    if(counter>=load)
                    {
                        for(int h = i - counter + 1; h < i + 1; h++)
                        {
                           //P.println("\nh is:" +h);
                           assignmnet[h]= 1; 
                        }
                        assigned = true;
                        break;
                    }
                    
                }
                else if(First_array[i]==1)
                {
                    counter = 0;
                }
                i++;

        }
        if(assigned== false)
        {
            assignmnet = null;
            if(Debug)
            P.println("  VON ID : "+ VON_ID +" FFC assignmnet: " + assignmnet);
            return assignmnet;                    
        }
        else{
            if(Debug){
                P.println("  VON ID : "+ VON_ID +" FFC the input First_array.length: " + First_array.length);
                P.print_1D("  VON ID : "+ VON_ID +" FFC the input First_array: ", First_array);
                P.println("  VON ID : "+ VON_ID +" FFC the result assignmnet.length: " +  assignmnet.length);
                P.print_1D("  VON ID : "+ VON_ID +" FFC the result assignmnet: ", assignmnet);
            }
            return assignmnet; 
        }
    }  
 
    public void show_all_link_resources(Network n)
    {
        List<Link> links = n.getmListLinks();
        List<Integer> added_aggs = new ArrayList<>();
        for(int i =0; i < links.size(); i++)
        {
            Link l = links.get(i);
            P.println("  \nVON ID : "+ VON_ID +" fin, show link resources " + l.getmID() + "\ttype:" + l.getmType());
            l.show_resrouces(VON_ID);
            //List<Integer> link_agg =   l.get_ParentClass_resrouces().getm_used_agg_layers_resources();
            
        
        }
        
    }
    public void show_all_node_resources(Network n)
    {
        List<Node> nodes = n.getmListNodes();
        List<Integer> added_aggs = new ArrayList<>();
        for(int i =0; i < nodes.size(); i++)
        {
            Node l = nodes.get(i);
            P.println("  \nVON ID : "+ VON_ID +" fin, show link resources " + l.getmID() + "\ttype:" + l.getmNode_Type());
            l.show_resrouces(VON_ID);
            //List<Integer> link_agg =   l.get_ParentClass_resrouces().getm_used_agg_layers_resources();
            
        
        }
        
    }

    public List<Req_IT_NET_timed> createSingleReqPermutes(Req_IT_NET_timed_startnet starreq)
    {
//        List<PermutationSrcDest> per = new ArrayList<>();
        List<Integer> l = starreq.returnSrcdest();
        List<Req_IT_NET_timed> singlereqa = new ArrayList<>();
        for (int i = 0; i < l.size(); i++)
        {
            
            for (int j = 0; j < l.size(); j++)
            {
                if(l.get(i) != l.get(j))
                {
//                    PermutationSrcDest psd  = new PermutationSrcDest(l.get(i), l.get(j));
                    //the permutations made ar edirectly filling up the request sets4
                    
                    P.println("  HERE IS THE PROBLEM WITH WRONG IDs");
                    Req_IT_NET_timed RIT = new Req_IT_NET_timed
                            (starreq,starreq.returnId(),l.get(i), l.get(j));
                    singlereqa.add(RIT); 
                }
            }
            
        }
        return singlereqa;
    }
   
    
    public HashMap<Integer, Rec_Node_Allocs> clone_nodeHashRecords(HashMap<Integer, Rec_Node_Allocs> hm)
    {
        HashMap clone = new HashMap();
        Set<Integer> keySet = hm.keySet();
        Object[] toArray = keySet.toArray();
        int [] intarr = new int [toArray.length];
        for (int i = 0; i < intarr.length; i++){
            intarr[i] = (int) toArray[i];                
        }
        
        //cloning is happening here
        for(int i :intarr)
        {
            clone.put(i,
                    new Rec_Node_Allocs(hm.get(i)));
        }
        return hm;
    }
 
    public HashMap<String, Rec_Link_Allocs> clone_linkHashRecords(HashMap<String, Rec_Link_Allocs> hm)
    {
        HashMap clone = new HashMap();
        Set<String> keySet = hm.keySet();
        Object[] toArray = keySet.toArray();
        String [] intarr = new String [toArray.length];
        for (int i = 0; i < intarr.length; i++){
            intarr[i] = (String) toArray[i];                
        }
        
        //cloning is happening here
        for(String i :intarr)
        {
            clone.put(i,
                    new Rec_Link_Allocs(hm.get(i)));
        }
        return hm;
    }
 
}
