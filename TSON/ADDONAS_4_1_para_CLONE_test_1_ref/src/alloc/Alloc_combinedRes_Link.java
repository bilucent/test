/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package alloc;

import Infrastructure.ResourceOperations;
import RequestTypes.Request_base;
import java.util.ArrayList;
import java.util.List;
import resources.Parent;
import utilities.Converters;
import utilities.Print;

/**
 *
 * @author Bijan
 */
public class Alloc_combinedRes_Link {
    
    private boolean Debug;
    private List<String> Link_res_info;
    private List<String> Node_res_info;
    
    private List<List> Link_resources;
//    private List<List> Node_resources;
    
    private List<List> alloc_Link_resources;
    private List<List> alloc_Node_resources;
//    private Req_NET_timed Req;
    private Request_base Req;
    
    private List<Integer> NET_req_list;
    private List<Integer> IT_req_list;
    
    private Alg_alloc_object currecnt_al;
    private boolean success = false;
    
    int subl_min,subl_max, lambda_min, lambda_max,  flex_max, flex_min;
    
    
    
    
    
    public Alloc_combinedRes_Link(boolean debug, 
            List<List> resources, Request_base req,List<String> res_info)
    {
        this.Debug = debug;
        this.Link_res_info = new ArrayList<>();
        this.Link_res_info = res_info;
        this.Link_resources = new ArrayList<>();
        this.Link_resources = resources;
        this.Req = req;
        this.NET_req_list = new ArrayList<>();
        this.NET_req_list = req.returnNETREQ();
        this.IT_req_list = new ArrayList<>();
        this.IT_req_list = req.returnITREQ();
        
        //run_all_T();
        
       
        
        //Print.println("  in new ass class alloc res:\n " + alloc_Link_resources.toString() + "\n");
    }
    public Alloc_combinedRes_Link(boolean debug,  Request_base req,
            List<List> link_resources,List<String> link_res_info,
//            List<List> node_resources,List<String> node_res_info,
            int time_spec_map, 
            int s_min,int s_max,int l_min,int l_max, int f_max,int f_min
            )
    {
        

        this.Debug = debug;
        this.Link_res_info = link_res_info;
//        this.Node_res_info = node_res_info;
        this.Link_resources = link_resources;
//        this.Node_resources = node_resources;
        this.Req = req;
        this.NET_req_list = req.returnNETREQ();
//        this.IT_req_list = req.returnITREQ();

        
        /*
         * I need the following block to define when I want to 
         * put constraints or when I want the whole rane to be used
         */
        
        
//            Print.println("  Link_res_infoLink_res_info " + convert_resInfo_to_resCap(Link_res_info).toString() );
        if(Debug)
        {
            Print.println("  subl_max " + s_max + "  subl_min " + s_min );
            Print.println("  lambda_max " + l_max + "  lambda_min " + l_min );
            Print.println("  flex_max " + f_max + "  flex_min " + f_min ); 
            Print.println(" the limits are modifed in link alloc combined ");
        }
        int layer_of_operation = NET_req_list.size() - 1;
//        int resLayerLenght =  Parent.returnResourcesInTreeLayers(link_res_info, layer_of_operation);
        this.subl_max = (s_max ==-1) ? convert_resInfo_to_resCap(Link_res_info).get(1) : s_max;
        this.lambda_max = (l_max ==-1) ? convert_resInfo_to_resCap(Link_res_info).get(1) : l_max;
        this.flex_max = (f_max ==-1) ? convert_resInfo_to_resCap(Link_res_info).get(1) : f_max;
        this.subl_min = (s_min ==-1) ? 0 : s_min;
        this.lambda_min = (l_min ==-1) ? 0 : l_min;
        this.flex_min = (f_min ==-1) ? 0 : f_min;
        if(Debug)
        {
            Print.println("  subl_max " + subl_max + "  subl_min " + subl_min );
            Print.println("  lambda_max " + lambda_max + "  lambda_min " + lambda_min );
            Print.println("  flex_max " + flex_max + "  flex_min " + flex_min );
        }

        Grid_or_Gridless(time_spec_map,  
                subl_min, subl_max, lambda_min, lambda_max,  flex_max, flex_min);
        //run_all_T();       
        
        //Print.println("  in new ass class alloc res:\n " + alloc_Link_resources.toString() + "\n");
    }    
    public void Grid_or_Gridless(int time_spec_map, 
            int subl_min,int subl_max,int lambda_min,int lambda_max, int flex_max,int flex_min)
    {
        
        
        if(Req.returnConnectionReqType()=="aa")
        {
//            Print.println("  subl_max " + subl_max + "  subl_min " + subl_min );

            run_subw_Grid_(time_spec_map, subl_min, subl_max );
        }
        else if(Req.returnConnectionReqType()=="bb"||Req.returnConnectionReqType()=="cc")
        {
            run_lambda_Grid( lambda_min, lambda_max);
        }        
        else if(Req.returnConnectionReqType()=="dd"||Req.returnConnectionReqType()=="ee")
        {
            run_Gridless( flex_max, flex_min);
        }
                
        else
        {
            Print.println(" problem in finding an algorithm: " + Req.returnConnectionReqType());
            System.exit(1414);
        }

    }
    
    /*
     * in thi sallocations are based on serialising all the layers list memebrs
     * in to a single array and working on them(slower)
     */
//    private void run(){
//       
//        int layer_of_operation = NET_req_list.size() - 1;
//        int num_alloc_slots = NET_req_list.get(layer_of_operation);
//        
//        int load = num_alloc_slots;
//        List<String> layer_res = Link_resources.get(layer_of_operation);
//        Print.println("  layer_res in new assign " + layer_res.toString() );
//        List<String> new_res_list =  make_string_list_zeros(layer_res);
//        
//        String all_concat = ResourceOperations.list_to_string(layer_res);
//        int [] all_concat_arr = Converters.convert_string_to_1Darray(all_concat);
//        
//        int delay = 0;
//        this.currecnt_al = new Alg_alloc_object
//                (load, all_concat_arr, all_concat_arr.length, delay);
//        boolean yes = run_M_assignment(currecnt_al);       
//
//        all_concat_arr = currecnt_al.get_result_assignmnet();
//        String str = Converters.convert_array_to_string(all_concat_arr);
//        Print.println("  alloc str " + str + "\tlayer_of_operation: " + layer_of_operation);
//        List<String> new_layer_res =string_to_list(str, Link_res_info.get(layer_of_operation).length());
//        alloc_Link_resources = reconstruct_allocs_with_layer(new_layer_res,layer_of_operation);
//        Print.println("  alloc_Link_resources " + alloc_Link_resources.toString());
//        
//        
//    }
      /*
     * in this one, I allocate per each list mamber, in case there are enough
     * resources (faster)
     * 
     * here, I have spec_time_map arg, which basically says for howmany times shopuld
     * I iterate the allocations on the requested number of consequential cells,
     * 
     */
    private boolean run_subw_Grid_(int spec_time_map, 
            int subl_min,int subl_max){
        
        
        ///
        Tech_behaviour Tech = new Tech_behaviour(Link_res_info);
//        Tech.TTT();
//        Print.println(" ------------  ");        
//        Tech.NTT();
//        Print.println(" ------------  ");                
//        Tech.TNT();
//        Print.println(" ------------  ");                
//        Tech.NNT();
//        Print.println(" ------------  ");                                       
//        Tech.TTN();
//        Print.println(" ------------  ");         
//        Tech.NTN();
//        Print.println(" ------------  ");                
//        Tech.TNN();
//        Print.println(" ------------  ");   
//        Tech.NNN();
//        Print.println(" ------------  ");              
//        ///
       
        int layer_of_operation = NET_req_list.size() - 1;
        int req_num_alloc_slots = NET_req_list.get(layer_of_operation);
        
        int load = req_num_alloc_slots;
        List<String> layer_res = Link_resources.get(layer_of_operation);
//        Print.println("  **** in ALOOC_COMBINED_RES_LINK " );
//        Print.println("  load to alloc " + req_num_alloc_slots);
//        Print.println("  layer_res in new assign " + layer_res.toString() );
        //List<String> new_res_list =  make_string_list_zeros(layer_res);
        
        //these lists are used to store the used cell information
        List<String> updated_cells = new ArrayList<>();
        List<Integer> updated_cells_position = new ArrayList<>();     
        List<Integer> cell_positions = new ArrayList<>();
        
        //Print.println("  the spec allocation maximum size " + layer_res.size() );
        //Print.println("  layer resourcs before assignmnet " + layer_res);
        
        
        //for(int i = 0; i < layer_res.size() ; i++)    
        //
        /*
         * why I am applying these min max ruleshere? because I want to bring the 
         * effect o flimited resrouces in the previous layer (spectrum) to this
         * layer, but this should not be the normal approach
         * 
         * the spec_time_map is there for the same reason. I inserted this time spec thingi
         * to reflect the effect of allocating time slices over Grid wavelength.
         * this means, that, if the wvaelength takes 4 spec slices, I shold allocate
         * on 4 of them.
         */
        
//        for(int i = subl_min; i < subl_max && (i+spec_time_map < layer_res.size()); i+=spec_time_map)
        for(int i = subl_min; i < subl_max ; i+=spec_time_map)
        {

            String ex_cell = layer_res.get(i);
            int available_in_cell = ResourceOperations.count_zeros(ex_cell);

            if(available_in_cell>0&& load!=0)
            {      
                if(Debug){
                    Print.println("  load to be accomodated " + load);
                    Print.println("  spec_time_map" + spec_time_map);
                    Print.println("  load of layer_res " + layer_res);
                    }
                int initial_load = load;
                int counter = 0;
                while(counter<spec_time_map)
                {
                    load = initial_load;
                    
                    int [] cell_arr = Converters.convert_string_to_1Darray(ex_cell);
                    //Print.print_1D(" the cell array " , cell_arr);
                    int delay = 0;
                    this.currecnt_al = new Alg_alloc_object
                            (load, cell_arr, cell_arr.length, delay);

                    List<String> alg_type_list = Req.get_NET_ALG_types();
                    if(alg_type_list.get(layer_of_operation).matches("S"))
                    {                
                        boolean yes = run_S_assignment(currecnt_al);
                        load  = load - currecnt_al.get_load();
                        cell_positions.add(i+counter);
                        if(Debug)
                        Print.println("  after running S algorithm in new ass remaining load  is: " + load);

                    }
                    else if(alg_type_list.get(layer_of_operation).matches("M"))
                    {                
                        boolean yes = run_M_assignment(currecnt_al);    
                        load  = load - currecnt_al.get_load();
                        cell_positions.add(i+counter);
                        if(Debug)
                        Print.println("  after running M algorithm in new ass remaining load  is: " + load);
                    }                
                    cell_arr = currecnt_al.get_result_assignmnet();
                    String new_cell = Converters.convert_array_to_string(cell_arr);  
                    //Print.println("  new_cell " + new_cell);
                    updated_cells.add(new_cell);
                    updated_cells_position.add(i+counter);
                    counter++;
                }

            }
                           
            if(load==0)
            {
                this.success = true;

                this.currecnt_al.set_cell_positions(cell_positions);
                    //this method is used for making one cell update in a lyer
                //alloc_Link_resources = reconstruct_allocs_with_cell(layer_res, new_cell, layer_of_operation, i);

                    //this method is used for making multiple cell updates in a lyer
                alloc_Link_resources = reconstruct_allocs_with_cell_list(layer_res, updated_cells, updated_cells_position, layer_of_operation);
//                Print.println("  >>>>>>>>>>>>>>>>the allocs seems to be successful with remaing load 0 :  " + alloc_Link_resources);
                return this.success;
            }
        }
        /*
         * it seems like the program never reaches here, in fact, if there are no
         * resrouces, it would not call this method to begin with
         */
        //Print.println("  >>>>>>>>>>>>>>>>Here I return the succes status, which should be fail actually reacing to this point:  "  );
        
        return  this.success;
      
        
    } 
      /*
     * in this one, I allocate per each list mamber, in case there are enough
     * resources (faster)
     */
    /*
     * in this one, I allocate per each list mamber, in case there are enough
     * resources (faster)
     */
    private boolean run_lambda_Grid(
            int lambda_min,int lambda_max){
        
        ///
//        Tech_behaviour Tech = new Tech_behaviour(Link_res_info);
//        Tech.NNT();
//        Tech.NTT();
//        Tech.TTT();
//        Tech.TNT();
        
        ///
       
        int layer_of_operation = NET_req_list.size() - 1;
        int req_num_alloc_slots = NET_req_list.get(layer_of_operation);
        
        int load = req_num_alloc_slots;
        List<String> layer_res = Link_resources.get(layer_of_operation);
        if(Debug)
        Print.println("  layer_res in new assign " + layer_res.toString() );
        //List<String> new_res_list =  make_string_list_zeros(layer_res);
        
        //these lists are used to store the used cell information
        List<String> updated_cells = new ArrayList<>();
        List<Integer> updated_cells_position = new ArrayList<>();     
        List<Integer> cell_positions = new ArrayList<>();
        //Print.println("  the spec allocation maximum size " + layer_res.size() );
        
        
        //beware, the layers stuff, has to deal with the resources in thils layer,
        //but encapsulated in lists reflecting the previous layer.
        // so I dont need to do any modification at this stage to make changes 
        //to allocaiton in this layer.
        for(int i = 0; i < layer_res.size() ; i++)
        //for(int i = lambda_min; i < lambda_max ; i++)
        //for(int i = layer_res.size() -1 ; i >= gridless_max_cell_access ; i--)
        {

            String ex_cell = layer_res.get(i);
            int available_in_cell = ResourceOperations.count_zeros(ex_cell);

            if(available_in_cell>0&& load!=0)
            {                
                int [] cell_arr = Converters.convert_string_to_1Darray(ex_cell);
                //Print.print_1D(" the cell array " , cell_arr);
                int delay = 0;
                this.currecnt_al = new Alg_alloc_object
                        (load, cell_arr, cell_arr.length, delay);
                
                List<String> alg_type_list = Req.get_NET_ALG_types();
                if(alg_type_list.get(layer_of_operation).matches("S"))
                {                
                    boolean yes = run_S_assignment(currecnt_al);
                    load  = load - currecnt_al.get_load();
                    cell_positions.add(i);
                    if(Debug)
                    Print.println("  after running S algorithm in new ass remaining load  is: " + load);

                }
                else if(alg_type_list.get(layer_of_operation).matches("M"))
                {                
                    boolean yes = run_M_assignment_lambda_grid(currecnt_al, lambda_min, lambda_max);    
                    load  = load - currecnt_al.get_load();
                    cell_positions.add(i);
                    if(Debug)
                    Print.println("  after running M algorithm in new ass remaining load  is: " + load);
                }                
                cell_arr = currecnt_al.get_result_assignmnet();
                String new_cell = Converters.convert_array_to_string(cell_arr);  
                //Print.println("  new_cell " + new_cell);
//                Print.println("\n  " );
                updated_cells.add(new_cell);
                updated_cells_position.add(i);
                
            }
                           
            if(load==0)
            {
                this.success = true;

                this.currecnt_al.set_cell_positions(cell_positions);

                alloc_Link_resources = reconstruct_allocs_with_cell_list(layer_res, updated_cells, updated_cells_position, layer_of_operation);
                return this.success;
            }
        }
        /*
         * it seems like the program never reaches here, in fact, if there are no
         * resrouces, it would not call this method to begin with
         */
        //Print.println("  >>>>>>>>>>>>>>>>Here I return the succes status, which should be fail actually reacing to this point:  "  );
        
        return  this.success;
        
    }        
        
     /*
     * in this one, I allocate per each list mamber, in case there are enough
     * resources (faster)
     */
    private boolean run_Gridless(int flex_max,int flex_min){
        
        ///
//        Tech_behaviour Tech = new Tech_behaviour(Link_res_info);
//        Tech.NNT();
//        Tech.NTT();
//        Tech.TTT();
//        Tech.TNT();
        
        ///
       
        int layer_of_operation = NET_req_list.size() - 1;
        int req_num_alloc_slots = NET_req_list.get(layer_of_operation);
        
        int load = req_num_alloc_slots;
        List<String> layer_res = Link_resources.get(layer_of_operation);
        //Print.println("  layer_res in new assign for flexgrid" + layer_res.toString() );
        //List<String> new_res_list =  make_string_list_zeros(layer_res);
        
        //these lists are used to store the used cell information
        List<String> updated_cells = new ArrayList<>();
        List<Integer> updated_cells_position = new ArrayList<>();     
        List<Integer> cell_positions = new ArrayList<>();
        
        /*
         * the flex grid was supposed to go the other way... to make 
         * prioritised shared and dedicated zones....
         * however, here we are dealing with high level(prev layer) allocations,
         * meaning we not have to play with the lists, but inside of them
         */
//       for(int i = layer_res.size() - 1; i >= 0 ; i++)
//        for(int i = flex_max - 1; i >= flex_min ; i--)
        for(int i = 0; i < layer_res.size() ; i++)            
        {

            String ex_cell = layer_res.get(i);
            int available_in_cell = ResourceOperations.count_zeros(ex_cell);
            //Print.println("  ex_cell " + ex_cell + "\t with availability of " + available_in_cell + " \t cell  number i " + i + " \tload " + load);
            //if(get_me_cell_availabilities_inlayer(layer_res, i)>= load)

            ///if(available_in_cell>=load && load!=0)
            
            
            
            if(available_in_cell>0&& load!=0)
            {                
                int [] cell_arr = Converters.convert_string_to_1Darray(ex_cell);
                //Print.print_1D(" the cell array " , cell_arr);
                int delay = 0;
                this.currecnt_al = new Alg_alloc_object
                        (load, cell_arr, cell_arr.length, delay);
                
                List<String> alg_type_list = Req.get_NET_ALG_types();
                if(alg_type_list.get(layer_of_operation).matches("S"))
                {                
                    boolean yes = run_S_assignment(currecnt_al);
                    load  = load - currecnt_al.get_load();
                    cell_positions.add(i);
                    if(Debug)
                    Print.println("  after running S algorithm in new ass remaining load  is: " + load);

                }
                else if(alg_type_list.get(layer_of_operation).matches("M"))
                {                
                    boolean yes = run_M_assignment_gridless(currecnt_al,flex_max,flex_min);    
                    load  = load - currecnt_al.get_load();
                    cell_positions.add(i);
                    if(Debug)
                    Print.println("  after running M algorithm in new ass remaining load  is: " + load);
                }                
                cell_arr = currecnt_al.get_result_assignmnet();
                String new_cell = Converters.convert_array_to_string(cell_arr);  
                //Print.println("  new_cell " + new_cell);
//                Print.println("\n  " );
                updated_cells.add(new_cell);
                updated_cells_position.add(i);
                

                ///load = 0;

                //Print.println("  alloc_Link_resources " + alloc_Link_resources.toString());

                //break;
                
            }
                           
            if(load==0)
            {
                this.success = true;

                this.currecnt_al.set_cell_positions(cell_positions);
                    //this method is used for making one cell update in a lyer
                //alloc_Link_resources = reconstruct_allocs_with_cell(layer_res, new_cell, layer_of_operation, i);

                    //this method is used for making multiple cell updates in a lyer
                alloc_Link_resources = reconstruct_allocs_with_cell_list(layer_res, updated_cells, updated_cells_position, layer_of_operation);
                 return this.success;
            }
        }
        /*
         * it seems like the program never reaches here, in fact, if there are no
         * resrouces, it would not call this method to begin with
         */
        //Print.println("  >>>>>>>>>>>>>>>>Here I return the succes status, which should be fail actually reacing to this point:  "  );
        
        return  this.success;
        
    }        
    
    private void run_all_T(){
        
        ///
        Tech_behaviour Tech = new Tech_behaviour(Link_res_info);
        Tech.NNT();
        Tech.NTT();
        Tech.TTT();
        Tech.TNT();
        
        ///
       
        int layer_of_operation = NET_req_list.size() - 1;
        int req_num_alloc_slots = NET_req_list.get(layer_of_operation);
        
        int load = req_num_alloc_slots;
        List<String> layer_res = Link_resources.get(layer_of_operation);
        //Print.println("  layer_res in new assign " + layer_res.toString() );
        //List<String> new_res_list =  make_string_list_zeros(layer_res);
        
        //these lists are used to store the used cell information
        List<String> updated_cells = new ArrayList<>();
        List<Integer> updated_cells_position = new ArrayList<>();     
        List<Integer> cell_positions = new ArrayList<>();
        for(int i = 0; i < layer_res.size() ; i++)
        {

            String ex_cell = layer_res.get(i);
            int available_in_cell = ResourceOperations.count_zeros(ex_cell);
            //Print.println("  ex_cell " + ex_cell + "\t with availability of " + available_in_cell + " \t cell  number i " + i + " \tload " + load);
            //if(get_me_cell_availabilities_inlayer(layer_res, i)>= load)

            ///if(available_in_cell>=load && load!=0)
            
            
            
            if(available_in_cell>0&& load!=0)
            {                
                int [] cell_arr = Converters.convert_string_to_1Darray(ex_cell);
                //Print.print_1D(" the cell array " , cell_arr);
                int delay = 0;
                this.currecnt_al = new Alg_alloc_object
                        (load, cell_arr, cell_arr.length, delay);
                
                List<String> alg_type_list = Req.get_NET_ALG_types();
                if(alg_type_list.get(layer_of_operation).matches("S"))
                {                
                    boolean yes = run_S_assignment(currecnt_al);
                    load  = load - currecnt_al.get_load();
                    cell_positions.add(i);
                    if(Debug)
                    Print.println("  after running S algorithm in new ass remaining load  is: " + load);

                }
                else if(alg_type_list.get(layer_of_operation).matches("M"))
                {                
                    boolean yes = run_M_assignment(currecnt_al);    
                    load  = load - currecnt_al.get_load();
                    cell_positions.add(i);
                    if(Debug)
                    Print.println("  after running M algorithm in new ass remaining load  is: " + load);
                }                
                cell_arr = currecnt_al.get_result_assignmnet();
                String new_cell = Converters.convert_array_to_string(cell_arr);  
                //Print.println("  new_cell " + new_cell);
//                Print.println("\n  " );
                updated_cells.add(new_cell);
                updated_cells_position.add(i);
                

                ///load = 0;

                //Print.println("  alloc_Link_resources " + alloc_Link_resources.toString());

                //break;
                
            }
                           
            if(load==0)
            {
                this.success = true;

                this.currecnt_al.set_cell_positions(cell_positions);
                    //this method is used for making one cell update in a lyer
                //alloc_Link_resources = reconstruct_allocs_with_cell(layer_res, new_cell, layer_of_operation, i);

                    //this method is used for making multiple cell updates in a lyer
                alloc_Link_resources = reconstruct_allocs_with_cell_list(layer_res, updated_cells, updated_cells_position, layer_of_operation);
            }
        }
      
        
    }    
    

      /*
     * in this one, I allocate per each list mamber, in case there are enough
     * resources (faster)
     */
    private void run_NTT(){
       
        int layer_of_operation = NET_req_list.size() - 1;
        int req_num_alloc_slots = NET_req_list.get(layer_of_operation);
        
        int load = req_num_alloc_slots;
        List<String> layer_res = Link_resources.get(layer_of_operation);
        //Print.println("  layer_res in new assign " + layer_res.toString() );
        //List<String> new_res_list =  make_string_list_zeros(layer_res);
        
        //these lists are used to store the used cell information
        List<String> updated_cells = new ArrayList<>();
        List<Integer> updated_cells_position = new ArrayList<>();     
        List<Integer> cell_positions = new ArrayList<>();
        for(int i = 0; i < layer_res.size() ; i++)
        {

            String ex_cell = layer_res.get(i);
            int available_in_cell = ResourceOperations.count_zeros(ex_cell);
            //Print.println("  ex_cell " + ex_cell + "\t with availability of " + available_in_cell + " \t cell  number i " + i + " \tload " + load);
            //if(get_me_cell_availabilities_inlayer(layer_res, i)>= load)

            ///if(available_in_cell>=load && load!=0)
            
            
            
            if(available_in_cell>0&& load!=0)
            {                
                int [] cell_arr = Converters.convert_string_to_1Darray(ex_cell);
                //Print.print_1D(" the cell array " , cell_arr);
                int delay = 0;
                this.currecnt_al = new Alg_alloc_object
                        (load, cell_arr, cell_arr.length, delay);
                
                List<String> alg_type_list = Req.get_NET_ALG_types();
                if(alg_type_list.get(layer_of_operation).matches("S"))
                {                
                    boolean yes = run_S_assignment(currecnt_al);
                    load  = load - currecnt_al.get_load();
                    cell_positions.add(i);
                    if(Debug)
                    Print.println("  after running S algorithm in new ass remaining load  is: " + load);

                }
                else if(alg_type_list.get(layer_of_operation).matches("M"))
                {                
                    boolean yes = run_M_assignment(currecnt_al);    
                    load  = load - currecnt_al.get_load();
                    cell_positions.add(i);
                    if(Debug)
                    Print.println("  after running M algorithm in new ass remaining load  is: " + load);
                }                
                cell_arr = currecnt_al.get_result_assignmnet();
                String new_cell = Converters.convert_array_to_string(cell_arr);  
                //Print.println("  new_cell " + new_cell);
//                Print.println("\n  " );
                updated_cells.add(new_cell);
                updated_cells_position.add(i);
                

                ///load = 0;

                //Print.println("  alloc_Link_resources " + alloc_Link_resources.toString());

                //break;
                
            }
                           
            if(load==0)
            {
                this.success = true;

                this.currecnt_al.set_cell_positions(cell_positions);
                    //this method is used for making one cell update in a lyer
                //alloc_Link_resources = reconstruct_allocs_with_cell(layer_res, new_cell, layer_of_operation, i);

                    //this method is used for making multiple cell updates in a lyer
                alloc_Link_resources = reconstruct_allocs_with_cell_list(layer_res, updated_cells, updated_cells_position, layer_of_operation);
            }
        }
      
        
    }        
    public boolean return_success_status()
    {
        return this.success;
    }
    
    
    public Alg_alloc_object get_allocation_object(){

        return currecnt_al;
        
    }
    
    public List<List> create_alloc_res_list(){
        List<List> allocaitons = new ArrayList<>();
        for(int i = 0; i < this.Link_resources.size(); i++)
        {
            List<String> res_layer = this.Link_resources.get(i);
            List<String> new_res_layer = make_string_list_zeros(res_layer);
            allocaitons.add(new_res_layer);
        }
        return allocaitons;
        
    }
    
    public List<String> make_string_list_zeros(List<String> layer_res)
    {
        List<String> to_b_return = new ArrayList<>();
        String sample = layer_res.get(0);
        String str = ResourceOperations.create_resource_string_zeroes(sample.length());
        
        for(int i = 0; i < layer_res.size(); i++)
        {
            to_b_return.add(str);
        }
        
        return to_b_return;
    }
        

    
    public List<List> reconstruct_allocs_with_layer(List<String> layer_res,  int layer_number)
    {
      List<List>  res_blank_list = create_alloc_res_list();
      if(Debug){
        Print.println("res_blank_list : " + res_blank_list.toString());
        Print.println("layer_number : " + layer_number);
      }
      res_blank_list.remove(layer_number);
      res_blank_list.add(layer_number, layer_res);
      return res_blank_list;        
        
    }

    public List<List> reconstruct_allocs_with_cell(List<String> ex_layer, String cell, int layer_number, int cell_number)
    {

        List<List>  res_blank_list = create_alloc_res_list();
        //Print.println("res_blank_list : " + res_blank_list.toString());
        //Print.println("layer_number : " + layer_number + " cell number  : " + cell_number);
        //Print.println(" new cell  : " + cell);

        List<String> layer = res_blank_list.remove(layer_number);
        layer.remove(cell_number);
        layer.add(cell_number,cell);
        res_blank_list.add(layer_number,layer);
        //res_blank_list.add(layer_number, new_layer_list);

        //return the updated resource list
        
        //Print.println("res_blank_list after allocations : " + res_blank_list.toString());
        return res_blank_list;        
        
    }
    
    /*
     * here I try to use a list of cells to reconstruct the array rather than 
     * having only one cell
     */
    public List<List> reconstruct_allocs_with_cell_list(List<String> ex_layer, 
            List <String> updated_cell_list,List <Integer> cell_list_numbers , 
            int layer_number)
    {

        //all zeroes tree
//        List<String> new_layer_list = new ArrayList();
//        for(int i = 0; i < ex_layer.size(); i++)
//        {
//            new_layer_list.add(ex_layer.get(i));
//        }
//        new_layer_list.remove(cell_number);
//        new_layer_list.add(cell_number,cell);
//        
        List<List>  res_blank_list = create_alloc_res_list();
        //Print.println("res_blank_list : " + res_blank_list.toString());
        for(int i = 0; i < updated_cell_list.size() ; i++)
        {
            String cell =  updated_cell_list.get(i);
            int cell_number = cell_list_numbers.get(i);
            if(Debug)
            {
                Print.println(" New_assign: cell_list.toString : " + updated_cell_list.toString());
                Print.println(" New_assign: cell_list_numbers.toString : " + cell_list_numbers.toString());
                Print.println(" New_assign: layer_number : " + layer_number + " cell number  : " + cell_number);
                Print.println(" New_assign: new cell  : " + cell);
                Print.println(" New_assign: cell_list.size( : " + updated_cell_list.size());
                Print.println(" New_assign: i   : " + i);
            }
            List<String> layer = res_blank_list.remove(layer_number);
//            Print.println(" New_assign: layer.size   : " + layer.size());
//            Print.println(" New_assign: layer.to string   : " + layer.toString());

            layer.remove(cell_number);
            layer.add(cell_number,cell);
            res_blank_list.add(layer_number,layer);
        }
        return res_blank_list;        
        
    }
    
    public List<String> string_to_list(String str, int sub_str_length)
    {
        List<String> string_to_list = new ArrayList<>();
        for(int i = 0; i < str.length(); i = i+sub_str_length)
        {
            string_to_list.add(str.substring(i, i + sub_str_length));
        }        
        
        
        return string_to_list;
    }    
    
    
    public boolean run_M_assignment(Alg_alloc_object current) 
    {
        //int[] already_assignmnet = new int[First_array.length];
        int counter = 0;
        int load = current.Load;
        if(Debug)
        Print.println("\t\t\t\t before  alocations Load  is :  "+ load);
        int [] already_assignmnet = current.get_already_assignmnet();
        int [] result_assignmnet = new int[current.already_assignmnet.length];
        //for(int i = First_delay; i < First_array.length; i++)
        int i = current.Delay;
        if(Debug)
        {
//            P.print("  VON ID : "+ VON_ID +"First_dealy = i " 
//                    + First_delay);  
//            P.print("  VON ID : "+ VON_ID +"First_array.length:" 
//                + already_assignmnet.length);  
        }
        while(load>0 && (i <already_assignmnet.length))
        {
                //P.println("  load: " + load + "\t i: " + i+ "\tassignmnet[i]: " + already_assignmnet[i]);            
                if(already_assignmnet[i]==0)
                {
                    //temp_arr[i] = 1;
                    result_assignmnet[i] = 1;
                    if(Debug)
                    Print.println("  load: " + load + "\t i: " + i + "\tassignmnet[i]: " + already_assignmnet[i]);
                    counter++;
                    load--;

                }
                i++;
                
        }
        {
            if(Debug){
                //P.print_1D("  VON ID : "+ VON_ID +" FF_2D the input First_array: ", First_array);
//                P.print_1D("  VON ID : "+ VON_ID +" FF_2D the result already_assignmnet: ", already_assignmnet);
            }
                    //P.println("\t\t\t\t Load allocated  is :  "+ i);

            current.set_load(counter);
            current.set_result_assignmnet(result_assignmnet);
            current.set_is_alloc(true);
            //assignmnet = copy_from_arr_to_arr(temp_arr, already_assignmnet);
            return true; 
        }
        
    } 
    
   
    public boolean run_M_assignment_lambda_grid(Alg_alloc_object current, int min, int max) 
    {
        //int[] already_assignmnet = new int[First_array.length];
        int counter = 0;
        int load = current.Load;
        if(Debug)
        Print.println("\t\t\t\t before  alocations Load  is :  "+ load);
        int [] already_assignmnet = current.get_already_assignmnet();
        int [] result_assignmnet = new int[current.already_assignmnet.length];
        //for(int i = First_delay; i < First_array.length; i++)
        int i = min;
        i = i + current.Delay;
        if(Debug)
        {
//            P.print("  VON ID : "+ VON_ID +"First_dealy = i " 
//                    + First_delay);  
//            P.print("  VON ID : "+ VON_ID +"First_array.length:" 
//                + already_assignmnet.length);  
        }
        //while(load>0 && (i <already_assignmnet.length))
        while(load>0 && (i <max))
        {
                //P.println("  load: " + load + "\t i: " + i+ "\tassignmnet[i]: " + already_assignmnet[i]);            
                if(already_assignmnet[i]==0)
                {
                    //temp_arr[i] = 1;
                    result_assignmnet[i] = 1;
                    if(Debug)
                    Print.println("  load: " + load + "\t i: " + i + "\tassignmnet[i]: " + already_assignmnet[i]);
                    counter++;
                    load--;

                }
                i++;
                
        }
        {
            if(Debug){
//                P.print_1D("  VON ID : "+ VON_ID +" FF_2D the input First_array: ", First_array);
//                P.print_1D("  VON ID : "+ VON_ID +" FF_2D the result already_assignmnet: ", already_assignmnet);
            }
                    //P.println("\t\t\t\t Load allocated  is :  "+ i);

            current.set_load(counter);
            current.set_result_assignmnet(result_assignmnet);
            current.set_is_alloc(true);
            //assignmnet = copy_from_arr_to_arr(temp_arr, already_assignmnet);
            return true; 
        }
        
    } 
    
   
    public boolean run_M_assignment_gridless(Alg_alloc_object current, int max , int min) 
    {
        //int[] already_assignmnet = new int[First_array.length];
        int counter = 0;
        int load = current.Load;
        if(Debug)
        Print.println("\t\t\t\t before  alocations Load  is :  "+ load);
        int [] already_assignmnet = current.get_already_assignmnet();
        int [] result_assignmnet = new int[current.already_assignmnet.length];
        //for(int i = First_delay; i < First_array.length; i++)
        //int i = current.Delay;
        
        //I used this to get the max size, and stert from there
//        int i = current.already_assignmnet.length - 1;
        
        //the max gives the starting point for allocating the spec slices 
        int i = max -1;
        if(Debug)
        {
//            P.print("  VON ID : "+ VON_ID +"First_dealy = i " 
//                    + First_delay);  
//            P.print("  VON ID : "+ VON_ID +"First_array.length:" 
//                + already_assignmnet.length);  
        }
        //while(load>0 && (i <already_assignmnet.length))
        while(load>0 && (i >= min))
        {
                //P.println("  load: " + load + "\t i: " + i+ "\tassignmnet[i]: " + already_assignmnet[i]);            
                if(already_assignmnet[i]==0)
                {
                    //temp_arr[i] = 1;
                    result_assignmnet[i] = 1;
                    if(Debug)
                    Print.println("  load: " + load + "\t i: " + i + "\tassignmnet[i]: " + already_assignmnet[i]);
                    counter++;
                    load--;

                }
                i--;
                
        }
        {
            if(Debug){
                //P.print_1D("  VON ID : "+ VON_ID +" FF_2D the input First_array: ", First_array);
//                P.print_1D("  VON ID : "+ VON_ID +" FF_2D the result already_assignmnet: ", already_assignmnet);
            }
                    //P.println("\t\t\t\t Load allocated  is :  "+ i);

            current.set_load(counter);
            current.set_result_assignmnet(result_assignmnet);
            current.set_is_alloc(true);
            //assignmnet = copy_from_arr_to_arr(temp_arr, already_assignmnet);
            return true; 
        }
        
    } 
    
       
    public boolean run_S_assignment(Alg_alloc_object current) 
    {
        //int[] already_assignmnet = new int[First_array.length];
        int load = current.Load;
        int [] already_assignmnet = current.get_already_assignmnet();
        int [] result_assignmnet = new int[current.already_assignmnet.length];
        //int [] temp_arr =  new int[current.already_assignmnet.length];
        int counter = 0;
        boolean assigned = false;
        
        int i = current.Delay;
        if(Debug){
//        P.print("  VON ID : "+ VON_ID +"  First_delay:" +First_delay+  "\tFirst_array.length:" 
//                + already_assignmnet.length);   
        }
        while(load>0 && (i <already_assignmnet.length))
        {
            /*
             * P.println("\ni is:" +i + "    in FFC, give the counter:"
             * + counter + "\t and the load requestd:" + load);
             */
                if(already_assignmnet[i]==0)
                {
                    counter++;
                    if(counter>=load)
                    {
                        for(int h = i - counter + 1; h < i + 1; h++)
                        {
                           //P.println("\nh is:" +h);
                           result_assignmnet[h]= 1; 
                        }
                        assigned = true;
                        break;
                    }
                    
                }
                else if(already_assignmnet[i]==1)
                {
                    counter = 0;
                }
                i++;

        }
        if(assigned== false)
        {
            already_assignmnet = null;
            if(Debug)
//            P.print("  VON ID : "+ VON_ID +" FFC already_assignmnet: " + already_assignmnet);
            return assigned;                    
        }
        else{
            if(Debug){
//                P.print_1D("  VON ID : "+ VON_ID +" FFC the result already_assignmnet: ", already_assignmnet);
            }
            current.set_result_assignmnet(result_assignmnet);
            current.set_load(0);
            current.set_is_alloc(assigned);
            //already_assignmnet = copy_from_arr_to_arr(temp_arr, already_assignmnet);
            return true; 
        }
        return assigned;
    } 
    
    public int[] copy_from_arr_to_arr(int[] copy_from, int[] copy_to)
    {
        for(int i = 0; i < copy_from.length ; i ++)
        {
            copy_to[i] = copy_from[i];
        }
        
        return copy_to;
    }
    public int[] retrun_zeroes_arr(int [] arr)
    {
        int[] this_array = new int[arr.length];
        for(int i = 0; i < arr.length; i++)
        {
            this_array[i] = 0;
        }
        return this_array;
    }    
    
    public List<List> getm_Resrouces_tb_updated()
    {
        return this.alloc_Link_resources;
    }  
    
    public int get_me_cell_availabilities_inall(List<List> Resources, int i, int j)
    {
        String cell = (String) Resources.get(i).get(j);
        int zeroes = ResourceOperations.count_zeros(cell);
        return zeroes;
    }
    
    public int get_me_cell_availabilities_inlayer(List<String> Layer_Resources, int i)
    {
        String cell = (String) Layer_Resources.get(i);
        int zeroes = ResourceOperations.count_zeros(cell);
        return zeroes;
    }
    
    public List<Integer> convert_resInfo_to_resCap(List<String> Res_info)
    {
        List<Integer> capacity = new ArrayList<>();
        //capacity.add(Res_info.get(0).length());
        int upto_this_layer =0;
        for(int i = 0 ; i < Res_info.size(); i++)
        {
            if(i==0)
            {
                upto_this_layer = Res_info.get(i).length();
                capacity.add(upto_this_layer);
            }
            else
            {
                upto_this_layer = upto_this_layer * Res_info.get(i).length();
                capacity.add(upto_this_layer);
            }
        }
        
        return capacity;
    }   

    

        
    
}
