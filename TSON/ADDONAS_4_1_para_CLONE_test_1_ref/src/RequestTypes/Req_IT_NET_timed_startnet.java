/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package RequestTypes;

import java.util.ArrayList;
import java.util.List;
import utilities.Print;

/**
 *
 * @author Bijan
 */
public class Req_IT_NET_timed_startnet extends Request_base {
    
    String Star_req_type = "";

    List<Integer> Srcdest = new ArrayList<>();
        public Req_IT_NET_timed_startnet(List NET_ALG_type,List IT_ALG_type, String Algtype,
                String req_type,
                String star_req_type,
                int id, List srcdest, int bw,int QOS,  
                List Net_req,List IT_req, int start_time, int duration){
            
        this.request_class = "Req_IT_NET_timed_startnet";    
            
        this.mAlgorithmType = Algtype;
        this.Star_req_type = star_req_type;
        this.Connection_ReqType = req_type;
        this.mId = id;
        this.mBW = bw;
        this.Srcdest = srcdest;
        this.mQOS = QOS;
        this.Net_requirements = Net_req;
        this.IT_requirements = IT_req;
        this.Start_time = start_time;
        this.Call_duration = duration;
        this.End_time = this.Start_time + this.Call_duration;
        this.NET_ALG_req =NET_ALG_type;
        this.IT_ALG_req =IT_ALG_type;
                
    }
    @Override
    public void displayAttributes(){
        
       Print.println("\n{{{{{{{{{{{  DISPLAY REQUEST ATTRIBUTES }}}}}}}}}}}}}}}}}");
       Print.println("{{{{{{{{{{{  this.mId   "+ this.mId + " }}}}}}}}}}}}}}}}}");
       Print.println("{{{{{{{{{{{  this.Srcdest list   "+ this.Srcdest.toString() + " }}}}}}}}}}}}}}}}}");
       Print.println("{{{{{{{{{{{  this.Start_time   "+ this.Start_time + " }}}}}}}}}}}}}}}}}");
       Print.println("{{{{{{{{{{{  this.End_time   "+ this.End_time + " }}}}}}}}}}}}}}}}}\n");
       
        
    };//    
    
    public List<Integer> returnSrcdest() {
        return Srcdest;
    }    
    @Override
    public String returnConnectionReqType() {
        return Connection_ReqType; 
    }
    
    public String returnStarReqType() {
        return Star_req_type; 
    }
    
    @Override
    public List<String> get_NET_ALG_types() {
        return NET_ALG_req; 
    }
             
    @Override
    public List<String> get_IT_ALG_types() {
        return IT_ALG_req; 
    }
             
        
    @Override
    public String returnAlgorithmType() {
        return mAlgorithmType; 
    }

    @Override
    public int returnId() {
        return mId;
    }

    @Override
    public int returnSrc() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public int returnsDest() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public int returnBW() {
        return mBW;
    }
    @Override
    public int returnStartTime() {
        return Start_time;
    }

    @Override
    public int returnStopTime() {
        return End_time;
    }

    @Override
    public int returnDurationTime() {
        return Call_duration;
    }

    @Override
    public List<Integer> returnNETREQ() {
        return Net_requirements;
    }

    @Override
    public List<Integer> returnITREQ() {
        return IT_requirements;
    }

    @Override
    public List returnNETCostREQ() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public List returnNETDelayREQ() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public List returnITDelayREQ() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public List returnITCostREQ() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }    
}
