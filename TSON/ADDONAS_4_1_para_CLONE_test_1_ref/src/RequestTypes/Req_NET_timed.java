/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package RequestTypes;

import java.util.List;
import utilities.Print;

/**
 *
 * @author Bijan
 */
public class Req_NET_timed extends Request_base {

        public Req_NET_timed(List ALG_type, String Algtype, String req_type, int id, int src, int dest, int bw,int QOS,  
                List Net_req, int start_time, int duration){
            
        this.request_class = "Req_NET_timed";    

        this.mAlgorithmType = Algtype;
        this.Connection_ReqType = req_type;
        this.mId = id;
        this.mBW = bw;
        this.mDest = dest;
        this.mSrc = src;
        this.mQOS = QOS;
        this.Net_requirements = Net_req;
        this.Start_time = start_time;
        this.Call_duration = duration;
        this.End_time = this.Start_time + this.Call_duration;
        this.NET_ALG_req =ALG_type;
                
    }
    @Override
    public void displayAttributes(){
        
       Print.println("\n{{{{{{{{{{{  DISPLAY REQUEST ATTRIBUTES }}}}}}}}}}}}}}}}}");
       Print.println("{{{{{{{{{{{  this.mId   "+ this.mId + " }}}}}}}}}}}}}}}}}");
       Print.println("{{{{{{{{{{{  this.mDest   "+ this.mDest + " }}}}}}}}}}}}}}}}}");
       Print.println("{{{{{{{{{{{  this.mSrc   "+ this.mSrc + " }}}}}}}}}}}}}}}}}");
       Print.println("{{{{{{{{{{{  this.Start_time   "+ this.Start_time + " }}}}}}}}}}}}}}}}}");
       Print.println("{{{{{{{{{{{  this.End_time   "+ this.End_time + " }}}}}}}}}}}}}}}}}\n");
       
        
    };//     
    @Override
    public String returnConnectionReqType() {
        return Connection_ReqType; 
    }
    @Override
    public List<String> get_NET_ALG_types() {
        return NET_ALG_req; 
    }
             
        
    @Override
    public String returnAlgorithmType() {
        return mAlgorithmType; 
    }

    @Override
    public int returnId() {
        return mId;
    }

    @Override
    public int returnSrc() {
        return mSrc;
    }

    @Override
    public int returnsDest() {
        return mDest;
    }

    @Override
    public int returnBW() {
        return mBW;
    }
    @Override
    public int returnStartTime() {
        return Start_time;
    }

    @Override
    public int returnStopTime() {
        return End_time;
    }

    @Override
    public int returnDurationTime() {
        return Call_duration;
    }

    @Override
    public List<Integer> returnNETREQ() {
        return Net_requirements;
    }

    @Override
    public List returnITREQ() {
        return IT_requirements;
    }

    @Override
    public List returnNETCostREQ() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public List returnNETDelayREQ() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public List returnITDelayREQ() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public List returnITCostREQ() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }    
}
