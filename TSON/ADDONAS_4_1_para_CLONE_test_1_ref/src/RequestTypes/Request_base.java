/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package RequestTypes;

import java.util.List;

/**
 *
 * @author Bijan
 */
public abstract class Request_base {
    String mAlgorithmType = null;
    String Connection_ReqType = null;
    
    String request_class = null;
    
    int mId = -1;
    int mSrc = -1;
    int mDest = -1;
    int mBW = -1;
    int mQOS = -1;
    int Start_time = -1;
    int Call_duration =-1;
    int End_time = -1;
    
    List NET_ALG_req = null;  
    List IT_ALG_req = null;  
    //simply the number of slices needed
    List<Integer>  Net_requirements = null; 
    List<Integer>  IT_requirements = null; 
//    List IT_Cost_requirements = null;
//    List Net_Cost_requirements = null; 
//    List IT_Delay_requirements = null;
//    List Net_Delay_requirements = null;     
    public String getRequest_class(){return request_class;
};    
    public String returnConnectionReqType(){return null;
};    
    public String returnAlgorithmType(){return null;
};
    public int returnId(){return -1;
};
    public int returnSrc(){return -1;
};
    public int returnsDest(){return -1;
};
    public int returnQOS(){return -1;
};    
    public int returnBW(){return -1;
};
    public int returnStartTime(){return -1;
};
    public int returnStopTime(){return -1;
};    
    public int returnDurationTime(){return -1;
};     
    public List returnNETCostREQ(){return null;
};//not in use 
    public List returnNETDelayREQ(){return null;
};//not in use 
    public List<Integer> returnNETREQ(){return Net_requirements;
};
    public List<Integer> returnITREQ(){return IT_requirements;
};
    public List<String> get_NET_ALG_types(){return NET_ALG_req;
};      
    public List<String> get_IT_ALG_types(){return IT_ALG_req;
};      
    public List returnITDelayREQ(){return null;
};//not in use 
    public List returnITCostREQ(){return null;
};//not in use 
    public void displayAttributes(){
};//
}
