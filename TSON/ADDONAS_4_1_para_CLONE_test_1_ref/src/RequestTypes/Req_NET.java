/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package RequestTypes;

import java.util.List;

/**
 *
 * @author Bijan
 */
public class Req_NET extends Request_base{

    public Req_NET(String type,int id, int src, int dest, int bw,  List Net_req){
        this.mAlgorithmType = type;
        this.mId = id;
        this.mBW = bw;
        this.mDest = dest;
        this.mSrc = src;
        this.Net_requirements = Net_req;
    }      

    @Override
    public String returnAlgorithmType() {
        return mAlgorithmType; 
    }

    @Override
    public int returnId() {
        return mId;
    }

    @Override
    public int returnSrc() {
        return mSrc;
    }

    @Override
    public int returnsDest() {
        return mDest;
    }

    @Override
    public int returnBW() {
        return mBW;
    }

    @Override
    public List returnNETREQ() {
        return Net_requirements;
    }

    @Override
    public List returnITREQ() {
        return IT_requirements;
    }

    @Override
    public List returnNETCostREQ() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public List returnNETDelayREQ() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public List returnITDelayREQ() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public List returnITCostREQ() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

}
