/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package statistics;

import utilities.ExcelWriter_star;
import utilities.ExcelWriter_star_flatRaw;
import Infrastructure.Link;
import Infrastructure.Network;
import Infrastructure.Node;
import Infrastructure.ResourceOperations;
import Record.Link_Rec_Obj;
import Record.Rec_Link_Allocs;
import Record.Rec_Node_Allocs;
import RequestTypes.Req_IT_NET_timed_startnet;
//import RequestTypes.Req_NET_timed;
import RequestTypes.Request_base;
import com.rits.cloning.Cloner;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.sql.Array;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;
import resources.Parent;
import routing.Route;
import utilities.Converters;
import utilities.ExcelWriter;
import utilities.Print;
import static utilities.Print.println;

/**
 *
 * @author Bijan
 * 
 * notes: the utilisation info, neeeds to be inits once, not every time, 
 * so the numbers are aggregated each time. the same is for blocking
 * so when you add anything it will add to the existing, not from 0
 *
 * 
 */
public class Stats_collector_Star {
    static int LinkresInfoSize = 3;
    static int NoderesInfoSize = 1;//if want to decreaase, you need to modify the excel writer
    boolean Debug;
    private HashMap<String,Rec_Link_Allocs> Link_Record_hash;
    private HashMap<Integer,Rec_Node_Allocs> Node_Record_hash;
    private Network mNetwork;
    
    String file_address;
    
    List<Route> Routes = new ArrayList<>();
    int route_lengths = 0;
    HashMap<String,Integer> Reqs_route_length25 = new HashMap<>();

//    ExcelWriter_star EWS = new ExcelWriter_star();    
    ExcelWriter_star_flatRaw EWS = new ExcelWriter_star_flatRaw();    
    
    //number of nodes/links of each type across the network
    HashMap<String,Integer> hash_network_node_types21 = new HashMap<>();
    HashMap<String,Integer> hash_network_link_types22 = new HashMap<>();
    

    //the available capacity in layers for each link/node
    HashMap<String,List> hash_all_link_type_caps19 = new HashMap<>();  
    HashMap<String,List> hash_all_node_type_caps20 = new HashMap<>();  
    

    // network wide link/node capacity in differnet layers,
    // the layers are the keys, and the rest are the network wide avaialbility in that layer
    HashMap<Integer,Integer> all_link_layers_cap_hash17 = new HashMap<>();
    HashMap<Integer,Integer> all_node_layers_cap_hash18 = new HashMap<>();
    

    //collecting blocking stats per each type of request
    HashMap<String,Integer> blocking_per_req_type_hash_link15 = new HashMap<>();
    HashMap<String,Integer> blocking_per_req_type_hash_node16 = new HashMap<>();
    
    //collecting admissions stats per each type of request
    HashMap<String,Integer> addmission_per_req_type_hash_link13 = new HashMap<>();
    HashMap<String,Integer> addmission_per_req_type_hash_node14 = new HashMap<>();




    //these hashes are used to keep per requect type information, per link and in total
    //see method process_linkRec_objects
    // this is the hashmap model  <linkid,type, utilised>
    HashMap<String,HashMap> Hashed_all_links_hash_util_perTypes11 = new HashMap<>();
    HashMap<Integer,HashMap> Hashed_all_nodes_hash_util_perTypes12 = new HashMap<>();
    

    // in this hash,I collect the overall
    HashMap<String,Integer> All_hash_link_util_perType9 = new HashMap<>();        
    HashMap<String,Integer> All_hash_node_util_perType10 = new HashMap<>(); 
    

    // this hash, keeps all the layers Type_aa_util lists of all the links
    HashMap<String,List> Util_all_links_pertype_alllayers23 = new HashMap<>();
    HashMap<String,List> Util_all_nodes_pertype_alllayers24 = new HashMap<>();
    
    // this hash, keeps all the layers Type_aa_util lists of all the links
    HashMap<String,List> Util_per_link_layers7 = new HashMap<>();
    HashMap<Integer,List> Util_per_node_layers8 = new HashMap<>();
    

    //this hash, keeps the aggregated/total Type_aa_util of the links layers
    HashMap<Integer, Integer> Total_util_of_link_layers5 = new HashMap<>();        
    HashMap<Integer, Integer> Total_util_of_node_layers6 = new HashMap<>(); 
    


    //counter for blocking of link/node requests
    int blocking_general_link2 = 0;
    int blocking_general_node4 = 0; 

    //counter for admissions of link/node requests
    int admitted_general_link1 = 0;
    int admitted_general_node3= 0;
    
    /*
     * added for start request
     */

    /*
     * I want to get the addmission and blocking for all the entities.
     * This might be a better aproach. So later on, I can process them 
     * and see which one belogs to which type. 
     */ 
    

    //collecting admissions stats per each type of request
    HashMap<String,Integer> addmission_per_STARreq_type_hash_link42 = new HashMap<>();
    HashMap<String,Integer> addmission_per_STARreq_type_hash_node44 = new HashMap<>();
        
    //collecting blocking stats per each type of request
    HashMap<String,Integer> blocking_per_STARreq_type_hash_link41 = new HashMap<>();
    HashMap<String,Integer> blocking_per_STARreq_type_hash_node43 = new HashMap<>();
        
    HashMap<String, Integer> per_link_addmission30 = new HashMap<>();        
    HashMap<String, Integer> per_link_blocking31 = new HashMap<>(); 
    HashMap<Integer, Integer> per_node_addmission32 = new HashMap<>(); 
    HashMap<Integer, Integer> per_node_blocking33 = new HashMap<>(); 
    
    
    HashMap<String,Integer> blocking_per_star_req_type_hash = new HashMap<>();
    //collecting admissions stats per each type of request
    HashMap<String,Integer> addmission_per_star_req_type_hash = new HashMap<>();  
    
    HashMap<Integer, Integer> Total_accept_node_id_hash = new HashMap<>();        
    HashMap<String, Integer> Total_accept_link_id_hash = new HashMap<>(); 
        
    //counter for blocking of link/node requests
    int blocking_star_req = 0;
    int admitted_star_req = 0;
    int total_star_reqs = 0;
    
    int blocking_star_node = 0;
    int blocking_star_link = 0;
    
    /*
     * rerouting information
     */
    public int totalrerouting = 0;
    public int totalreroute_link = 0;
    public int totalreroute_node = 0;
    public HashMap<String,Integer> reroute_per_star_req_type_hash = new HashMap<>();       
    public HashMap<String,Integer> reroute_per_star_req_node_type_hash = new HashMap<>();       
    public HashMap<String,Integer> reroute_per_star_req_linktype_hash = new HashMap<>();       

    
    //
    HashMap<Integer, Integer> Total_star_block_node_id_hash = new HashMap<>();        
    HashMap<String, Integer> Total_star_block_link_id_hash = new HashMap<>();     
    
    /*
     *  link and nodes identifiers
     */    
    public List<Integer> nodes_used = new ArrayList<>();       
    public List<String> links_used = new ArrayList<>();    
    
    
    
    String[] Link_Req_types;
    String[] Star_req_types;
        
    public Stats_collector_Star(boolean debug, Network network, 
            HashMap<String,Rec_Link_Allocs> link_record_hash, 
            HashMap<Integer,Rec_Node_Allocs> node_record_hash, 
            String excel_file_addrs,
            String[] req_types,
            String[] star_req_types)
    {
//        super(debug, network, link_record_hash, excel_file_addrs, req_types);
        
        this.Debug = debug;
        this.Star_req_types = star_req_types;
        
        this.Link_Req_types =req_types;

//       for(int i = 0;i <req_types.length;i++)
//       {
//          
//       }               
        this.Link_Record_hash = link_record_hash;
        this.Node_Record_hash = node_record_hash;
        this.mNetwork = network;
        
        this.file_address = excel_file_addrs;
        
        this.init_star_Reqs_route_length25();
        
        
        //process_linkRec_objects();
        
//        this.process_linkRec_objects();
        
        this.cal_link_network_cap_in_layers();
        
        this.cal_node_network_cap_in_layers();
        
        this.collect_network_info();
        

        
        initAll_hash_link_util_perType9();
        initAll_hash_node_util_perType9();
        init_addmitting_star_request();
        init_blocking_star_request();
        init_reqType_allLayers_hashList_link23(LinkresInfoSize);
        init_reqType_allLayers_hashList_node23(NoderesInfoSize);
        init_star_Reqs_route_length25();
        initaddmission_per_STARreq_type_hash40();
        initblocking_per_STARreq_type_hash41();
        initaddmission_per_connection_req_type_hash_13();
        initblocking_per_connection_req_type_hash_15();
        init_accept_nodeid_linkid_hash();
        init_reroutes();
        init_star_accept_node_and_links();
        
        
    }   
    
    public void init_star_accept_node_and_links()
    {
        List<Node> ln = mNetwork.getmListNodes();
        List<Link> ll = mNetwork.getmListLinks();
        for(Node n: ln)
            Total_star_block_node_id_hash.put(n.getmID(), 0);
        for(Link l: ll)
            Total_star_block_link_id_hash.put(l.getmID(), 0);
    }
    public void addto_Total_star_block_link_id_hash(String linkid)
    {
        int existing =  Total_star_block_link_id_hash.get(linkid);
        Total_star_block_link_id_hash.put(linkid, existing+ 1);
    }
    public void addto_Total_star_block_node_id_hash(int nodeid)
    {
        int existing =  Total_star_block_node_id_hash.get(nodeid);
        Total_star_block_node_id_hash.put(nodeid, existing+ 1);
    }

    
    public void add_blocking_star_node()
    {
        blocking_star_node++;
    }
    public void add_blocking_star_routeLinks()
    {
        blocking_star_link++;
    }

    
    public void init_reroutes()
    {
       if(Debug)
       Print.print_1D("   initiailiase Reqs_route_length25 with these req types", Star_req_types);
       for(int i = 0;i <Star_req_types.length;i++)
       {
            if(!reroute_per_star_req_type_hash.containsKey(Star_req_types[i]))
            {
                //int cap = addmission_per_req_type_hash_link13.get(req_type);
                reroute_per_star_req_type_hash.put(Star_req_types[i], 0);
            }
            if(!reroute_per_star_req_node_type_hash.containsKey(Star_req_types[i]))
            {
                //int cap = addmission_per_req_type_hash_link13.get(req_type);
                reroute_per_star_req_node_type_hash.put(Star_req_types[i], 0);
            }
            if(!reroute_per_star_req_linktype_hash.containsKey(Star_req_types[i]))
            {
                //int cap = addmission_per_req_type_hash_link13.get(req_type);
                reroute_per_star_req_linktype_hash.put(Star_req_types[i], 0);
            }
 
       }
    }
    
    
    public void add_to_reroute_type(String startype)
    {
        Print.println("startypestartypestartype   startypestartype  " + startype);
        int existing = reroute_per_star_req_type_hash.get(startype);
        reroute_per_star_req_type_hash.put(startype, existing + 1);        
    }
    
    public void add_to_reroute_type_node(String startype)
    {
        int existing = reroute_per_star_req_node_type_hash.get(startype);
        reroute_per_star_req_node_type_hash.put(startype, existing + 1);        
    }
    
    public void add_to_reroute_type_link(String startype)
    {
        int existing = reroute_per_star_req_linktype_hash.get(startype);
        reroute_per_star_req_linktype_hash.put(startype, existing + 1);
    }
    
    
 /*
  * once itiitialise at the beginning, then keep on
  * accumulating
  */   
private void init_accept_nodeid_linkid_hash()
{
    List<Node> node_list = mNetwork.getmListNodes();
    List<Link> link_list = mNetwork.getmListLinks();
    for(Node n : node_list)
    {
        Total_accept_node_id_hash.put(n.getmID(),0);
    }
    for(Link l : link_list)
    {
        Total_accept_link_id_hash.put(l.getmID(),0);
    }
}

    
public void print_accept_nodeid_linkid_hash(Network mnet)
{
    List<Node> node_list = mnet.getmListNodes();
    List<Link> link_list = mnet.getmListLinks();    
    
    Print.println("-->Printing off the total number of accepted nodes and links");
//    Print.printResHash_IntInt(" acceptance for each NODE id ", Total_accept_node_id_hash);
    Set<Integer> keySet = Total_accept_node_id_hash.keySet();
    Object[] toArray = keySet.toArray();
    int [] intarr = new int [toArray.length];
    for (int i = 0; i < intarr.length; i++){
        intarr[i] = (int) toArray[i];
        //HashMap rec_res =  this.Allocation_id2res_hash.get(intarr[i]).return_allocresrouces();
        Print.println("\n\n printing nodes info ");
        
        println(" \tnode key : "+ intarr[i] +   " total accepted --> " + (int)Total_accept_node_id_hash.get(intarr[i]));
        println(" \tnode key : "+ intarr[i] +   " total res used --> " + node_list.get(intarr[i]).
                get_ParentClass_resrouces().getm_used_agg_layers_resources().toString());
        println(" \tnode key : "+ intarr[i] +   " total res left --> " + node_list.get(intarr[i]).
                get_ParentClass_resrouces().getm_Agg_layers_resources().toString());

    }    
    
    
    
//    Print.printResHash_StrInt(" acceptance for each LINK id ", Total_accept_link_id_hash);
    
    Set<String> LinkkeySet = Total_accept_link_id_hash.keySet();
    Object[] toStrArray = LinkkeySet.toArray();
    String [] strarr = new String [toStrArray.length];
    for (int i = 0; i < strarr.length; i++){
        strarr[i] = (String) toStrArray[i].toString();
        //HashMap rec_res =  this.Allocation_id2res_hash.get(intarr[i]).return_allocresrouces();
        Print.println("\n\n printing links info ");
        
        println(" \tlink key : "+ strarr[i] +   " total accepted --> " + Total_accept_link_id_hash.get(strarr[i]));
        println(" \tlink key : "+ strarr[i] +   " total res used --> " + link_list.get(mnet.get_link_num_from_id(strarr[i])).
                get_ParentClass_resrouces().getm_used_agg_layers_resources().toString());
        println(" \tlink key : "+ strarr[i] +   " total res left --> " + link_list.get(mnet.get_link_num_from_id(strarr[i])).
                get_ParentClass_resrouces().getm_Agg_layers_resources().toString());

    }    
    Print.println("/-->Printing off the total number of accepted nodes and links");
        
    
}



    
    public void init_reqType_allLayers_hashList_link23(int res_info_size)
    {
       for(int i = 0;i <Link_Req_types.length;i++)
       {
       List<Integer> list = ResourceOperations.generate_list_zeros(res_info_size);
           
            if(!Util_all_links_pertype_alllayers23.containsKey(Link_Req_types[i]))
            {
                //int cap = addmission_per_req_type_hash_link13.get(req_type);
                Util_all_links_pertype_alllayers23.put(Link_Req_types[i], list);
            }           
       }
    }    
    
    public void init_reqType_allLayers_hashList_node23(int res_info_size)
    {
       for(int i = 0;i <Link_Req_types.length;i++)
       {
       List<Integer> list = ResourceOperations.generate_list_zeros(res_info_size);
                   
            if(!Util_all_nodes_pertype_alllayers24.containsKey(Link_Req_types[i]))
            {
                //int cap = addmission_per_req_type_hash_link13.get(req_type);
                Util_all_nodes_pertype_alllayers24.put(Link_Req_types[i], list);
            } 
       }
    }    
    
    public void initaddmission_per_connection_req_type_hash_13()
    {
        
        
       for(int i = 0;i <Link_Req_types.length;i++)
       {
          
            if(!addmission_per_req_type_hash_link13.containsKey(Link_Req_types[i]))
            {
                //int cap = addmission_per_req_type_hash_link13.get(req_type);
                addmission_per_req_type_hash_link13.put(Link_Req_types[i], 0);
            }           
            if(!addmission_per_req_type_hash_node14.containsKey(Link_Req_types[i]))
            {
                //int cap = addmission_per_req_type_hash_link13.get(req_type);
                addmission_per_req_type_hash_node14.put(Link_Req_types[i], 0);
            } 
       }
       if(Debug)
       Print.println("   bijan addmission_per_req_type_hash_link13 " + addmission_per_req_type_hash_link13.toString());
    }    
    public void initblocking_per_connection_req_type_hash_15()
    {
       if(Debug) 
       Print.print_1D("   blocking  adasdfasdfasd ", Link_Req_types);
       for(int i = 0;i <Link_Req_types.length;i++)
       {
            if(!blocking_per_req_type_hash_link15.containsKey(Link_Req_types[i]))
            {
                //int cap = addmission_per_req_type_hash_link13.get(req_type);
                blocking_per_req_type_hash_link15.put(Link_Req_types[i], 0);
            }           
            if(!blocking_per_req_type_hash_node16.containsKey(Link_Req_types[i]))
            {
                //int cap = addmission_per_req_type_hash_link13.get(req_type);
                blocking_per_req_type_hash_node16.put(Link_Req_types[i], 0);
            } 
       }
    }
    
    public void initaddmission_per_STARreq_type_hash40()
    {
        
        
       for(int i = 0;i <Star_req_types.length;i++)
       {
          
            if(!addmission_per_STARreq_type_hash_link42.containsKey(Star_req_types[i]))
            {
                //int cap = addmission_per_req_type_hash_link13.get(req_type);
                addmission_per_STARreq_type_hash_link42.put(Star_req_types[i], 0);
            }           
            if(!addmission_per_STARreq_type_hash_node44.containsKey(Star_req_types[i]))
            {
                //int cap = addmission_per_req_type_hash_link13.get(req_type);
                addmission_per_STARreq_type_hash_node44.put(Star_req_types[i], 0);
            } 
       }
       if(Debug)
       Print.println("   bijan addmission_per_req_type_hash_link13 " + addmission_per_req_type_hash_link13.toString());
    }    
    public void initblocking_per_STARreq_type_hash41()
    {
       if(Debug) 
       Print.print_1D("   blocking  adasdfasdfasd ", Star_req_types);
       for(int i = 0;i <Star_req_types.length;i++)
       {
            if(!blocking_per_STARreq_type_hash_link41.containsKey(Star_req_types[i]))
            {
                //int cap = addmission_per_req_type_hash_link13.get(req_type);
                blocking_per_STARreq_type_hash_link41.put(Star_req_types[i], 0);
            }           
            if(!blocking_per_STARreq_type_hash_node43.containsKey(Star_req_types[i]))
            {
                //int cap = addmission_per_req_type_hash_link13.get(req_type);
                blocking_per_STARreq_type_hash_node43.put(Star_req_types[i], 0);
            } 
       }
    }
    

    
    

    
    public void initAll_hash_link_util_perType9()
    {
       if(Debug)
       Print.print_1D("   dada  adasdfasdfasd ", Link_Req_types);
       for(int i = 0;i <Link_Req_types.length;i++)
       {
            if(!All_hash_link_util_perType9.containsKey(Link_Req_types[i]))
            {
                //int cap = addmission_per_req_type_hash_link13.get(req_type);
                All_hash_link_util_perType9.put(Link_Req_types[i], 0);
            }

       }
       
       
       
       
    }    
    public void initAll_hash_node_util_perType9()
    {
       if(Debug)
       Print.print_1D("   dada  adasdfasdfasd ", Link_Req_types);
       for(int i = 0;i <Link_Req_types.length;i++)
       {

            if(!All_hash_node_util_perType10.containsKey(Link_Req_types[i]))
            {
                //int cap = addmission_per_req_type_hash_link13.get(req_type);
                All_hash_node_util_perType10.put(Link_Req_types[i], 0);
            }
       }
    }    
    public void init_star_Reqs_route_length25()
    {
       if(Debug)
       Print.print_1D("   initiailiase Reqs_route_length25 with these req types", Star_req_types);
       for(int i = 0;i <Star_req_types.length;i++)
       {
            if(!Reqs_route_length25.containsKey(Star_req_types[i]))
            {
                //int cap = addmission_per_req_type_hash_link13.get(req_type);
                Reqs_route_length25.put(Star_req_types[i], 0);
            }
 
       }
    }    
    public void init_addmitting_star_request()
    {
       if(Debug)
       Print.print_1D("   initiailiase Reqs_route_length25 with these req types", Star_req_types);
       for(int i = 0;i <Star_req_types.length;i++)
       {
            if(!addmission_per_star_req_type_hash.containsKey(Star_req_types[i]))
            {
                //int cap = addmission_per_req_type_hash_link13.get(req_type);
                addmission_per_star_req_type_hash.put(Star_req_types[i], 0);
            }
 
       }
    }    
    public void init_blocking_star_request()
    {
       if(Debug)
       Print.print_1D("   initiailiase Reqs_route_length25 with these req types", Star_req_types);
       for(int i = 0;i <Star_req_types.length;i++)
       {
            if(!blocking_per_star_req_type_hash.containsKey(Star_req_types[i]))
            {
                //int cap = addmission_per_req_type_hash_link13.get(req_type);
                blocking_per_star_req_type_hash.put(Star_req_types[i], 0);
            }
 
       }
    }    
    public void update_rec_hash_link(HashMap<String,Rec_Link_Allocs> link_record_hash)
    {
        this.Link_Record_hash = link_record_hash;
    }
    public void update_rec_hash_node(HashMap<Integer,Rec_Node_Allocs> node_record_hash)
    {
        this.Node_Record_hash = node_record_hash;
    }
    
    /*
     * in this mehod, i lppk into each links, just to show
     */
//    public void demonstrate_linkRec_objects()
//    {
//        //Record_hash this hash map holds the information for all the links.
//        //it is a hash of rec_link_allocs.
//        //it uses the keyset of all the links used
//        String[] key_arr = Converters.key_set_obj_to_str_array(this.Link_Record_hash);
//        for (int i = 0; i < key_arr.length; i++)
//        {
//            // for each link, I have recording hash, which keeps the allocated info
//            Rec_Link_Allocs rec_link = this.Link_Record_hash.get(key_arr[i]);
//            
//            //I have embedded the utilisation info extraction in each link record objet
//            //I read all the alloc objects in each recording unit, and return the
//            //usage per type. 
//            HashMap<String,Integer> type_utils = rec_link.return_aggregated_across_layers_util_of_allocs_per_type();
//            
//            
//            String[] types_arr = Converters.key_set_obj_to_str_array(type_utils);
//        
//            // now each link, I am reading aggregate allocs per each type
//            for(int j = 0; j < types_arr.length; j++)
//            {
//                int agg_load = type_utils.get(types_arr[j]);
//                Print.println(" in statistics collector link key_arr[i]: " + key_arr[i] +" \tthe type: " + types_arr[j] + "\tthe load: " + agg_load);
//            }
//        }
//    }
//    
//    public void calculate_resources_per_type()
//    {
//        HashMap link_types_dimension  = this.mNetwork.retrun_link_types_dimension_hash();
//        HashMap<String,Integer> link_types_capacity = new HashMap();
//        String[] l_types = ResourceOperations.return_rec_keys_str(link_types_dimension);
//        for(int i  = 0; i < link_types_dimension.size(); i++)
//        {
//            link_types_capacity.put(null, i);
//        }
//    }
    
    /*
     * this method obtains the node and links general types in the network
     */
    public void collect_network_info()
    {
     
        /*
         * link information: type;capacity and 
         */

        
        List<Link> links_list = this.mNetwork.getmListLinks();
        //here I make a hash of the link types and their quantity
        for(int i =0; i < links_list.size() ;i++)
        {
            
            Link l = links_list.get(i);
            String l_type = l.getmType();
            List<Integer> link_capa = l.get_ParentClass_resrouces().give_me_all_capacity_per_layer();
            hash_all_link_type_caps19.put(l_type,link_capa);
            if(!hash_network_link_types22.containsKey(l_type))
            {
                hash_network_link_types22.put(l_type, 1);
            }
            else
            {

                int counter = hash_network_link_types22.get(l_type);
                hash_network_link_types22.put(l_type, counter +1);
                
            }
            
        }
        
        List<Node> node_list = this.mNetwork.getmListNodes();
        //here I make a hash of the node types and their quantity
        for(int i =0; i < node_list.size() ;i++)
        {
            
            Node n = node_list.get(i);
            String n_type = n.getmNode_Type();
            List<Integer> node_capa = n.get_ParentClass_resrouces().give_me_all_capacity_per_layer();
            hash_all_node_type_caps20.put(n_type,node_capa);
            if(!hash_network_node_types21.containsKey(n_type))
            {
                hash_network_node_types21.put(n_type, 1);
            }
            else
            {

                int counter = hash_network_node_types21.get(n_type);
                hash_network_node_types21.put(n_type, counter +1);
                
            }
            
        }

        
    }
    
    
    /*
     * this method, goes on eby one sin the links, and adds the capacity in each layer
     */
    public void cal_link_network_cap_in_layers()
    {
        List<Link> links_list = this.mNetwork.getmListLinks();
        //here I make a hash of the link types and their quantity
        
        all_link_layers_cap_hash17 = new HashMap<>();
        
        for(int i =0; i < links_list.size(); i++)
        {
            
            Link l = links_list.get(i);
            String l_type = l.getmType();
            List<Integer> link_cp_layers = l.get_ParentClass_resrouces().give_me_all_capacity_per_layer();            
            for(int j = 0; j < link_cp_layers.size(); j++)
            {
                if(!all_link_layers_cap_hash17.containsKey(j))
                {
                    int cap = link_cp_layers.get(j);
                    all_link_layers_cap_hash17.put(j, cap);
                }
                else
                {
                    int existing_cap = all_link_layers_cap_hash17.get(j);
                    int this_cap = link_cp_layers.get(j);
                    all_link_layers_cap_hash17.put(j, existing_cap + this_cap);
                    
                }
            }
        }
                
    }       
    
    /*
     * this method, goes on eby one sin the links, and adds the capacity in each layer
     */
    public void cal_node_network_cap_in_layers()
    {
        List<Node> nodes_list = this.mNetwork.getmListNodes();
        //here I make a hash of the link types and their quantity
        
        all_node_layers_cap_hash18 = new HashMap<>();
        
        for(int i =0; i < nodes_list.size(); i++)
        {
            
            Node n = nodes_list.get(i);
            String n_type = n.getmNode_Type();
            List<Integer> node_cp_layers = n.get_ParentClass_resrouces().give_me_all_capacity_per_layer();            
           
            Print.println("Node id  "+ n.getmID() +  "  node_cp_layers  " + node_cp_layers.toString());
            
            for(int j = 0; j < node_cp_layers.size(); j++)
            {
                if(!all_node_layers_cap_hash18.containsKey(j))
                {
                    int cap = node_cp_layers.get(j);
//                    int cap = node_cp_layers.get(j) * nodes_list.size();
                    all_node_layers_cap_hash18.put(j, cap);
                }
                else
                {
                    int existing_cap = all_node_layers_cap_hash18.get(j);
                    int this_cap = node_cp_layers.get(j);
                    all_node_layers_cap_hash18.put(j, existing_cap + this_cap);
                    
                }
            }
        }
                        Print.printResHash_IntInt(" all_node_layers_cap_hash18all_node_layers_cap_hash18   ", all_node_layers_cap_hash18);
    
    }     
    public void print_network_info()
    {
        Print.println("\n  in statistics: admitted_general_link1: " + this.admitted_general_link1);
        Print.println("\n  in statistics: blocking_general_link2: " + this.blocking_general_link2);
        Print.println("\n  in statistics: admitted_general_node3: " + this.admitted_general_node3);
        Print.println("\n  in statistics: blocking_general_node4: " + this.blocking_general_node4);
        Print.printResHash_IntInt("\n  in statistics: Total_util_of_link_layers5: " , this.Total_util_of_link_layers5);
        Print.printResHash_IntInt("\n  in statistics: Total_util_of_node_layers6: " , this.Total_util_of_node_layers6);
        Print.printResHash_StrList("\n  in statistics: Util_per_link_layers7: " , this.Util_per_link_layers7);
        Print.printResHash_IntList("\n  in statistics: Util_per_node_layers8: " , this.Util_per_node_layers8);
        Print.printResHash_StrInt("\n  in statistics: All_hash_link_util_perType9: " , this.All_hash_link_util_perType9);
        Print.printResHash_StrInt("\n  in statistics: All_hash_node_util_perType10: " , this.All_hash_node_util_perType10);
        Print.printResHash_StrHash_Strint("\n  in statistics: Hashed_all_links_hash_util_perTypes11: " , this.Hashed_all_links_hash_util_perTypes11);
        Print.printResHash_IntHash_Strint("\n  in statistics: Hashed_all_nodes_hash_util_perTypes12: " , this.Hashed_all_nodes_hash_util_perTypes12);
        Print.printResHash_StrInt("\n  in statistics: addmission_per_req_type_hash_link13: " , this.addmission_per_req_type_hash_link13);
        Print.printResHash_StrInt("\n  in statistics: addmission_per_req_type_hash_node14: " , this.addmission_per_req_type_hash_node14);
        Print.printResHash_StrInt("\n  in statistics: blocking_per_req_type_hash_link15: " , this.blocking_per_req_type_hash_link15);
        Print.printResHash_StrInt("\n  in statistics: blocking_per_req_type_hash_node16: " , this.blocking_per_req_type_hash_node16);  
        Print.printResHash_IntInt(" in statistics: all_link_layers_cap_hash17: ", all_link_layers_cap_hash17);
        Print.printResHash_IntInt(" in statistics: all_node_layers_cap_hash18: ", all_node_layers_cap_hash18);
        Print.println("\n  in statistics: hash_all_link_caps19: " + hash_all_link_type_caps19.toString());
        Print.println("\n  in statistics: hash_all_node_caps20: " + hash_all_node_type_caps20.toString());
        Print.printResHash_StrInt(" in statistics: hash_network_node_types21 ", hash_network_node_types21);
        Print.printResHash_StrInt(" in statistics: hash_network_link_types22 ", hash_network_link_types22);
        //Print.println("\n  in statistics: return_total_admitted: " + this.return_total_admitted_link());
        //Print.println("\n  in statistics: return_total_blocked: " + this.return_total_blocked_link());

    }
   public void add_to_route_list(Route route)
   {    
       this.route_lengths = route_lengths + route.getmLength();
       this.Routes.add(route);
   }
   public void add_to_route_list_per_req(Route route, String req_type)
   {    

        if(Debug)
        Print.printResHash_StrInt(" checking route star req types ", Reqs_route_length25);

        int length_so_far = Reqs_route_length25.get(req_type);

        this.Reqs_route_length25.put(req_type, route.getmLength() + length_so_far);
        Print.println("");

   }
   
   /////
   /*
    * general node and link allow and block, no matter of type categories
    */
   
   public void add_to_blocking_ink()
   {
        this.blocking_general_link2++;
   }
  public void add_to_admitted_link(String link_id)
   {
       
       /*
        * just to keep record per link id addmision
        */
        int existing = this.Total_accept_link_id_hash.get(link_id);
        this.Total_accept_link_id_hash.put(link_id,++existing);       
               
       /*
        * just to keep record total links addmision
        */         
        this.admitted_general_link1++;
   } 
   public void add_to_blocking_node()
   {
        this.blocking_general_node4++;        
   }
  public void add_to_admitted_node(int node_id)
   {
       
       /*
        * just to keep record per node id addmision
        */
        int existing = this.Total_accept_node_id_hash.get(node_id);
        this.Total_accept_node_id_hash.put(node_id,++existing);       
               
       /*
        * just to keep record total nodes addmision
        */               
        this.admitted_general_node3++;
   }    
   
   /////////////
  /*
   * record the connection type node and link performance 
   * 4 methods: add and block for links and nodes
   */        
   public void add_to_blocking_list_link_type(String req_type)
   {
       //reinitialise
      // blocking_per_req_type_hash_link15 = new HashMap<>();
       initblocking_per_connection_req_type_hash_15();
       
            if(!blocking_per_req_type_hash_link15.containsKey(req_type))
            {
                //int cap = blocking_per_req_type_hash_link15.get(req_type);
                blocking_per_req_type_hash_link15.put(req_type, 1);
            }
            else
            {
                int existing_b = blocking_per_req_type_hash_link15.get(req_type);
                blocking_per_req_type_hash_link15.put(req_type, existing_b + 1);

            }       
   }
  public void add_to_admitted_list_link_type(String req_type)
   {
       //reinitialise
       //addmission_per_req_type_hash_link13 = new HashMap<>();
       initaddmission_per_connection_req_type_hash_13();

            if(!addmission_per_req_type_hash_link13.containsKey(req_type))
            {
                //int cap = addmission_per_req_type_hash_link13.get(req_type);
                addmission_per_req_type_hash_link13.put(req_type, 1);
            }
            else
            {
                int existing_b = addmission_per_req_type_hash_link13.get(req_type);
                addmission_per_req_type_hash_link13.put(req_type, existing_b + 1);

            }       
   } 
  
   public void add_to_blocking_list_node_type(String req_type)
   {
       //reinitialise
       //blocking_per_req_type_hash_node16 = new HashMap<>();
       initblocking_per_connection_req_type_hash_15();
       
       
            if(!blocking_per_req_type_hash_node16.containsKey(req_type))
            {
                //int cap = blocking_per_req_type_hash_link15.get(req_type);
                blocking_per_req_type_hash_node16.put(req_type, 1);
            }
            else
            {
                int existing_b = blocking_per_req_type_hash_node16.get(req_type);
                blocking_per_req_type_hash_node16.put(req_type, existing_b + 1);

            }       
   }
  public void add_to_admitted_list_node_type(String req_type)
   {
       //reinitialise       
       //addmission_per_req_type_hash_node14 = new HashMap<>();
       
       initaddmission_per_connection_req_type_hash_13();
       
            if(!addmission_per_req_type_hash_node14.containsKey(req_type))
            {
                //int cap = addmission_per_req_type_hash_link13.get(req_type);
                addmission_per_req_type_hash_node14.put(req_type, 1);
            }
            else
            {
                int existing_b = addmission_per_req_type_hash_node14.get(req_type);
                addmission_per_req_type_hash_node14.put(req_type, existing_b + 1);

            }       
   } 
  
   /////////////
  /*
   * the same function for the connection, but this time record the start 
   * types.: 4 methods: add and block for links and nodes
   */
   public void add_to_blocking_link_STAR_type(Req_IT_NET_timed_startnet Req)
   {
       //reinitialise
      // blocking_per_req_type_hash_link15 = new HashMap<>();
       initblocking_per_connection_req_type_hash_15();
       
//       this.blocking_general_link2++;
       String req_type = Req.returnStarReqType();
            if(!blocking_per_STARreq_type_hash_link41.containsKey(req_type))
            {
                //int cap = blocking_per_req_type_hash_link15.get(req_type);
                blocking_per_STARreq_type_hash_link41.put(req_type, 1);
            }
            else
            {
                int existing_b = blocking_per_STARreq_type_hash_link41.get(req_type);
                blocking_per_STARreq_type_hash_link41.put(req_type, existing_b + 1);

            }       
   }  
  public void add_to_admitted_link_STAR_type(Req_IT_NET_timed_startnet Req)
   {
       //reinitialise
       //addmission_per_req_type_hash_link13 = new HashMap<>();
       initaddmission_per_STARreq_type_hash40();
       
//       this.admitted_general_link1 ++;
       String req_type = Req.returnStarReqType();
            if(!addmission_per_STARreq_type_hash_link42.containsKey(req_type))
            {
                //int cap = addmission_per_req_type_hash_link13.get(req_type);
                addmission_per_STARreq_type_hash_link42.put(req_type, 1);
            }
            else
            {
                int existing_b = addmission_per_STARreq_type_hash_link42.get(req_type);
                addmission_per_STARreq_type_hash_link42.put(req_type, existing_b + 1);

            }       
   } 
  
   public void add_to_blocking_node_STAR_type(Req_IT_NET_timed_startnet Req)
   {
       //reinitialise
       //blocking_per_req_type_hash_node16 = new HashMap<>();
       init_blocking_star_request();
       
       String req_type = Req.returnStarReqType();
            if(!blocking_per_STARreq_type_hash_node43.containsKey(req_type))
            {
                //int cap = blocking_per_req_type_hash_link15.get(req_type);
                blocking_per_STARreq_type_hash_node43.put(req_type, 1);
            }
            else
            {
                int existing_b = blocking_per_STARreq_type_hash_node43.get(req_type);
                blocking_per_STARreq_type_hash_node43.put(req_type, existing_b + 1);

            }       
   }
  public void add_to_admitted_node_STAR_type(Req_IT_NET_timed_startnet Req)
   {
       //reinitialise       
       //addmission_per_req_type_hash_node14 = new HashMap<>();
       init_addmitting_star_request();
       
       String req_type = Req.returnStarReqType();
            if(!addmission_per_STARreq_type_hash_node44.containsKey(req_type))
            {
                //int cap = addmission_per_req_type_hash_link13.get(req_type);
                addmission_per_STARreq_type_hash_node44.put(req_type, 1);
            }
            else
            {
                int existing_b = addmission_per_STARreq_type_hash_node44.get(req_type);
                addmission_per_STARreq_type_hash_node44.put(req_type, existing_b + 1);

            }       
   } 
  
  /////
   public void add_to_blocking_Star_request(Req_IT_NET_timed_startnet Req)
   {
       //reinitialise
       //blocking_per_req_type_hash_node16 = new HashMap<>();
       initblocking_per_connection_req_type_hash_15();
       initaddmission_per_connection_req_type_hash_13();
       
       
       String req_type = Req.returnStarReqType();
            if(!blocking_per_star_req_type_hash.containsKey(req_type))
            {
                //int cap = blocking_per_req_type_hash_link15.get(req_type);
                blocking_per_star_req_type_hash.put(req_type, 1);
            }
            else
            {
                int existing_b = blocking_per_star_req_type_hash.get(req_type);
                blocking_per_star_req_type_hash.put(req_type, existing_b + 1);

            }       
   }
  public void add_to_admitted_Star_request(Req_IT_NET_timed_startnet Req)
   {
       //reinitialise       
       //addmission_per_req_type_hash_node14 = new HashMap<>();
       
       initblocking_per_connection_req_type_hash_15();
       initaddmission_per_connection_req_type_hash_13();
       
       String req_type = Req.returnStarReqType();
            if(!addmission_per_star_req_type_hash.containsKey(req_type))
            {
                //int cap = addmission_per_req_type_hash_link13.get(req_type);
                addmission_per_star_req_type_hash.put(req_type, 1);
            }
            else
            {
                int existing_b = addmission_per_star_req_type_hash.get(req_type);
                addmission_per_star_req_type_hash.put(req_type, existing_b + 1);

            }       
   } 
 
  
  
  
//   public void add_to_admission_list_node(int nodeid)
//   {
//            if(!nodes_used.contains(nodeid))
//            {
//                nodes_used.add(nodeid);
//            }     
//            if(!per_node_addmission32.containsKey(nodeid))
//            {
//                //int cap = blocking_per_req_type_hash_link15.get(req_type);
//                per_node_addmission32.put(nodeid, 1);
//            }
//            else
//            {
//                int existing_b = per_node_addmission32.get(nodeid);
//                per_node_addmission32.put(nodeid, existing_b + 1);
//
//            }       
//   }
   public void add_to_blocking_list_node(int nodeid)
   {

            if(!per_node_blocking33.containsKey(nodeid))
            {
                //int cap = blocking_per_req_type_hash_link15.get(req_type);
                per_node_blocking33.put(nodeid, 1);
            }
            else
            {
                int existing_b = per_node_blocking33.get(nodeid);
                per_node_blocking33.put(nodeid, existing_b + 1);

            }       
   }
   public void add_to_blocking_list_link(String lid)
   {

            if(!per_link_blocking31.containsKey(lid))
            {
                //int cap = blocking_per_req_type_hash_link15.get(req_type);
                per_link_blocking31.put(lid, 1);
            }
            else
            {
                int existing_b = per_link_blocking31.get(lid);
                per_link_blocking31.put(lid, existing_b + 1);

            }       
   }
//   public void add_to_admission_list_link(String lid)
//   {
//            if(!links_used.contains(lid))
//            {
//                links_used.add(lid);
//            }     
//       
//            if(!per_link_addmission30.containsKey(lid))
//            {
//                //int cap = blocking_per_req_type_hash_link15.get(req_type);
//                per_link_addmission30.put(lid, 1);
//            }
//            else
//            {
//                int existing_b = per_link_addmission30.get(lid);
//                per_link_addmission30.put(lid, existing_b + 1);
//
//            }       
//   }
   
   
   

   
  
  


  // total number of admissions
  public int return_total_admitted_link()
  {
      return this.admitted_general_link1;
  }
  public int return_total_admitted_node()
  {
      return this.admitted_general_node3;
  }  
  
  
  //total number of blockings
  public int return_total_blocked_link()
  {
      return this.blocking_general_link2;
  }  
  public int return_total_blocked_node()
  {
      return this.blocking_general_node4;
  }
  
 
  
  //List of network wide utilisations
  public HashMap<Integer,Integer> return_Total_util_of_link_layers()
  {
      return this.Total_util_of_link_layers5;
  }  
  public HashMap<Integer,Integer> return_Total_util_of_node_layers()
  {
      return this.Total_util_of_node_layers6;
  }  
  
  
  
  //hashmap<linkid,List<agg>> of links layers utilisation
  public HashMap<String,List> return_Util_per_link_layers()
  {
      return this.Util_per_link_layers7;
  }
  public HashMap<Integer,List> return_Util_per_node_layers()
  {
      return this.Util_per_node_layers8;
  }
  
    
  //hashmap<type,add load>
  public HashMap<String,Integer> return_All_hash_util_perType_link()
  {
      return this.All_hash_link_util_perType9;
  }  
  public HashMap<String,Integer> return_All_hash_util_perType_node()
  {
      return this.All_hash_node_util_perType10;
  }      
  
  //hashmap<link,type,add load>
  public HashMap<String,HashMap> return_Hashed_all_links_hash_util_perTypes()
  {
      return this.Hashed_all_links_hash_util_perTypes11;
  }
  public HashMap<Integer,HashMap> return_Hashed_all_nodes_hash_util_perTypes()
  {
      return this.Hashed_all_nodes_hash_util_perTypes12;
  }  
  

  
  
  public HashMap<String,Integer> return_addmission_per_req_type_hash_link()
  {
      return this.addmission_per_req_type_hash_link13;
  }  
  public HashMap<String,Integer> return_addmission_per_req_type_hash_node()
  {
      return this.addmission_per_req_type_hash_node14;
  }    
      
    
    
  
  public HashMap<String,Integer> return_blocking_per_req_type_hash_link()
  {
      return this.blocking_per_req_type_hash_link15;
  }  
  public HashMap<String,Integer> return_blocking_per_req_type_hash_node()
  {
      return this.blocking_per_req_type_hash_node16;
  }    
      

  
  //List of network wide utilisations
  public HashMap<Integer,Integer> return_all_link_layers_cap_hash()
  {
      return this.all_link_layers_cap_hash17;
  }  
  public HashMap<Integer,Integer> return_all_node_layers_cap_hash()
  {
      return this.all_node_layers_cap_hash18;
  }  

  
  //hashmap<type,add load>
  public HashMap<String,List> return_hash_all_link_caps()
  {
      return this.hash_all_link_type_caps19;
  }  
  public HashMap<String,List> return_hash_all_node_caps()
  {
      return this.hash_all_node_type_caps20;
  }          
  
    
  //List of network wide utilisations
  public HashMap<String,Integer> return_network_nodes_hash()
  {
      return this.hash_network_node_types21;
  }  
  public HashMap<String,Integer> return_network_links_hash()
  {
      return this.hash_network_link_types22;
  }  
  
  
  

  
    /*
     * in this mehod, i lppk into each links, get all the information 
     * required from per type per layer utilisations.
     * 
     * NOTE: why I am recreating the utilisation data structures.
     * The reason is, apaert from initialisation, I need to see the
     * network occupation at each time, when some connections are expired.
     * this is a differnet case than recording blockings and stuff. since 
     * they need to jsut aggregate
     */
    public void process_linkRec_objects(Network mNet, HashMap<String,Rec_Link_Allocs> link_Record_hash)
    {
        //Record_hash this hash map holds the information for all the links.
        //it is a hash of rec_link_allocs.
        //it is the keyset of all the links

        // I need to re initialise these vairaible, so to avoid accumulating the 
        //previous coolections againa and again
        Util_all_links_pertype_alllayers23 = new HashMap<>();
        
        ////will fix it later on
        init_reqType_allLayers_hashList_link23(LinkresInfoSize);
        
        Total_util_of_link_layers5 = new HashMap<>();
        
        
        All_hash_link_util_perType9 = new HashMap<>();
        initAll_hash_link_util_perType9();  
        Hashed_all_links_hash_util_perTypes11 = new HashMap<>();
        
        //HashMap<String,Integer> per_link_per_type_util
        String[] key_arr_links = Converters.key_set_obj_to_str_array( link_Record_hash);
        if(Debug)
        Print.println("  in process_linkRec_objects: " + key_arr_links.length);
        for (int i = 0; i < key_arr_links.length; i++)
        {
            String link_id = key_arr_links[i]; 
            
            // for each link, I have recording hash, which keeps the allocated info
            Rec_Link_Allocs rec_link =  link_Record_hash.get(link_id);
            
            //I have embedded the utilisation info extraction in each link record objet
            //I read all the alloc objects in each recording unit, and return the
            //usage per type. 
            HashMap<String,Integer> hash_util_perType = rec_link.return_max_layer_util_of_link_allocs_per_type();
            //here, I am making my hash<link_id, links_hash> of links hash<type,load>
            Hashed_all_links_hash_util_perTypes11.put(link_id, hash_util_perType);

//            List<Integer> link_utilres_layers = rec_link.return_all_layer_util_of_link_allocs();
                        
            
            //this is the aggregation of all links, aggregate usage in differnet layers
            List<Integer> link_utilres_layers = rec_link.return_all_layer_util_of_link_allocs_modified(mNet);
            Util_per_link_layers7.put(link_id, link_utilres_layers);
            
            
            for(int j = 0; j < link_utilres_layers.size(); j++)
            {
                //Print.println("  tracking Type_aa_util in layers....Total_util_of_link_layers5: " + Total_util_of_link_layers5.toString());
                if(Total_util_of_link_layers5.containsKey(j)){
                    
                    
                    int ex_util = Total_util_of_link_layers5.get(j);
                    int this_util = link_utilres_layers.get(j);
                    Total_util_of_link_layers5.remove(j);
                    Total_util_of_link_layers5.put(j, this_util + ex_util);
                    //Print.println(" gggggggggggggggggggg ex_util " + ex_util + " this_util " + this_util) ;
                }
                else{
                    int this_util = link_utilres_layers.get(j);
                    Total_util_of_link_layers5.put(j, this_util);                    
                }
            }

            //HashMap<String,List> type_resources_per_link = rec_link.return_all_layers_util_of_allocs_per_type();
            HashMap<String,List> type_resources_per_link = rec_link.return_all_layers_util_of_link_allocs_per_type_modified();

           
            //<  this bit here works on the types
            /*
             * *********************************************************
             */
            
            //from here on wards, is for making the aggregation hash
            String[] types_arr = Converters.key_set_obj_to_str_array(type_resources_per_link);
            for(int j = 0; j < types_arr.length; j++)
            {
                String req_type = types_arr[j];// the requests type
                List<Integer> type_utils = new ArrayList<>();
                type_utils = type_resources_per_link.get(req_type);


                //here, per each type, I accumultate the allocs.
                // so I get per link/ per type accumulated allocatoins
                if(!Util_all_links_pertype_alllayers23.containsKey(req_type))
                {
                    Util_all_links_pertype_alllayers23.put(req_type, type_utils);
                    
                }
                else
                {
                    List<Integer> type_resources_already = Util_all_links_pertype_alllayers23.remove(req_type);

                    
                    for(int k =0;k <LinkresInfoSize;k++ )
                    {
                        int all_ones = type_utils.get(k);
                        
                        int layers_allocs_already = type_resources_already.get(k);
                        
                        all_ones = layers_allocs_already + all_ones;
                        type_resources_already.remove(k);
                        type_resources_already.add(k,all_ones);

                    }        

                    Util_all_links_pertype_alllayers23.put(req_type, type_resources_already);
                    if(Debug)
                    Print.println(" in Util_all_links_pertype_alllayers23: " + Util_all_links_pertype_alllayers23.toString());
                


                }                
                //Print.println(" in statistics collector link key_arr[i]: " + key_arr_links[i] +" \tthe type: " + types_arr[j] + "\tthe load: " + agg_load);
            }             
            

            
            
            /*
             * now each link, I am reading aggregate allocs per each type
             * then in each link, I look for differnet requests types.
            */ 
            for(int j = 0; j < types_arr.length; j++)
            {
                String req_type = types_arr[j];// the requests type
                int agg_load = hash_util_perType.get(req_type);// the associated utilisation
                
               

                //here, per each type, I accumultate the allocs.
                // so I get per link/ per type accumulated allocatoins
                if(!All_hash_link_util_perType9.containsKey(req_type))
                {
                    All_hash_link_util_perType9.put(req_type, agg_load);
                }
                else
                {
                    int allocs_already = All_hash_link_util_perType9.get(req_type);
                    agg_load = allocs_already + agg_load;
                    All_hash_link_util_perType9.put(req_type, agg_load);

                }  
                if(Debug)
                Print.printResHash_StrInt(" All_hash_link_util_perType9  ", All_hash_link_util_perType9);
            }
        }
    }  
    
  /*
     * in this mehod, i lppk into each links, get all the information 
     * required from per type per layer utilisations
     */
    
 public void process_nodeRec_objects(Network mNet, HashMap<Integer,Rec_Node_Allocs> node_Record_hash)
    {
        //Record_hash this hash map holds the information for all the nodes.
        //it is a hash of rec_node_allocs.
        //it is the keyset of all the nodes

        // I need to re initialise these vairaible, so to avoid accumulating the 
        //previous coolections againa and again
        
        Print.println("  in node process_ " );
        
        
        Util_all_nodes_pertype_alllayers24 = new HashMap<>();
//        
//        ////will fix it later on
        init_reqType_allLayers_hashList_node23(NoderesInfoSize);
        
        Total_util_of_node_layers6 = new HashMap<>();
        
        
        All_hash_node_util_perType10 = new HashMap<>();
        initAll_hash_node_util_perType9();  
        Hashed_all_nodes_hash_util_perTypes12 = new HashMap<>();
        
        //HashMap<String,Integer> per_node_per_type_util
        int[] key_arr_nodes = Converters.key_set_obj_to_int_array(node_Record_hash);
//        if(Debug)
        Print.println("  in process_nodeRec_objects: " + key_arr_nodes.length);
        for (int i = 0; i < key_arr_nodes.length; i++)
        {
            int node_id = key_arr_nodes[i]; 
            
            // for each node, I have recording hash, which keeps the allocated info
            Rec_Node_Allocs rec_node = node_Record_hash.get(node_id);
//            Print.println("  node_id: " + node_id);
            
            //I have embedded the utilisation info extraction in each node record objet
            //I read all the alloc objects in each recording unit, and return the
            //usage per type. 
//            Print.println("  rec_noderec_noderec_node " + rec_node.return_all_layer_util_of_node_allocs().toString());

            Node n = mNet.getNode(node_id);
//            Print.println(" nnnn nnnn  " + n.get_ParentClass_resrouces().getme_List_all_layered_resources());
            
            
            
            HashMap<String,Integer> hash_util_perType = rec_node.return_max_layer_util_of_node_allocs_per_type();

//            Print.printResHash_StrInt("  see the rec node hash util per type " , hash_util_perType);
            
            //here, I am making my hash<node_id, nodes_hash> of nodes hash<type,load>
            Hashed_all_nodes_hash_util_perTypes12.put(node_id, hash_util_perType);
 
            
            
//            List<Integer> node_utilres_layers = rec_node.return_all_layer_util_of_node_allocs();
                        
            
            //this is the aggregation of all nodes, aggregate usage in differnet layers
            List<Integer> node_utilres_layers = rec_node.return_all_layer_util_of_node_allocs_modified(mNet);
            Util_per_node_layers8.put(node_id, node_utilres_layers);
//            Print.println(" node_utilres_layersnode_utilres_layers " + node_utilres_layers.toString()) ;
            
            for(int j = 0; j < node_utilres_layers.size(); j++)
            {
                //Print.println("  tracking Type_aa_util in layers....Total_util_of_node_layers5: " + Total_util_of_node_layers5.toString());
                if(Total_util_of_node_layers6.containsKey(j)){
                    
                    
                    int ex_util = Total_util_of_node_layers6.get(j);
                    int this_util = node_utilres_layers.get(j);
                    Total_util_of_node_layers6.remove(j);
                    Total_util_of_node_layers6.put(j, this_util + ex_util);
//                    Print.println(" gggggggggggggggggggg ex_util " + ex_util + " this_util " + this_util) ;
                }
                else{
                    int this_util = node_utilres_layers.get(j);
                    Total_util_of_node_layers6.put(j, this_util);                    
                }
            }

//            HashMap<String,List> type_resources_per_node = rec_node.return_all_layers_util_of_node_allocs_per_type();
            HashMap<String,List> type_resources_per_node = rec_node.return_all_layers_util_of_node_allocs_per_type_modified();

           
            //<  this bit here works on the types
            /*
             * *********************************************************
             */
            
            //from here on wards, is for making the aggregation hash
            String[] types_arr = Converters.key_set_obj_to_str_array(type_resources_per_node);
            for(int j = 0; j < types_arr.length; j++)
            {
                String req_type = types_arr[j];// the requests type
                List<Integer> type_utils = new ArrayList<>();
                type_utils = type_resources_per_node.get(req_type);
//                    Print.println(" in type_utilstype_utils: " + type_utils.toString());


                //here, per each type, I accumultate the allocs.
                // so I get per node/ per type accumulated allocatoins
                if(!Util_all_nodes_pertype_alllayers24.containsKey(req_type))
                {
                    Util_all_nodes_pertype_alllayers24.put(req_type, type_utils);
                    
                }
                else
                {
                    List<Integer> type_resources_already = Util_all_nodes_pertype_alllayers24.remove(req_type);

                    
                    for(int k =0;k <NoderesInfoSize;k++ )
                    {
                        int all_ones = type_utils.get(k);
                        
                        int layers_allocs_already = type_resources_already.get(k);
                        
                        all_ones = layers_allocs_already + all_ones;
                        type_resources_already.remove(k);
                        type_resources_already.add(k,all_ones);

                    }        

                    Util_all_nodes_pertype_alllayers24.put(req_type, type_resources_already);
                    
                    if(Debug)
                    Print.println(" in Util_all_nodes_pertype_alllayers24: " + Util_all_nodes_pertype_alllayers24.toString());
                


                }                
//                Print.println(" in statistics collector node key_arr[i]: " + key_arr_nodes[i] +" \tthe type: " + types_arr[j] + "\tthe load: " + agg_load);
            }             
            

            
            
            /*
             * now each node, I am reading aggregate allocs per each type
             * then in each node, I look for differnet requests types.
            */ 
            for(int j = 0; j < types_arr.length; j++)
            {
                String req_type = types_arr[j];// the requests type
                int agg_load = hash_util_perType.get(req_type);// the associated utilisation
 
                //here, per each type, I accumultate the allocs.
                // so I get per node/ per type accumulated allocatoins
                if(!All_hash_node_util_perType10.containsKey(req_type))
                {
                    All_hash_node_util_perType10.put(req_type, agg_load);
                }
                else
                {
                    int allocs_already = All_hash_node_util_perType10.get(req_type);
//                    System.out.println( "allocs_already: " +allocs_already +  "  agg_load:: " + agg_load + " and req_type: " + req_type );
                    
                    agg_load = allocs_already + agg_load;
                    All_hash_node_util_perType10.put(req_type, agg_load);

                }  
//                if(Debug)
//                Print.printResHash_StrInt(" All_hash_node_util_perType10  ", All_hash_node_util_perType10);
            }
        }
    }  
        
    
//    public void process_nodeRec_objects()
//    {
//        //Record_hash this hash map holds the information for all the links.
//        //it is a hash of rec_link_allocs.
//        //it is the keyset of all the links
//
//        // I need to re initialise these vairaible, so to avoid accumulating the 
//        //previous coolections againa and again
//        
//        
//        Util_all_nodes_pertype_alllayers24 = new HashMap<>();
//        init_reqType_allLayers_hashList_node23(LinkresInfoSize);
//        All_hash_node_util_perType10 = new HashMap<>();
//        initAll_hash_node_util_perType9();          
//        Hashed_all_nodes_hash_util_perTypes12 = new HashMap<>();
//        Total_util_of_node_layers6 = new HashMap<>();
//                
//        //HashMap<String,Integer> per_link_per_type_util
//        String[] key_arr_links = Converters.key_set_obj_to_str_array(this.Link_Record_hash);
//        for (int i = 0; i < key_arr_links.length; i++)
//        {
//            String link_id = key_arr_links[i]; 
//            
//            // for each link, I have recording hash, which keeps the allocated info
//            Rec_Link_Allocs rec_link = this.Link_Record_hash.get(link_id);
//            
//            //I have embedded the utilisation info extraction in each link record objet
//            //I read all the alloc objects in each recording unit, and return the
//            //usage per type. 
//            HashMap<String,Integer> hash_util_perType = rec_link.return_max_layer_util_of_link_allocs_per_type();
//            
//            //here, I am making my hash<link_id, links_hash> of links hash<type,load>
//            Hashed_all_nodes_hash_util_perTypes12.put(link_id, hash_util_perType);
//            
//
//            
//            //this is the aggregation of all links, aggregate usage in differnet layers
//            List<Integer> node_utilres_layers = rec_link.return_all_layer_util_of_link_allocs();
//            Util_per_node_layers8.put(link_id, node_utilres_layers);
//            
//            for(int j = 0; j < node_utilres_layers.size(); j++)
//            {
//                if(Total_util_of_node_layers6.containsKey(j)){
//                    int ex_util = Total_util_of_node_layers6.get(j);
//                    int this_util = node_utilres_layers.get(j);
//                    Total_util_of_node_layers6.remove(j);
//                    Total_util_of_node_layers6.put(j, this_util + ex_util);
//                }
//                else{
//                    int this_util = node_utilres_layers.get(j);
//                    Total_util_of_node_layers6.put(j, this_util);                    
//                }
//            }
//            
//            
//            
//            
//            //from here on wards, is for making the aggregation hash
//            String[] types_arr = Converters.key_set_obj_to_str_array(hash_util_perType);
//        
//            /*
//             * now each link, I am reading aggregate allocs per each type
//             * then in each link, I look for differnet requests types.
//            */ 
//            for(int j = 0; j < types_arr.length; j++)
//            {
//                String req_type = types_arr[j];// the requests type
//                int agg_load = hash_util_perType.get(req_type);// the associated utilisation
//
//                //here, per each type, I accumultate the allocs.
//                // so I get per link/ per type accumulated allocatoins
//                if(!All_hash_node_util_perType10.containsKey(req_type))
//                {
//                    All_hash_node_util_perType10.put(req_type, agg_load);
//                }
//                else
//                {
//                    int allocs_already = All_hash_node_util_perType10.get(req_type);
//                    agg_load = allocs_already + agg_load;
//                    All_hash_node_util_perType10.put(req_type, agg_load);
//
//                }                
//                //Print.println(" in statistics collector link key_arr[i]: " + key_arr_links[i] +" \tthe type: " + types_arr[j] + "\tthe load: " + agg_load);
//            }
//        }
//    }     
    public void print_them_in_excel(int line, int horiz)
    {
            Print.println(" **********************calling to print");
        
//        print_network_info();
        if(Debug)
        {
            Print.println(" ********************** now time to print excel stufff  in line:"
                    + "  "  + line + "  \tin horiz: "+ horiz);
            Print.printResHash_IntInt(" look  Total_util_of_link_layers5   ", Total_util_of_link_layers5);
            Print.printResHash_IntInt(" look  all_link_layers_cap_hash17   ", all_link_layers_cap_hash17);
    //        double Type_aa_util = (double)Total_util_of_link_layers5.get(1) /all_link_layers_cap_hash17.get(1);
    //        
    //        double Type_bb_util = (double)Total_util_of_link_layers5.get(2) /all_link_layers_cap_hash17.get(2);
    //        
    //        Print.println(" look  typeaa   "+Type_aa_util);

            Print.println("  admitted_general_link1 " + admitted_general_link1);                    
            Print.println("  blocking_general_link2 " + blocking_general_link2);                    
            Print.println(" bubu blocking_per_req_type_hash_link15) " + blocking_per_req_type_hash_link15.get("aa"));                    


            Print.println("  addmission_per_req_type_hash_link13.get(\"aa\") " + addmission_per_req_type_hash_link13.get("aa"));                    
            Print.println("  blocking_per_req_type_hash_link15.get(\"aa\") " + blocking_per_req_type_hash_link15.get("aa"));                    
            Print.println("  (double)blocking_per_req_type_hash_link15.get(\"aa\")/(addmission_per_req_type_hash_link13.get(\"aa\")+blocking_per_req_type_hash_link15.get(\"aa\")) " + 
                    (double)blocking_per_req_type_hash_link15.get("aa")/(addmission_per_req_type_hash_link13.get("aa")+blocking_per_req_type_hash_link15.get("aa")));                    
            Print.println("  All_hash_link_util_perType9.get(\"aa\") " + All_hash_link_util_perType9.get("aa"));        
            Print.println("  (double)all_link_layers_cap_hash17.get(2) " + (double)all_link_layers_cap_hash17.get(2));        
            Print.println("  (double)All_hash_link_util_perType9.get(\"aa\")/(double)all_link_layers_cap_hash17.get(2)) " + 
                    (double)All_hash_link_util_perType9.get("aa")/((double)all_link_layers_cap_hash17.get(2)));        

            Print.println("  Nodes allocs print check" );                    
            Print.println("  addmission_per_req_type_hash_node14.get(\"aa\") " + addmission_per_req_type_hash_node14.get("aa"));                    
            Print.println("  blocking_per_req_type_hash_node16.get(\"aa\") " + blocking_per_req_type_hash_node16.get("aa"));                    
            Print.println("  (double)blocking_per_req_type_hash_node16.get(\"aa\")/(addmission_per_req_type_hash_node14.get(\"aa\")+blocking_per_req_type_hash_node16.get(\"aa\")) " + 
                    (double)blocking_per_req_type_hash_node16.get("aa")/(addmission_per_req_type_hash_node14.get("aa")+blocking_per_req_type_hash_node16.get("aa")));                    
            Print.println("  All_hash_node_util_perType10.get(\"aa\") " + All_hash_node_util_perType10.get("aa"));        
            Print.println("  (double)all_node_layers_cap_hash18.get(2) " + (double)all_node_layers_cap_hash18.get(2));        
            Print.println("  (double)All_hash_node_util_perType10.get(\"aa\")/(double)all_node_layers_cap_hash18.get(2)) " + 
                    (double)All_hash_node_util_perType10.get("aa")/((double)all_node_layers_cap_hash18.get(2)));        

            Print.println("  addmission_per_req_type_hash_link13.get(\"bb\") " + addmission_per_req_type_hash_link13.get("bb"));                    
            Print.println("  blocking_per_req_type_hash_link15.get(\"bb\") " + blocking_per_req_type_hash_link15.get("bb"));                    
            Print.println("  (double)blocking_per_req_type_hash_link15.get(\"bb\")/(addmission_per_req_type_hash_link13.get(\"bb\")+blocking_per_req_type_hash_link15.get(\"bb\")) " + 
                    (double)blocking_per_req_type_hash_link15.get("bb")/(addmission_per_req_type_hash_link13.get("bb")+blocking_per_req_type_hash_link15.get("bb")));                    
            Print.println("  All_hash_link_util_perType9.get(\"bb\") " + All_hash_link_util_perType9.get("bb"));        
            Print.println("  (double)all_link_layers_cap_hash17.get(0) " + (double)all_link_layers_cap_hash17.get(1));        
            Print.println("  (double)All_hash_link_util_perType9.get(\"bb\")/((double)all_link_layers_cap_hash17.get(1)) " + 
                    (double)All_hash_link_util_perType9.get("bb")/((double)all_link_layers_cap_hash17.get(1)));        


            Print.println("  addmission_per_req_type_hash_link13.get(\"cc\") " + addmission_per_req_type_hash_link13.get("cc"));                    
            Print.println("  blocking_per_req_type_hash_link15.get(\"cc\") " + blocking_per_req_type_hash_link15.get("cc"));                    
            Print.println("  (double)blocking_per_req_type_hash_link15.get(\"cc\")/(addmission_per_req_type_hash_link13.get(\"cc\")+blocking_per_req_type_hash_link15.get(\"cc\")) " + 
                    (double)blocking_per_req_type_hash_link15.get("cc")/(addmission_per_req_type_hash_link13.get("cc")+blocking_per_req_type_hash_link15.get("cc")));                    
            Print.println("  All_hash_link_util_perType9.get(\"cc\") " + All_hash_link_util_perType9.get("cc"));        
            Print.println("  (double)all_link_layers_cap_hash17.get(1) " + (double)all_link_layers_cap_hash17.get(1));        
            Print.println("  (double)All_hash_link_util_perType9.get(\"cc\")/((double)all_link_layers_cap_hash17.get(1)) " + 
                    (double)All_hash_link_util_perType9.get("cc")/((double)all_link_layers_cap_hash17.get(1)));        

                    Print.println("  addmission_per_req_type_hash_link13.get(\"dd\") " + addmission_per_req_type_hash_link13.get("dd"));                    
            Print.println("  blocking_per_req_type_hash_link15.get(\"dd\") " + blocking_per_req_type_hash_link15.get("dd"));                    
            Print.println("  (double)blocking_per_req_type_hash_link15.get(\"dd\")/(addmission_per_req_type_hash_link13.get(\"dd\")+blocking_per_req_type_hash_link15.get(\"dd\")) " + 
                    (double)blocking_per_req_type_hash_link15.get("dd")/(addmission_per_req_type_hash_link13.get("dd")+blocking_per_req_type_hash_link15.get("dd")));                    
            Print.println("  All_hash_link_util_perType9.get(\"dd\") " + All_hash_link_util_perType9.get("dd"));        
            Print.println("  (double)all_link_layers_cap_hash17.get(0) " + (double)all_link_layers_cap_hash17.get(1));        
            Print.println("  (double)All_hash_link_util_perType9.get(\"dd\")/((double)all_link_layers_cap_hash17.get(1)) " + 
                    (double)All_hash_link_util_perType9.get("dd")/((double)all_link_layers_cap_hash17.get(1)));        

                    Print.println("  aeemission_per_req_type_hash_link13.get(\"ee\") " + addmission_per_req_type_hash_link13.get("ee"));                    
            Print.println("  blocking_per_req_type_hash_link15.get(\"ee\") " + blocking_per_req_type_hash_link15.get("ee"));                    
            Print.println("  (double)blocking_per_req_type_hash_link15.get(\"ee\")/(aeemission_per_req_type_hash_link13.get(\"ee\")+blocking_per_req_type_hash_link15.get(\"ee\")) " + 
                    (double)blocking_per_req_type_hash_link15.get("ee")/(addmission_per_req_type_hash_link13.get("ee")+blocking_per_req_type_hash_link15.get("ee")));                    
            Print.println("  All_hash_link_util_perType9.get(\"ee\") " + All_hash_link_util_perType9.get("ee"));        
            Print.println("  (double)all_link_layers_cap_hash17.get(1) " + (double)all_link_layers_cap_hash17.get(1));        
            Print.println("  (double)All_hash_link_util_perType9.get(\"ee\")/((double)all_link_layers_cap_hash17.get(1)) " + 
                    (double)All_hash_link_util_perType9.get("ee")/((double)all_link_layers_cap_hash17.get(1)));        

            Print.println("\n\n  (double)Util_all_links_pertype_alllayers23.get(aa/).get(0) " + Util_all_links_pertype_alllayers23.get("aa").get(0));        
            Print.println("  (double)Util_all_links_pertype_alllayers23.get(aa/).get(1) " + Util_all_links_pertype_alllayers23.get("aa").get(1));        
            Print.println("  (double)Util_all_links_pertype_alllayers23.get(aa/).get(2) " + Util_all_links_pertype_alllayers23.get("aa").get(2));        
            Print.println("  (double)Util_all_links_pertype_alllayers23.get(aa/).get(3) " + Util_all_links_pertype_alllayers23.get("aa").get(3));        
            Print.println("  (double)Util_all_links_pertype_alllayers23.get(bb/).get(0) " + Util_all_links_pertype_alllayers23.get("bb").get(0));        
            Print.println("  (double)Util_all_links_pertype_alllayers23.get(bb/).get(1) " + Util_all_links_pertype_alllayers23.get("bb").get(1));        
            Print.println("  (double)Util_all_links_pertype_alllayers23.get(bb/).get(2) " + Util_all_links_pertype_alllayers23.get("bb").get(2));        
            Print.println("  (double)Util_all_links_pertype_alllayers23.get(bb/).get(3) " + Util_all_links_pertype_alllayers23.get("bb").get(3));        
            Print.println("  (double)Util_all_links_pertype_alllayers23.get(cc/).get(0) " + Util_all_links_pertype_alllayers23.get("cc").get(0));        
            Print.println("  (double)Util_all_links_pertype_alllayers23.get(cc/).get(1) " + Util_all_links_pertype_alllayers23.get("cc").get(1));        
            Print.println("  (double)Util_all_links_pertype_alllayers23.get(cc/).get(2) " + Util_all_links_pertype_alllayers23.get("cc").get(2));        
            Print.println("  (double)Util_all_links_pertype_alllayers23.get(cc/).get(3) " + Util_all_links_pertype_alllayers23.get("cc").get(3));        
            Print.println("  (double)Util_all_links_pertype_alllayers23.get(dd/).get(0) " + Util_all_links_pertype_alllayers23.get("dd").get(0));        
            Print.println("  (double)Util_all_links_pertype_alllayers23.get(dd/).get(1) " + Util_all_links_pertype_alllayers23.get("dd").get(1));        
            Print.println("  (double)Util_all_links_pertype_alllayers23.get(dd/).get(2) " + Util_all_links_pertype_alllayers23.get("dd").get(2));        
            Print.println("  (double)Util_all_links_pertype_alllayers23.get(dd/).get(3) " + Util_all_links_pertype_alllayers23.get("dd").get(3));        
            Print.println("  (double)Util_all_links_pertype_alllayers23.get(ee/).get(0) " + Util_all_links_pertype_alllayers23.get("ee").get(0));        
            Print.println("  (double)Util_all_links_pertype_alllayers23.get(ee/).get(1) " + Util_all_links_pertype_alllayers23.get("ee").get(1));        
            Print.println("  (double)Util_all_links_pertype_alllayers23.get(ee/).get(2) " + Util_all_links_pertype_alllayers23.get("ee").get(2));        
            Print.println("  (double)Util_all_links_pertype_alllayers23.get(ee/).get(3) " + Util_all_links_pertype_alllayers23.get("ee").get(3));        

            Print.println("  (double)all_link_layers_cap_hash17.get(0) " + (double)all_link_layers_cap_hash17.get(0));     
            
            
            //nodes
            
            Print.println("  addmission_per_req_type_hash_node14.get(\"aa\") " + addmission_per_req_type_hash_node14.get("aa"));                    
            Print.println("  blocking_per_req_type_hash_node16.get(\"aa\") " + blocking_per_req_type_hash_node16.get("aa"));                    
            Print.println("  (double)blocking_per_req_type_hash_node16.get(\"aa\")/(addmission_per_req_type_hash_node14.get(\"aa\")+blocking_per_req_type_hash_node16.get(\"aa\")) " + 
                    (double)blocking_per_req_type_hash_node16.get("aa")/(addmission_per_req_type_hash_node14.get("aa")+blocking_per_req_type_hash_node16.get("aa")));                    
            Print.println("  All_hash_node_util_perType10.get(\"aa\") " + All_hash_node_util_perType10.get("aa"));        
            Print.println("  (double)all_node_layers_cap_hash18.get(2) " + (double)all_node_layers_cap_hash18.get(2));        
            Print.println("  (double)All_hash_node_util_perType10.get(\"aa\")/(double)all_node_layers_cap_hash18.get(2)) " + 
                    (double)All_hash_node_util_perType10.get("aa")/((double)all_node_layers_cap_hash18.get(2)));        

            Print.println("  addmission_per_req_type_hash_node14.get(\"bb\") " + addmission_per_req_type_hash_node14.get("bb"));                    
            Print.println("  blocking_per_req_type_hash_node16.get(\"bb\") " + blocking_per_req_type_hash_node16.get("bb"));                    
            Print.println("  (double)blocking_per_req_type_hash_node16.get(\"bb\")/(addmission_per_req_type_hash_node14.get(\"bb\")+blocking_per_req_type_hash_node16.get(\"bb\")) " + 
                    (double)blocking_per_req_type_hash_node16.get("bb")/(addmission_per_req_type_hash_node14.get("bb")+blocking_per_req_type_hash_node16.get("bb")));                    
            Print.println("  All_hash_node_util_perType10.get(\"bb\") " + All_hash_node_util_perType10.get("bb"));        
            Print.println("  (double)all_node_layers_cap_hash18.get(0) " + (double)all_node_layers_cap_hash18.get(1));        
//            Print.println("  (double)All_hash_node_util_perType10.get(\"bb\")/((double)all_node_layers_cap_hash18.get(1)) " + 
//                    (double)All_hash_node_util_perType10.get("bb")/((double)all_node_layers_cap_hash18.get(1)));        
            Print.println("  (double)All_hash_node_util_perType10.get(\"bb\")/((double)all_node_layers_cap_hash18.get(1)) " + 
                    (double)All_hash_node_util_perType10.get("bb"));        


            Print.println("  addmission_per_req_type_hash_node14.get(\"cc\") " + addmission_per_req_type_hash_node14.get("cc"));                    
            Print.println("  blocking_per_req_type_hash_node16.get(\"cc\") " + blocking_per_req_type_hash_node16.get("cc"));                    
            Print.println("  (double)blocking_per_req_type_hash_node16.get(\"cc\")/(addmission_per_req_type_hash_node14.get(\"cc\")+blocking_per_req_type_hash_node16.get(\"cc\")) " + 
                    (double)blocking_per_req_type_hash_node16.get("cc")/(addmission_per_req_type_hash_node14.get("cc")+blocking_per_req_type_hash_node16.get("cc")));                    
            Print.println("  All_hash_node_util_perType10.get(\"cc\") " + All_hash_node_util_perType10.get("cc"));        
            Print.println("  (double)all_node_layers_cap_hash18.get(1) " + (double)all_node_layers_cap_hash18.get(1));        
            Print.println("  (double)All_hash_node_util_perType10.get(\"cc\")/((double)all_node_layers_cap_hash18.get(1)) " + 
                    (double)All_hash_node_util_perType10.get("cc")/((double)all_node_layers_cap_hash18.get(1)));        

                    Print.println("  addmission_per_req_type_hash_node14.get(\"dd\") " + addmission_per_req_type_hash_node14.get("dd"));                    
            Print.println("  blocking_per_req_type_hash_node16.get(\"dd\") " + blocking_per_req_type_hash_node16.get("dd"));                    
            Print.println("  (double)blocking_per_req_type_hash_node16.get(\"dd\")/(addmission_per_req_type_hash_node14.get(\"dd\")+blocking_per_req_type_hash_node16.get(\"dd\")) " + 
                    (double)blocking_per_req_type_hash_node16.get("dd")/(addmission_per_req_type_hash_node14.get("dd")+blocking_per_req_type_hash_node16.get("dd")));                    
            Print.println("  All_hash_node_util_perType10.get(\"dd\") " + All_hash_node_util_perType10.get("dd"));        
            Print.println("  (double)all_node_layers_cap_hash18.get(0) " + (double)all_node_layers_cap_hash18.get(1));        
            Print.println("  (double)All_hash_node_util_perType10.get(\"dd\")/((double)all_node_layers_cap_hash18.get(1)) " + 
                    (double)All_hash_node_util_perType10.get("dd")/((double)all_node_layers_cap_hash18.get(1)));        

                    Print.println("  aeemission_per_req_type_hash_node14.get(\"ee\") " + addmission_per_req_type_hash_node14.get("ee"));                    
            Print.println("  blocking_per_req_type_hash_node16.get(\"ee\") " + blocking_per_req_type_hash_node16.get("ee"));                    
            Print.println("  (double)blocking_per_req_type_hash_node16.get(\"ee\")/(aeemission_per_req_type_hash_node14.get(\"ee\")+blocking_per_req_type_hash_node16.get(\"ee\")) " + 
                    (double)blocking_per_req_type_hash_node16.get("ee")/(addmission_per_req_type_hash_node14.get("ee")+blocking_per_req_type_hash_node16.get("ee")));                    
            Print.println("  All_hash_node_util_perType10.get(\"ee\") " + All_hash_node_util_perType10.get("ee"));        
            Print.println("  (double)all_node_layers_cap_hash18.get(1) " + (double)all_node_layers_cap_hash18.get(1));        
            Print.println("  (double)All_hash_node_util_perType10.get(\"ee\")/((double)all_node_layers_cap_hash18.get(1)) " + 
                    (double)All_hash_node_util_perType10.get("ee")/((double)all_node_layers_cap_hash18.get(1)));        

            Print.println("\n\n  (double)Util_all_nodes_pertype_alllayers24.get(aa/).get(0) " + Util_all_nodes_pertype_alllayers24.get("aa").get(0));        
            Print.println("  (double)Util_all_nodes_pertype_alllayers24.get(aa/).get(1) " + Util_all_nodes_pertype_alllayers24.get("aa").get(1));        
            Print.println("  (double)Util_all_nodes_pertype_alllayers24.get(aa/).get(2) " + Util_all_nodes_pertype_alllayers24.get("aa").get(2));        
            Print.println("  (double)Util_all_nodes_pertype_alllayers24.get(aa/).get(3) " + Util_all_nodes_pertype_alllayers24.get("aa").get(3));        
            Print.println("  (double)Util_all_nodes_pertype_alllayers24.get(bb/).get(0) " + Util_all_nodes_pertype_alllayers24.get("bb").get(0));        
            Print.println("  (double)Util_all_nodes_pertype_alllayers24.get(bb/).get(1) " + Util_all_nodes_pertype_alllayers24.get("bb").get(1));        
            Print.println("  (double)Util_all_nodes_pertype_alllayers24.get(bb/).get(2) " + Util_all_nodes_pertype_alllayers24.get("bb").get(2));        
            Print.println("  (double)Util_all_nodes_pertype_alllayers24.get(bb/).get(3) " + Util_all_nodes_pertype_alllayers24.get("bb").get(3));        
            Print.println("  (double)Util_all_nodes_pertype_alllayers24.get(cc/).get(0) " + Util_all_nodes_pertype_alllayers24.get("cc").get(0));        
            Print.println("  (double)Util_all_nodes_pertype_alllayers24.get(cc/).get(1) " + Util_all_nodes_pertype_alllayers24.get("cc").get(1));        
            Print.println("  (double)Util_all_nodes_pertype_alllayers24.get(cc/).get(2) " + Util_all_nodes_pertype_alllayers24.get("cc").get(2));        
            Print.println("  (double)Util_all_nodes_pertype_alllayers24.get(cc/).get(3) " + Util_all_nodes_pertype_alllayers24.get("cc").get(3));        
            Print.println("  (double)Util_all_nodes_pertype_alllayers24.get(dd/).get(0) " + Util_all_nodes_pertype_alllayers24.get("dd").get(0));        
            Print.println("  (double)Util_all_nodes_pertype_alllayers24.get(dd/).get(1) " + Util_all_nodes_pertype_alllayers24.get("dd").get(1));        
            Print.println("  (double)Util_all_nodes_pertype_alllayers24.get(dd/).get(2) " + Util_all_nodes_pertype_alllayers24.get("dd").get(2));        
            Print.println("  (double)Util_all_nodes_pertype_alllayers24.get(dd/).get(3) " + Util_all_nodes_pertype_alllayers24.get("dd").get(3));        
            Print.println("  (double)Util_all_nodes_pertype_alllayers24.get(ee/).get(0) " + Util_all_nodes_pertype_alllayers24.get("ee").get(0));        
            Print.println("  (double)Util_all_nodes_pertype_alllayers24.get(ee/).get(1) " + Util_all_nodes_pertype_alllayers24.get("ee").get(1));        
            Print.println("  (double)Util_all_nodes_pertype_alllayers24.get(ee/).get(2) " + Util_all_nodes_pertype_alllayers24.get("ee").get(2));        
            Print.println("  (double)Util_all_nodes_pertype_alllayers24.get(ee/).get(3) " + Util_all_nodes_pertype_alllayers24.get("ee").get(3));        
            Print.printResHash_StrInt("addmission_per_STARreq_type_hash_link42  ", addmission_per_star_req_type_hash);
            Print.println("  (double)all_node_layers_cap_hash18.get(0) " + (double)all_node_layers_cap_hash18.get(0));        
            Print.println("  (double)total_star_reqs.get(0) " + (double)total_star_reqs );        
               

////////////////
            
            
        }

        try {
            EWS.writer(
                    total_star_reqs, admitted_star_req, blocking_star_req, 
                    (double)blocking_star_req/(double)total_star_reqs, 
                    links_used.size(), nodes_used.size(),totalrerouting, 
                    (double)this.route_lengths/(double)this.Routes.size(), 
                    totalreroute_link, totalreroute_node,

                    //I will make per star type acceptance ratio in the writer
                    addmission_per_star_req_type_hash, 
                    
                    blocking_per_star_req_type_hash,
                    
                    //use this with the hash of addmission per start type to
                    //make the average per type
                    this.Reqs_route_length25 ,
                    
                    reroute_per_star_req_type_hash,
                    
                    
                    admitted_general_link1,blocking_general_link2,((double)blocking_general_link2/(blocking_general_link2+admitted_general_link1)),
                    
                    addmission_per_req_type_hash_link13.get("aa"),blocking_per_req_type_hash_link15.get("aa"),
                    (double)addmission_per_req_type_hash_link13.get("aa")/(addmission_per_req_type_hash_link13.get("aa")+blocking_per_req_type_hash_link15.get("aa")),
                    All_hash_link_util_perType9.get("aa"),(double)all_link_layers_cap_hash17.get(2),
                    (double)All_hash_link_util_perType9.get("aa")/((double)all_link_layers_cap_hash17.get(2)),
                    (int)Util_all_links_pertype_alllayers23.get("aa").get(0),(int)Util_all_links_pertype_alllayers23.get("aa").get(1),
                    (int)Util_all_links_pertype_alllayers23.get("aa").get(2),0,

                    addmission_per_req_type_hash_link13.get("bb"),blocking_per_req_type_hash_link15.get("bb"),
                    (double)addmission_per_req_type_hash_link13.get("bb")/(addmission_per_req_type_hash_link13.get("bb")+blocking_per_req_type_hash_link15.get("bb")),
                    All_hash_link_util_perType9.get("bb"),(double)all_link_layers_cap_hash17.get(1),
                    (double)All_hash_link_util_perType9.get("bb")/((double)all_link_layers_cap_hash17.get(1)),
                    (int)Util_all_links_pertype_alllayers23.get("bb").get(0),(int)Util_all_links_pertype_alllayers23.get("bb").get(1),
                    (int)Util_all_links_pertype_alllayers23.get("bb").get(2),0,                    

                    addmission_per_req_type_hash_link13.get("cc"),blocking_per_req_type_hash_link15.get("cc"),
                    (double)addmission_per_req_type_hash_link13.get("cc")/(double)(addmission_per_req_type_hash_link13.get("cc")+blocking_per_req_type_hash_link15.get("cc")),
                    All_hash_link_util_perType9.get("cc"),(double)all_link_layers_cap_hash17.get(1),
                    (double)All_hash_link_util_perType9.get("cc")/((double)all_link_layers_cap_hash17.get(1)),
                    (int)Util_all_links_pertype_alllayers23.get("cc").get(0),(int)Util_all_links_pertype_alllayers23.get("cc").get(1),
                    (int)Util_all_links_pertype_alllayers23.get("cc").get(2),0,                          
                    

                    addmission_per_req_type_hash_link13.get("dd"),blocking_per_req_type_hash_link15.get("dd"),
                    (double)addmission_per_req_type_hash_link13.get("dd")/(double)(addmission_per_req_type_hash_link13.get("dd")+blocking_per_req_type_hash_link15.get("dd")),
                    All_hash_link_util_perType9.get("dd"),(double)all_link_layers_cap_hash17.get(1),
                    (double)All_hash_link_util_perType9.get("dd")/((double)all_link_layers_cap_hash17.get(1)),
                    (int)Util_all_links_pertype_alllayers23.get("dd").get(0),(int)Util_all_links_pertype_alllayers23.get("dd").get(1),
                    (int)Util_all_links_pertype_alllayers23.get("dd").get(2),0,                    
                    

                    addmission_per_req_type_hash_link13.get("ee"),blocking_per_req_type_hash_link15.get("ee"),
                    (double)addmission_per_req_type_hash_link13.get("ee")/(double)(addmission_per_req_type_hash_link13.get("ee")+blocking_per_req_type_hash_link15.get("ee")),
                    All_hash_link_util_perType9.get("ee"),(double)all_link_layers_cap_hash17.get(1),
                    (double)All_hash_link_util_perType9.get("ee")/((double)all_link_layers_cap_hash17.get(1)),
                    (int)Util_all_links_pertype_alllayers23.get("ee").get(0), (int)Util_all_links_pertype_alllayers23.get("ee").get(1),
                    (int)Util_all_links_pertype_alllayers23.get("ee").get(2), 0,                    
                    

                    
                    Total_util_of_link_layers5.get(0),(double)all_link_layers_cap_hash17.get(0),
                    (double)Total_util_of_link_layers5.get(0)/((double)all_link_layers_cap_hash17.get(0)),
                    
                    Total_util_of_link_layers5.get(1),(double)all_link_layers_cap_hash17.get(1),
                    (double)Total_util_of_link_layers5.get(1)/((double)all_link_layers_cap_hash17.get(1)),
                    
                    Total_util_of_link_layers5.get(2),(double)all_link_layers_cap_hash17.get(2),
                    (double)Total_util_of_link_layers5.get(2)/((double)all_link_layers_cap_hash17.get(2)),

                    0,0,
                    0,
                    
                    
//nodes start                    
                    
                    admitted_general_node3,blocking_general_node4,((double)blocking_general_node4/(blocking_general_node4+admitted_general_node3)),
                    
                    addmission_per_req_type_hash_node14.get("aa"),blocking_per_req_type_hash_node16.get("aa"),
                    (double)addmission_per_req_type_hash_node14.get("aa")/(addmission_per_req_type_hash_node14.get("aa")+blocking_per_req_type_hash_node16.get("aa")),
                    (double) (Integer)Util_all_nodes_pertype_alllayers24.get("aa").get(0),(double)all_node_layers_cap_hash18.get(0),
                    (double)All_hash_node_util_perType10.get("aa")/((double)all_node_layers_cap_hash18.get(0)),
                    (int)Util_all_nodes_pertype_alllayers24.get("aa").get(0),0,
                    0, 0,                    
                    
                    
                    addmission_per_req_type_hash_node14.get("bb"),blocking_per_req_type_hash_node16.get("bb"),
                    (double)addmission_per_req_type_hash_node14.get("bb")/(addmission_per_req_type_hash_node14.get("bb")+blocking_per_req_type_hash_node16.get("bb")),
                    All_hash_node_util_perType10.get("bb"),0,
                    0,
                    (int)Util_all_nodes_pertype_alllayers24.get("bb").get(0),0,
                    0, 0,                    
                    
                    
                    addmission_per_req_type_hash_node14.get("cc"),blocking_per_req_type_hash_node16.get("cc"),
                    (double)addmission_per_req_type_hash_node14.get("cc")/(addmission_per_req_type_hash_node14.get("cc")+blocking_per_req_type_hash_node16.get("cc")),
                    All_hash_node_util_perType10.get("cc"),0,
                    0,
                    (int)Util_all_nodes_pertype_alllayers24.get("cc").get(0),0,
                    0, 0,                    
                    

                    addmission_per_req_type_hash_node14.get("dd"),blocking_per_req_type_hash_node16.get("dd"),
                    (double)addmission_per_req_type_hash_node14.get("dd")/(addmission_per_req_type_hash_node14.get("dd")+blocking_per_req_type_hash_node16.get("dd")),
                    All_hash_node_util_perType10.get("dd"),0,
                    0,
                    (int)Util_all_nodes_pertype_alllayers24.get("dd").get(0),0,
                    0, 0,                    
                    

                    addmission_per_req_type_hash_node14.get("ee"),blocking_per_req_type_hash_node16.get("ee"),
                    (double)addmission_per_req_type_hash_node14.get("ee")/(addmission_per_req_type_hash_node14.get("ee")+blocking_per_req_type_hash_node16.get("ee")),
                    All_hash_node_util_perType10.get("ee"),0,
                    0,
                    (int)Util_all_nodes_pertype_alllayers24.get("ee").get(0),0,
                    0, 0,                    
                    

                    
                    Total_util_of_node_layers6.get(0),(double)all_node_layers_cap_hash18.get(0),
                    (double)Total_util_of_node_layers6.get(0)/((double)all_node_layers_cap_hash18.get(0)),
                    
                    0,0,
                    0,
                    0,0,
                    0,
                    
                    0,0,
                    0,
                    
                    blocking_star_node,
                    blocking_star_link,
                    
                    Total_star_block_node_id_hash,
/// nodes done                    

                    
                    
                    

                    horiz, line, file_address);
        } catch (FileNotFoundException ex) {
            Logger.getLogger(Stats_collector_Star.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(Stats_collector_Star.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    
    
    public void view_per_node_addmission(){
        Print.printResHash_IntInt(" nodes admission ", per_node_addmission32);
    }
    public void view_per_link_addmission(){
        Print.printResHash_StrInt(" links admission ", per_link_addmission30);
    }
    
    
    public void addmitting_star_request(Req_IT_NET_timed_startnet RIT)
    {
        String type = RIT.returnStarReqType();
        init_addmitting_star_request();
        this.admitted_star_req++;
        this.total_star_reqs++;
        if(!addmission_per_star_req_type_hash.containsKey(type))
        {
            //int cap = blocking_per_req_type_hash_link15.get(req_type);
            addmission_per_star_req_type_hash.put(type, 1);
        }
        else
        {
            int existing_b = addmission_per_star_req_type_hash.get(type);
            addmission_per_star_req_type_hash.put(type, existing_b + 1);

        }            
        
    }
 
    public void blocking_star_request(Req_IT_NET_timed_startnet RIT)
    {
        String type = RIT.returnStarReqType();
        init_blocking_star_request();
        this.blocking_star_req++;
        this.total_star_reqs++;
        if(!blocking_per_star_req_type_hash.containsKey(type))
        {
            //int cap = blocking_per_req_type_hash_link15.get(req_type);
            blocking_per_star_req_type_hash.put(type, 1);
        }
        else
        {
            int existing_b = blocking_per_star_req_type_hash.get(type);
            blocking_per_star_req_type_hash.put(type, existing_b + 1);

        }            
        
    }
    
    public void display_rerouting_info(){
        Print.println("totalrerouting: " +  totalrerouting);
        Print.println("totalreroute_link: " +  totalreroute_link);
        Print.println("totalreroute_node: " +  totalreroute_node);

        Print.printResHash_StrInt(" reroute_per_star_req_type_hash ", reroute_per_star_req_type_hash);
        Print.printResHash_StrInt(" reroute_per_star_req_node_type_hash ", reroute_per_star_req_node_type_hash);
        Print.printResHash_StrInt(" reroute_per_star_req_linktype_hash ", reroute_per_star_req_linktype_hash);
    }
 
    
//    public double get_addmission_per_star_req_type_hash_size_all()
//    {
//        
//    }
}
