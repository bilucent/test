/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package routing;

import Infrastructure.Network;
import Infrastructure.Node;
import com.rits.cloning.Cloner;
import java.sql.Array;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import utilities.Print;
import utilities.Sorting;

/**
 *
 * @author Bijan
 */
public class RouteProcess {
    
    boolean Debug;
    Network mNetwork;
    List<Route> Routes;
    List<int[]> routes_nodesIds = new ArrayList<>();
    List<int[]> routes_linkIds = new ArrayList<>();
    
    List<Route> node_disjoint;
    List<Route> link_disjoint;
    List<Route> link_node_disjoint;
    List<Route> link_cost;
    List<Route> node_cost;
    List<Route> link_node_cost;
    List<Route> link_delay;
    List<Route> node_delay;
    List<Route> link_node_delay;   
    public RouteProcess(boolean debug, Network network, List<Route> routes)
    {
        this.mNetwork = network;
        this.Routes = routes;
    }
    public void get_me_node_types(){

        
        for(int i = 0; i < Routes.size(); i++)
        {
            Route r = Routes.get(i);
            int link_ids[] = mNetwork.return_linksIDs_in_route(r.return_hops_in_route());
            int nodes_ids[] = r.return_nodesIDs_in_route();
            routes_nodesIds.add(nodes_ids);   
            routes_linkIds.add(link_ids);
        }
    }    
    
    public List<Route> getm_processed_by_availabilites(List<Route> routes, int layer){
        List<Integer> agg_avail = new ArrayList<>();
        
        Cloner cloner=new Cloner();
        List<Route> processed_routes = cloner.deepClone(routes);        
        
        /*
         * here, first I make a list of the all routes aggreagted availabilties 
         * in the layer of interest
         */
        for(int i = 0; i < processed_routes.size(); i++)
        {
            Route r = processed_routes.get(i);
            agg_avail.add(mNetwork.return_route_min_link_availabilities(r, layer));
            
        }   
        if(Debug){
            Print.println(" Route before being processed based on their availabilities" + processed_routes.toString());
            Print.println(" Routes availabilities" + agg_avail.toString()); 
        }
        //Sorting.sortByWeightIncreasing_route(processed_routes, agg_avail);
        Sorting.sortByWeightDecreasing_route(processed_routes, agg_avail);

        if(Debug)
        Print.println(" Route processed based on their availabilities\t\t" + processed_routes.toString());
        
        return processed_routes;
    }
    public Route getm_least_used_processed_by_availabilites(List<Route> routes, int layer){

        return getm_processed_by_availabilites(routes, layer).get(0);
    }    
    
    
    
    public List<Route> process_by_delay(List<Route> routes){
        List<Route> processed = new ArrayList<>();
        
        return processed;
    }
    public Route process_by_least_hop(List<Route> routes,int index){
        
        
        return routes.get(index);
    }
    
    public List<Route> process_by_cost(List<Route> routes){
        List<Route> processed = new ArrayList<>();
        
        return processed;        
    }    
    public List<Route> process_by_location(List<Route> routes){
        List<Route> processed = new ArrayList<>();
        
        return processed;        
    }    
  
    public void process_by_none(){
        
    }   
    public List<Route> getm_filtered_routes_only_these_nodes(List<Route> routes,String[] nodes_acceptable){
        List<Route> filtered_routes = new ArrayList<>();
        
        
        Cloner cloner=new Cloner();
        List<Route> processed_routes = cloner.deepClone(routes);        
        
        /*
         * here, first I make a list of the all routes aggreagted availabilties 
         * in the layer of interest
         */
        for(int i = 0; i < processed_routes.size(); i++)
        {
            
            Route r = processed_routes.get(i);
            String path_nodes[] = mNetwork.return_route_nodes_string_arr(r.getmPath());
            List nodes_acceptable_list = Arrays.asList(nodes_acceptable);
            boolean these_nodes = true;
            for(int j = 0; j < path_nodes.length ; j++)
            {
                if(!nodes_acceptable_list.contains(path_nodes[j]))
                {
                    these_nodes = false;
                    break;
                   
                }
            }
            if(these_nodes)
            {
                filtered_routes.add(r);
            }
            
        }       

        return filtered_routes;        
    }     
    public List<Route> getm_filtered_routes_none_of_these_nodes(List<Route> routes,String[] nodes_dissmissable){
        List<Route> filtered_routes = new ArrayList<>();
        
        
        Cloner cloner=new Cloner();
        List<Route> processed_routes = cloner.deepClone(routes);        
        
        /*
         * here, first I make a list of the all routes aggreagted availabilties 
         * in the layer of interest
         */
        for(int i = 0; i < processed_routes.size(); i++)
        {
            
            Route r = processed_routes.get(i);
            String path_nodes[] = mNetwork.return_route_nodes_string_arr(r.getmPath());
            List path_nodeslist = Arrays.asList(path_nodes);
                    //Print.println(" path_nodeslist" + path_nodeslist.toString());

            boolean other_nodes = false;
            for(int j = 0; j < nodes_dissmissable.length ; j++)
            {
                if(path_nodeslist.contains(nodes_dissmissable[j]))
                {
                    if(Debug)
                    Print.println(" \tother_nodes true for : " + nodes_dissmissable[j]);
                    other_nodes = true;
                    //break;
                }
            }
            if(!other_nodes)
            {
               
                //Print.println(" filtered_routes added" + r.toString());
                filtered_routes.add(r);
                
            }
        }        
        //Print.println(" filtered_routes" + filtered_routes.toString());

        return filtered_routes;     
    }
    
//    public List<Route> get_multicast_to_these_routes(int src, int[] dest){
//        List<Route>
//                
//    }
//    
//    public List<Route> combinatorialRoutes_StarBiSP(int star_dest, int[]sources )
//    {
//        
//        
//        
//        
//    } 
//    
    public HashMap<Integer, List<Route>> bidirectional_lightpaths(int nodeid1, int nodeid2, int Knumroutes )
    {

        HashMap<Integer, List<Route>> nodeRouts = new HashMap<>();
       
        List<Route> routes = new ArrayList<>();
        int routes_avaialble = 0;
        routes_avaialble = mNetwork.getmListNodes().get(nodeid1).
                getmRouting().getmRoutingTable().get_number_of_routes_to_dest(nodeid2);
        for(int i=0; i < Knumroutes && i < routes_avaialble ; i++)
        {
            routes.add(mNetwork.getmListNodes().get(nodeid1).
                    getmRouting().getmRoutingTable().getRoute(nodeid2, i));
        }
        nodeRouts.put(nodeid2, routes);
        
        routes_avaialble = 0;
        routes_avaialble = mNetwork.getmListNodes().get(nodeid2).
                getmRouting().getmRoutingTable().get_number_of_routes_to_dest(nodeid2);
        for(int i=0; i < Knumroutes && i < routes_avaialble ; i++)
        {
            mNetwork.getmListNodes().get(nodeid1).getmRouting().getmRoutingTable().getRoute(nodeid1, i);
        }
        nodeRouts.put(nodeid1, Routes);

        
        return nodeRouts;
        
    } 
    
    
         
//    public List<Route> bidirectional_lightpaths_BWaware(List<Route> allroutes,int source, int dest, int similarity_degree, int bandwidth )
//    {
//        
//        
//        
//        
//    } 
         
}
