package routing;

import Infrastructure.Link;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import utilities.Comparator;
import utilities.Print;

/**
 * This class implements a Route object which defines the path
 * between an origin and a destination node.
 * 
 * @author Joan Triay
 *
 */
public class Route extends Comparator implements Cloneable {
	/** an array defining the list of nodes that form the path. */
	private int mPath[];
	/** the origin node. */
	private int mOrigin;
	/** the destination node. */
	private int mDest;
	/** length of the path in number of hops. */
	private int mLength;
        
        //I add the link types to the route
        private String types;
        
        private List<Link> mLinks;
        
        //private HashMap<Integer, Integer> cross_layer_availabilties = new HashMap();
        
        Print P;
	
	/**
	 * Class constructor.
	 * @param path The array of integers defining the route.
	 */
	public Route(int path[]){
		// set the origin, which is the first element in the array
		this.mOrigin = path[0];
		// set the destination, which is the last element in the array
		this.mDest = path[path.length-1];
		this.mPath = path;
		// lenght is number of hops
		this.mLength = path.length-1;
		// set the weight, equal to the length, for comparison purposes
		this.weight = (double)this.mLength;
                
                this.P = new Print();
                
                //this.mLinks = link_list;
                
                //in this method, I make a string of link types, for filtering later
                //generate_type_string();
	}

	/**
	 * Implement the clone method, since this class implements the 
	 * Cloneable interface.
	 */
	public Object clone() {
        try {
            return super.clone();
        }
        catch (CloneNotSupportedException e) {
            // This should never happen
            throw new InternalError(e.toString());
        }
    }

	
	public String toString(){
		String out = "";
		out = out + mOrigin + "\t" + mDest + "\t" + mLength + "\t";
		for(int i=0; i<mPath.length; i++){
			out = out + mPath[i];
			if(i != (mPath.length-1))
				out = out + ":";
		}
		return out;
	}
    
            // this method returns the ids of link hops in a path
        public List<String> return_hops_in_route()
        {
            List hops = new ArrayList();
            int path[] = mPath;
            //P.println("the route is:" + route.toString() + " route length: " + route.mLength);
            // cause the number of link hops is one less than the numbe rof node hops
            for(int i =0; i<path.length-1 ; i++)
            {
                String link_id = ""+path[i]+":"+path[i+1] ;
                //P.println("link_id in getmhop  " + link_id);
                hops.add(link_id);
            }        
        return hops;
        } 
        /*
        public void generate_type_string()
        {
            String link_string ="";
            for(int i =0; i < mPath.length;i++)
            {
                link_string = link_string+ mLinks.get(i).getmType();
            }
            
            if(link_string.length()!= mPath.length)
            {
                P.println(" !!!! warning aabout the link string !!!!");
            }
        }
        * 
        */
    
	// getters and setters
	public int[] getmPath() {
		return mPath;
	}

	public int getmOrigin() {
		return mOrigin;
	}

	public int getmDest() {
		return mDest;
	}

	public int getmLength() {
		return mLength;
	}
        
        public int[] return_nodesIDs_in_route()
        {
            return mPath;
        }      
        
        
      
}
