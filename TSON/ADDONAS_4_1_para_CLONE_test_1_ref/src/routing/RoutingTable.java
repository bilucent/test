package routing;

import java.util.ArrayList;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.Hashtable;
import java.util.List;
import utilities.Print;

import utilities.Sorting;

public class RoutingTable {
	/** A table with all the routes to each destination. */
	private Hashtable<Integer,List<Route>> mRoutingTable;
	/** The network node holding this routing table. */
	private int mNode;
        
        Print P  = new Print();
                
	
	
	/**
	 * Class constructor.
	 * @param node The network node holding this table.
	 */
	public RoutingTable(int node){
		this.mRoutingTable = new Hashtable<Integer,List<Route>>();
		this.mNode = node;
	}
        
	public RoutingTable(RoutingTable copy){
		this.mRoutingTable = copy.mRoutingTable;
		this.mNode = copy.mNode;
	}
        
        public int get_number_of_routes_to_dest(int dest)
        {
            List<Route> dest_route_list  = mRoutingTable.get(dest);
            if(dest_route_list == null)
                return 0;
            else
                return dest_route_list.size();
        }        
        public int get_number_of_routes_all()
        {
            int total_routes = 0;
            Enumeration<List<Route>> enumListRoutes = mRoutingTable.elements();
            while(enumListRoutes.hasMoreElements()){
                    List<Route> listRoute = enumListRoutes.nextElement();
                    int sizeList = listRoute.size();
                    total_routes = sizeList + total_routes;
            }
            
            return total_routes;
                
        }        
        public Hashtable get_routes_list()
        {
            return mRoutingTable;
        }     
        
	/**
	 * Add a route to the list of routes for a specific
	 * destination node.
	 * @param dest Destination node of this route.
	 * @param path The array of integers with the list of nodes
	 * conforming such path.
	 */
	public void addRoute(int dest, int path[]){
		// if the table does not contain any route for the
		// specified destination node, then we
		//  have to create a new entry
		if(!mRoutingTable.containsKey(dest)){
			List<Route> listRoutes = new ArrayList<Route>();
			mRoutingTable.put(dest, listRoutes);
		}
		
		// create the route object from the integer array
		// and add it to the routing table
		Route route = new Route(path);
		mRoutingTable.get(dest).add(route);
		
                //P.println(" %RoutingTable% \tthis route added:" + route.toString());
		// order the routes in the table by increasing
		// length of the path
		List<Route> listRoutes = mRoutingTable.get(dest);
		// order the list of routes in increasing length
		// then, always, the first entry in the list will be the
		// shortest route.
		Sorting.sortByWeightIncreasing(listRoutes);
		mRoutingTable.put(dest, listRoutes);
	}
	/**
         * Bijan: method add is modified to delete all the routes,
         * reason is, I dont want any route memory there,
         * as network condition might change, and the routes might go stale
         * 
	 * Delete all a route to the list of routes for a specific
	 * destination node.
	 * @param dest Destination node of this route.
	 * @param path The array of integers with the list of nodes
	 * conforming such path.
	 */
	public void delRouteS(int dest ){

            this.mRoutingTable = new Hashtable<Integer,List<Route>>();
	}
	
	
	/**
	 * Get a route to the specified destination, and specifically
	 * the route in the list in position 'index'.
	 * @param dest The destination.
	 * @param index The position index.
	 * @return A route object.
	 */
	public Route getRoute(int dest, int index){
		List<Route> list = mRoutingTable.get(dest);
		Route route = list.get(index);
		return route;
	}

	
	/**
	 * This method returns the shortest route to the destination
	 * given as a parameter to the function.
	 * @param dest The destination node.
	 * @return The shortest route if found, null otherwise.
	 */
	public Route getShortestRoute(int dest){
		List<Route> listRoute = mRoutingTable.get(dest);
		
		// since routes are ordered in increasing length
		// when added to the list, always, the first entry
		// should be the shortest
		if(listRoute != null){
			Route shortest = listRoute.get(0);
			return shortest;
		}
		
		return null;
	}
	
	
	public String printTable(){
            
		String output = new String();
                //output = null;
		Enumeration<List<Route>> enumListRoutes = mRoutingTable.elements();
                
		while(enumListRoutes.hasMoreElements()){
			List<Route> listRoute = enumListRoutes.nextElement();
			int sizeList = listRoute.size();
			
			for(int i=0; i<sizeList; i++){
				output = output + listRoute.get(i).toString() + "\n";
			}
		}
		System.out.println(" ************* \n" + output);
                
                if(output==null){output= "empty";}
                
		return output;
	}
        
        
        /*
         * I get all the routes to the dest, so I can process with regards to differnet aspects
         */
        public List<Route> get_dest_routes_list(int dest)
        {
		List<Route> listRoute = mRoutingTable.get(dest);
		
		// since routes are ordered in increasing length
		// when added to the list, always, the first entry
		// should be the shortest
		if(listRoute != null){
			
			return listRoute;
		}

		return null;
        }             
        public List<Route> get_dest_routes_list_capped(int dest, int cap_number)
        {
		List<Route> listRoute = mRoutingTable.get(dest);

                try {
                    listRoute.size();
                }
                catch(NullPointerException e)
                {
                    System.out.println(" no routes??? to " +  dest);
                }
                    
		// since routes are ordered in increasing length
		// when added to the list, always, the first entry
		// should be the shortest
                
                List<Route> capped_listRoute = new ArrayList<>();
            
                for(int i = 0; i < listRoute.size(); i ++)
                {
                    if(i<cap_number)
                        capped_listRoute.add(listRoute.get(i));
                }
		if(capped_listRoute != null){
			
			return capped_listRoute;
		}

		return null;
        }             
        
}
