


/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package Record;

import Infrastructure.Link;
import Infrastructure.Network;
import Infrastructure.ResourceOperations;
import com.rits.cloning.Cloner;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Set;
import utilities.Converters;
import utilities.Print;

/**
 *
 * @author Bijan
 * a recorder calss, to be created for all the links initially
 */
public class Rec_Link_Allocs extends Recorder {
    
    private static int counter;
    private int link_number;
    private String link_id;
    
    // I use two hash maps here, for id to resrouces, and id to time
    private HashMap<Integer, Link_Rec_Obj> Linkres_allocation_id2res_hash;
    private HashMap<Integer, Integer> Linkres_allocation_id2time_hash;
//    private Network mNetwork;
    private boolean Debug;
    private List<String> Link_res_info;
    Print P = new Print();
    

    public Rec_Link_Allocs(boolean debug, String linkid, int link_num, List<String> res_info) {
        this.Debug = debug;
        this.Link_res_info = res_info;
//        this.mNetwork = network;
        this.Linkres_allocation_id2res_hash = new HashMap<Integer, Link_Rec_Obj>();
        this.Linkres_allocation_id2time_hash = new HashMap<Integer, Integer>();
        this.counter = 0;
        this.link_number = link_num;
        this.link_id = linkid;
    }

    public Rec_Link_Allocs(Rec_Link_Allocs copy) {
        this.Debug = copy.Debug;
        Cloner cloner=new Cloner();
        this.Linkres_allocation_id2time_hash = new HashMap<>();
        this.Linkres_allocation_id2time_hash = cloner.deepClone(copy.Linkres_allocation_id2time_hash);        
        this.Link_res_info = 
                        cloner.deepClone(copy.Link_res_info);        
//        this.mNetwork = new Network(copy.mNetwork);
        
        this.counter = cloner.deepClone(copy.counter);
        
        this.link_number = cloner.deepClone(copy.link_number);
        this.link_id = cloner.deepClone(copy.link_id);

        
        // this block clones the hashmap with objects in it
        this.Linkres_allocation_id2res_hash = new HashMap<Integer, Link_Rec_Obj>();
        Set<Integer> keySet = copy.Linkres_allocation_id2res_hash.keySet();
        Object[] toArray = keySet.toArray();
        int [] intarr = new int [toArray.length];
        for (int i = 0; i < intarr.length; i++){
            intarr[i] = (int) toArray[i];                
        }
//        cloning is happening here
        
//                    this.Linkres_allocation_id2res_hash = copy.Linkres_allocation_id2res_hash;

        for(int i :intarr)
        {
//
            this.Linkres_allocation_id2res_hash.put(i,
                    new Link_Rec_Obj(copy.Linkres_allocation_id2res_hash.get(i)));
            
        }
            Print P = copy.P;

     
    }
    
    
    public int add_to_link(int id, Link_Rec_Obj aloc_instance){
//        this.Allocation_id2res_hash.put(this.counter, aloc_instance);
//        this.Allocation_id2time_hash.put(this.counter, aloc_instance.StopTime);        
        this.Linkres_allocation_id2res_hash.put(id, aloc_instance);
        this.Linkres_allocation_id2time_hash.put(id, aloc_instance.return_linkres_stop_time());
        return this.counter++;
    }

    
    public List<Integer> delete_expired_linkres(Network mNetwork, int time){
       
        List<Integer>  deleted = new ArrayList<>();
        List<Integer>  deleted_times = new ArrayList<>();

//       P.println("this.Linkres_allocation_id2time_hash:size " + this.Linkres_allocation_id2time_hash.size());
//       P.println("this.Linkres_allocation_id2res_hash:size " + this.Linkres_allocation_id2res_hash.size());
       if(this.Linkres_allocation_id2time_hash.size()>0){
           
           //check the time value of the recorded keys, so to be deleted maybe
            Set<Integer> keySet = this.Linkres_allocation_id2time_hash.keySet();
            Object[] toArray = keySet.toArray();
            int [] intarr = new int [toArray.length];
            for (int i = 0; i < intarr.length; i++){
                intarr[i] = (int) toArray[i];
            }
            //P.print_1D("  The keyset intarr[i] "+ intarr[i]);
            //P.print_1D("  in expiry stuff, if need to delete? the keyset", intarr);
            for (int i = 0; i < intarr.length; i++){

                
                //P.println(" \ncheck id for deletion, id is : " + intarr[i]+ "  in link:" + link_id);
                if(this.Linkres_allocation_id2time_hash.get(intarr[i])<=time){

                   if(Debug)
                   P.println(" something LINKS "+link_id + "  need to be deleted.... con id: " + intarr[i]+ " with expiry time of:  " +
                             this.Linkres_allocation_id2time_hash.get(intarr[i]) + " \t and time is" + time + "");
                   
                   //P.println(" new introduced to track the removals : resrouces recorded and to be deleted are:\n " + this.Allocation_id2res_hash.get(intarr[i]).Alloc_res_family.toString() );
                    //make a record for displaying
                   deleted.add(intarr[i]);
                   deleted_times.add(this.Linkres_allocation_id2time_hash.get(intarr[i]));

                   //remove from link resources
                   Link link = mNetwork.getLink(link_id);
                   try{

                       /*/
                        * 
                        * the tradithional all look up way, to timely ineffective
                        */
//                        ResourceOperations.removeTheAllocation_resFamily_link(link, Allocation_id2res_hash.
//                                get(intarr[i]).return_linkres_allocresrouces_family());
//                        
                        
                       /*/
                        * 
                        * the layer/ cell only, removal and replacement, to make
                        * it timely effective
                        */                        
                       int layer = Linkres_allocation_id2res_hash.
                                get(intarr[i]).return_linkres_req_depth();
                       List<Integer> cell_positions = Linkres_allocation_id2res_hash.
                                get(intarr[i]).get_link_cell_positions();
                       ResourceOperations.removeTheAllocation_resFamily_link(link, Linkres_allocation_id2res_hash.
                                get(intarr[i]).return_linkres_allocresrouces_family());
//                        ResourceOperations.removeTheAllocation_resFamily_link_2(link, Allocation_id2res_hash.
//                                get(intarr[i]).return_linkres_allocresrouces_family(), layer, cell_positions);                        
                        
                       
                   }
                   catch(NullPointerException e)
                   {
                       P.println(" \nwarning    the link has no allocated resources...  linkid: " + link.getmID());
                   }
                   //remove from record list
                   this.Linkres_allocation_id2time_hash.remove(intarr[i]);
                   this.Linkres_allocation_id2res_hash.remove(intarr[i]);
                } 
            }        
            return deleted;

        }
       else return null;
    }
    

//    public void  printResHash()
//    {
//        Collection resValues = this.Allocation_id2res_hash.values();
//        Set keys = this.Allocation_id2res_hash.keySet();
//        Object[] KeysArray = keys.toArray();
//        int counter =0;
//        while(counter < Res_hash.size())
//        {
//           String k =  KeysArray[counter].toString();
//           Print.println(" key: " + KeysArray[counter].toString() +" "
//                   + "\t Res: " + Res_hash.get( k)); 
//           counter++;
//        }
//        
//    }
    public void  printResHash()
    {    
            Set<Integer> keySet = this.Linkres_allocation_id2res_hash.keySet();
            Object[] toArray = keySet.toArray();
            int [] intarr = new int [toArray.length];
            for (int i = 0; i < intarr.length; i++){
                intarr[i] = (int) toArray[i];
                //HashMap rec_res =  this.Allocation_id2res_hash.get(intarr[i]).return_allocresrouces();
                this.Linkres_allocation_id2res_hash.get(intarr[i]).display();
                
            }
            
    }
    private int[] return_link_rec_keys()
    {
            Set<Integer> keySet = this.Linkres_allocation_id2res_hash.keySet();
            Object[] toArray = keySet.toArray();
            int [] intarr = new int [toArray.length];
            for (int i = 0; i < intarr.length; i++){
                intarr[i] = (int) toArray[i];
            }
            return intarr;
        
    }    
    
    
    public HashMap<String,Integer> return_aggregated_linkres_across_layers_util_of_allocs_per_type()
    {
        
        HashMap<String,Integer> all_recs_hash = new HashMap<>();
        int keys_arr[] = return_link_rec_keys();
        
        for (int i = 0; i < keys_arr.length; i++){
            
            Link_Rec_Obj rec_obj =this.Linkres_allocation_id2res_hash.get(keys_arr[i]);
            String rec_type = rec_obj.return_linkres_req_type();
            List<List> allocs_tree = rec_obj.return_linkres_allocresrouces_family();
            List<Integer> ones = ResourceOperations.count_ones_in_res_family(allocs_tree);
            
            //here, I am coagulating all the ones_all_layers into one, which is not right
            int all_ones = ResourceOperations.aggre_listInts_to_oneInt(ones);
            
            if(!all_recs_hash.containsKey(rec_type))
            {
                all_recs_hash.put(rec_type, all_ones);
            }
            else
            {
                int allocs_already = all_recs_hash.get(rec_type);
                all_ones = allocs_already + all_ones;
                all_recs_hash.put(rec_type, all_ones);
                
            }
            
        }
        return all_recs_hash;

    }
    
    /*
     * this method is the amended version of the method above, so it tries to return only
     * the layer which te resources were allocated. however it wont make a difference
     * on the specific aloatio nmethodoly I am using now which I go directly to the
     * layer I want and allocate in there.
     */
    public HashMap<String,Integer> return_max_layer_util_of_link_allocs_per_type()
    {
        
        HashMap<String,Integer> all_recs_hash = new HashMap<>();
        int keys_arr[] = return_link_rec_keys();
        
        
        // search through all the requests types available there
        for (int i = 0; i < keys_arr.length; i++){
            Link_Rec_Obj rec_obj =this.Linkres_allocation_id2res_hash.get(keys_arr[i]);
            String rec_type = rec_obj.return_linkres_req_type();

            List<List> allocs_tree = rec_obj.return_linkres_allocresrouces_family();
            
            List<Integer> ones = ResourceOperations.count_ones_in_res_family(allocs_tree);
            
            //Print.println("  the ones_all_layers list " + ones_all_layers);
            
            //here, I am getting only the max layers util
            int all_ones = ones.get(rec_obj.return_linkres_req_depth());
            
            
            // Print.println("  all_onesall_onesall_onesall_ones " + all_ones + "   rec_obj.return_linkres_req_depth(): " + rec_obj.return_linkres_req_depth());
            //here, per each type, I accumultate the allocs.
            // so I get per link/ per type accumulated allocatoins
            if(!all_recs_hash.containsKey(rec_type))
            {
                all_recs_hash.put(rec_type, all_ones);
            }
            else
            {
                int allocs_already = all_recs_hash.get(rec_type);
                all_ones = allocs_already + all_ones;
                all_recs_hash.put(rec_type, all_ones);
                
            }
            
        }
        return all_recs_hash;

    }        
    /*
     * this method is the amended version of the method above, so it tries to return not only
     * the layer which te resources were allocated. however it wont make a difference
     * on the specific aloatio nmethodoly I am using now which I go directly to the
     * layer I want and allocate in there.
     */
    public HashMap<String,List> return_all_layers_util_of_link_allocs_per_type()
    {
        
        //HashMap<String,List, Integer> all_layers_resources = new HashMap<>();
        HashMap<String,List> all_layers_resources = new HashMap<>();
        int keys_arr[] = return_link_rec_keys();
        
        
        // search through all the requests types available there
        if(Debug)
        Print.println("  \n ((( number of records in each link   : " + keys_arr.length);
        for (int i = 0; i < keys_arr.length; i++){
            
            Link_Rec_Obj rec_obj =this.Linkres_allocation_id2res_hash.get(keys_arr[i]);
            if(Debug)
            Print.println("  \n ((( the link is   : " + link_id);
            String rec_type = rec_obj.return_linkres_req_type();
            if(Debug)
            Print.println("  \n ((( the links rec_type  : " + rec_type);
            List<List> allocs_tree = rec_obj.return_linkres_allocresrouces_family();
            if(Debug)
            Print.println("  \n ((( before per link  : " + allocs_tree.toString());
            List<List> cloned_resources = spread_linkres_all_over_combined(allocs_tree, Link_res_info);
            if(Debug)
            Print.println("  \n ((( after cloning and switping per link  : " + cloned_resources.toString());
            
            List<Integer> ones_all_layers = ResourceOperations.count_ones_in_res_family(cloned_resources);
            
            //Print.println("  the ones_all_layers list " + ones_all_layers);
            
            //here, I am getting only the max layers util
            

            // Print.println("  all_onesall_onesall_onesall_ones " + all_ones + "   rec_obj.return_linkres_req_depth(): " + rec_obj.return_linkres_req_depth());
            //here, per each type, I accumultate the allocs.
            // so I get per link/ per type accumulated allocatoins
            if(!all_layers_resources.containsKey(rec_type))
            {
                all_layers_resources.put(rec_type, ones_all_layers);
                if(Debug){
                    Print.println("  \n ((( the updated util per type   : " + ones_all_layers.toString());
                    Print.println("  \n ((( the updated all_layers_resources   : " + all_layers_resources.toString());
                }
                
            }
            else
            {
                List<Integer> allocs_already = all_layers_resources.get(rec_type);
                if(Debug)
                Print.println("  \n ((( the NOT updated util per type   : " + allocs_already.toString());
                
                for(int j =0;j <Link_res_info.size();j++ )
                {
                    
                    int all_ones = ones_all_layers.get(j);
                    int layers_allocs_already = allocs_already.get(j);
                    all_ones = layers_allocs_already + all_ones;
                    allocs_already.remove(j);
                    allocs_already.add(j,all_ones);


                }                
                    all_layers_resources.put(rec_type, allocs_already);
                    if(Debug)
                    Print.println("  \n ((( the updated util per type   : " + allocs_already.toString());


            }
        }
        return all_layers_resources;

    }        
    
    /*
     * this method is the amended version of the method above, so it tries to return not only
     * the layer which te resources were allocated. however it wont make a difference
     * on the specific aloatio nmethodoly I am using now which I go directly to the
     * layer I want and allocate in there.
     */
    public HashMap<String,List> return_all_layers_util_of_link_allocs_per_type_modified()
    {
        
        //HashMap<String,List, Integer> all_layers_resources = new HashMap<>();
        HashMap<String,List> all_layers_resources = new HashMap<>();
        
        int keys_arr[] = return_link_rec_keys();
        
        //hashmap type to List
        //HashMap<String, List> eachType_res = new HashMap<>();
        
        // search through all the requests types available there
        if(Debug)
        Print.println("  \n ((( number of records in each link   : " + keys_arr.length);
        for (int i = 0; i < keys_arr.length; i++){
            
            Link_Rec_Obj rec_obj =this.Linkres_allocation_id2res_hash.get(keys_arr[i]);
            //Print.println("  \n ((( the link is   : " + link_id);
            String rec_type = rec_obj.return_linkres_req_type();
            //Print.println("  \n ((( the links rec_type  : " + rec_type);
            List<List> allocs_tree = rec_obj.return_linkres_allocresrouces_family();
            //Print.println("  \n ((( before per link  : " + allocs_tree.toString());
            //List<List> cloned_resources = spread_all_over_combined(allocs_tree, Res_info);
            //Print.println("  \n ((( after cloning and switping per link  : " + cloned_resources.toString());
            
            //List<Integer> ones_all_layers = ResourceOperations.count_ones_in_res_family(cloned_resources);
            
            //Print.println("  the ones_all_layers list " + ones_all_layers);
            
            //here, I am getting only the max layers util
            

            // Print.println("  all_onesall_onesall_onesall_ones " + all_ones + "   rec_obj.return_linkres_req_depth(): " + rec_obj.return_linkres_req_depth());
            //here, per each type, I accumultate the allocs.
            // so I get per link/ per type accumulated allocatoins
            if(!all_layers_resources.containsKey(rec_type))
            {
                List<List> holder_of_all_allocs_perType = new ArrayList<>();
                holder_of_all_allocs_perType.add(allocs_tree);
                all_layers_resources.put(rec_type, holder_of_all_allocs_perType);
                if(Debug){
                    Print.println("  \n ((( the updated util per type   : " + holder_of_all_allocs_perType.toString());
                    Print.println("  \n ((( the updated all_layers_resources   : " + all_layers_resources.toString());
                }
                
            }
            else
            {
//                List<List> holder_of_all_allocs_perType = all_layers_resources.get(rec_type);
//                Print.println("  \n ((( the NOT updated util per type   : " + holder_of_all_allocs_perType.toString());
//                holder_of_all_allocs_perType.add(allocs_tree);
                all_layers_resources.get(rec_type).add(allocs_tree);
                if(Debug)
                    Print.println("  \n ((( the updated util per type   : " + all_layers_resources.toString());


            }
             
        }
                    
            /*
             * now I have to the number of req types, lists of allocated trees
             * 
             * I go through each, make them one list, and then, count the ones per list,
             * 
             * and give that one as in return 
             */
            String keys[] = ResourceOperations.return_rec_keys_str(all_layers_resources);
           
            HashMap<String,List> all_types_combined = new HashMap<>();
            for (int i = 0; i < keys.length; i++){
                
                List<List> listed_alloc_tree = all_layers_resources.get(keys[i]);
                List<List> combined_perType_allocs =  final_linkres_combination(listed_alloc_tree);
                List<List> cloned_resources = spread_linkres_all_over_combined(combined_perType_allocs, Link_res_info);
                if(Debug)
                Print.println("  \n ((( after cloning and switping per link  : " + cloned_resources.toString());

                List<Integer> ones_all_layers = ResourceOperations.count_ones_in_res_family(cloned_resources);
                         
                all_types_combined.put(keys[i],ones_all_layers);

            }

            /*
             * now I count the ones per list, but first I need to spread the ones up and down
             * 
             * and give that one as in return 
             */
               
        
        
        
        
        
        return all_types_combined;

    }        
//    
    public List<Integer> return_all_layer_util_of_link_allocs(Network mNetwork)
    {
        
        //HashMap<String,Integer> all_layers_resources = new HashMap<>();
        Link l = mNetwork.getLink(link_id);
        
        List<Integer> link_res_util = l.get_ParentClass_resrouces().getm_used_agg_layers_resources();

        return link_res_util;

    }      
    
    public List<Integer> return_all_layer_util_of_link_allocs_modified(Network mNetwork)
    {
        
        //HashMap<String,Integer> all_layers_resources = new HashMap<>();
        Link l = mNetwork.getLink(link_id);
 
            List<List> allocs_tree = l.get_ParentClass_resrouces().getme_List_all_layered_resources();
            //Print.println("  \n ((( before per link  : " + allocs_tree.toString());
            List<List> cloned_resources = spread_linkres_all_over_combined(allocs_tree, Link_res_info);
            //Print.println("  \n ((( after cloning and switping per link  : " + cloned_resources.toString());
            
            List<Integer> ones_all_layers = ResourceOperations.count_ones_in_res_family(cloned_resources);
                   
        
        return ones_all_layers;

    }      


    
    
    public List<List> spread_linkres_all_over_combined(List<List> cloned_res, List<String> resinfo)
    {
         

            Cloner cloner=new Cloner();
            List<List> cured_link_res = cloner.deepClone(cloned_res);
           
            
            cured_link_res = spread_linkres_downwards_over_combined(cured_link_res, resinfo);
            if(Debug)
            Print.println("  \nresult of downwards resource spread : " + cured_link_res);
            cured_link_res = spread_linkres_upwards_over_combined(cured_link_res, resinfo);
            if(Debug)
            Print.println("  result of upwards resource spread : " + cured_link_res);
        
        return cured_link_res;
    }
    public List<List> spread_linkres_downwards_over_combined(List<List> existing_resources, List<String> resinfo)
    {

         int depth = existing_resources.size();
         List<List> com_link_resources = new ArrayList<>();
         com_link_resources.add(existing_resources.get(0));

         for(int i = 0; i < depth - 1; i++ )
                 {
                     List<String> ex_layers_list = existing_resources.get(i);
                     List<String> ex_next_layers_list = existing_resources.get(i+1);
                     List<String> new_next_layers_list = new ArrayList<>();
                     for(int j = 0; j < ex_layers_list.size(); j++ )
                     {
                         String ex_cell_str = ex_layers_list.get(j);
//                         Print.println(" ex_cell_str " + ex_cell_str);
                         String[] splt_ex_cell_str = ex_cell_str.split("");
                         
                         // the k starts from one ecause of stupid split method behaviour
                         //Print.println(" splt_ex_cell_str[ size" + splt_ex_cell_str.length);
                         for (int k = 1; k < splt_ex_cell_str.length; k++)
                         {
                             //Print.println(" splt_ex_cell_str[k] " + splt_ex_cell_str[k]);
                             String com = "";
                             if(splt_ex_cell_str[k].matches("1"))
                             {
                                 com = ResourceOperations.create_resource_string_ones(resinfo.get(i+1).length());
                             }
                             else if(splt_ex_cell_str[k].matches("0"))
                             {
                                 
                                 int Desired_cell_address = j * resinfo.get(i).length() + k - 1;
                                 //Print.println(" J " + j + " \t [k] " + k + " Desired_cell_address: " + Desired_cell_address);
                                 String ex_cell_str_next_line = ex_next_layers_list.get(Desired_cell_address);
                                 com = ex_cell_str_next_line;
                             }
                             else
                             {
                                 System.exit(99320);
                             }
                             new_next_layers_list.add(com);
                         }
                     }
//                     Print.println("\ni: " + i );
//                     Print.println("  new_next_layers_list get i " + new_next_layers_list);
//                     Print.println("  existing_resources get i   " + existing_resources.get(i));
                     com_link_resources.add(new_next_layers_list);
                     existing_resources.remove(i+1);
//                     Print.println("i: " + i + "  existing_resources " + existing_resources.toString());
                     existing_resources.add(i+1, new_next_layers_list);
//                     Print.println("i: " + i + "  existing_resources " + existing_resources.toString());
                 }
                 
        return com_link_resources;
    }       
    public List<List> spread_linkres_upwards_over_combined(List<List> existing_resources, List<String> resinfo)
    {
         List<List> com_link_resources = new ArrayList<>();
         com_link_resources.add(existing_resources.get(existing_resources.size()-1));
         for(int i = existing_resources.size()-1; i > 0; i-- )
                 {
                     List<String> ex_layers_list = existing_resources.get(i);
                     List<String> ex_previous_layers_list = existing_resources.get(i - 1);
                     List<String> new_previous_layers_list = existing_resources.get(i - 1);
                     //HashMap<Integer, String>  bbij  =  new HashMap();
//                     Print.println(" \n\niiii  " + i);
                     for(int j = 0; j < ex_layers_list.size(); j++ )
                     {
                         String ex_cell_str = ex_layers_list.get(j);
                         String[] splt_ex_cell_str = ex_cell_str.split("");
                         // the k starts from one ecause of stupid split method behaviour
                          
                         for (int k = 1; k < splt_ex_cell_str.length; k++)
                         {
                             //Print.println(" \t give me J" + j + " \t\tsplt_ex_cell_str[k] " + splt_ex_cell_str[k]);
                             
                             //String com = "";
                             if(splt_ex_cell_str[k].matches("1"))
                             {
                                 int prev_list_lenght = ex_previous_layers_list.get(0).length();
                                 int prev_cell_position = j/prev_list_lenght;
                                 //Print.println(" prev_cell_position " + prev_cell_position ); 
                                 //int prev_char_position = j - prev_cell_position*prev_list_lenght;
                                 int prev_char_position = j%prev_list_lenght;
                                 //Print.println(" prev_char_position " + prev_char_position ); 

                                 
                                 String ex_perv_cell = ex_previous_layers_list.get(prev_cell_position);
                                 int[] ex_perv_cell_arr = Converters.convert_string_to_1Darray(ex_perv_cell);
                                 ex_perv_cell_arr[prev_char_position] = 1;
                                 String new_perv_cell = Converters.convert_array_to_string(ex_perv_cell_arr);
                                 
                                 if(Debug){
                                    Print.println(" ex_previous_layers_list " + ex_previous_layers_list.toString() );
                                    Print.println(" ex_layers_list " + ex_layers_list.toString() ); 
                                    Print.println(" new_perv_cell " + new_perv_cell ); 
                                    Print.println(" prev_list_lenght " + prev_list_lenght + 
                                            " \t prev_cell_position " + prev_cell_position + 
                                            " \t prev_char_position: " + prev_char_position);        
                                 }
                                 //now replace it 
                                 new_previous_layers_list.remove(prev_cell_position);
                                 new_previous_layers_list.add(prev_cell_position, new_perv_cell);
                                 
                                 ex_previous_layers_list = new_previous_layers_list;
                                 existing_resources.remove(i - 1);
                                 existing_resources.add(i-1, ex_previous_layers_list);

                             }
                             else if(splt_ex_cell_str[k].matches("0"))
                             {
                                 //j++;
//                                 int Desired_cell_address = j / resinfo.get(i-1).length();
////                                 Print.println(" J " + j + " \t [k] " + k + " Desired_cell_address: " + Desired_cell_address);
//                                 
//                                 String ex_cell_str_prev_line = ex_previous_layers_list.get(Desired_cell_address);
//                                 com = ex_cell_str_prev_line;
                             }
                             else
                             {
                                 System.exit(99321);
                             }

                         }//Print.println("  bbij.toString()  " + bbij.toString());
                     }
    
                       com_link_resources.add(new_previous_layers_list);
              
                     
                 }
         
        //com_link_resources.add(existing_resources.get(existing_resources.size() - 1)); 
        return existing_resources;         
        //return com_link_resources;
    }

    public  List<List> sort_layercombined_perTypes_inLink(List<List> all_layered_resoruces)
    {
        List<List> sorted_layers = new ArrayList<>();
        
        // I dont want to hardoce the size, however this can be problematic 
        //if having unbalanced sizes
        for(int i = 0; i <all_layered_resoruces.get(0).size(); i++ )
        {
            List<List> layers = new ArrayList<>();
            for(int j = 0; j <all_layered_resoruces.size(); j++ )
            {
                // ecch of the added layered_hops still are a list of a number of strings
                if(Debug)
                    Print.println(" <in recLink preparing before getting statistics>in sorting thinig" + all_layered_resoruces.get(j).get(i));
                layers.add((List)all_layered_resoruces.get(j).get(i));
            }
            //for the numbe rof hops, I have sortedlayer-->lists-->list of strings
            sorted_layers.add(layers);
            //Print.println(" subroute sorting thinig" + Sub_route.toString());
            
        }
        //Print.println(" layers are organised in lists" + sorted_layers );
        return sorted_layers;
    }
        
    public List<List> final_linkres_combination(List<List> all_layered_resoruces)
    {
        //List<List> final_allLayers_combined = new ArrayList<>();
        List<List> final_allLayers_combined = new ArrayList<>();
        List<List> sorted = sort_layercombined_perTypes_inLink(all_layered_resoruces);
        
        // through the layered_hops
        for(int i = 0; i <sorted.size(); i++ )
        {
            List<String> final_layers = new ArrayList<>();
            List<List> layered_hops = sorted.get(i);
            //Print.println("  layered_hops " + layered_hops);
            
            
            /*
             * note: here we have an in to out approach,
             * meaning, we are skimming through the memebrs first, and then the 
             * arrays, so instead of first choosing an array, and then p[laying
             * with the memebrs, we are choosing memerbs across all array
             * there fore J and K counter are reversely put
             */
                    
            
            //through each layer--> hops
            for(int j = 0; j <layered_hops.get(0).size(); j++ )
            {
                
                List<String> hops_cells = new ArrayList();
                String combined_cell = "";
                //get the cell, in each hop
                for(int k = 0; k <layered_hops.size();k++ )
                {
                    List<String> in_each_cell_list = layered_hops.get(k);
                    //Print.println("  in_each_cell_list " + in_each_cell_list);
                    hops_cells.add(in_each_cell_list.get(j));
                    
                }
                combined_cell = res_combiner(hops_cells);
                //Print.println(" \ngood loock at tthis combined_cell " + combined_cell);
                final_layers.add(combined_cell);
            }   
            final_allLayers_combined.add(final_layers);
        }
        return final_allLayers_combined;
        //Print.println(" \n The links are combined now and to be used for allocations " + final_allLayers_combined);
    }
//    public List<List> return_final_combination()
//    {
//       
//        return final_allLayers_combined;
//    }
        
    public String res_combiner(List<String> res){
        
        int [] layer_arr = new int[res.get(0).length()];
        for(int i = 0; i <res.size(); i++ )
        {    
            int [] this_layer = Converters.convert_string_to_1Darray(res.get(i));
            int []combined_arr = ResourceOperations.combine_arrays(layer_arr,this_layer,0);
            layer_arr = combined_arr;
        }
        

        return Converters.convert_array_to_string(layer_arr);
        
    }
        
    public HashMap<Integer, Link_Rec_Obj> returnLinkres_allocation_id2res_hash()
    {
        return Linkres_allocation_id2res_hash;
    }
    public HashMap<Integer, Integer> returnLinkres_allocation_id2time_hash()
    {
        return Linkres_allocation_id2time_hash;
    }
    public int size_Linkres_allocation_id2res_hash()
    {
        return Linkres_allocation_id2res_hash.size();
    }    
}
