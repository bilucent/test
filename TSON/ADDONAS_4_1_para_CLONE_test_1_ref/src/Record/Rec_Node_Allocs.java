


/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package Record;

import Infrastructure.Network;
import Infrastructure.Node;
import Infrastructure.ResourceOperations;
import com.rits.cloning.Cloner;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Set;
import utilities.Converters;
import utilities.Print;
import static utilities.Print.println;

/**
 *
 * @author Bijan
 * a recorder calss, to be created for all the nodes initially
 */
public class Rec_Node_Allocs extends Recorder {
    
    private static int counter;
    private int node_id;
    
    // I use two hash maps here, for id to resrouces, and id to time
    private HashMap<Integer, Node_Rec_Obj> Noderes_allocation_id2res_hash;
    private HashMap<Integer, Integer> Noderes_allocation_id2time_hash;
//    private Network mNetwork;
    private boolean Debug;
    private List<String> Node_res_info;
    Print P = new Print();
    

    public Rec_Node_Allocs(boolean debug, int node_id, List<String> res_info) {
        this.Debug = debug;
        this.Node_res_info = res_info;
//        this.mNetwork = network;
        this.Noderes_allocation_id2res_hash = new HashMap<Integer, Node_Rec_Obj>();
        this.Noderes_allocation_id2time_hash = new HashMap<Integer, Integer>();
        this.counter = 0;
        this.node_id = node_id;
    }
    
    public Rec_Node_Allocs(Rec_Node_Allocs copy) {
                        
        Cloner cloner=new Cloner();
        this.Noderes_allocation_id2time_hash = new HashMap<>();
        this.Noderes_allocation_id2time_hash = cloner.deepClone(copy.Noderes_allocation_id2time_hash);
        this.Node_res_info = 
                        cloner.deepClone(copy.Node_res_info);
        this.Debug = cloner.deepClone(copy.Debug);
        this.node_id = cloner.deepClone(copy.node_id);
        this.counter = cloner.deepClone(copy.counter);
//        this.mNetwork = new Network(copy.mNetwork);
        
        // this block clones the hashmap with objects in it
        this.Noderes_allocation_id2res_hash = new HashMap<Integer, Node_Rec_Obj>();
        Set<Integer> keySet = copy.Noderes_allocation_id2res_hash.keySet();
        Object[] toArray = keySet.toArray();
        int [] intarr = new int [toArray.length];
        for (int i = 0; i < intarr.length; i++){
            intarr[i] = (int) toArray[i];                
        }
        
        //cloning is happening here
        for(int ii :intarr)
        {
            this.Noderes_allocation_id2res_hash.put(ii,
                    new Node_Rec_Obj(copy.Noderes_allocation_id2res_hash.get(ii)));
        }
            
            
    }
    
    public int add_to_node(int id, Node_Rec_Obj aloc_instance){
//        this.Allocation_id2res_hash.put(this.counter, aloc_instance);
//        this.Allocation_id2time_hash.put(this.counter, aloc_instance.StopTime);        
        this.Noderes_allocation_id2res_hash.put(id, aloc_instance);        
        this.Noderes_allocation_id2time_hash.put(id, aloc_instance.return_noderes_stop_time());
        return this.counter++;
    }
    
    public List<Integer> delete_expired_noderes(Network mNetwork,int time){
       
        List<Integer>  deleted = new ArrayList<>();
        List<Integer>  deleted_times = new ArrayList<>();

//       P.println("this.Noderes_allocation_id2time_hash:size " + this.Noderes_allocation_id2time_hash.size());
//       P.println("this.Noderes_allocation_id2res_hash:size " + this.Noderes_allocation_id2res_hash.size());
       if(this.Noderes_allocation_id2time_hash.size()>0){
           
           //check the time value of the recorded keys, so to be deleted maybe
            Set<Integer> keySet = this.Noderes_allocation_id2time_hash.keySet();
            Object[] toArray = keySet.toArray();
            int [] intarr = new int [toArray.length];
            for (int i = 0; i < intarr.length; i++){
                intarr[i] = (int) toArray[i];
            }
            //P.print_1D("  The keyset intarr[i] "+ intarr[i]);
            //P.print_1D("  in expiry stuff, if need to delete? the keyset", intarr);
            for (int i = 0; i < intarr.length; i++){

                
                if(this.Noderes_allocation_id2time_hash.get(intarr[i])<=time){

                   if(Debug)
                   P.println(" something in NODE "+node_id + " need to be deleted.... con id: " + intarr[i]+ " with expiry time of:  " +
                             this.Noderes_allocation_id2time_hash.get(intarr[i]) + " \t and time is" + time);
                   
                   //P.println(" new introduced to track the removals : resrouces recorded and to be deleted are:\n " + this.Allocation_id2res_hash.get(intarr[i]).Alloc_res_family.toString() );
                    //make a record for displaying
                   deleted.add(intarr[i]);
                   deleted_times.add(this.Noderes_allocation_id2time_hash.get(intarr[i]));

                   //remove from node resources
                   Node node = mNetwork.getNode(node_id);
                   try{

                        
                       /*/
                        * 
                        * the layer/ cell only, removal and replacement, to make
                        * it timely effective
                        */                        
                       int layer = Noderes_allocation_id2res_hash.
                                get(intarr[i]).return_noderes_req_depth();
                       List<Integer> cell_positions = Noderes_allocation_id2res_hash.
                                get(intarr[i]).get_node_cell_positions();
                       ResourceOperations.removeTheAllocation_resFamily_node(node, Noderes_allocation_id2res_hash.
                                get(intarr[i]).return_noderes_allocresrouces_family());

                       
                   }
                   catch(NullPointerException e)
                   {
                       P.println(" \nwarning    the node has no allocated resources...  node: " + node.getmID());
                   }
                   //remove from record list
                   this.Noderes_allocation_id2time_hash.remove(intarr[i]);
                   this.Noderes_allocation_id2res_hash.remove(intarr[i]);
                } 
            }        
            return deleted;

        }
       else return null;
    }
    

//    public void  printResHash()
//    {
//        Collection resValues = this.Allocation_id2res_hash.values();
//        Set keys = this.Allocation_id2res_hash.keySet();
//        Object[] KeysArray = keys.toArray();
//        int counter =0;
//        while(counter < Res_hash.size())
//        {
//           String k =  KeysArray[counter].toString();
//           Print.println(" key: " + KeysArray[counter].toString() +" "
//                   + "\t Res: " + Res_hash.get( k)); 
//           counter++;
//        }
//        
//    }
    public void  printResHash()
    {    
            Set<Integer> keySet = this.Noderes_allocation_id2res_hash.keySet();
            Object[] toArray = keySet.toArray();
            int [] intarr = new int [toArray.length];
            for (int i = 0; i < intarr.length; i++){
                intarr[i] = (int) toArray[i];
                //HashMap rec_res =  this.Allocation_id2res_hash.get(intarr[i]).return_allocresrouces();
                this.Noderes_allocation_id2res_hash.get(intarr[i]).display();
                
            }
            
    }
    private int[] return_node_rec_keys()
    {
            Set<Integer> keySet = this.Noderes_allocation_id2res_hash.keySet();
            Object[] toArray = keySet.toArray();
            int [] intarr = new int [toArray.length];
            for (int i = 0; i < intarr.length; i++){
                intarr[i] = (int) toArray[i];
            }
            return intarr;
        
    }    
    
    
    public HashMap<String,Integer> return_aggregated_noderes_across_layers_util_of_allocs_per_type()
    {
        
        HashMap<String,Integer> all_recs_hash = new HashMap<>();
        int keys_arr[] = return_node_rec_keys();
        
        for (int i = 0; i < keys_arr.length; i++){
            
            Node_Rec_Obj rec_obj =this.Noderes_allocation_id2res_hash.get(keys_arr[i]);
            String rec_type = rec_obj.return_noderes_req_type();
            List<List> allocs_tree = rec_obj.return_noderes_allocresrouces_family();
            List<Integer> ones = ResourceOperations.count_ones_in_res_family(allocs_tree);
            
            //here, I am coagulating all the ones_all_layers into one, which is not right
            int all_ones = ResourceOperations.aggre_listInts_to_oneInt(ones);
            
            if(!all_recs_hash.containsKey(rec_type))
            {
                all_recs_hash.put(rec_type, all_ones);
            }
            else
            {
                int allocs_already = all_recs_hash.get(rec_type);
                all_ones = allocs_already + all_ones;
                all_recs_hash.put(rec_type, all_ones);
                
            }
            
        }
        return all_recs_hash;

    }
    
    /*
     * this method is the amended version of the method above, so it tries to return only
     * the layer which te resources were allocated. however it wont make a difference
     * on the specific aloatio nmethodoly I am using now which I go directly to the
     * layer I want and allocate in there.
     */
    public HashMap<String,Integer> return_max_layer_util_of_node_allocs_per_type()
    {
        
        HashMap<String,Integer> all_recs_hash = new HashMap<>();
        int keys_arr[] = return_node_rec_keys();
        
        
        // search through all the requests types available there
        for (int i = 0; i < keys_arr.length; i++){
            
            
            Node_Rec_Obj rec_obj =this.Noderes_allocation_id2res_hash.get(keys_arr[i]);
            String rec_type = rec_obj.return_noderes_req_type();

            List<List> allocs_tree = rec_obj.return_noderes_allocresrouces_family();
            
            List<Integer> ones = ResourceOperations.count_ones_in_res_family(allocs_tree);
        
            //Print.println("  the ones_all_layers list " + ones_all_layers);
            
            //here, I am getting only the max layers util
            int all_ones = ones.get(rec_obj.return_noderes_req_depth());

            
//            P.println("  now in the noderec rex hash lets see the keys length allocs_tree:" + allocs_tree.toString());
            
            // Print.println("  all_onesall_onesall_onesall_ones " + all_ones + "   rec_obj.return_noderes_req_depth(): " + rec_obj.return_linkres_req_depth());
            //here, per each type, I accumultate the allocs.
            // so I get per node/ per type accumulated allocatoins
            if(!all_recs_hash.containsKey(rec_type))
            {
                all_recs_hash.put(rec_type, all_ones);
            }
            else
            {
                int allocs_already = all_recs_hash.get(rec_type);
                all_ones = allocs_already + all_ones;
                all_recs_hash.put(rec_type, all_ones);
                
            }
            
            
        }
        return all_recs_hash;

    }        
    /*
     * this method is the amended version of the method above, so it tries to return not only
     * the layer which te resources were allocated. however it wont make a difference
     * on the specific aloatio nmethodoly I am using now which I go directly to the
     * layer I want and allocate in there.
     */
    public HashMap<String,List> return_all_layers_util_of_node_allocs_per_type()
    {
        
        //HashMap<String,List, Integer> all_layers_resources = new HashMap<>();
        HashMap<String,List> all_layers_resources = new HashMap<>();
        int keys_arr[] = return_node_rec_keys();
        
        
        // search through all the requests types available there
        if(Debug)
        Print.println("  \n ((( number of records in each node   : " + keys_arr.length);
        for (int i = 0; i < keys_arr.length; i++){
            
            Node_Rec_Obj rec_obj =this.Noderes_allocation_id2res_hash.get(keys_arr[i]);
            if(Debug)
            Print.println("  \n ((( the node is   : " + node_id);
            String rec_type = rec_obj.return_noderes_req_type();
            if(Debug)
            Print.println("  \n ((( the ndoes rec_type  : " + rec_type);
            List<List> allocs_tree = rec_obj.return_noderes_allocresrouces_family();
            if(Debug)
            Print.println("  \n ((( before per node  : " + allocs_tree.toString());
            List<List> cloned_resources = spread_noderes_all_over_combined(allocs_tree, Node_res_info);
            if(Debug)
            Print.println("  \n ((( after cloning and switping per node  : " + cloned_resources.toString());
            
            List<Integer> ones_all_layers = ResourceOperations.count_ones_in_res_family(cloned_resources);
            
            //Print.println("  the ones_all_layers list " + ones_all_layers);
            
            //here, I am getting only the max layers util
            

            // Print.println("  all_onesall_onesall_onesall_ones " + all_ones + "   rec_obj.return_noderes_req_depth(): " + rec_obj.return_linkres_req_depth());
            //here, per each type, I accumultate the allocs.
            // so I get per node/ per type accumulated allocatoins
            if(!all_layers_resources.containsKey(rec_type))
            {
                all_layers_resources.put(rec_type, ones_all_layers);
                if(Debug){
                    Print.println("  \n ((( the updated util per type   : " + ones_all_layers.toString());
                    Print.println("  \n ((( the updated all_layers_resources   : " + all_layers_resources.toString());
                }
                
            }
            else
            {
                List<Integer> allocs_already = all_layers_resources.get(rec_type);
                if(Debug)
                Print.println("  \n ((( the NOT updated util per type   : " + allocs_already.toString());
                
                for(int j =0;j <Node_res_info.size();j++ )
                {
                    
                    int all_ones = ones_all_layers.get(j);
                    int layers_allocs_already = allocs_already.get(j);
                    all_ones = layers_allocs_already + all_ones;
                    allocs_already.remove(j);
                    allocs_already.add(j,all_ones);


                }                
                    all_layers_resources.put(rec_type, allocs_already);
                    if(Debug)
                    Print.println("  \n ((( the updated util per type   : " + allocs_already.toString());


            }
        }
        return all_layers_resources;

    }        
    
    /*
     * this method is the amended version of the method above, so it tries to return not only
     * the layer which te resources were allocated. however it wont make a difference
     * on the specific aloatio nmethodoly I am using now which I go directly to the
     * layer I want and allocate in there.
     */
    public HashMap<String,List> return_all_layers_util_of_node_allocs_per_type_modified()
    {
        
        //HashMap<String,List, Integer> all_layers_resources = new HashMap<>();
        HashMap<String,List> all_layers_resources = new HashMap<>();
        
        int keys_arr[] = return_node_rec_keys();
        
        //hashmap type to List
        //HashMap<String, List> eachType_res = new HashMap<>();
        
        // search through all the requests types available there
//        if(Debug)
//        Print.println("  \n ((( number of records in each node   : " + keys_arr.length);
        for (int i = 0; i < keys_arr.length; i++){
            
            Node_Rec_Obj rec_obj =this.Noderes_allocation_id2res_hash.get(keys_arr[i]);
//            Print.println("  \n ((( the node is   : " + node_id);
            String rec_type = rec_obj.return_noderes_req_type();
//            Print.println("  \n ((( the nodes rec_type  : " + rec_type);
            List<List> allocs_tree = rec_obj.return_noderes_allocresrouces_family();
            //Print.println("  \n ((( before per node  : " + allocs_tree.toString());
            //List<List> cloned_resources = spread_all_over_combined(allocs_tree, Res_info);
            //Print.println("  \n ((( after cloning and switping per node  : " + cloned_resources.toString());
            
            //List<Integer> ones_all_layers = ResourceOperations.count_ones_in_res_family(cloned_resources);
            
            //Print.println("  the ones_all_layers list " + ones_all_layers);
            
            //here, I am getting only the max layers util
            

            // Print.println("  all_onesall_onesall_onesall_ones " + all_ones + "   rec_obj.return_noderes_req_depth(): " + rec_obj.return_linkres_req_depth());
            //here, per each type, I accumultate the allocs.
            // so I get per node/ per type accumulated allocatoins

            List<List> holder_of_all_allocs_perType = new ArrayList<>();
//            Print.println("  \n ((( before do resource update all_layers_resources.size()  : " + all_layers_resources.size());

            if(!all_layers_resources.containsKey(rec_type))
            {
                
                holder_of_all_allocs_perType.add(allocs_tree);
                all_layers_resources.put(rec_type, holder_of_all_allocs_perType);
                if(Debug){
                    Print.println("  \n ((( the updated util per type   : " + holder_of_all_allocs_perType.toString());
                    Print.println("  \n ((( the updated all_layers_resources   : " + all_layers_resources.toString());
                }
                
            }
            else
            {
//                List<List> holder_of_all_allocs_perType = all_layers_resources.get(rec_type);
//                Print.println("  \n ((( the NOT updated util per type   : " + holder_of_all_allocs_perType.toString());
//                holder_of_all_allocs_perType.add(allocs_tree);
                all_layers_resources.get(rec_type).add(allocs_tree);
                if(Debug)
                    Print.println("  \n ((( the updated util per type   : " + all_layers_resources.toString());


            }
             
        }
                    
            /*
             * now I have to the number of req types, lists of allocated trees
             * 
             * I go through each, make them one list, and then, count the ones per list,
             * 
             * and give that one as in return 
             */
            String keys[] = ResourceOperations.return_rec_keys_str(all_layers_resources);
           
            HashMap<String,List> all_types_combined = new HashMap<>();
            for (int i = 0; i < keys.length; i++){
                
                List<List> listed_alloc_tree = all_layers_resources.get(keys[i]);
                List<List> combined_perType_allocs =  final_noderes_combination(listed_alloc_tree);
                List<List> cloned_resources = spread_noderes_all_over_combined(combined_perType_allocs, Node_res_info);
                if(Debug)
                Print.println("  \n ((( after cloning and switping per node  : " + cloned_resources.toString());

                List<Integer> ones_all_layers = ResourceOperations.count_ones_in_res_family(cloned_resources);
//                Print.println("  ones_all_layersones_all_layers: " + ones_all_layers.toString());
                         
                all_types_combined.put(keys[i],ones_all_layers);

            }

            /*
             * now I count the ones per list, but first I need to spread the ones up and down
             * 
             * and give that one as in return 
             */
               
        
        
//                Print.println("  \nall_types_combinedall_types_combined : " + all_types_combined.toString());
        
        
        
        return all_types_combined;

    }        
    
    public List<Integer> return_all_layer_util_of_node_allocs(Network mNetwork)
    {
        
        //HashMap<String,Integer> all_layers_resources = new HashMap<>();
        Node n = mNetwork.getNode(node_id);
        
        List<Integer> noderes_util = n.get_ParentClass_resrouces().getm_used_agg_layers_resources();

        return noderes_util;

    }      
    
    public List<Integer> return_all_layer_util_of_node_allocs_modified(Network mNetwork)
    {
        
        //HashMap<String,Integer> all_layers_resources = new HashMap<>();
        Node n = mNetwork.getNode(node_id);
 
            List<List> allocs_tree = n.get_ParentClass_resrouces().getme_List_all_layered_resources();
            //Print.println("  \n ((( before per node  : " + allocs_tree.toString());
            List<List> cloned_resources = spread_noderes_all_over_combined(allocs_tree, Node_res_info);
            //Print.println("  \n ((( after cloning and switping per node  : " + cloned_resources.toString());
            
            List<Integer> ones_all_layers = ResourceOperations.count_ones_in_res_family(cloned_resources);
                   
        
        return ones_all_layers;

    }      
    
   

    
    
    public List<List> spread_noderes_all_over_combined(List<List> cloned_res, List<String> resinfo)
    {
         

            Cloner cloner=new Cloner();
            List<List> cured_node_res = cloner.deepClone(cloned_res);
           
            
            cured_node_res = spread_noderes_downwards_over_combined(cured_node_res, resinfo);
            if(Debug)
            Print.println("  \nresult of downwards resource spread : " + cured_node_res);
            cured_node_res = spread_noderes_upwards_over_combined(cured_node_res, resinfo);
            if(Debug)
            Print.println("  result of upwards resource spread : " + cured_node_res);
        
        return cured_node_res;
    }
    public List<List> spread_noderes_downwards_over_combined(List<List> existing_resources, List<String> resinfo)
    {

         int depth = existing_resources.size();
         List<List> com_node_resources = new ArrayList<>();
         com_node_resources.add(existing_resources.get(0));

         for(int i = 0; i < depth - 1; i++ )
                 {
                     List<String> ex_layers_list = existing_resources.get(i);
                     List<String> ex_next_layers_list = existing_resources.get(i+1);
                     List<String> new_next_layers_list = new ArrayList<>();
                     for(int j = 0; j < ex_layers_list.size(); j++ )
                     {
                         String ex_cell_str = ex_layers_list.get(j);
//                         Print.println(" ex_cell_str " + ex_cell_str);
                         String[] splt_ex_cell_str = ex_cell_str.split("");
                         
                         // the k starts from one ecause of stupid split method behaviour
                         //Print.println(" splt_ex_cell_str[ size" + splt_ex_cell_str.length);
                         for (int k = 1; k < splt_ex_cell_str.length; k++)
                         {
                             //Print.println(" splt_ex_cell_str[k] " + splt_ex_cell_str[k]);
                             String com = "";
                             if(splt_ex_cell_str[k].matches("1"))
                             {
                                 com = ResourceOperations.create_resource_string_ones(resinfo.get(i+1).length());
                             }
                             else if(splt_ex_cell_str[k].matches("0"))
                             {
                                 
                                 int Desired_cell_address = j * resinfo.get(i).length() + k - 1;
                                 //Print.println(" J " + j + " \t [k] " + k + " Desired_cell_address: " + Desired_cell_address);
                                 String ex_cell_str_next_line = ex_next_layers_list.get(Desired_cell_address);
                                 com = ex_cell_str_next_line;
                             }
                             else
                             {
                                 System.exit(99320);
                             }
                             new_next_layers_list.add(com);
                         }
                     }
//                     Print.println("\ni: " + i );
//                     Print.println("  new_next_layers_list get i " + new_next_layers_list);
//                     Print.println("  existing_resources get i   " + existing_resources.get(i));
                     com_node_resources.add(new_next_layers_list);
                     existing_resources.remove(i+1);
//                     Print.println("i: " + i + "  existing_resources " + existing_resources.toString());
                     existing_resources.add(i+1, new_next_layers_list);
//                     Print.println("i: " + i + "  existing_resources " + existing_resources.toString());
                 }
                 
        return com_node_resources;
    }       
    public List<List> spread_noderes_upwards_over_combined(List<List> existing_resources, List<String> resinfo)
    {
         List<List> com_node_resources = new ArrayList<>();
         com_node_resources.add(existing_resources.get(existing_resources.size()-1));
         for(int i = existing_resources.size()-1; i > 0; i-- )
                 {
                     List<String> ex_layers_list = existing_resources.get(i);
                     List<String> ex_previous_layers_list = existing_resources.get(i - 1);
                     List<String> new_previous_layers_list = existing_resources.get(i - 1);
                     //HashMap<Integer, String>  bbij  =  new HashMap();
//                     Print.println(" \n\niiii  " + i);
                     for(int j = 0; j < ex_layers_list.size(); j++ )
                     {
                         String ex_cell_str = ex_layers_list.get(j);
                         String[] splt_ex_cell_str = ex_cell_str.split("");
                         // the k starts from one ecause of stupid split method behaviour
                          
                         for (int k = 1; k < splt_ex_cell_str.length; k++)
                         {
                             //Print.println(" \t give me J" + j + " \t\tsplt_ex_cell_str[k] " + splt_ex_cell_str[k]);
                             
                             //String com = "";
                             if(splt_ex_cell_str[k].matches("1"))
                             {
                                 int prev_list_lenght = ex_previous_layers_list.get(0).length();
                                 int prev_cell_position = j/prev_list_lenght;
                                 //Print.println(" prev_cell_position " + prev_cell_position ); 
                                 //int prev_char_position = j - prev_cell_position*prev_list_lenght;
                                 int prev_char_position = j%prev_list_lenght;
                                 //Print.println(" prev_char_position " + prev_char_position ); 

                                 
                                 String ex_perv_cell = ex_previous_layers_list.get(prev_cell_position);
                                 int[] ex_perv_cell_arr = Converters.convert_string_to_1Darray(ex_perv_cell);
                                 ex_perv_cell_arr[prev_char_position] = 1;
                                 String new_perv_cell = Converters.convert_array_to_string(ex_perv_cell_arr);
                                 
                                 if(Debug){
                                    Print.println(" ex_previous_layers_list " + ex_previous_layers_list.toString() );
                                    Print.println(" ex_layers_list " + ex_layers_list.toString() ); 
                                    Print.println(" new_perv_cell " + new_perv_cell ); 
                                    Print.println(" prev_list_lenght " + prev_list_lenght + 
                                            " \t prev_cell_position " + prev_cell_position + 
                                            " \t prev_char_position: " + prev_char_position);        
                                 }
                                 //now replace it 
                                 new_previous_layers_list.remove(prev_cell_position);
                                 new_previous_layers_list.add(prev_cell_position, new_perv_cell);
                                 
                                 ex_previous_layers_list = new_previous_layers_list;
                                 existing_resources.remove(i - 1);
                                 existing_resources.add(i-1, ex_previous_layers_list);

                             }
                             else if(splt_ex_cell_str[k].matches("0"))
                             {
                                 //j++;
//                                 int Desired_cell_address = j / resinfo.get(i-1).length();
////                                 Print.println(" J " + j + " \t [k] " + k + " Desired_cell_address: " + Desired_cell_address);
//                                 
//                                 String ex_cell_str_prev_line = ex_previous_layers_list.get(Desired_cell_address);
//                                 com = ex_cell_str_prev_line;
                             }
                             else
                             {
                                 System.exit(99321);
                             }

                         }//Print.println("  bbij.toString()  " + bbij.toString());
                     }
    
                       com_node_resources.add(new_previous_layers_list);
              
                     
                 }
         
        //com_node_resources.add(existing_resources.get(existing_resources.size() - 1)); 
        return existing_resources;         
        //return com_node_resources;
    }

    public  List<List> sort_layercombined_perTypes_inNode(List<List> all_layered_resoruces)
    {
        List<List> sorted_layers = new ArrayList<>();
        
        // I dont want to hardoce the size, however this can be problematic 
        //if having unbalanced sizes
        for(int i = 0; i <all_layered_resoruces.get(0).size(); i++ )
        {
            List<List> layers = new ArrayList<>();
            for(int j = 0; j <all_layered_resoruces.size(); j++ )
            {
                // ecch of the added layered_hops still are a list of a number of strings
                if(Debug)
                    Print.println(" <in recnode preparing before getting statistics>in sorting thinig" + all_layered_resoruces.get(j).get(i));
                layers.add((List)all_layered_resoruces.get(j).get(i));
            }
            //for the numbe rof hops, I have sortedlayer-->lists-->list of strings
            sorted_layers.add(layers);
            //Print.println(" subroute sorting thinig" + Sub_route.toString());
            
        }
        //Print.println(" layers are organised in lists" + sorted_layers );
        return sorted_layers;
    }
        
    public List<List> final_noderes_combination(List<List> all_layered_resoruces)
    {
        //List<List> final_allLayers_combined = new ArrayList<>();
        List<List> final_allLayers_combined = new ArrayList<>();
        List<List> sorted = sort_layercombined_perTypes_inNode(all_layered_resoruces);
        
        // through the layered_hops
        for(int i = 0; i <sorted.size(); i++ )
        {
            List<String> final_layers = new ArrayList<>();
            List<List> layered_hops = sorted.get(i);
            //Print.println("  layered_hops " + layered_hops);
            
            
            /*
             * note: here we have an in to out approach,
             * meaning, we are skimming through the memebrs first, and then the 
             * arrays, so instead of first choosing an array, and then p[laying
             * with the memebrs, we are choosing memerbs across all array
             * there fore J and K counter are reversely put
             */
                    
            
            //through each layer--> hops
            for(int j = 0; j <layered_hops.get(0).size(); j++ )
            {
                
                List<String> hops_cells = new ArrayList();
                String combined_cell = "";
                //get the cell, in each hop
                for(int k = 0; k <layered_hops.size();k++ )
                {
                    List<String> in_each_cell_list = layered_hops.get(k);
                    //Print.println("  in_each_cell_list " + in_each_cell_list);
                    hops_cells.add(in_each_cell_list.get(j));
                    
                }
                combined_cell = res_combiner(hops_cells);
                //Print.println(" \ngood loock at tthis combined_cell " + combined_cell);
                final_layers.add(combined_cell);
            }   
            final_allLayers_combined.add(final_layers);
        }
        return final_allLayers_combined;
        //Print.println(" \n The nodes are combined now and to be used for allocations " + final_allLayers_combined);
    }
//    public List<List> return_final_combination()
//    {
//       
//        return final_allLayers_combined;
//    }
        
    public String res_combiner(List<String> res){
        
        int [] layer_arr = new int[res.get(0).length()];
        for(int i = 0; i <res.size(); i++ )
        {    
            int [] this_layer = Converters.convert_string_to_1Darray(res.get(i));
            int []combined_arr = ResourceOperations.combine_arrays(layer_arr,this_layer,0);
            layer_arr = combined_arr;
        }
        

        return Converters.convert_array_to_string(layer_arr);
        
    }
        
    public HashMap<Integer, Node_Rec_Obj> returnNoderes_allocation_id2res_hash()
    {
        return Noderes_allocation_id2res_hash;
    }
    public HashMap<Integer, Integer> returnNoderes_allocation_id2time_hash()
    {
        return Noderes_allocation_id2time_hash;
    }
    public int size_Noderes_allocation_id2res_hash()
    {
        return Noderes_allocation_id2res_hash.size();
    }          
    
}
