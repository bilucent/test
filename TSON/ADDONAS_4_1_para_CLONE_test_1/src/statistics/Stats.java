/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package statistics;

import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Bijan
 */
public class Stats {
    List<Integer>  Allocs;
    int counter;
    String Req_type;
    
    
    //create a result instance in the beginning, for each type of request
    // add this class to each link and node
    public Stats(String req_type)
    {
        //init this alloc instace for this type of reqs
        this.Req_type = req_type;
        
        //this will hold the numbers allocated per each yes
        this.Allocs = new ArrayList<>();
        
        // no matter yes or no, the counter will go up
        this.counter =  0;
        
    }
    
    public int givemethe_counter()
    {
        return counter;
    }
    
    public String givemethe_associated_reqtype()
    {
        return Req_type;
    }
    
    public List<Integer>  givemethe_alloc_list()
    {
        return Allocs;
    }
    
    public void incremenet_counter()
    {
        this.counter++;
    }
    
    public void add_to_allo_list(int allocated_slots)
    {
        this.Allocs.add(counter);
    }
  
    
}
