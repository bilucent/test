/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package alloc;

import routing.Route;

/**
 *
 * @author Bijan
 * the only thing this class does is to associate the route with the
 * allocation objects
 */
public class LinkAllocPacked {
    private Route subroute = null;
    private Alloc_combinedRes_Link  ACL = null;
    public LinkAllocPacked(Route r, Alloc_combinedRes_Link acl)
    {
        subroute = r;
        ACL = acl;
    }
    
    public Route getSubRoute()
    {
        return subroute;
    }
    public Alloc_combinedRes_Link getAlloc_combinedRes_Link()
    {
        return ACL;
    }
}
