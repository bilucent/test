/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package alloc;

import Infrastructure.Link;
import Infrastructure.Network;
import Infrastructure.Node;
import Infrastructure.ResourceOperations;
import java.util.ArrayList;
import java.util.List;
import resources.Parent;
import routing.Route;
import utilities.Converters;
import utilities.Maths;
import utilities.Print;

/**
 *
 * @author Bijan
 * This method calcualted resoruce per node and links on the route.
 * this might not be the case always. So I wil make a stand alone 
 * node allocation
 * 
 */
public class Link_res_aloc_route {
    
    Route Sub_route;
    String alloc_policy;
    List<Link> links_list;
    List<Node> nodes_list;
    
    List<Parent> links_res_family;
    List<Parent> nodes_res_family;
    
    List<List> combof_all_links_layered_resoruces;
    List<List> combof_all_nodes_layered_resoruces;
    
    List<List> all_links_layered_resoruces;
    List<List> all_nodes_layered_resoruces;
    
    List<List> final_all_links_Layers_combined;
    List<List> final_all_nodes_Layers_combined;
    
    Network mNetwork;
    int link_resource_layers =0;
    int node_resource_layers =0;
    
    int hop_number_with_index_of_min_level_res_links = 0;
    int hop_number_with_index_of_min_level_res_nodes = 0;
    public Link_res_aloc_route(Network network, Route sub_route, boolean noderes_included)
    {
        this.mNetwork = network;
        this.Sub_route = sub_route;
        
        
        /*
         * this is the link resource processing
         */        
        this.links_list = mNetwork.return_links_in_route(sub_route);
        this.combof_all_links_layered_resoruces = new ArrayList<>();
        this.all_links_layered_resoruces = new ArrayList<>();
        this.final_all_links_Layers_combined = new ArrayList<>();
        this.links_res_family = new ArrayList<>();
        for(int i =0; i < links_list.size(); i ++)
        {
            // get all the parent calsses
            this.links_res_family.add(links_list.get(i).get_ParentClass_resrouces());
            //Print.println(" \t th resources per hop in the GetRouteLinkResources class " + links_list.get(i).get_ParentClass_resrouces().getme_List_all_layered_resources().toString());

            //get all the layered lists
            this.all_links_layered_resoruces.add(links_res_family.get(i).getme_List_all_layered_resources());
        }

        this.cal_route_wide_level_of_link_res_layer();
        //this.sort_link_layercombined_inRoute_res_family();
        this.final_link_combination();
                

        /*
         * the allocation per node on the route can happen here.
         * however for OFC since I need end node resource allocation
         * I allocate the node resrouces when I have processed the links
         */
        if(noderes_included)
        {
            /*
             * this is the node resource processing
             */        
            this.nodes_list = mNetwork.return_nodes_in_route(sub_route);
            this.combof_all_nodes_layered_resoruces = new ArrayList<>();
            this.all_nodes_layered_resoruces = new ArrayList<>();
            this.final_all_nodes_Layers_combined = new ArrayList<>();
            this.nodes_res_family = new ArrayList<>();
            for(int i =0; i < nodes_list.size(); i ++)
            {
                // get all the parent calsses
                this.nodes_res_family.add(nodes_list.get(i).get_ParentClass_resrouces());
                //Print.println(" \t th resources per hop in the GetRouteLinkResources class " + links_list.get(i).get_ParentClass_resrouces().getme_List_all_layered_resources().toString());

                //get all the layered lists
                this.all_nodes_layered_resoruces.add(nodes_res_family.get(i).getme_List_all_layered_resources());
            }


            this.cal_route_wide_level_of_node_res_layer();
            //this.sort_link_layercombined_inRoute_res_family();
            this.final_node_combination();
        }
        
        
    }
    /*
     * since the links in a synchronised system can have multilayers
     * the alloation should consider the layers
     */
    
    public void cal_route_wide_level_of_link_res_layer()
    {
        int[] res_levels = new int[links_list.size()];
        for(int i =0; i < links_list.size(); i ++)
        {
            res_levels[i] = links_res_family.get(i).return_res_number_layers();
        }
        int min = Maths.find_min(res_levels);
        int min_index = Maths.find_min_return_index(res_levels);
        this.link_resource_layers = min;
        this.hop_number_with_index_of_min_level_res_links = min_index;
        
        //Print.println("  in cal route leevl minimum level ... " + min +   " and the index is " + min_index);
 
    }    
    public void cal_route_wide_level_of_node_res_layer()
    {
        int[] res_levels = new int[nodes_list.size()];
        for(int i =0; i < nodes_list.size(); i ++)
        {
            res_levels[i] = nodes_res_family.get(i).return_res_number_layers();
        }
        int min = Maths.find_min(res_levels);
        int min_index = Maths.find_min_return_index(res_levels);
        this.node_resource_layers = min;
        this.hop_number_with_index_of_min_level_res_nodes = min_index;
        
        //Print.println("  in cal route leevl minimum level ... " + min +   " and the index is " + min_index);
    }    
    public List<String> res_info_structure()
    {
        List<Link> links = mNetwork.return_links_in_route(Sub_route);
        Link l =links.get(hop_number_with_index_of_min_level_res_links);
        return l.get_ParentClass_resrouces().getm_resinfo();
    }
    
    
    public int getm_route_wide_levleof_res_layer()
    {
 
        return this.link_resource_layers;
    }
    public int getm_number_hops_subroute()
    {
        return Sub_route.getmLength();
    }

    public  List<List> sort_link_layercombined_inRoute_res_family()
    {
        List<List> sorted_layers = new ArrayList<>(); 
        for(int i = 0; i <this.link_resource_layers; i++ )
        {
            List<List> layers = new ArrayList<>();
            for(int j = 0; j <this.all_links_layered_resoruces.size(); j++ )
            {
                // ecch of the added layered_hops still are a list of a number of strings
                //Print.println(" in sorting thinig" + this.all_links_layered_resoruces.get(j).get(i));
                layers.add((List)this.all_links_layered_resoruces.get(j).get(i));
            }
            //for the numbe rof hops, I have sortedlayer-->lists-->list of strings
            sorted_layers.add(layers);
            //Print.println(" subroute sorting thinig" + Sub_route.toString());
            
        }
        //Print.println(" layers are organised in lists" + sorted_layers );
        return sorted_layers;
    }
        
    public void final_link_combination()
    {
        //List<List> final_all_links_Layers_combined = new ArrayList<>();
        
        List<List> sorted = sort_link_layercombined_inRoute_res_family();
        
        // through the layered_hops
        for(int i = 0; i <sorted.size(); i++ )
        {
            List<String> final_layers = new ArrayList<>();
            List<List> layered_hops = sorted.get(i);
            //Print.println("  layered_hops " + layered_hops);
            
            
            /*
             * note: here we have an in to out approach,
             * meaning, we are skimming through the memebrs first, and then the 
             * arrays, so instead of first choosing an array, and then p[laying
             * with the memebrs, we are choosing memerbs across all array
             * there fore J and K counter are reversely put
             */
                    
            
            //through each layer--> hops
            for(int j = 0; j <layered_hops.get(0).size(); j++ )
            {
                
                List<String> hops_cells = new ArrayList();
                String combined_cell = "";
                //get the cell, in each hop
                for(int k = 0; k <layered_hops.size();k++ )
                {
                    List<String> in_each_cell_list = layered_hops.get(k);
                    //Print.println("  in_each_cell_list " + in_each_cell_list);
                    hops_cells.add(in_each_cell_list.get(j));
                    
                }
                combined_cell = res_combiner(hops_cells);
                //Print.println(" \ngood loock at tthis combined_cell " + combined_cell);
                final_layers.add(combined_cell);
            }   
            final_all_links_Layers_combined.add(final_layers);
        }
        
        //Print.println(" \n The links are combined now and to be used for allocations " + final_all_links_Layers_combined);
    }
    public  List<List> sort_node_layercombined_inRoute_res_family()
    {
        List<List> sorted_layers = new ArrayList<>(); 
        for(int i = 0; i <this.node_resource_layers; i++ )
        {
            List<List> layers = new ArrayList<>();
            for(int j = 0; j <this.all_nodes_layered_resoruces.size(); j++ )
            {
                // ecch of the added layered_hops still are a list of a number of strings
                //Print.println(" in sorting thinig" + this.all_links_layered_resoruces.get(j).get(i));
                layers.add((List)this.all_nodes_layered_resoruces.get(j).get(i));
            }
            //for the numbe rof hops, I have sortedlayer-->lists-->list of strings
            sorted_layers.add(layers);
            //Print.println(" subroute sorting thinig" + Sub_route.toString());
            
        }
        //Print.println(" layers are organised in lists" + sorted_layers );
        return sorted_layers;
    }
        
    public void final_node_combination()
    {
        //List<List> final_all_links_Layers_combined = new ArrayList<>();
        
        List<List> sorted = sort_node_layercombined_inRoute_res_family();
        
        // through the layered_hops
        for(int i = 0; i <sorted.size(); i++ )
        {
            List<String> final_layers = new ArrayList<>();
            List<List> layered_hops = sorted.get(i);
            //Print.println("  layered_hops " + layered_hops);
            
            
            /*
             * note: here we have an in to out approach,
             * meaning, we are skimming through the memebrs first, and then the 
             * arrays, so instead of first choosing an array, and then p[laying
             * with the memebrs, we are choosing memerbs across all array
             * there fore J and K counter are reversely put
             */
                    
            
            //through each layer--> hops
            for(int j = 0; j <layered_hops.get(0).size(); j++ )
            {
                
                List<String> hops_cells = new ArrayList();
                String combined_cell = "";
                //get the cell, in each hop
                for(int k = 0; k <layered_hops.size();k++ )
                {
                    List<String> in_each_cell_list = layered_hops.get(k);
                    //Print.println("  in_each_cell_list " + in_each_cell_list);
                    hops_cells.add(in_each_cell_list.get(j));
                    
                }
                combined_cell = res_combiner(hops_cells);
                //Print.println(" \ngood loock at tthis combined_cell " + combined_cell);
                final_layers.add(combined_cell);
            }   
            final_all_nodes_Layers_combined.add(final_layers);
        }
        
        //Print.println(" \n The links are combined now and to be used for allocations " + final_all_links_Layers_combined);
    }
    public List<List> return_final_links_combination()
    {
       
        return final_all_links_Layers_combined;
    }
    public List<List> return_final_nodes_combination()
    {
       
        return final_all_nodes_Layers_combined;
    }
        
    public String res_combiner(List<String> res){
        
        int [] layer_arr = new int[res.get(0).length()];
        for(int i = 0; i <res.size(); i++ )
        {    
            int [] this_layer = Converters.convert_string_to_1Darray(res.get(i));
            int []combined_arr = ResourceOperations.combine_arrays(layer_arr,this_layer,0);
            layer_arr = combined_arr;
        }
        

        return Converters.convert_array_to_string(layer_arr);
        
    }
    
    
     public int [] allocated_add_existing(int [] existing_array, int [] new_array)
     {
         int [] final_array = new int [existing_array.length];
         int i = 0;
         while((i )<existing_array.length)
         {
             final_array[i] = existing_array[i] + new_array[i];
             i++;
         }
         
         return final_array;
     } 
      
}
