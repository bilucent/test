/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package alloc;

import Infrastructure.Network;
//import Infrastructure.ResourcesTable;
import assignments.*;
import routing.Route;
import routing.RoutingTable;

/**
 *
 * @author Bijan
 */
public abstract class Heuristic_base {
    
    /*
    the algorithm representation is like this:
    type(type)_(Dim)(dimension) 
    */ 
    public static final int FF_1D = 1;
    public static final int FF_MD = 2;
    public static final int FFC_1D = 3;
    public static final int FFC_MD = 4;
    public static final int FFR_1D = 5;
    public static final int FFR_MD = 6;
    
	/** The temporary resources table. */
	//protected ResourcesTable mTempResTable;
	/** A reference to the network. */
	protected Network mNetwork;
	protected Network CopyNetwork;
     
       
	/** The type of algorithm. */
	protected int mAlgorithmType;
	/** The type of sorting/ordering the input request table. */
	protected int mOrderType;
	/** The maximum number of k routes used in the routing module. */
	protected int mNumKRoutes;   
        
            
    public void process_by_delay(Route route){
        
        
    }
    public void process_by_cost(){
        
    }    
    public void process_by_location(){
        
    }    
    public void process_by_delay(){
        
    }    
    public void process_by_none(){
        
    }  
        
        
   
    
}
