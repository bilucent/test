/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package alloc;

import java.util.List;

/**
 *
 * @author Bijan
 */
public class Alg_alloc_object {
    boolean is_allocated;
    int Load;
    int Delay;
    int [] already_assignmnet;
    int [] result_assignmnet;
    
    //I am trying this, to optimise the tracking after the allocaitons
    List<Integer> cell_positions;
    public Alg_alloc_object(int load, int [] already_ass, int arr_size, int delay ){
        this.Delay = delay;
        this.is_allocated = false;
        this.Load = load;
        this.already_assignmnet = already_ass;
        this.result_assignmnet = new int[arr_size];
    
    }

    public void set_cell_positions(List<Integer> cel_pos)
    {
        this.cell_positions = cel_pos;
    }    
    
    public List<Integer> get_cell_positions()
    {
        return cell_positions;
    }
            
    
    public int get_Delay()
    {
        return this.Delay;
    }    
    public void set_load(int l)
    {
        this.Load = l;
    }
    public void set_result_assignmnet(int[] ass)
    {
        this.result_assignmnet = ass;
    }
    
    public void set_result_in_already_assignmnet(int[] ass)
    {
        this.result_assignmnet = ass;
    }  
    
    public void set_is_alloc(boolean is)
    {
        this.is_allocated = is;
    }    
    public boolean get_is_alloc()
    {
        return this.is_allocated;
    }    
    public int get_load()
    {
        return this.Load;
    }
    public int[] get_already_assignmnet()
    {
        return this.already_assignmnet;
    }    
    public int[] get_result_assignmnet()
    {
        return this.result_assignmnet;
    }     
}
