/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package alloc;

import Infrastructure.*;
import Record.Link_Rec_Obj;
import Record.Node_Rec_Obj;
import Record.Rec_Link_Allocs;
import Record.Rec_Node_Allocs;
import RequestTypes.Req_BW;
import RequestTypes.Req_IT_NET_timed;
import RequestTypes.Request_base;
import RequestTypes.Request_base;
import RequestTypes.Req_IT_NET_timed_startnet;
import addonas.ADDONAS;
import alloc.GetRoute;
import assignments.Assignment_alg_return;
import assignments.Assignment_base;
import com.mysql.jdbc.exceptions.DeadlockTimeoutRollbackMarker;
import java.sql.SQLException;
//import com.sun.org.apache.bcel.internal.generic.AALOAD;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.openxmlformats.schemas.drawingml.x2006.chart.STScatterStyle;
import routing.Find_SUN;
import routing.Route;
import routing.RoutingTable;
import statistics.Stats_collector;
import statistics.Stats_collector_Star;
import utilities.Print;
import utilities.Converters;
import utilities.InputReader;

/**
 *
 * @author Bijan
 */
public class RUN_Heuristic_Star extends Heuristic_base{
    
    String Route_select;
    int Spec_time_map;
    int SubL_max;
    int SubL_min;
    int Lambda_min;
    int Lambda_max;
    int Flex_min;      
    int Flex_max;  
    static int star_request_blocked;
    static int star_request_allowed;
    static int star_request;
    static int star_request_route_failed;
    
    static int static_IDI;

    
    int Added_Link_Con;
    int Deleted_Link_Con;
    
    //int Con_duration;
    Print P = new Print();
    Converters  C; 
    int Con_time;
    int Star_id;
    boolean Debug;
    int VON_ID;
    HashMap<String,Rec_Link_Allocs> Record_hash_link;
    HashMap<Integer,Rec_Node_Allocs> Record_hash_node;
    
    HashMap<String,Rec_Link_Allocs>  temporal_Link_record_hash;
    HashMap<Integer,Rec_Node_Allocs>  temporal_Node_record_hash;    
    
    String Star_Req_type;
    boolean STARNET;
//    Request_base StarRequest;
    Req_IT_NET_timed_startnet StarRequest;
    Stats_collector_Star SC;
    GetStarNet getstar;
    GetStarNet_nonodebb getstarnbb;
    /*
     * in constructure the class is made witht its corresponding network in it
     */
    public RUN_Heuristic_Star(boolean debug, int VON_id, Network mNetwork, 
            int numRoutes, Stats_collector_Star sc, 
            HashMap<String,Rec_Link_Allocs> link_record_hash, 
            HashMap<Integer,Rec_Node_Allocs> node_record_hash, 
            int spec_time_map, int subl_min, int subl_max, int lambda_min,
            int lambda_max, int flex_max, int flex_min, String route_select,
            boolean Starnet, int static_idi) {
        this.STARNET = Starnet;
        this.mNetwork = mNetwork;
        this.mNumKRoutes = numRoutes;
        this.P = new Print();
        this.C = new Converters();
        this.VON_ID = VON_id;
        this.star_request_blocked = 0;
        this.star_request_allowed  = 0;
        this.star_request = 0;
        this.star_request_route_failed = 0;     
        this.static_IDI = static_idi;

        //this.Con_duration  = duration;
        this.Debug = debug;
        this.Record_hash_link = link_record_hash;
        this.Record_hash_node = node_record_hash;
//        this.temporal_Link_record_hash = Record_hash_link;
//        this.temporal_Node_record_hash = Record_hash_node;
        this.temporal_Link_record_hash = new HashMap<>();
        this.temporal_Node_record_hash = new HashMap<>();
        init_recordList();
        //req_type = "";
        this.Lambda_min = lambda_min;
        this.Lambda_max = lambda_max;
        this.SubL_max = subl_max;
        this.SubL_min = subl_min;
        this.Flex_max = flex_max;
        this.Flex_min = flex_min;
        
        
        
        
        this.Spec_time_map = spec_time_map;    
        this.Route_select = route_select;
        this.SC = sc;
        
//        P.println("  VON ID : "+ VON_ID +" link resources:::: &&&*************************************  ");
        //

    }

    /*
     * this function is the gate, gets the list of reqs, so to process them
     */
    public void init_process_request(List<Req_IT_NET_timed_startnet> req_list, int con_id, int time)
    {
        //List req_list = req_list;
        int num_reqs = req_list.size();
        
        this.Con_time = time;
        this.Star_id = con_id;
        
        this.StarRequest = req_list.get(Star_id);
        //P.println("num_reqs: " + num_reqs );

        this.Star_Req_type = this.StarRequest.returnStarReqType();
        P.println("\n\n  VON ID : "+ VON_ID +" &&&******RUN_Heuristic  ************  ");
        P.println("  VON ID : "+ VON_ID +" &&&*************************************  ");
        P.println("  VON ID : "+ VON_ID +" &&&****  REQUEST NUMBER : " + this.Star_id+ " ******  ");

        if(!STARNET)
        {
            P.println("  VON ID : "+ VON_ID +" &&&****  SRC  : " + this.StarRequest.returnSrc()+ " ******  ");
            P.println("  VON ID : "+ VON_ID +" &&&****  DEST  : " + this.StarRequest.returnsDest()+ " ******  ");
        }
        if(STARNET)
        {
            Req_IT_NET_timed_startnet tempreq = (Req_IT_NET_timed_startnet)StarRequest;
            P.println("  VON ID : "+ VON_ID +" &&&****  StarRequest REQ TIME : " + StarRequest.returnStartTime()+ " ******  ");        
         
            P.println("  VON ID : "+ VON_ID +" &&&****  SRC&DEST  : " + tempreq.returnSrcdest().toString()+ " ******  ");          
            
        }
        P.println("  VON ID : "+ VON_ID +" &&&****  REQ TIME : " + this.Con_time+ " ******  ");        
        P.println("  VON ID : "+ VON_ID +" &&&****  REQ TYPE : " + this.Star_Req_type+ " ******  ");        
        P.println("  VON ID : "+ VON_ID +" &&&****  EXP TIME : " +
                StarRequest.returnStopTime() + " ******  ");        
        P.println("  VON ID : "+ VON_ID +" &&&*************************************  ");
//        P.println("  VON ID : "+ VON_ID +" link resources:::: &&&*************************************  ");
//

        
        if(Debug)
        this.StarRequest.displayAttributes();
        

        //if((Request_base)req_list.get(this.Star_id)!= null)
        if(this.Star_id<req_list.size())
        {
            if(STARNET)
            {
                start_process_request_Star((Req_IT_NET_timed_startnet)req_list.get(this.Star_id));
            }
            else
            {
//                start_process_request_GR(nodes_list,(Request_base)req_list.get(this.Star_id));
                    throw new UnsupportedOperationException("Not supported anymore."); //To change body of generated methods, choose Tools | Templates.
            }
        
        }
        else 
        {
            P.println("  VON ID : "+ VON_ID +" **** No requests left in the list!! cleaning the records **** ");
            swipeAllTheRecords(mNetwork,Record_hash_link,Record_hash_node );
            //swipeAllLinksForTheRecords(mNetwork.getmListLinks());
        }
    }
    public void fin()
    {
        show_all_link_resources(mNetwork);

        P.println("  VON ID : "+ VON_ID +" &&&******FINISHING THIS INSTANCE ****************  ");
        P.println("  VON ID : "+ VON_ID +" &&&****  BLOCKED: " + this.star_request_blocked+ " * + ALLOWED: " 
                + this.star_request_allowed+"*****  ");
        P.println("  VON ID : "+ VON_ID +" &&&**********************************************  ");
    
    }
    
    /*
     * this methods is called from the VODthread... class
     */   
    public void cleanUpTheRecords()
    {

            swipeAllTheRecords(mNetwork,Record_hash_link,Record_hash_node );
    
    }
    
    /*
     * there are two lists, one for nodes, one for links
     */
    public void init_recordList()
    {
        
        
        
        List<Link> link_list = mNetwork.getmListLinks();
        List<Node> node_list = mNetwork.getmListNodes();
        
        //for each link, make a recording object
        for(int i = 0; i < link_list.size(); i++)
        {
            Record_hash_link.put(link_list.get(i).getmID(), new Rec_Link_Allocs
                    (Debug,link_list.get(i).getmID(), link_list.get(i).getmNumber(), 
                    link_list.get(i).get_ParentClass_resrouces().getm_resinfo()));
        }
        //and for each node
        for(int i = 0; i < node_list.size(); i++)
        {
            Record_hash_node.put(node_list.get(i).getmID(), new Rec_Node_Allocs
                    (Debug, node_list.get(i).getmID(), 
                    node_list.get(i).get_ParentClass_resrouces().getm_resinfo()));
        }
    }
        
//    public void start_process_request_GR(List nodes_list ,Request_base req)
//    {
//                Print.println(" ITREQ  " + req.returnITREQ());
//                int src = req.returnSrc();
//                int dest = req.returnsDest();
//                List  requested_network_resource =  req.returnNETREQ();          
//                int request_dimension = requested_network_resource.size();
//                String req_algorithm_type = req.returnAlgorithmType();  
//                //this.Star_Req_type = req.returnConnectionReqType();
//                if(Debug)
//                P.println("  VON ID : ****** "+ VON_ID +" \t Start_process_request req number " + Star_id + 
//                        "  with request category " + Star_Req_type  + "\n\tsrc : " + src
//                        + "\tdest : " + dest + "  req has  "+ request_dimension+
//                        " layers" + "\t and needs req_ALG_type: " + req_algorithm_type + "\n*****");
//                
//                //get the node information for retreiving routing table 
//                Node node = (Node) nodes_list.get(src);
//                if(Debug){
//                    P.println("  VON ID : "+ VON_ID +" routes avaialble for node " + node.getmID());
//                    node.getmRouting().print_routes();
//                }
//
//                P.println(" +++++++++++++++++++++++++++++++++++++++++: " );
//                //if(Debug)
//                //show_records();
//                //P.println(" +++++++++++++++++++++++++++++++++++++++++: " );
//                
//                
//                //I changed my code, so prior to getting to the routing, I clear up 
//                //the resource database, rather than clearing a a selected route
//                //because my routing now is based on availabilities, not only the
//                // number of the paths
//                swipeAllTheRecords();
//                GetRoute getroute = new GetRoute(Debug, mNetwork, node, req, VON_ID, Con_time, Star_id, 
//                        Record_hash_link,
//                        Record_hash_node,
//                        Spec_time_map, 
//                        SubL_min,SubL_max,Lambda_min,Lambda_max, Flex_max, Flex_min,
//                        Route_select);
//                
//                int number_route_to_try = mNumKRoutes;
//                
//                //node resource allocation should happen in here
//                /*
//                 * the run_routing function bascailly, starts a chain of actions
//                 * finding a route, which can be differnet algorithms
//                 * then 
//                 */
//                boolean what_happened = getroute.run_routing(number_route_to_try, this.star_request_blocked,this.star_request_allowed);
//                
//                
//                if(what_happened)
//                {
//                    this.star_request_allowed++;
////                    this.SC.add_to_admitted_list_link_type(StarRequest);
////                    this.SC.add_to_admitted_list_node_type(StarRequest);
//                    this.SC.addmitting_star_request(StarRequest);
//                    this.SC.add_to_admitted_link_STAR_type( StarRequest);
//                    this.SC.add_to_admitted_node_STAR_type(StarRequest);
//                    P.println(" ++++++++++++++++++++++++ ADD to the route list: " + getroute.get_route() );
//
//                    this.SC.add_to_route_list(getroute.get_route());
//                    this.SC.add_to_route_list_per_req(getroute.get_route(), Star_Req_type);
//                    P.println(" +++++++++++++++++++++++++++++++++++++++++: " );
//                }
//                else
//                {
//                    this.star_request_blocked++;
////                    this.SC.add_to_blocking_list_link_type(req);
//                }
//                
//                //P.println(" <<<<<<<<<<<<<<<< the run_routing is done: this is what happened: "  + goodstuff);
//                //P.println(" <<<<<<<<<<<<< plus the detailed resoruces of the network");
//                //show_all_link_resources(mNetwork);
//                //this.SC.demonstrate_linkRec_objects();
//                
//                
//
//                     
//    } 
    public void start_process_request_Star(Req_IT_NET_timed_startnet reqs)  
    {
        
//        show_records("ONEONEONE++++++++++++++++++++++++++++++++++ check the bstard records before anything: ");

        Print.println(" Star Net not STAR  ");
        List<Route> allRoutes = new ArrayList<>();
        //create a copy of the network
//        mNetwork.createNetworkGraph();

        
//        Network CopyNetwork = new Network(mNetwork);
		List<Link> mListLinks = new ArrayList<>();
                for(Link l :mNetwork.getmListLinks()) mListLinks.add(new Link(l));
                
		List<Node> mListNodes = new ArrayList<>();
                for(Node n :mNetwork.getmListNodes()) mListNodes.add(new Node(n)); 
 

                Network CopyNetwork = new Network(mListNodes,mListLinks,
                mNetwork.retrun_node_types_dimension_hash(),
                mNetwork.retrun_link_types_dimension_hash(), 
                mNetwork.retrun_link_adj_hash());    
        
        
//                mNetwork.createNetworkGraph();
                fillRoutes( mNetwork, 3);        

		for(int i=0; i<mNetwork.getmListNodes().size(); i++){
			Node node = mNetwork.getmListNodes().get(i);
//                        P.println(" mNetwork TEST node.getmRouting().get_number_of_routes_all() " + node.getmRouting().get_number_of_routes_all());
			
//                        node.getmRouting().getmRoutingTable().delRouteS(i);
//                        node.getmRouting().searchKShortestPaths(CopyNetwork, 3); 
                }
                CopyNetwork.createNetworkGraph();
                CopyNetwork.create_link_hash();                
                fillRoutes( CopyNetwork, 3);   
               
                init_recordList_temporal_ones(CopyNetwork);

                
		for(int i=0; i<CopyNetwork.getmListNodes().size(); i++){
			Node node = CopyNetwork.getmListNodes().get(i);
//                        P.println(" CopyNetwork TEST node.getmRouting().get_number_of_routes_all() " + node.getmRouting().get_number_of_routes_all());
			
//                        node.getmRouting().getmRoutingTable().delRouteS(i);
//                        node.getmRouting().searchKShortestPaths(CopyNetwork, 3); 
                }
                
//                CopyNetwork.getmRouting().getmRoutingTable().delRouteS(dest);
                
        
//        CopyNetwork.createNetworkGraph();
//        CopyNetwork.create_link_hash();
        
//        P.println(" the numbef od nodews in the copynetwork:" + CopyNetwork.getmListNodes().size());
//        fillRoutes( CopyNetwork, 3);        
//        ADDONAS.fillRoutes(CopyNetwork, 3);        
//        HashMap<Integer, Rec_Node_Allocs> tmp_recnode = (Record_hash_node);
//        HashMap<String, Rec_Link_Allocs> tmp_reclink = (Record_hash_link);
//        HashMap<Integer, Rec_Node_Allocs> tmp_recnode = new HashMap<>();
//        init_NoderecordList(tmp_recnode);
//        tmp_recnode = clone_nodeHashRecords(Record_hash_node);
//        tmp_recnode = (Record_hash_node);
        
        
//        HashMap<String, Rec_Link_Allocs> tmp_reclink = new HashMap<>();
//        init_LinkrecordList(tmp_reclink);
//        tmp_reclink = clone_linkHashRecords(Record_hash_link);
//        tmp_reclink = Record_hash_link;
        
        
//        HashMap tmp_recnode = (Record_hash_node);
//        HashMap tmp_reclink = (Record_hash_link);
        Print.println("  \n****************  Process the star request  ********************" + star_request++ );
        Print.println("  ****************  Temp network resources/node and link hashes created  ********************");        
//        Print.println(" tempnet node sizes  " + CopyNetwork.getmListNodes().size());
//        Print.println(" mNetwork node sizes  " + mNetwork.getmListNodes().size());
//        Print.println(" show the records ");
//        Print.println("  \n****************  test  ********************" );
        if(Debug)
        {
            P.println(" @INFO############################## before swipping " + Star_id);
            P.println(" @INFO############################## Show the link resources @ Request " + Star_id);
            show_all_link_resources(CopyNetwork);
            P.println(" @INFO############################## /Show the link resources  " + Star_id);
            P.println(" @INFO############################## Show the node resources  " + Star_id);
            show_all_node_resources(CopyNetwork);
            P.println(" @INFO############################## /Show the node resources  " + Star_id);
        }


        swipeAllTheRecords(CopyNetwork,Record_hash_link,Record_hash_node );
        swipeAllTheRecords(mNetwork,Record_hash_link,Record_hash_node );
//        
//        if(StarRequest.returnId()>=24&&StarRequest.returnId()<=26)    
        if(Debug)    
        {
            P.println(" @INFO############################## after swipping " + Star_id);
            P.println(" @INFO############################## Show the link resources @ Request " + Star_id);
            show_all_link_resources(CopyNetwork);
            P.println(" @INFO############################## /Show the link resources  " + Star_id);
            P.println(" @INFO############################## Show the node resources  " + Star_id);
            show_all_node_resources(CopyNetwork);
            P.println(" @INFO############################## /Show the node resources  " + Star_id);
        }        
        
        
        Find_SUN fs = new Find_SUN(Debug, CopyNetwork, static_IDI);
        List stars_list = new ArrayList();
//        stars_list.add(0);
//        stars_list.add(1);
//        stars_list.add(2);
//        stars_list.add(3);
//        stars_list.add(5);

        
        stars_list = reqs.returnSrcdest();
        List potential_suns = new ArrayList();
        potential_suns.add(7);
        potential_suns.add(8);
        potential_suns.add(17);
        potential_suns.add(18);

//        potential_suns.add(4);
//        
//        List selected_suns = new ArrayList();
        
        
        int number_suns_wanted = 1;
        
        List [][] route_list = fs.create_routeLIST_matrices_for_star_sun1(stars_list, potential_suns, mNumKRoutes);
        Print.println("  \n****************  /route_list  ********************" + route_list.length);
        
        double [][] distance_matrix = fs.star_sun_create_distance_matrix_from_route_matrix2(route_list);
        double[] distance_vec = fs.create_distanceVector_from_disMatrix3(distance_matrix);
        HashMap<Double, Integer> hash_dist_vector = fs.hash_the_vector_to_keep_indices4(distance_vec);
        List selected_suns = fs.return_index_list_ofSuns_with_SP_stars5(hash_dist_vector, distance_vec, number_suns_wanted);
//        List selected_suns = fs.return_index_list_ofSuns_with_RND_distnace_to_stars5_1(hash_dist_vector, distance_vec, number_suns_wanted);
        List RITrequests =  fs.create_star_sun_req_list6(stars_list, selected_suns, reqs, (100* Star_id) + (10*reqs.returnId())+static_IDI);
//        
//        
//        
        
//        List RITrequests =  fs.create_star_sun_req_list_test(stars_list, stars_list, reqs, (100* Star_id) + (10*reqs.returnId())+static_IDI);

        
        
        
        
        Print.println("  \n Star are " +  stars_list.toString());
//        Print.println("  \n Suns are " +  selected_suns.toString());
        Print.println("  \n Routes are " );
            
        
//        show_all_node_resources(mNetwork);
//        show_records();
        boolean totalwhat_happened = true;
        if(Debug)
        Print.println("  ****************  request permuataions created, now processing each single req  ********************" );
        
        List<Req_IT_NET_timed> createSingleReqPermutes = RITrequests;
        
        if(Debug)
        for(Req_IT_NET_timed rit: createSingleReqPermutes)
            P.println(" rit ssrc  " + rit.returnSrc() + " AND rit dest  " + rit.returnsDest());
        int totalrerouting= 0;
//        List<Req_IT_NET_timed> createSingleReqPermutes = createSingleReqPermutes_meshReqs(reqs);
//        List<Req_IT_NET_timed> createSingleReqPermutes = createSingleReqPermutes_meshReqs(reqs);
//        GetStarNet getstarnbb = null;
        
        
//        if(Debug)
//        
//        if(StarRequest.returnId()>=24&&StarRequest.returnId()<=26)    
//        if(Debug)

                            
        for(int i = 0 ; i <createSingleReqPermutes.size(); i ++)
        {
                Req_IT_NET_timed  RINT  = createSingleReqPermutes.get(i);
//                Print.println(" ITNETREQ  " + RINT.returnITREQ());
//                Print.println(" SRC  " + RINT.returnSrc() + "   DEST" + RINT.returnsDest());
                
//                int src = RINT.returnSrc();
//                int dest = RINT.returnsDest();
//                List  requested_network_resource =  RINT.returnNETREQ();          
//                int request_dimension = requested_network_resource.size();
//                String req_algorithm_type = RINT.returnAlgorithmType();  
//                //this.Star_Req_type = req.returnConnectionReqType();
//                //if(Debug)
//                P.println("  VON ID : ****** "+ VON_ID +" \t Start_process_request req number " + Star_id + 
//                        "  with request category " + Star_Req_type  + "\n\tsrc : " + src
//                        + "\tdest : " + dest + "  req has  "+ request_dimension+
//                        " layers" + "\t and needs req_ALG_type: " + req_algorithm_type + "\n*****");
//                
                //get the node information for retreiving routing table 
                
                int src = RINT.returnSrc();
                Node node = (Node) CopyNetwork.getmListNodes().get(src);
                if(Debug){
                    P.println("  VON ID : "+ VON_ID +" routes avaialble for node " + node.getmID());
                    node.getmRouting().print_routes();
                }

                //if(Debug)
                //show_records();
                //P.println(" +++++++++++++++++++++++++++++++++++++++++: " );
                
                
                //I changed my code, so prior to getting to the routing, I clear up 
                //the resource database, rather than clearing a a selected route
                //because my routing now is based on availabilities, not only the
                // number of the paths

               /*
                * I need to differentitate between ids. so the normal ones are the
                * star ids. but the connection ids are synthesised.
                * I multiply by 100 the star id. this gives me enough space to define 
                * unique ids.
                * if I have 6 nodes, I will need twice as many nodes.12. so 10 was not 
                * enough.
                * 100 is perhaps too much. but what ever.
                */
                
//                Print.println("  ****************  pass the CopyNetwork t0 GetStarNet ********************" );
//                Print.println("  double check the number so nodes " +  CopyNetwork.getmListNodes().size() );
//                Print.println("  double check the node before sending it in  " +  node.getmRouting().getmRoutingTable().get_number_of_routes_all() );
//                
//                temporal_Link_record_hash = new HashMap();
//                temporal_Node_record_hash = new HashMap();
                
                    
                
                Print.println("  ****************  databases and records are cleaned ********************" );        
//                getstarnbb = new GetStarNet(Debug, CopyNetwork, node, RINT, VON_ID, Con_time,
                getstarnbb = new GetStarNet_nonodebb(Debug, CopyNetwork, node, RINT, VON_ID, Con_time,
                        Star_id,                        
                        Star_Req_type,
                        (Star_id*100) + (++Star_id), 
                        temporal_Link_record_hash,
                        temporal_Node_record_hash,
                        Spec_time_map, 
                        SubL_min,SubL_max,Lambda_min,Lambda_max, Flex_max, Flex_min,
                        Route_select);
                
                int number_route_to_try = mNumKRoutes;
                
                //node resource allocation should happen in here
                /*
                 * the run_routing function bascailly, starts a chain of actions
                 * finding a route, which can be differnet algorithms
                 * then 
                 */
                Print.println("  ****************  Allocation finished  ********************" );
                
                boolean goodstuff = getstarnbb.run_routing(SC,
                        number_route_to_try,
                        //this.star_request_blocked,this.star_request_allowed,
                        totalrerouting,
                        SC.totalreroute_link,
                        SC.totalreroute_node,
                        
                        SC.reroute_per_star_req_linktype_hash,
                        SC.reroute_per_star_req_node_type_hash);
                SC.totalrerouting += totalrerouting;
                update_reroute_starreqtype(reqs.returnStarReqType(), totalrerouting);
                if(!goodstuff)
                {
                    totalwhat_happened = false;
                    Print.println("  ****************  Star Allocation will fail as one of the links failed  ********************" + star_request_route_failed++);
                    allRoutes = new ArrayList<>();
                    break;
                }
                else
                {
                    allRoutes.add(getstarnbb.getCurrentRoute());
                }
                
       }        
        
        if(totalwhat_happened)
        {
            P.println(" +++++++++++++++++++ Allowed ++++++++++++++++++++++: " + ++star_request_allowed);
            P.println(" +++++++++++++++++++ update the databases ++++++++++++++++++++++: " );
            P.println(" +++++++++++++++++++ net work test started ++++++++++++++++++++++: " );
            if(Debug){           
                show_all_node_resources(mNetwork);
                show_all_link_resources(mNetwork);
            }           
            mNetwork = CopyNetwork;
            
            
            
            
            updateNodeLinkRECHASH();
//        show_records("LASTLAST++++++++++++++++++++++++++++++++++ check the bstard records before anything: ");

//            
//        Record_hash_link = clone_linkHashRecords(tmp_reclink);
//        Record_hash_node = clone_nodeHashRecords(tmp_recnode);
            
//            Record_hash_link = tmp_reclink;
//            Record_hash_node = tmp_recnode;
            
//                        P.println(" +++++++++++++++++++ after setting equal ++++++++++++++++++++++: " );

        
            if(Debug){               
                   show_all_node_resources(mNetwork);
                   show_all_link_resources(mNetwork);            
            }
            P.println(" +++++++++++++++++++ net work test finisheed ++++++++++++++++++++++: " );
         
//            mNetwork = new Network(tempnet);           
//            
//            Record_hash_link = new HashMap<>();
//            Record_hash_link = clone_linkHashRecords(tmp_reclink);
//            
//            Record_hash_node = new HashMap<>();
//            Record_hash_node = clone_nodeHashRecords(tmp_recnode);
 
            
 
//            show_recordsArbitraryHashLinkRec(Record_hash_link);
            
            
            if(Debug)
            for(int i = 0; i < CopyNetwork.getmListNodes().size() ; i ++){
//                P.println(" ++++++++&&  Record_hash_node +++++++++++: " + Record_hash_node.get(i).return_all_layer_util_of_node_allocs().toString() );
                P.println(" ++++++++&&  Record_hash_node rere+++++++++++: " + Record_hash_node.get(i).return_max_layer_util_of_node_allocs_per_type() );
                P.println(" ++++++++&&  return_all_layers_util_of_node_allocs_per_type rere+++++++++++: " + Record_hash_node.get(i).return_all_layers_util_of_node_allocs_per_type() );
            }
//            for(int i = 0; i < mNetwork.getmListLinks().size() ; i ++){
////                P.println(" ++++++++&&  Record_hash_node +++++++++++: " + Record_hash_node.get(i).return_all_layer_util_of_node_allocs().toString() );
//                P.println(" ++++++++&&  Record_hash_link rere+++++++++++: " + Record_hash_link.get("2:7").return_max_layer_util_of_link_allocs_per_type() );
//                P.println(" ++++++++&&  return_all_layers_util_of_link_allocs_per_type rere+++++++++++: " + Record_hash_link.get("2:7").return_all_layers_util_of_link_allocs_per_type() );
//            }


            
//            this.star_request_allowed++;
            processAcceptedStarNodeandLinkForSC(allRoutes);
            
//            show_JustGotrecords(temporal_Link_record_hash, temporal_Node_record_hash);
//            SC.view_per_node_addmission();
//            SC.view_per_link_addmission();
            //            this.SC.add_to_admitted_list_link_type(req);
//            this.SC.add_to_route_list(getroute.get_route());
//            this.SC.add_to_route_list_per_req(getstarnbb.get_route(), Star_Req_type);
//            this.SC.add_to_blocking_list_link_type(RINT);
            
            P.println(" +++++++++++++++++++ Request done ++++++++++++++++++++++: " );
        }
        else
        {
//            this.star_request_blocked++;
            processBlockedStarNodeandLinkForSC(allRoutes);
//            this.SC.add_to_blocking_list_link_type(RINT);
            P.println(" +++++++++++++++++++ Blocked ++++++++++++++++++++++: " + ++star_request_blocked);
            P.println(" +++++++++++++++++++ Request done ++++++++++++++++++++++: " );
            
        }
        
        

        //P.println(" <<<<<<<<<<<<<<<< the run_routing is done: this is what happened: "  + goodstuff);
        if(Debug){
            P.println(" <<<<<<<<<<<<< plus the detailed resoruces of the network LINKS:");
            show_all_link_resources(mNetwork);
            P.println(" <<<<<<<<<<<<< plus the detailed resoruces of the network NODES:");
            show_all_node_resources(mNetwork);
            P.println(" <<<<<<<<<<<<< show all the link and node recorded allocations:");
//            show_records("");
        }
        //this.SC.demonstrate_linkRec_objects();
                     
    } 
    
     public void swipeAllTheRecords(Network net, 
             HashMap<String, Rec_Link_Allocs>  r, HashMap<Integer,Rec_Node_Allocs>  rn)
     {
        P.println(" +++++++++++++++++++ clean up of the records begins ++++++++++++++++++++++: " );
         
         for (int i = 0; i < net.getmListLinks().size(); i++){
             Link l = net.getmListLinks().get(i);
             String hop_id = l.getmID();
            r.get(hop_id).delete_expired_linkres(net,Con_time);
            
         } 
         
        //and for each node
         
        for(int i = 0; i < net.getmListNodes().size(); i++)
        {
             Node n = net.getmListNodes().get(i);
             int node_id = n.getmID();
             rn.get(node_id).delete_expired_noderes(net,Con_time);

        }         
        P.println(" +++++++++++++++++++ clean up of the records is done ++++++++++++++++++++++: " );
         
     }    
    
    /*
     * I have the hash map of all the links with the recording objects.
     * Get all those objects.
     * inside each object, then I have the allocated objetes,
     * each alloc aobject contains the res list....
     */
    public void show_records (String message)
    {
        P.println(" <<<<<<<<< show_records called" + message);

        String [] keys_arr = Converters.key_set_obj_to_str_array(Record_hash_link);
        for(int i  = 0; i < keys_arr.length; i ++)
        {
            P.println(" <<<<<<<<< Link id  -->  "+ keys_arr[i] );
            Rec_Link_Allocs link_rec  = Record_hash_link.get(keys_arr[i]);
            link_rec.printResHash();
//            P.println(" <<<<<<<<< link fuckers  -->  "+ link_rec.return_all_layer_util_of_link_allocs().toString() ); 
                        
        }
        int [] keys_arr_node = Converters.key_set_obj_to_int_array(Record_hash_node);
            
        
//        P.println(" <<<<<<<<< Record_hash_node.size() -->  "+ Record_hash_node.size());
        
        
        for(int i  = 0; i < keys_arr_node.length; i ++)
        {
            P.println(" <<<<<<<<< node id  -->  "+ keys_arr_node[i] );
            Rec_Node_Allocs node_rec  = Record_hash_node.get(keys_arr_node[i]);
            node_rec.printResHash();
//            P.println(" <<<<<<<<< node fuckers  -->  "+ node_rec.return_all_layer_util_of_node_allocs().toString() ); 
            
//            node_rec.printResHash();
        }
        

    }
    
//     private boolean process_and_algcall(Route route,String[] req_type_split,
//             List requests,List<Boolean> Delays)
//     {
//         
//        List links = mNetwork.getmListLinks();
//        HashMap link_adj = mNetwork.return_link_str2num_hash_list();
//        List<String> hops = route.return_hops_in_route();
//        int cumulative_delay =0;
//        int num_hops = hops.size();
//        P.println("  VON ID : "+ VON_ID +" number of hops in FF link: " + num_hops);
//        
//        for (int i = 0; i < num_hops; i++){
//            
//            int link_number = (int) link_adj.get(hops.get(i));
//             
//            Link link = (Link) links.get(link_number);
//            //int delay = 8;
//            P.println("  VON ID : "+ VON_ID +" link.getmLength: " + link.getmLength());
//            int delay = (int) Double.parseDouble(link.getmLength());
//            P.println("  VON ID : "+ VON_ID +" link delay: " + delay);
//            HashMap<Integer,String> resources = link.get_resrouces();
//            P.println("  VON ID : "+ VON_ID +" link resources size: " + resources.size());
//            
//            //now process differnet layers
//            for(int u = 0; u < req_type_split.length;u++)
//            {
//                P.println("  VON ID : "+ VON_ID +" ^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^  ");
//                String res_str = resources.get("D"+u);
//                P.println("  VON ID : "+ VON_ID +" resoruce string retrived " + res_str + "  with number D" + u);
//                int [] IntArray_resoruce = C.convert_string_to_1Darray(res_str);
//                int value  = (int) requests.get(u);
//                Boolean if_delay = Delays.get(u);
//                if(!if_delay){delay=0;}
//                P.println("  VON ID : "+ VON_ID +" req_type " + req_type_split[u] );
//                String alg_type=  req_type_split[u];
//                if(alg_type.matches("S"))
//                {
//                    P.println("  VON ID : "+ VON_ID +"  the dimensions layer:" + u+ "   value:" + value 
//                            +"   delay: " + delay);
//                    P.print_1D("  VON ID : "+ VON_ID +"    IntArray_resoruce: ", IntArray_resoruce);
//                    
//                    run_FFC_assignment(value, IntArray_resoruce , delay);
//                }
//                else if(alg_type.matches("M"))
//                {
//                    P.println("  VON ID : "+ VON_ID +"  the dimensions layer:" + u+ "   value:" + value 
//                            +"   delay: " + delay);
//                    P.print_1D("  VON ID : "+ VON_ID +"   IntArray_resoruce: ", IntArray_resoruce);                   
//                    run_FF_assignment(value, IntArray_resoruce , delay);                   
//                }
//                else 
//                {
//                    P.println("  VON ID : "+ VON_ID +"  no algorithm   selected.. so exit");
//                    System.exit(553);
//                }
//            }
//        }
//       return true;
//     }
//    
     
     
     /*
      * final bit of allocation
      */
     
//     private boolean assignTheAllocation(Route route,
//             List<int[]> allocations_across_layers,
//             List<Boolean> Delays_if_apply)
//     {
//         
//        List links = mNetwork.getmListLinks();
//        HashMap link_adj = mNetwork.return_link_str2num_hash_list();
//        List<String> route_hops = route.return_hops_in_route();
//        int num_hops = route_hops.size();
//        HashMap<String,int[]> linkResourcesRecord = new HashMap<>();
//               
//        for (int i = 0; i < num_hops; i++){
//            
//            int link_number = (int) link_adj.get(route_hops.get(i));
//            
//            Link link = (Link) links.get(link_number);
//            
//            
//            int delay = (int) Double.parseDouble(link.getmLength());
//            //P.println(" link delay: " + delay);
//            HashMap<String,String> resources = link.get_resrouces();
//            //P.println(" link resources size: " + resources.size());
//            
//            HashMap<String,String> linkResources = resources;
//            //now process differnet layers
//            //for(int u =0; u<allocations_across_layers.size();u++)
//            for(int u =0; u<allocations_across_layers.size() && 
//                    !(u>link.get_resrouces_dimension());u++)
//            {
//                
//                int [] res= allocations_across_layers.get(u);
//                int applied_delay = delay;
//                
//                if (Delays_if_apply.get(u)==false){
//                    applied_delay = 0;
//                }
//                
//                int[] array_shifted_res = shift_allocation(res, applied_delay);
//
//                String old_res = resources.get("D"+u);
//                
//
//                int [] existing_alloc = C.convert_string_to_1Darray(old_res);
//                
//                int[] allocted_plus_previous = allocated_add_existing(existing_alloc, array_shifted_res);
//                                
//                
//                String new_res = C.convert_array_to_string(res);
//                String new_res_shifted = C.convert_array_to_string(array_shifted_res);
//                String string_final_res = C.convert_array_to_string(allocted_plus_previous);
//                P.println("  VON ID : "+ VON_ID +  "  Con ID : "+ Star_id +" \n%assignTheAllocation%  allocations in hop   :"
//                        + "" + i + "\twith linkId:  " + link.getmID() + "  allocations in layer   :"+u 
//                        +" \n \t Link old_res---> BEFORE :         " + old_res+ ""
//                        +" \n \t Link final_res---> FINAL:         " + string_final_res);
//                linkResources.put("D"+u, string_final_res);
//                linkResourcesRecord.put("D"+u, array_shifted_res);
//                
//            }
//            
//            link.set_resrouces(linkResources);
//        }
//        //put_it_in_records(route_hops, linkResourcesRecord);
//        return true;
//     }
     
//     public void put_it_in_records(List<String> route_hops, HashMap linkResources)
//     {
//         for (int i = 0; i < route_hops.size(); i++){                       
//            Link_Rec_Obj aloc_instance = new Link_Rec_Obj(this.req_type, Star_id,Con_time , Con_duration, linkResources);            
//            String hop_id = route_hops.get(i);
//            if(Debug){
//                P.println( "  VON ID : "+ VON_ID +" -------ADDED-----------  ");        
//                P.println("  VON ID : "+ VON_ID +" hop_id in put in records:  " + hop_id);                        
//                P.println("  VON ID : "+ VON_ID +" StarRequest type: " + this.req_type + " con id: " + this.Star_id + "\tcon time: " + this.Con_time + "\tlink res.size:" + linkResources.size());
//            }
//            this.Record_hash_link.get(hop_id).add_to_link(aloc_instance);
//         }
//     }
     
    
     
     public int [] allocated_add_existing(int [] existing_array, int [] new_array)
     {
         int [] final_array = new int [existing_array.length];
         int i = 0;
         while((i )<existing_array.length)
         {
             final_array[i] = existing_array[i] + new_array[i];
             i++;
         }
         
         return final_array;
     }


     
     public int [] shift_allocation(int [] allocation, int delay)
     {
         int [] shifted_array = new int [allocation.length];
         int i = 0;
         while((i + delay)<allocation.length)
         {
             shifted_array[i + delay] = allocation[i];
             i++;
         }
         
         return shifted_array;
     }
     
     
     /* badly defined method
      * This method called after the start process, and since there exists a route
      * for the asked connection, it can process the request
      * 
      * this method basiclly considres the hop delays and so in the next step,
      * and calls a synch allocation method. thi smeans, the route proces should 
      * over aggregated links availablitiles,
      * it wont be over separate links
      *
      */
//     private boolean process_combine_algcall(Route route,String[] req_type_split,
//             List requests,List<Boolean> Delays_if_apply)
//     {
//         
//        List links = mNetwork.getmListLinks();
//        HashMap link_adj = mNetwork.return_link_str2num_hash_list();
//        List<String> hops = route.return_hops_in_route();
//        
//        //get the delays values per hop
//        int cumulative_delay =0;
//        List<Integer> Delays_per_hop = new ArrayList();
//       // P.println("  VON ID : "+ VON_ID +" in process_combine_algcall route.getmLength: " + route.getmLength());
//        for (int i = 0; i < route.getmLength(); i++){
//            
//            int link_number = (int) link_adj.get(hops.get(i));
//             
//            Link link = (Link) links.get(link_number);
//            //int delay = 8;
//            //P.println("  VON ID : "+ VON_ID +" in process_combine_algcall link.getmLength: " + link.getmLength());
//            int delay = (int) Double.parseDouble(link.getmLength());
//            if(i>0){
//                cumulative_delay = + delay;
//                Delays_per_hop.add(cumulative_delay);            
//            }
//            else
//            {
//                //for the firs hop, no delays are considered
//                Delays_per_hop.add(0);
//            }
//
//        }
//        //now I all the synchronous allocation of the resources
//         return use_alg_synch(route, req_type_split, requests, link_adj, Delays_per_hop, Delays_if_apply);
//
//     }
     
     /*
      * using this method, first links availabilities are aggregated on a route
      * whcih is set of a whole process, to convert layeres of resrouces in differnet
      * links to be summed up
      */
     
//     public boolean use_alg_synch(Route route, String[] req_type_split,List requests,
//             HashMap link_adj,List<Integer> Delays_per_hop,
//             List<Boolean> Delays_if_apply)
//     {
//
//         
//        ResourceOperations ro = new ResourceOperations(Debug, mNetwork,link_adj);
//        List<int []> combined_resoruces_in_layers = ro.route_link_resource_combiner
//                (route,req_type_split.length,Delays_per_hop,Delays_if_apply);
//
//        
//        List<int[]> allocs = new ArrayList();
//         //here, I reiterate calling the algortihms, for different layers
//        //so I have the combined and aggregated tables for each layer
//        
//        //Here, I am usign the maximum layers I can have in my nodes
//        int [] allocation = null;
//        for(int u = 0; u < req_type_split.length;u++)
//        {
//
//
//            // get the layers resrouces in form of array
//            int [] IntArray_resoruce = (int[]) combined_resoruces_in_layers.get(u);
//            
//            // get the slots per layer request
//            int value  = (int) requests.get(u);
//            
//            //get the algortihm type to be deployed
//            String alg_type=  req_type_split[u];
//
//
//            //deal with the delays that you nned to introduce for alllcaition
//            //this has been considered in funding the slots already
//            //bear in mind, if one link, you have no delays, so the
//            //object is empty
//            int tempdelay =0;
//            
//            //please note, in synchronous assignmnet, we dont need to
//            // consider the delays. However, to actually record the allocation,
//            // I need to consider the delay.  
//
//            allocation = new int[IntArray_resoruce.length];
//
//
//            //allocation = run_FFC_assignment(value, IntArray_resoruce , tempdelay);
//            
//            if(alg_type.matches("S"))
//            {
//
//                allocation = run_FFC_assignment(value, IntArray_resoruce , tempdelay);
//            }
//            else if(alg_type.matches("M"))
//            {
//                
//                allocation = run_FF_assignment(value, IntArray_resoruce , tempdelay);                   
//            }
//            else 
//            {
//                P.println("  VON ID : "+ VON_ID +"  Warning no algorithm   selected.. so exit");
//                System.exit(553);
//            }
//        
//            if(allocation== null)
//            {
//                P.println("  VON ID : "+ VON_ID +"  unsuccessful allocation so BLOCKED ");
//                return false;
//            }
//            else{
//
//                allocs.add(allocation);
//            }
//        }
//        assignTheAllocation(route, allocs, Delays_if_apply);
//        return true;
//    }    
     /*
      * using this method, first links availabilities are aggregated on a route
      * whcih is set of a whole process, to convert layeres of resrouces in differnet
      * links to be summed up
      */
     
//     public boolean approach_to_allocations(Route route, String[] req_type_split,List requests,
//             HashMap link_adj,List<Integer> Delays_per_hop,
//             List<Boolean> Delays_if_apply)
//     {
//
//         
//        ResourceOperations ro = new ResourceOperations(Debug, mNetwork,link_adj);
//        List<int []> combined_resoruces_in_layers = ro.route_link_resource_combiner
//                (route,req_type_split.length,Delays_per_hop,Delays_if_apply);
//
//        
//        List<int[]> allocs = new ArrayList();
//         //here, I reiterate calling the algortihms, for different layers
//        //so I have the combined and aggregated tables for each layer
//        
//        //Here, I am usign the maximum layers I can have in my nodes
//        int [] allocation = null;
//        for(int u = 0; u < req_type_split.length;u++)
//        {
//
//
//            // get the layers resrouces in form of array
//            int [] IntArray_resoruce = (int[]) combined_resoruces_in_layers.get(u);
//            
//            // get the slots per layer request
//            int value  = (int) requests.get(u);
//            
//            //get the algortihm type to be deployed
//            String alg_type=  req_type_split[u];
//
//
//            //deal with the delays that you nned to introduce for alllcaition
//            //this has been considered in funding the slots already
//            //bear in mind, if one link, you have no delays, so the
//            //object is empty
//            int tempdelay =0;
//            
//            //please note, in synchronous assignmnet, we dont need to
//            // consider the delays. However, to actually record the allocation,
//            // I need to consider the delay.  
//
//            allocation = new int[IntArray_resoruce.length];
//
//
//            //allocation = run_FFC_assignment(value, IntArray_resoruce , tempdelay);
//            
//            if(alg_type.matches("S"))
//            {
//
//                allocation = run_FFC_assignment(value, IntArray_resoruce , tempdelay);
//            }
//            else if(alg_type.matches("M"))
//            {
//                
//                allocation = run_FF_assignment(value, IntArray_resoruce , tempdelay);                   
//            }
//            else 
//            {
//                P.println("  VON ID : "+ VON_ID +"  Warning no algorithm   selected.. so exit");
//                System.exit(553);
//            }
//        
//            if(allocation== null)
//            {
//                P.println("  VON ID : "+ VON_ID +"  unsuccessful allocation so BLOCKED ");
//                return false;
//            }
//            else{
//
//                allocs.add(allocation);
//            }
//        }
//        assignTheAllocation(route, allocs, Delays_if_apply);
//        return true;
//    }    

 
    public int[] run_FF_assignment(int First_value,int[] First_array ,int First_delay) 
    {
        int[] assignmnet = new int[First_array.length];
        int load = First_value;
        ArrayList D0_slots = new ArrayList();
        int [] temp_arr = new int[First_array.length];
        
        //for(int i = First_delay; i < First_array.length; i++)
        int i = First_delay;
        if(Debug){
            P.println("  VON ID : "+ VON_ID +"First_dealy = i " 
                    + First_delay);  
            P.println("  VON ID : "+ VON_ID +"First_array.length:" 
                + First_array.length);  
        }
        while(load>0 && (i <First_array.length))
        {
                //P.println("  load: " + load + "\t i: " + i+ "\tassignmnet[i]: " + assignmnet[i]);            
                if(First_array[i]==0)
                {
                    temp_arr[i] = 1;
                    assignmnet[i] = 1;
                    //P.println("  load: " + load + "\t i: " + i + "\tassignmnet[i]: " + assignmnet[i]);
                    i++;
                    load--;

                }
                else 
                {
                    i++;
                }
        }
        if(load>0)
        {
           assignmnet = null;
           return assignmnet;
        }
        else{
            if(Debug){
                P.print_1D("  VON ID : "+ VON_ID +" FF_2D the input First_array: ", First_array);
                P.print_1D("  VON ID : "+ VON_ID +" FF_2D the result assignmnet: ", assignmnet);
            }
            return assignmnet; 
        }
    } 
    
    
    public int[] run_FFC_assignment(int First_value,int[] First_array ,int First_delay) 
    {
        int[] assignmnet = new int[First_array.length];
        int load = First_value;
        boolean assigned = false;
        ArrayList D0_slots = new ArrayList();
        int [] temp_arr = new int[First_array.length];
        int counter = 0;
        int startSlot = -1;
        
        int i = First_delay;
        if(Debug){
        P.println("  VON ID : "+ VON_ID +"  First_delay:" +First_delay+  "\tFirst_array.length:" 
                + First_array.length);   
        }
        while(load>0 && (i <First_array.length))
        {
            /*
             * P.println("\ni is:" +i + "    in FFC, give the counter:"
             * + counter + "\t and the load requestd:" + load);
             */
                if(First_array[i]==0)
                {
                    counter++;
                    if(counter>=load)
                    {
                        for(int h = i - counter + 1; h < i + 1; h++)
                        {
                           //P.println("\nh is:" +h);
                           assignmnet[h]= 1; 
                        }
                        assigned = true;
                        break;
                    }
                    
                }
                else if(First_array[i]==1)
                {
                    counter = 0;
                }
                i++;

        }
        if(assigned== false)
        {
            assignmnet = null;
            if(Debug)
            P.println("  VON ID : "+ VON_ID +" FFC assignmnet: " + assignmnet);
            return assignmnet;                    
        }
        else{
            if(Debug){
                P.println("  VON ID : "+ VON_ID +" FFC the input First_array.length: " + First_array.length);
                P.print_1D("  VON ID : "+ VON_ID +" FFC the input First_array: ", First_array);
                P.println("  VON ID : "+ VON_ID +" FFC the result assignmnet.length: " +  assignmnet.length);
                P.print_1D("  VON ID : "+ VON_ID +" FFC the result assignmnet: ", assignmnet);
            }
            return assignmnet; 
        }
    }  
 
    public void show_all_link_resources(Network n)
    {
        List<Link> links = n.getmListLinks();
        List<Integer> added_aggs = new ArrayList<>();
        for(int i =0; i < links.size(); i++)
        {
            Link l = links.get(i);
//            P.println("  \nVON ID : "+ VON_ID +" fin, show link resources " + l.getmID() + "\ttype:" + l.getmType());
            l.show_resrouces(VON_ID);
            //List<Integer> link_agg =   l.get_ParentClass_resrouces().getm_used_agg_layers_resources();
            
        
        }
        
    }
    public void show_all_node_resources(Network n)
    {
        List<Node> nodes = n.getmListNodes();
        List<Integer> added_aggs = new ArrayList<>();
        for(int i =0; i < nodes.size(); i++)
        {
            Node l = nodes.get(i);
//            P.println("  \nVON ID : "+ VON_ID +" fin, show link resources " + l.getmID() + "\ttype:" + l.getmNode_Type());
            l.show_resrouces(VON_ID);
            //List<Integer> link_agg =   l.get_ParentClass_resrouces().getm_used_agg_layers_resources();
            
        
        }
        
    }
//
//    public List<Req_IT_NET_timed> createSingleReqPermutes_meshReqs(Req_IT_NET_timed_startnet starreq)
//    {
////        List<PermutationSrcDest> per = new ArrayList<>();
//        List<Integer> l = starreq.returnSrcdest();
//        List<Req_IT_NET_timed> singlereqa = new ArrayList<>();
//        for (int i = 0; i < l.size(); i++)
//        {
//            
//            for (int j = 0; j < l.size(); j++)
//            {
//                if(l.get(i) != l.get(j))
//                {
////                    PermutationSrcDest psd  = new PermutationSrcDest(l.get(i), l.get(j));
//                    //the permutations made ar edirectly filling up the request sets
//                    Req_IT_NET_timed RIT = new Req_IT_NET_timed
//                            (starreq, static_IDI++ , l.get(i), l.get(j));
//                    singlereqa.add(RIT); 
//                }
//            }
//            
//        }
//        return singlereqa;
//    }
   
    
    public HashMap<Integer, Rec_Node_Allocs> clone_nodeHashRecords(HashMap<Integer, Rec_Node_Allocs> hm)
    {
        HashMap<Integer, Rec_Node_Allocs> clone = new HashMap();
        Set<Integer> keySet = hm.keySet();
        Object[] toArray = keySet.toArray();
        int [] intarr = new int [toArray.length];
        for (int i = 0; i < intarr.length; i++){
            intarr[i] = (int) toArray[i];                
        }
        
        //cloning is happening here
        for(int i :intarr)
        {
            clone.put(i,
                    new Rec_Node_Allocs(hm.get(i)));
        }
        return clone;
    }
 
    public HashMap<String, Rec_Link_Allocs> clone_linkHashRecords(HashMap<String, Rec_Link_Allocs> hm)
    {
        HashMap<String, Rec_Link_Allocs>  clone = new HashMap();
        Set<String> keySet = hm.keySet();
        Object[] toArray = keySet.toArray();
        String [] intarr = new String [toArray.length];
        for (int i = 0; i < intarr.length; i++){
            intarr[i] = (String) toArray[i];                
        }
        
        //cloning is happening here
        for(String i :intarr)
        {
            clone.put(i,
                    new Rec_Link_Allocs(hm.get(i)));
        }
        return clone;
    }
 
    
    public void processAcceptedStarNodeandLinkForSC(List<Route> lr){
        
//        Print.println(" is Star_Req_type for star?  "+  Star_Req_type);
        SC.add_to_admitted_Star_request(StarRequest);  
        String ConnectionReqType = StarRequest.returnConnectionReqType();
//        P.println(" ++++++++++++++++++++++++ lr.size(): " + lr.size() );

        for(Route r : lr)
        {
//            P.println(" ++++++++++++++++++++++++ ADD to the route list: " + r.toString() );
            this.SC.add_to_route_list(r);
            this.SC.add_to_route_list_per_req(r, Star_Req_type);
            
//            List<Integer> nodes  = getSrcDest_node_from_route(r);
            List<Integer> nodes  = getSrc_node_from_route(r);
//                P.println(" ++++++++++++++++++++++++nodes list to be added " + nodes.toString() );
            
            List<String> links  = getLinks_from_route(r);
            for(int nid : nodes)
            {
                SC.add_to_admitted_node(nid);
//                SC.add_to_admission_list_node(nid);
                SC.add_to_admitted_node_STAR_type(StarRequest);
                SC.add_to_admitted_list_node_type(ConnectionReqType);
                
            }
            for(String lid : links)
            {
                SC.add_to_admitted_link(lid);
                SC.add_to_admitted_link_STAR_type(StarRequest);
//                SC.add_to_admission_list_link(lid);
                SC.add_to_admitted_list_link_type(ConnectionReqType);
            }
        }
        
        /*
         * save the general accept per req
         */
        SC.addmitting_star_request(StarRequest);
    
    }
    public void processBlockedStarNodeandLinkForSC(List<Route> lr){
           
        P.println(" +++++++++++++++++++ processBlockedStarNodeandLinkForSC ++++++++++++++++++++++: ");
        SC.add_to_blocking_Star_request(StarRequest);  
        String ConnectionReqType = StarRequest.returnConnectionReqType();
        
        for(Route r : lr)
        {
            List<Integer> nodes  = getSrcDest_node_from_route(r);
            
            
            List<String> links  = getLinks_from_route(r);
            
            for(int nid : nodes)
            {
                
        P.println(" +++++++++++++++++++ SC.add_to_blocking_node() ++++++++++++++++++++++: ");
                
                SC.add_to_blocking_node();
                SC.add_to_blocking_list_node(nid);
                SC.add_to_blocking_list_node_type(ConnectionReqType);
                
            }
            for(String lid : links)
            {
                SC.add_to_blocking_ink();
                SC.add_to_admitted_link_STAR_type(StarRequest);
                SC.add_to_blocking_list_link(lid);
                SC.add_to_blocking_list_link_type(ConnectionReqType);
            }            

        }
        /*
         * save the general accept per req
         */
        SC.blocking_star_request(StarRequest);        
    }
    
    public List<Integer> getSrcDest_node_from_route(Route r){
               
        
        List<Integer> l = new ArrayList<>();
        int[] nodes = r.return_nodesIDs_in_route();
//        P.print_1D("  the nodes in the route are@ ", nodes);
        l.add(nodes[0]);
        l.add(nodes[nodes.length - 1]);
//        P.println(" ++++++++processed routes to make the nodes list to be added " + l.toString() );
        
        
        return l;
    }
    public List<Integer> getSrc_node_from_route(Route r){
               
        
        List<Integer> l = new ArrayList<>();
        int[] nodes = r.return_nodesIDs_in_route();
//        P.print_1D("  the nodes in the route are@ ", nodes);
        l.add(nodes[0]);
//        P.println(" ++++++++processed routes to make the nodes list to be added " + l.toString() );
        
        
        return l;
    }
    
    public List<String> getLinks_from_route(Route r){
        List<String> l = new ArrayList<>();
        return r.return_hops_in_route();
    }
 
    
    public void update_reroute_starreqtype(String starreqtype, int totalreq)
    {
            if(!SC.reroute_per_star_req_type_hash.containsKey(starreqtype))
            {
                //int cap = addmission_per_req_type_hash_link13.get(req_type);
                SC.reroute_per_star_req_type_hash.put(starreqtype, totalreq);
            }
            else
            {
                int existing_b = SC.reroute_per_star_req_type_hash.get(starreqtype);
                SC.reroute_per_star_req_type_hash.put(starreqtype, existing_b + totalreq);

            }         
    }    
    public void show_recordsArbitraryHashNodeRec (HashMap<Integer, Rec_Node_Allocs> hm)
    {
        String [] keys_arr = Converters.key_set_obj_to_str_array(hm);
        for(int i  = 0; i < keys_arr.length; i ++)
        {
            P.println(" <<<<<<<<< node id  -->  "+ keys_arr[i] );
            Rec_Node_Allocs node_rec  = hm.get(keys_arr[i]);
            node_rec.printResHash();
//            P.println(" <<<<<<<<< node fuckers  -->  "+ node_rec.return_all_layer_util_of_node_allocs().toString() ); 
                        
        }
 

    }   
    public void show_recordsArbitraryHashLinkRec (HashMap<String, Rec_Link_Allocs> hm)
    {
        String [] keys_arr = Converters.key_set_obj_to_str_array(hm);
        for(int i  = 0; i < keys_arr.length; i ++)
        {
            P.println(" <<<<<<<<< Link id  -->  "+ keys_arr[i] );
            Rec_Link_Allocs link_rec  = hm.get(keys_arr[i]);
            link_rec.printResHash();
//            P.println(" <<<<<<<<< link fuckers  -->  "+ link_rec.return_all_layer_util_of_link_allocs().toString() ); 
                        
        }
  
    } 
    public void init_LinkrecordList(HashMap<String, Rec_Link_Allocs> hm)
    {
        
        
        List<Link> link_list = mNetwork.getmListLinks();
        
        //for each link, make a recording object
        for(int i = 0; i < link_list.size(); i++)
        {
            Record_hash_link.put(link_list.get(i).getmID(), new Rec_Link_Allocs
                    (Debug,link_list.get(i).getmID(), link_list.get(i).getmNumber(), 
                    link_list.get(i).get_ParentClass_resrouces().getm_resinfo()));
        }

    }    
    
    public void init_NoderecordList(HashMap<Integer, Rec_Node_Allocs> hm)
    {
        
        List<Node> node_list = mNetwork.getmListNodes();
        
        for(int i = 0; i < node_list.size(); i++)
        {
            Record_hash_node.put(node_list.get(i).getmID(), new Rec_Node_Allocs
                    (Debug, node_list.get(i).getmID(), 
                    node_list.get(i).get_ParentClass_resrouces().getm_resinfo()));
        }
    }    
	public Network fillRoutes(Network copynetwork, int k){
//            P.println(" IN FILLROUTES number of shortest paths to be allocated to the nodes: " +k);
		List<Node> listNodes = copynetwork.getmListNodes();
                           
//                P.println(" listNodes size" + listNodes.size());

               
		for(int i=0; i<listNodes.size(); i++){
			Node node = listNodes.get(i);
//                        P.println("  node.getmRouting().get_number_of_routes_all() " + node.getmRouting().get_number_of_routes_all());
			node.getmRouting().searchKShortestPaths(copynetwork, k);
		}
                return copynetwork;
	}   

        public void updateNodeLinkRECHASH(){
            HashMap<String,Rec_Link_Allocs> hmlink = temporal_Link_record_hash;
            HashMap<Integer,Rec_Node_Allocs> hmnode = temporal_Node_record_hash;
//            HashMap<String,Rec_Link_Allocs> hmlink = getstarnbb.return_LinkHash();
//            HashMap<Integer,Rec_Node_Allocs> hmnode = getstarnbb.return_NodeHash();
            
//            P.println(" show_JustGotrecords()");
//            show_JustGotrecords( hmlink, hmnode);
            
            
            
            addto_linkHashRecords(hmlink);
            addto_nodeHashRecords(hmnode);
            
//            show_records(" updateNodeLinkRECHASH  ");
        }
    
    public void addto_nodeHashRecords(HashMap<Integer, Rec_Node_Allocs> hm)
    {
        Set<Integer> keySet = hm.keySet();
        Object[] toArray = keySet.toArray();
        int [] intarr = new int [toArray.length];
        for (int i = 0; i < toArray.length; i++){
            intarr[i] = (int) toArray[i];                
        }
        
        //cloning is happening here
        
        //cloning is happening here
        for(int nodeid :intarr)
        {
            Rec_Node_Allocs RNA = hm.get(nodeid);
            HashMap<Integer,Node_Rec_Obj> innerhm = RNA.returnNoderes_allocation_id2res_hash();
//            int existing_recs = hm.get(linkid).size_Linkres_allocation_id2res_hash();
////            for (int i= 0 ; i < existing_recs; i ++)
            Set<Integer> innerkeySet = innerhm.keySet();
            Object[] innertoArray = innerkeySet.toArray();
            int [] innerintarr = new int [innertoArray.length];
            for (int j = 0; j < innertoArray.length; j++){
                innerintarr[j] =  (int) innertoArray[j];                
            }                
            for (int j = 0; j < innerintarr.length; j++){
                Record_hash_node
                        .get(nodeid)
                        .add_to_node(
                        innerintarr[j]
                        , 
                        innerhm.get(innerintarr[j]));
            }
        }
            
        
    }
 
    public void addto_linkHashRecords(HashMap<String, Rec_Link_Allocs> hm)
    {
        Set<String> keySet = hm.keySet();
        Object[] toArray = keySet.toArray();
        String [] intarr = new String [toArray.length];
        for (int i = 0; i < toArray.length; i++){
            intarr[i] = (String) toArray[i];                
        }
        
        //cloning is happening here
        for(String linkid :intarr)
        {
            Rec_Link_Allocs RLA = hm.get(linkid);
            HashMap<Integer,Link_Rec_Obj> innerhm = RLA.returnLinkres_allocation_id2res_hash();
            int existing_recs = hm.get(linkid).size_Linkres_allocation_id2res_hash();
//            P.println("  existing_recs " + existing_recs);
////            for (int i= 0 ; i < existing_recs; i ++)
            Set<Integer> innerkeySet = innerhm.keySet();
            Object[] innertoArray = innerkeySet.toArray();
            int [] innerintarr = new int [innertoArray.length];
            for (int j = 0; j < innertoArray.length; j++){
                innerintarr[j] =  (int) innertoArray[j];                
            }                
            for (int j = 0; j < innerintarr.length; j++){
                Record_hash_link
                        .get(linkid)
                        .add_to_link(
                        innerintarr[j]
                        , 
                        innerhm.get(innerintarr[j]));
            }                           
            
        }
    }
    
    public void show_JustGotrecords(HashMap<String,Rec_Link_Allocs> Record_hashlink, 
            HashMap<Integer,Rec_Node_Allocs> Record_hashnode){
        P.println(" ***************** the recorded from getstar  " );
        P.println(" <<<<<<<<< JUST Record_hash_link.size() -->  "+ Record_hashlink.size());
        String [] keys_arr = Converters.key_set_obj_to_str_array(Record_hashlink);
        for(int i  = 0; i < keys_arr.length; i ++)
        {
            P.println(" <<<<<<<<< Link id  -->  "+ keys_arr[i] );
            Rec_Link_Allocs link_rec  = Record_hashlink.get(keys_arr[i]);
            link_rec.printResHash();
//            P.println(" <<<<<<<<< link fuckers  -->  "+ link_rec.return_all_layer_util_of_link_allocs().toString() ); 
                        
        }
        int [] keys_arr_node = Converters.key_set_obj_to_int_array(Record_hashnode);
            
        
        P.println(" <<<<<<<<< JUST Record_hash_node.size() -->  "+ Record_hashnode.size());
        
        
        for(int i  = 0; i < keys_arr_node.length; i ++)
        {
            P.println(" <<<<<<<<< node id  -->  "+ keys_arr_node[i] );
            Rec_Node_Allocs node_rec  = Record_hashnode.get(keys_arr_node[i]);
            node_rec.printResHash();
//            P.println(" <<<<<<<<< node fuckers  -->  "+ node_rec.return_all_layer_util_of_node_allocs().toString() ); 
            
//            node_rec.printResHash();
        }
                
        
    }    
    
    public void init_recordList_temporal_ones(Network mnetwork)
    {
        
//        temporal_Link_record_hash = new HashMap<>();
//        temporal_Node_record_hash = new HashMap<>();
        List<Link> link_list = mnetwork.getmListLinks();
        List<Node> node_list = mnetwork.getmListNodes();
        
        P.println("  node_list in temproal " + node_list.size());
        
        //for each link, make a recording object
        for(int i = 0; i < link_list.size(); i++)
        {
            temporal_Link_record_hash.put(link_list.get(i).getmID(), new Rec_Link_Allocs
                    (Debug,link_list.get(i).getmID(), link_list.get(i).getmNumber(), 
                    link_list.get(i).get_ParentClass_resrouces().getm_resinfo()));
        }
        //and for each node
        for(int i = 0; i < node_list.size(); i++)
        {
            temporal_Node_record_hash.put(node_list.get(i).getmID(), new Rec_Node_Allocs
                    (Debug, node_list.get(i).getmID(), 
                    node_list.get(i).get_ParentClass_resrouces().getm_resinfo()));
        }
    }    
    
    
    public Network getNetworkObject()
    {
        return mNetwork;
    }
    public Network getCopyNetObject()
    {
        return CopyNetwork;
    }
    public HashMap<String,Rec_Link_Allocs>  getRecord_hash_link()
    {
        return this.Record_hash_link;
    }
    public HashMap<Integer,Rec_Node_Allocs>  getRecord_hash_node()
    {
        return this.Record_hash_node;
    }
}
