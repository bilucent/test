/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package alloc;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import utilities.Print;

/**
 *
 * @author Bijan
 */
public class Tech_behaviour {
    private List<String> Res_info;
    private List<Integer> Res_info_int;
    public Tech_behaviour(List<String> res_info)
    {
        this.Res_info = res_info;
        
        // initialise res_inf_int
        get_int_resInf();

    
    }
    private void get_int_resInf()
    {
        this.Res_info_int = new ArrayList<>();
        for (int i = 0; i < Res_info.size(); i++)
        {
            Res_info_int.add(Res_info.get(i).length());
        }
//        Print.println("   Res_info " + Res_info.toString());
//        Print.println("   Res_info_int " + Res_info_int.toString());
        
    }
    
    
    public void TTT()
    {
        /*
         * I have considered atleast three levels of resources
         */
        HashMap<Integer, List> patterns = new HashMap();   
        List one_pattern = new ArrayList();
        
        HashMap<Integer, List> cell_patterns = new HashMap();   
        List cell_one_pattern = new ArrayList();
        for (int i = 0; i < Res_info_int.get(0); i++)
        {
            for (int j = 0; j < Res_info_int.get(1); j++)
            {
                int cell_index = j + i * Res_info_int.get(1);
                cell_one_pattern.add(cell_index);
                for (int k = 0; k < Res_info_int.get(2); k++)
                {
                    int index = k + j * Res_info_int.get(2) + i * Res_info_int.get(1)* Res_info_int.get(2);
                    one_pattern.add(index);
                    
                }                 
            }            
        }
        cell_patterns.put(0, cell_one_pattern);
        Print.println("  CELL patern in TTT : " + cell_patterns.toString());
        
        patterns.put(0,one_pattern);
        Print.println("  patern in TTT : " + patterns.toString());
    }

    public void NTT()
    {
        
        HashMap<Integer, List> patterns = new HashMap();        
 
        HashMap<Integer, List> cell_patterns = new HashMap();   
        for (int i = 0; i < Res_info_int.get(0); i++)
        {        
            List cell_one_pattern = new ArrayList();        

            List one_pattern = new ArrayList();
            for (int j = 0; j < Res_info_int.get(1); j++)
            {
                int cell_index = j + i * Res_info_int.get(1);
                cell_one_pattern.add(cell_index);
                for (int k = 0; k < Res_info_int.get(2); k++)
                {
                    int index = k + j * Res_info_int.get(2) + i * Res_info_int.get(1)* Res_info_int.get(2);
                    one_pattern.add(index);
                }                 
            }   
            patterns.put(i,one_pattern); 
            cell_patterns.put(i, cell_one_pattern);        
        }
        
        Print.println("  CELL patern in NTT : " + cell_patterns.toString());
                
        Print.println("  patern in NTT : " + patterns.toString());
    }

    public void NNT()
    {
        
        HashMap<Integer, List> patterns = new HashMap();        
        
        HashMap<Integer, List> cell_patterns = new HashMap();   
        
        for (int i = 0; i < Res_info_int.get(0); i++)
        {
            
            for (int j = 0; j < Res_info_int.get(1); j++)
            {
                
                List one_pattern = new ArrayList();
                
                
                List cell_one_pattern = new ArrayList(); 
                int cell_index = j + i * Res_info_int.get(1);                
                cell_one_pattern.add(cell_index);
                
                for (int k = 0; k < Res_info_int.get(2); k++)
                {
                    int index = k + j * Res_info_int.get(2) + i * Res_info_int.get(1)* Res_info_int.get(2);
                    one_pattern.add(index);
                } 
                patterns.put(i*Res_info_int.get(1) + j,one_pattern);     
                cell_patterns.put(i*Res_info_int.get(1) + j, cell_one_pattern);        

            }   
                    
        }
        Print.println("  CELL patern in NNT : " + cell_patterns.toString());
        
        Print.println("  patern in NNT : " + patterns.toString());
    }

    public void TNT()
    {
        
        HashMap<Integer, List> patterns = new HashMap();
        
        HashMap<Integer, List> cell_patterns = new HashMap();   

        for (int i = 0; i < Res_info_int.get(0); i++)
        {
            
            for (int j = 0; j < Res_info_int.get(1); j++)
            {
                List one_pattern = new ArrayList();
                
                
                
                ///
                /*
                 * this bit is to get the highlevel cell information rather than
                 * each inside cell k based characters
                 */
                List cell_one_pattern = new ArrayList(); 
                int cell_index = j + i * Res_info_int.get(1);                
                if(cell_patterns.containsKey(j%Res_info_int.get(1)))
                {
                    cell_patterns.get(j%Res_info_int.get(1)).add(cell_index);
                }
                else 
                {
                    cell_one_pattern.add(cell_index);
                    cell_patterns.put(j%Res_info_int.get(1),cell_one_pattern); 
                }          
                ///
                
                
                for (int k = 0; k < Res_info_int.get(2); k++)
                {
                    int index = k + j * Res_info_int.get(2) + i * Res_info_int.get(1)* Res_info_int.get(2);
//                    Print.println("  j%Res_info_int.get(1) " + j%Res_info_int.get(1) + " the index is " + index);
                    if(patterns.containsKey(j%Res_info_int.get(1)))
                    {
                        patterns.get(j%Res_info_int.get(1)).add(index);
                    }
                    else 
                    {
                        one_pattern.add(index);
                        patterns.put(j%Res_info_int.get(1),one_pattern); 
                    }
                }
                
                
              
                
                               
            }   
                     
        }
        Print.println("  CELL patern in TNT : " + cell_patterns.toString());

        
        Print.println("  patern in TNT : " + patterns.toString());
    }    
    
    public void TTN()
    {
        
        HashMap<Integer, List> patterns = new HashMap();   
        List one_pattern = new ArrayList();
        
        HashMap<Integer, List> cell_patterns = new HashMap();   
        List cell_one_pattern = new ArrayList();
        for (int i = 0; i < Res_info_int.get(0); i++)
        {
            for (int j = 0; j < Res_info_int.get(1); j++)
            {
                int cell_index = j + i * Res_info_int.get(1);
                cell_one_pattern.add(cell_index);
                for (int k = 0; k < Res_info_int.get(2); k++)
                {
                    int index = k + j * Res_info_int.get(2) + i * Res_info_int.get(1)* Res_info_int.get(2);
                    one_pattern.add(index);
                    
                }                 
            }            
        }
        cell_patterns.put(0, cell_one_pattern);
        Print.println("  CELL patern in TTN : " + cell_patterns.toString());
        
        patterns.put(0,one_pattern);
        Print.println("  patern in TTN : " + patterns.toString());
    }

    public void NTN()
    {
        
        HashMap<Integer, List> patterns = new HashMap();        
 
        HashMap<Integer, List> cell_patterns = new HashMap();   
        for (int i = 0; i < Res_info_int.get(0); i++)
        {        
            List cell_one_pattern = new ArrayList();        

            List one_pattern = new ArrayList();
            for (int j = 0; j < Res_info_int.get(1); j++)
            {
                int cell_index = j + i * Res_info_int.get(1);
                cell_one_pattern.add(cell_index);

//            List one_pattern = new ArrayList();
//            for (int j = 0; j < Res_info_int.get(1); j++)
//            {
//                int cell_index = j + i * Res_info_int.get(1);
//                cell_one_pattern.add(cell_index);                
                
/////////////                
             
                int total_resoruces = Res_info_int.get(0)* Res_info_int.get(1)* Res_info_int.get(2);
                for (int k = 0; k < Res_info_int.get(2); k++)
                {
                    int index = k + j * Res_info_int.get(2) + i * Res_info_int.get(1)* Res_info_int.get(2);
                    int placeHolder = index%(Res_info_int.get(0)*Res_info_int.get(2));
                    if(index<total_resoruces/Res_info_int.get(0))
                    {

                        placeHolder = index%Res_info_int.get(2);
//                        Print.println("  placeHolder " + placeHolder + " and the index is " + index);

                        if(patterns.containsKey(placeHolder))
                        {
                            //Print.println("  patterns contains " + placeHolder);

                            patterns.get(placeHolder).add(index);
                        }
                        else 
                        {
                            // the re initialisation of one-pattern corrects the
                            // allocations. 
                            one_pattern = new ArrayList();

                            one_pattern.add(index);
                            patterns.put(placeHolder,one_pattern); 
                        }
//                        Print.println("  patterns in steps " + patterns.toString());                        
                    }
                    else
                    {

                        placeHolder = index%Res_info_int.get(2)+ Res_info_int.get(2);
//                        Print.println("  placeHolder " + placeHolder + " and the index is " + index);
                        
                        if(patterns.containsKey(placeHolder))
                        {
                            //Print.println("  patterns contains " + placeHolder);

                            patterns.get(placeHolder).add(index);
                        }
                        else 
                        {
                            // the re initialisation of one-pattern corrects the
                            // allocations. 
                            one_pattern = new ArrayList();

                            one_pattern.add(index);
                            patterns.put(placeHolder,one_pattern); 
                        }
//                        Print.println("  patterns in steps " + patterns.toString());                         
                    }

                    
                }


/////////////                

                
            }   
//            patterns.put(i,one_pattern); 
            cell_patterns.put(i, cell_one_pattern);        
        }
        
        Print.println("  CELL patern in NTN : " + cell_patterns.toString());
                
        Print.println("  patern in NTN : " + patterns.toString());
    }

    public void NNN()
    {
        
        HashMap<Integer, List> patterns = new HashMap();        
        
        HashMap<Integer, List> cell_patterns = new HashMap();   
        
        for (int i = 0; i < Res_info_int.get(0); i++)
        {
            
            for (int j = 0; j < Res_info_int.get(1); j++)
            {
                
                /*
                * for each 4 of k, a set is made, with increasing i and j from prevois layers.
                * Therefore if k is not suposed to be in the set of 4, make it 4 individual sets
                * to comply with N
                */
                for (int k = 0; k < Res_info_int.get(2); k++)
                {
                    List one_pattern = new ArrayList();
                    List cell_one_pattern = new ArrayList();                     
                    int cell_index = k + j * Res_info_int.get(2) + i * Res_info_int.get(1)* Res_info_int.get(2);   
                    cell_one_pattern.add(cell_index);                    
                    int index = k + j * Res_info_int.get(2) + i * Res_info_int.get(1)* Res_info_int.get(2);
                    one_pattern.add(index);
                    patterns.put(k + j * Res_info_int.get(2) + i * Res_info_int.get(1)* Res_info_int.get(2),one_pattern); 
                    cell_patterns.put(k + j * Res_info_int.get(2) + i * Res_info_int.get(1)* Res_info_int.get(2), cell_one_pattern);        
                }
            }   
                    
        }
        Print.println("  CELL patern in NNN : " + cell_patterns.toString());
        
        Print.println("  patern in NNN : " + patterns.toString());
    }

    public void TNN()
    {
        
        HashMap<Integer, List> patterns = new HashMap();
        
        HashMap<Integer, List> cell_patterns = new HashMap();   

        for (int i = 0; i < Res_info_int.get(0); i++)
        {
            
            for (int j = 0; j < Res_info_int.get(1); j++)
            {
                List one_pattern = new ArrayList();
                
                
                
                ///
                /*
                 * this bit is to get the highlevel cell information rather than
                 * each inside cell k based characters
                 */
                List cell_one_pattern = new ArrayList(); 
                
                // here the cells are created, where the j%Res_info_int.get(1)
                //creates the desired formations
                int cell_index = j + i * Res_info_int.get(1);                
                if(cell_patterns.containsKey(j%Res_info_int.get(1)))
                {
                    cell_patterns.get(j%Res_info_int.get(1)).add(cell_index);
                }
                else 
                {
                    cell_one_pattern.add(cell_index);
                    cell_patterns.put(j%Res_info_int.get(1),cell_one_pattern); 
                }          
                ///
                
                // 
                for (int k = 0; k < Res_info_int.get(2); k++)
                {
                    int index = k + j * Res_info_int.get(2) + i * Res_info_int.get(1)* Res_info_int.get(2);
                    int placeHolder = index%(Res_info_int.get(1)*Res_info_int.get(2));
                    //Print.println("  placeHolder " + placeHolder + " and the index is " + index);
                    if(patterns.containsKey(index%(Res_info_int.get(1)*Res_info_int.get(2))))
                    {
                        //Print.println("  patterns contains " + placeHolder);

                        patterns.get((index)%(Res_info_int.get(1)*Res_info_int.get(2))).add(index);
                    }
                    else 
                    {
                        // the re initialisation of one-pattern corrects the
                        // allocations. 
                        one_pattern = new ArrayList();
                        
                        one_pattern.add(index);
                        patterns.put((index)%(Res_info_int.get(1)*Res_info_int.get(2)),one_pattern); 
                    }
                    //Print.println("  patterns in steps " + patterns.toString());
                    
                }


            }   
                     
        }
        Print.println("  CELL patern in TNN : " + cell_patterns.toString());

        
        Print.println("  patern in TNN : " + patterns.toString());
    }    
}
