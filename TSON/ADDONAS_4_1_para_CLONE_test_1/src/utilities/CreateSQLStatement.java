/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package utilities;
import java.sql.*;
/**
 *
 * @author ferdiansyah.dolot
 */
public class CreateSQLStatement {
    private Statement statement;
    boolean Debug;
    public CreateSQLStatement(boolean debug) throws SQLException{
        this.Debug = debug;
        makeStatement();
    }
    public void UseDatabase(String databasename) throws SQLException{

        try{
            statement.execute("USE "+ databasename);
        }catch (Exception e)
        {
            e.printStackTrace();
        }
    }    
    
    public void CreateDB(String databasename) throws SQLException{

        try{
            statement.execute("DROP DATABASE IF EXISTS "+ databasename);
            statement.execute("CREATE DATABASE "+ databasename);
            statement.execute("USE "+ databasename);
        }catch (Exception e)
        {
            e.printStackTrace();
        }
    }
    
    public Statement makeStatement() throws SQLException{
        ConnectToMySQLServer c = new ConnectToMySQLServer();
        Connection conn = c.makeConnection();
        statement = conn.createStatement();
        return statement;
    }
    public void insert(String name,int number)throws SQLException{
        //statement.execute("insert into student values(\""+name+"\""+number+")");
        statement.execute("insert into student values ('"+name+"', '"+number+"')");
    }
    public void insert_nodes(int id, String type,String cost, String delay, 
            String D1, String D2, String D3)throws SQLException{
        //statement.execute("insert into student values(\""+name+"\""+number+")");
        statement.execute("insert into nodes values ('"+id+"', '"+type+
                "', '"+cost+"', '"+delay+"', '"+D1+"', '"+D2+"', '"+D3+"')");
    }    
 
    public void insert_Into_tables_(String value)throws SQLException{
 
        statement.execute(value);
    }     
 
    public void insert_Into_tables_(String DB,String table, String column, int line, String value)throws SQLException{
 
        if(Debug)
        System.out.println(" UPDATE `"+ DB+"`.`"+ table +"` SET `" + column +"`='"+ value +"' WHERE `id`='"+line+ "';");
        statement.execute(" UPDATE `"+ DB+"`.`"+ table +"` SET `" + column +"`='"+ value +"' WHERE `id`='"+line+ "';");
    }    
    
    //this function adds int to columns
    public void insert_ID_into_tables_(String table, String column, int value)throws SQLException{
         
        statement.execute("insert into " + table + " (" + column + ") values (" + value+")");
    } 
    
    
    /*
    // this function adds strings to columns
    public void insert_Into_tables_(String table, String column, String value)throws SQLException{
        //statement.execute("insert into student values(\""+name+"\""+number+")");
        statement.execute("insert into " + table + " (" + column + ") values ('" + value+"')");
    }    
    
    //this function adds int to columns
    public void insert_ID_into_tables_(String table, String column, int value)throws SQLException{
        //statement.execute("insert into student values(\""+name+"\""+number+")");
        statement.execute("insert into " + table + " (" + column + ") values (" + value+")");
    }     
    */ 
    public void read(String table)throws SQLException{
        //statement.execute("insert into student values(\""+name+"\""+number+")");
        ResultSet rs = statement.executeQuery("SELECT * FROM " + table);
        while (rs.next())
        {
            System.out.println( rs.getString("cost") + " : " + rs.getString("type"));
        }
        
        //System.out.println("  hsadf " + rs.getString(7));
        
    }

    public String read_cell(String column, String table, int line)throws SQLException{
        //statement.execute("insert into student values(\""+name+"\""+number+")");
       // ResultSet rs = statement.executeQuery("SELECT "+ column + " FROM "+ table + " where id='" +line +"';" );
       ResultSet rs = statement.executeQuery("SELECT * FROM "+ table +";" );
       // ResultSet rs = statement.executeQuery("SELECT "+ column + " FROM "+ table +";" );

       rs.absolute(line);
       String bijan = rs.getString(column);

       return bijan;
        
    }    
    public ResultSet get_ResultSet_(String table)throws SQLException{

       ResultSet rs = statement.executeQuery("SELECT * FROM "+ table +";" );

        return rs;
        
    }        
    public void createTable(String QueryString) throws SQLException{
        int i = 0;

        {
                statement.executeUpdate(QueryString);
                System.out.println("QueryString  =  " + QueryString);      
        }
    }

    void insert_nodes(int id, String type, String cost, String delay) {
        throw new UnsupportedOperationException("Not yet implemented");
    }
}