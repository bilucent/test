/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package utilities;

import com.mysql.jdbc.Driver;
import java.sql.*;
/**
 *
 * @author Ferdiansyah Dolot
 */
public class ConnectToMySQLServer {
    public ConnectToMySQLServer() throws SQLException{
        //makeConnection(DBname);
    } 

    private Connection conn;  

     public  Connection makeConnection() throws SQLException {
        if (conn == null) {
             new Driver();
            //  conn
             conn = DriverManager.getConnection(
                       "jdbc:mysql://localhost:3306/",
                       "root",
                       "root");
         }
         return conn;
     }  

     /*
     public static void main(String args[]) {
         try {
             ConnectToMySQLServer c = new ConnectToMySQLServer();
             System.out.println("Connection established");
         }
         catch (SQLException e) {
             e.printStackTrace();
             System.err.println("Connection Failure");
         }  

    }
    * 
    */
}