/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package utilities;

import java.util.HashMap;
import java.util.List;
import java.util.Set;

/**
 *
 * @author br12367
 */
public  class Print {
    public static void print(String message){
        System.out.print(message);
    }    
    public static void println(String message){
        System.out.println(message);
    }  
    
    public static void print_1D(String message, int[] array){
        System.out.println(message);
        for (int i = 0;i < array.length;i++)
        {
            System.out.print(array[i] + " ");
        }
        System.out.println(" *** ");
    }
 
    
    public static void print_1D(String message, double[] array){
        System.out.println(message);
        for (int i = 0;i < array.length;i++)
        {
            System.out.print(array[i] + " ");
        }
        System.out.println(" *** ");
    }
 
    public static void print_1D(String message, Object[] array){
        System.out.println(message);
        for (int i = 0;i < array.length;i++)
        {
            System.out.print(array[i] + " ");
        }
        System.out.println(" *** ");
    }    
    public static void print_1D(String message, String[] array){
        System.out.println(message);
        for (int i = 0;i < array.length;i++)
        {
            System.out.print("i" + i +":" +array[i] + "\t");
        }
        System.out.println(" *** ");
    }      
    public static void print_2D(String message,int[][] array){
        System.out.println(message);
        for (int i = 0;i < array.length;i++)
        {
            for (int j = 0; j < array.length;j++)
            {            
            System.out.print(array[i][j] + " ");
            } System.out.println();   
        }
        System.out.println(" *** ");
    } 
    
    
    
    public static void  printResHash_IntInt(String message,HashMap<Integer,Integer> hm)
    {    
            Set<Integer> keySet = hm.keySet();
            Object[] toArray = keySet.toArray();
            int [] intarr = new int [toArray.length];
            for (int i = 0; i < intarr.length; i++){
                intarr[i] = (int) toArray[i];
                //HashMap rec_res =  this.Allocation_id2res_hash.get(intarr[i]).return_allocresrouces();
                println("  print hm <Int,Int> " + message + " key : "+ intarr[i] +   " --> " + (int)hm.get(intarr[i]));
                
            }
            
    }    
    public static void  printResHash_IntStr(String message,HashMap<Integer,String> hm)
    {    
            Set<Integer> keySet = hm.keySet();
            Object[] toArray = keySet.toArray();
            int [] intarr = new int [toArray.length];
            for (int i = 0; i < intarr.length; i++){
                intarr[i] = (int) toArray[i];
                //HashMap rec_res =  this.Allocation_id2res_hash.get(intarr[i]).return_allocresrouces();
                println("  print hm <Int,Str> " + message + " key : "+ intarr[i] +  " --> " + hm.get(intarr[i]));
                
            }
            
    }       
    
    public static void  printResHash_StrStr(String message,HashMap<String,String> hm)
    {    
            Set<String> keySet = hm.keySet();
            Object[] toArray = keySet.toArray();
            String [] arrstr = new String [toArray.length];
            for (int i = 0; i < arrstr.length; i++){
                arrstr[i] = toArray[i].toString();
                //HashMap rec_res =  this.Allocation_id2res_hash.get(intarr[i]).return_allocresrouces();
                println("  print hm <Str,Str> " + message + " key : "+ arrstr[i] +   " --> " + hm.get(arrstr[i]));
                
            }
            
    }     
    public static void  printResHash_StrInt(String message,HashMap<String,Integer> hm)
    {    
            Set<String> keySet = hm.keySet();
            Object[] toArray = keySet.toArray();
            String [] arrstr = new String [toArray.length];
            for (int i = 0; i < arrstr.length; i++){
                arrstr[i] = toArray[i].toString();
                //HashMap rec_res =  this.Allocation_id2res_hash.get(intarr[i]).return_allocresrouces();
                println("  print hm <Str,Int> " + message + " key : "+ arrstr[i] +    " --> " + hm.get(arrstr[i]));
                
            }
            
    }       
    public static void  printResHash_StrList(String message,HashMap<String,List> hm)
    {    
            Set<String> keySet = hm.keySet();
            Object[] toArray = keySet.toArray();
            String [] arrstr = new String [toArray.length];
            for (int i = 0; i < arrstr.length; i++){
                arrstr[i] = toArray[i].toString();
                //HashMap rec_res =  this.Allocation_id2res_hash.get(intarr[i]).return_allocresrouces();
                println("  print hm <Str,List> " + message + " key : "+ arrstr[i] +    " --> " + hm.get(arrstr[i]).toString());
                
            }
            
    }      
    
    public static void  printResHash_IntList(String message,HashMap<Integer,List> hm)
    {    
            Set<Integer> keySet = hm.keySet();
            Object[] toArray = keySet.toArray();
            int [] intarr = new int [toArray.length];
            for (int i = 0; i < intarr.length; i++){
                intarr[i] = (int) toArray[i];
                //HashMap rec_res =  this.Allocation_id2res_hash.get(intarr[i]).return_allocresrouces();
                println("  print hm <Int,List> " + message + " key : "+ intarr[i] +    " --> " + hm.get(intarr[i]).toString());
                
            }
            
    }       
    public static void  printResHash_StrHash_Strint(String message,HashMap<String,HashMap> hm)
    {    
            Set<String> keySet = hm.keySet();
            Object[] toArray = keySet.toArray();
            String [] arrstr = new String [toArray.length];
            for (int i = 0; i < arrstr.length; i++){
                arrstr[i] = toArray[i].toString();
                //HashMap rec_res =  this.Allocation_id2res_hash.get(intarr[i]).return_allocresrouces();
                printResHash_StrInt("  hash in hash print hm <Int,List> " + message + " key : "+ arrstr[i] +    " --> " , hm.get(arrstr[i]));
                
            }
            
    }     
    public static void  printResHash_IntHash_Strint(String message,HashMap<Integer,HashMap> hm)
    {    
            Set<Integer> keySet = hm.keySet();
            Object[] toArray = keySet.toArray();
            int [] arrstr = new int [toArray.length];
            for (int i = 0; i < arrstr.length; i++){
                arrstr[i] = Integer.parseInt(toArray[i].toString());
                //HashMap rec_res =  this.Allocation_id2res_hash.get(intarr[i]).return_allocresrouces();
                printResHash_StrInt("  hash in hash print hm <Int,List> " + message + " key : "+ arrstr[i] +    " --> " , hm.get(arrstr[i]));
                
            }
            
    }     
}
