package utilities;

import org.apache.commons.math.linear.Array2DRowRealMatrix;
import org.apache.commons.math.linear.MatrixUtils;

/**
 * This class is a custom implementation of the Array2DRowRealMatrix
 * provided by the commons math library.
 * 
 * @author Joan Triay
 *
 */
public class Matrix extends Array2DRowRealMatrix {
	/** Serial version.*/
	private static final long serialVersionUID = 1L;

	/**
	 * Class constructor
	 */
	public Matrix() {
		super();
	}

	/**
	 * Class constructor to create an array.
	 * @param v The array of doubles.
	 */
	public Matrix(double[] v) {
		super(v);
	}

	/**
	 * Class constructor to create a matrix using a double array
	 * 
	 * @param d The double array with the input data.
	 * @param copyArray
	 * @throws IllegalArgumentException
	 * @throws NullPointerException
	 */
	public Matrix(double[][] d, boolean copyArray)
			throws IllegalArgumentException, NullPointerException {
		super(d, copyArray);
	}

	/**
	 * Class constructor to create a matrix using a double array
	 * @param d The double array with the input data.
	 * @throws IllegalArgumentException
	 * @throws NullPointerException
	 */
	public Matrix(double[][] d) throws IllegalArgumentException,
			NullPointerException {
		super(d);
	}

	/**
	 * Class constructor to create an empty matrix with specific
	 * number of rows and columns.
	 * @param rowDimension Number of rows.
	 * @param columnDimension Number of columns.
	 * @throws IllegalArgumentException
	 */
	public Matrix(int rowDimension, int columnDimension)
			throws IllegalArgumentException {
		super(rowDimension, columnDimension);
	}

	/**
	 * It resets all values of this matrix to 0.0.
	 */
	public void clearValues(){
		for(int i=0; i<this.getRowDimension(); i++)
			for(int j=0; j<this.getColumnDimension(); j++)
				this.setEntry(i, j, 0.0);
	}
	
	/**
	 * This method transforms the matrix with real values to
	 * integer values.
	 * @return A matrix array with integer values.
	 */
	public int[][] getDataInt(){
		int _data[][] = new int[this.getRowDimension()][this.getColumnDimension()];
		for(int i=0; i<this.getRowDimension(); i++){
			for(int j=0; j<this.getColumnDimension(); j++){
				_data[i][j] = (int)this.data[i][j];
			}
		}
		return _data;
	}
	
	/**
	 * Initialize the matrix values using a matrix array of integer
	 * values.
	 * @param values The matrix array with integer values.
	 */
	public void setValuesFromIntArray(int values[][]){
		int numRow = values.length;
		int numCol = values[0].length;
		
		for(int i=0; i<numRow; i++){
			for(int j=0; j<numCol; j++){
				this.setEntry(i, j, (double)values[i][j]);
			}
		}
	}
	
    /**
     * Compute the sum of this and <code>m</code>.
     *
     * @param m    matrix to be added
     * @return     this + m
     * @throws  IllegalArgumentException if m is not the same size as this
     */
    public Matrix add(final Matrix m)
        throws IllegalArgumentException {

        // safety check
        MatrixUtils.checkAdditionCompatible(this, m);

        final int rowCount    = getRowDimension();
        final int columnCount = getColumnDimension();
        final double[][] outData = new double[rowCount][columnCount];
        for (int row = 0; row < rowCount; row++) {
            final double[] dataRow    = data[row];
            final double[] mRow       = m.data[row];
            final double[] outDataRow = outData[row];
            for (int col = 0; col < columnCount; col++) {
                outDataRow[col] = dataRow[col] + mRow[col];
            }
        }

        return new Matrix(outData, false);

    }
}
