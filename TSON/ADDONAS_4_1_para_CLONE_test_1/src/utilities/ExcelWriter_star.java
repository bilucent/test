/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package utilities;

/**
 *
 * @author Bijan Rofoee
 */


import java.io.*;
import java.util.*;
//import org.apache.poi.hssf.usermodel.HSSFCell;
//import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
//import org.apache.poi.poifs.filesystem.POIFSFileSystem;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
//import utilities.Print;

public class ExcelWriter_star {
    List<String>  sheet_labels = new ArrayList<>();
    public ExcelWriter_star()
    {
        
    }
    public void writer(
            double star_all, double star_admission, double star_blocking, double star_throughput, double star_link_used, double star_node_used,
            double total_reroute, double avg_route_length, double retoureLink ,double retoureNode,
            
            
            HashMap<String, Integer> addmission_per_star_req_type_hash,
            
            HashMap<String, Integer> blocking_per_star_req_type_hash,
            
            HashMap<String, Integer> avg_route_length_hash,
            
//            double avg_route_length_a,
//            double avg_route_length_b,
//            double avg_route_length_c,
//            double avg_route_length_d,
//            double avg_route_length_e,               
            
            HashMap<String,Integer> reroutes_star_perType,

            double admission_link, double blocking_link, double throughput_type_link,
            double total_accepted_aa_link,double total_blocked_aa_link, double total_throughput_aa_link, double total_used_aa_link,double total_cap_aa_link, double total_troughput_aa_link,int L0_util_aa_link, int L1_util_aa_link, int L2_util_aa_link, int L3_util_aa_link,
            double total_accepted_bb_link,double total_blocked_bb_link, double total_throughput_bb_link, double total_used_bb_link,double total_cap_bb_link, double total_troughput_bb_link,int L0_util_bb_link, int L1_util_bb_link, int L2_util_bb_link, int L3_util_bb_link,
            double total_accepted_cc_link,double total_blocked_cc_link, double total_throughput_cc_link, double total_used_cc_link,double total_cap_cc_link, double total_troughput_cc_link,int L0_util_cc_link, int L1_util_cc_link, int L2_util_cc_link, int L3_util_cc_link,
            double total_accepted_dd_link,double total_blocked_dd_link, double total_throughput_dd_link, double total_used_dd_link,double total_cap_dd_link, double total_troughput_dd_link,int L0_util_dd_link, int L1_util_dd_link, int L2_util_dd_link, int L3_util_dd_link,
            double total_accepted_ee_link,double total_blocked_ee_link, double total_throughput_ee_link, double total_used_ee_link,double total_cap_ee_link, double total_troughput_ee_link,int L0_util_ee_link, int L1_util_ee_link, int L2_util_ee_link, int L3_util_ee_link,
            double total_used_L0_link,double total_cap_L0_link, double total_troughput_L0_link,                                                                                                                                                           
            double total_used_L1_link,double total_cap_L1_link, double total_troughput_L1_link,                                                                                                                                                           
            double total_used_L2_link,double total_cap_L2_link, double total_troughput_L2_link,                                                                                                                                                           
            double total_used_L3_link,double total_cap_L3_link, double total_troughput_L3_link,                                                                                                                                                           
            //node information
            double admission_node, double blocking_node, double throughput_type_node,                                                                                                                                                                     
            double total_accepted_aa_node,double total_blocked_aa_node, double total_throughput_aa_node, double total_used_aa_node,double total_cap_aa_node, double total_troughput_aa_node,int L0_util_aa_node, int L1_util_aa_node, int L2_util_aa_node, int L3_util_aa_node,
            double total_accepted_bb_node,double total_blocked_bb_node, double total_throughput_bb_node, double total_used_bb_node,double total_cap_bb_node, double total_troughput_bb_node,int L0_util_bb_node, int L1_util_bb_node, int L2_util_bb_node, int L3_util_bb_node,
            double total_accepted_cc_node,double total_blocked_cc_node, double total_throughput_cc_node, double total_used_cc_node,double total_cap_cc_node, double total_troughput_cc_node,int L0_util_cc_node, int L1_util_cc_node, int L2_util_cc_node, int L3_util_cc_node,
            double total_accepted_dd_node,double total_blocked_dd_node, double total_throughput_dd_node, double total_used_dd_node,double total_cap_dd_node, double total_troughput_dd_node,int L0_util_dd_node, int L1_util_dd_node, int L2_util_dd_node, int L3_util_dd_node,
            double total_accepted_ee_node,double total_blocked_ee_node, double total_throughput_ee_node, double total_used_ee_node,double total_cap_ee_node, double total_troughput_ee_node,int L0_util_ee_node, int L1_util_ee_node, int L2_util_ee_node, int L3_util_ee_node,
            double total_used_L0_node,double total_cap_L0_node, double total_troughput_L0_node,
            double total_used_L1_node,double total_cap_L1_node, double total_troughput_L1_node,
            double total_used_L2_node,double total_cap_L2_node, double total_troughput_L2_node,
            double total_used_L3_node,double total_cap_L3_node, double total_troughput_L3_node,

                        
            int excel_horiz, Integer line,String file_address) throws FileNotFoundException, IOException
    {
        //String file_address = "C:\\Users\\Bijan\\Dropbox\\Under_Work\\ADDONAS_1_5_1_2\\JAVAEXCEL\\new.xls";
        HSSFWorkbook workbook; 
        HSSFSheet sheet;
        
        Print.println("star_allstar_all: "+ star_all);
        Print.println("admission_linkadmission_link: "+ admission_link);
        Print.println("admission_nodeadmission_node: "+ admission_node);

        
        
        FileInputStream file = new FileInputStream(new File(file_address));
        workbook = new HSSFWorkbook(file);
        String excel_sheet_label = "series"+excel_horiz;
        Print.println("excel_sheet_label: "+ excel_sheet_label);
        Print.println("sheet_labels: "+ sheet_labels.toString());
        if(sheet_labels.contains(excel_sheet_label))
        {

            sheet = workbook.getSheet(excel_sheet_label);
        }
        else
        {
            Print.println("make excel_sheet_label: "+ excel_sheet_label);
            sheet_labels.add(excel_sheet_label);
            Print.println("sheet_labels: "+ sheet_labels.toString());

            sheet = workbook.createSheet(excel_sheet_label);
        }
//                if(!(sheet = workbook.createSheet("excel_horiz")))

        //if(excel_horiz==0)
        {

            HashMap<Integer, Object[]> data = new HashMap<>();

            Print.println(" linewriter  " + line);
            data.put(line, new Object[] {
                " star_all ", " star_admission ", " star_blocking ", " star_throughput ", " star_links_used "," star_nodes_used ",
                " total_reroute ", " avg_route_length ", " retoureLink "," retoureNode ",
                
                //per star type results
                " addmission for a stars",
                " addmission for b stars",
                " addmission for c stars",
                " addmission for d stars",
                " addmission for e stars",
                " addmission for f stars",


                " avg_route_length_a ",
                " avg_route_length_b ",
                " avg_route_length_c ",
                " avg_route_length_d ",
                " avg_route_length_e ",   
                " avg_route_length_f ",   

                " reroutes_a ",
                " reroutes_b ",
                " reroutes_c ",
                " reroutes_d ",
                " reroutes_e ",						
                " reroutes_f "						

            });
            Print.println(" linewriter  " + line);

            data.put(line + 1, new Object[] {

                star_all,  star_admission,  star_blocking,  star_throughput,  star_link_used, star_node_used, 
                total_reroute,  avg_route_length,  retoureLink , retoureNode,

                //per star type results
                (double)addmission_per_star_req_type_hash.get("a")/(addmission_per_star_req_type_hash.get("a") + blocking_per_star_req_type_hash.get("a")),
                (double)addmission_per_star_req_type_hash.get("b")/(addmission_per_star_req_type_hash.get("b") + blocking_per_star_req_type_hash.get("b")),
                (double)addmission_per_star_req_type_hash.get("c")/(addmission_per_star_req_type_hash.get("c") + blocking_per_star_req_type_hash.get("c")),
                (double)addmission_per_star_req_type_hash.get("d")/(addmission_per_star_req_type_hash.get("d") + blocking_per_star_req_type_hash.get("d")),
                (double)addmission_per_star_req_type_hash.get("e")/(addmission_per_star_req_type_hash.get("e") + blocking_per_star_req_type_hash.get("e")),
                (double)addmission_per_star_req_type_hash.get("f")/(addmission_per_star_req_type_hash.get("f") + blocking_per_star_req_type_hash.get("e")),

                
                (double)avg_route_length_hash.get("a")/(double)addmission_per_star_req_type_hash.get("a"),
                (double)avg_route_length_hash.get("b")/(double)addmission_per_star_req_type_hash.get("b"),
                (double)avg_route_length_hash.get("c")/(double)addmission_per_star_req_type_hash.get("c"),
                (double)avg_route_length_hash.get("d")/(double)addmission_per_star_req_type_hash.get("d"),
                (double)avg_route_length_hash.get("e")/(double)addmission_per_star_req_type_hash.get("e"),
                (double)avg_route_length_hash.get("f")/(double)addmission_per_star_req_type_hash.get("f"),
//                avg_route_length_a,
//                avg_route_length_b,
//                avg_route_length_c,
//                avg_route_length_d,
//                avg_route_length_e,   

                reroutes_star_perType.get("a"),
                reroutes_star_perType.get("b"),
                reroutes_star_perType.get("c"),
                reroutes_star_perType.get("d"),
                reroutes_star_perType.get("e"),
                reroutes_star_perType.get("f")


            });
            Print.println(" linewriter  " + line);

            data.put(line + 2, new Object[] {


                "admission_link","  blocking_link", " throughput_type_link",
                "total link aa admitted","total link aa blocked","total link aa throughput", "total link aa used","total link aa cap","total link aa util","L0 link  aa util","L1 link  aa util","L2 link  aa util","L3 link  aa util",
                "total link bb admitted","total link bb blocked","total link bb throughput", "total link bb used","total link bb cap","total link bb util","L0 link  bb util","L1 link  bb util","L2 link  bb util","L3 link  bb util",
                "total link cc admitted","total link cc blocked","total link cc throughput", "total link cc used","total link cc cap","total link cc util","L0 link  cc util","L1 link  cc util","L2 link  cc util","L3 link  cc util",
                "total link dd admitted","total link dd blocked","total link dd throughput", "total link dd used","total link dd cap","total link dd util","L0 link  dd util","L1 link  dd util","L2 link  dd util","L3 link  dd util",                         
                "total link ee admitted","total link ee blocked","total link ee throughput", "total link ee used","total link ee cap","total link ee util","L0 link  ee util","L1 link  ee util","L2 link  ee util","L3 link  ee util",

                "total link L0 link  used","total link L0 link  cap","total link L0 link  util",
                "total link L1 link  used","total link L1 link  cap","total link L1 link  util",
                "total link L2 link  used","total link L2 link  cap","total link L2 link  util",
                "total link L3 link  used","total link L3 link  cap","total link L3 link  util",						


            });
    //        data.put("2", new Object[] {1d, "John", 1500000d});
            Print.println(" linewriter  " + line);
            data.put(line + 3, new Object[] {


                admission_link,  blocking_link,  throughput_type_link,
                total_accepted_aa_link, total_blocked_aa_link,  total_throughput_aa_link,  total_used_aa_link, total_cap_aa_link,  total_troughput_aa_link,L0_util_aa_link, L1_util_aa_link, L2_util_aa_link, L3_util_aa_link,
                total_accepted_bb_link, total_blocked_bb_link,  total_throughput_bb_link,  total_used_bb_link, total_cap_bb_link,  total_troughput_bb_link,L0_util_bb_link, L1_util_bb_link, L2_util_bb_link, L3_util_bb_link,
                total_accepted_cc_link, total_blocked_cc_link,  total_throughput_cc_link,  total_used_cc_link, total_cap_cc_link,  total_troughput_cc_link,L0_util_cc_link, L1_util_cc_link, L2_util_cc_link, L3_util_cc_link,
                total_accepted_dd_link, total_blocked_dd_link,  total_throughput_dd_link,  total_used_dd_link, total_cap_dd_link,  total_troughput_dd_link,L0_util_dd_link, L1_util_dd_link, L2_util_dd_link, L3_util_dd_link,
                total_accepted_ee_link, total_blocked_ee_link,  total_throughput_ee_link,  total_used_ee_link, total_cap_ee_link,  total_troughput_ee_link,L0_util_ee_link, L1_util_ee_link, L2_util_ee_link, L3_util_ee_link,

                total_used_L0_link, total_cap_L0_link,  total_troughput_L0_link,                                                                                                                                                           
                total_used_L1_link, total_cap_L1_link,  total_troughput_L1_link,                                                                                                                                                           
                total_used_L2_link, total_cap_L2_link,  total_troughput_L2_link,                                                                                                                                                           
                total_used_L3_link, total_cap_L3_link,  total_troughput_L3_link,                                                                                                                                                           


            });
            Print.println(" linewriter  " + line);

            data.put(line + 4, new Object[] {

                "admission_node","  blocking_node", " throughput_type_node",
                "total node aa admitted","total node aa blocked","total node aa throughput", "total node aa used","total node aa cap","total node aa util","L0 node  aa util","L1 node  aa util","L2 node  aa util","L3 node  aa util",
                "total node bb admitted","total node bb blocked","total node bb throughput", "total node bb used","total node bb cap","total node bb util","L0 node  bb util","L1 node  bb util","L2 node  bb util","L3 node  bb util",
                "total node cc admitted","total node cc blocked","total node cc throughput", "total node cc used","total node cc cap","total node cc util","L0 node  cc util","L1 node  cc util","L2 node  cc util","L3 node  cc util",
                "total node dd admitted","total node dd blocked","total node dd throughput", "total node dd used","total node dd cap","total node dd util","L0 node  dd util","L1 node  dd util","L2 node  dd util","L3 node  dd util",                         
                "total node ee admitted","total node ee blocked","total node ee throughput", "total node ee used","total node ee cap","total node ee util","L0 node  ee util","L1 node  ee util","L2 node  ee util","L3 node  ee util",

                "total node L0 node  used","total node L0 node  cap","total node L0 node  util",
                "total node L1 node  used","total node L1 node  cap","total node L1 node  util",
                "total node L2 node  used","total node L2 node  cap","total node L2 node  util",
                "total node L3 node  used","total node L3 node  cap","total node L3 node  util",						

            });
    //        data.put("2", new Object[] {1d, "John", 1500000d});
    //        data.put("3", new Object[] {2d, "Sam", 800000d});
            data.put(line + 5, new Object[] {


               //node information
                admission_node,  blocking_node,  throughput_type_node,                                                                                                                                                                     
                total_accepted_aa_node, total_blocked_aa_node,  total_throughput_aa_node,  total_used_aa_node, total_cap_aa_node,  total_troughput_aa_node,L0_util_aa_node, L1_util_aa_node, L2_util_aa_node, L3_util_aa_node,
                total_accepted_bb_node, total_blocked_bb_node,  total_throughput_bb_node,  total_used_bb_node, total_cap_bb_node,  total_troughput_bb_node,L0_util_bb_node, L1_util_bb_node, L2_util_bb_node, L3_util_bb_node,
                total_accepted_cc_node, total_blocked_cc_node,  total_throughput_cc_node,  total_used_cc_node, total_cap_cc_node,  total_troughput_cc_node,L0_util_cc_node, L1_util_cc_node, L2_util_cc_node, L3_util_cc_node,
                total_accepted_dd_node, total_blocked_dd_node,  total_throughput_dd_node,  total_used_dd_node, total_cap_dd_node,  total_troughput_dd_node,L0_util_dd_node, L1_util_dd_node, L2_util_dd_node, L3_util_dd_node,
                total_accepted_ee_node, total_blocked_ee_node,  total_throughput_ee_node,  total_used_ee_node, total_cap_ee_node,  total_troughput_ee_node,L0_util_ee_node, L1_util_ee_node, L2_util_ee_node, L3_util_ee_node,

                total_used_L0_node, total_cap_L0_node,  total_troughput_L0_node,
                total_used_L1_node, total_cap_L1_node,  total_troughput_L1_node,
                total_used_L2_node, total_cap_L2_node,  total_troughput_L2_node,
                total_used_L3_node, total_cap_L3_node,  total_troughput_L3_node

            });

            Set<Integer> keyset = data.keySet();
            int rownum = 0;
            for (int key : keyset) {
                //Row row = sheet.createRow(Integer.parseInt(key));
                
                Row row = sheet.createRow(key);
                
                Object [] objArr = data.get(key); 

                ///I added this bit to move in the excel file running more simulations
//                        int cellnum = excel_horiz;
                int cellnum = 0;

                for (Object obj : objArr) {
                    Cell cell = row.createCell(cellnum++);
                    if(obj instanceof Date) 
                        cell.setCellValue((Date)obj);
                    else if(obj instanceof Boolean)
                        cell.setCellValue((Boolean)obj);
                    else if(obj instanceof String)
                        cell.setCellValue((String)obj);
                    else if(obj instanceof Double)
                        cell.setCellValue((Double)obj);
                    else if(obj instanceof Integer)
                        cell.setCellValue((Integer)obj);
                }
            }

            try {
                try (FileOutputStream out = new FileOutputStream(new File(file_address))) {
                    workbook.write(out);
                }
                System.out.println("Excel written successfully..");

            } catch (FileNotFoundException e) {
            } catch (IOException e) {
            }                    
        }


        
//
//        HashMap<String, Object[]> data = new HashMap<>();
//
//        data.put("0", new Object[] {"admission_link","blocking_link", "throughput",
//            "total aa used","total aa cap","total aa util",
//            "total bb used","total bb cap","total bb util"," avg route lengths"});
////        data.put("2", new Object[] {1d, "John", 1500000d});
////        data.put("3", new Object[] {2d, "Sam", 800000d});
//        data.put(line, new Object[] {blocking_link, admission_link, throughput_type_link,total_used_aa_link, total_cap_aa_link, 
//            total_troughput_aa_link, total_usedbb, total_capbb,  total_troughputbb,avg_route_length});
//
//        Set<String> keyset = data.keySet();
//        int rownum = 0;
//        for (String key : keyset) {
//            //Row row = sheet.createRow(Integer.parseInt(key));
//            Row row = sheet.createRow(Integer.parseInt(key));
//            Object [] objArr = data.get(key); 
//            
//            ///I added this bit to move in the excel file running more simulations
//            int cellnum = excel_horiz;
//            
//            for (Object obj : objArr) {
//                Cell cell = row.createCell(cellnum++);
//                if(obj instanceof Date) 
//                    cell.setCellValue((Date)obj);
//                else if(obj instanceof Boolean)
//                    cell.setCellValue((Boolean)obj);
//                else if(obj instanceof String)
//                    cell.setCellValue((String)obj);
//                else if(obj instanceof Double)
//                    cell.setCellValue((Double)obj);
//            }
//        }
//
//        try {
//            FileOutputStream out = 
//                    new FileOutputStream(new File(file_address));
//            workbook.write(out);
//            out.close();
//            System.out.println("Excel written successfully..");
//
//        } catch (FileNotFoundException e) {
//            e.printStackTrace();
//        } catch (IOException e) {
//            e.printStackTrace();
//        }
    }
} 