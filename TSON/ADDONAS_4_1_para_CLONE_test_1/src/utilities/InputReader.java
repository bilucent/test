/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package utilities;

import Infrastructure.Link;
import Infrastructure.Node;
import Vritualisation.VirInstnace;
import java.io.FileInputStream;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.*;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.apache.poi.hssf.usermodel.HSSFCell;
import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.poifs.filesystem.POIFSFileSystem;
import resources.Parent;
import routing.Routing;

/**
 *
 * @author Bijan
 */
public class InputReader {
    boolean Debug = false;
    CreateSQLStatement S;
    Converters C;
    Print P;
    Maths M;
    
    //before getting to the dimensions
    //node: in excel, these numbers are +1
    
    //exclduing dimensions, is general use, not resrouces
    int Num_general_node_info;// =7;
    int Num_general_link_info;// =4;
    
    //start of the info in excel sheet
    int excelrow_network_information;// = 1;
    
    //not an excel row, general number and types of nodes and links
    int num_network_information_rows;// = 4;
       
    int excel_types_rows;// = 2;
    int excel_interInfo_rows;// = 2;

    int excelrow_nodes_types_dimensions;// = 7;
    int excelrow_links_types_dimensions;// = 11;
    int excelrow_network_topology;// = 15;
    
    int max_node_dimension;
    int max_link_dimension;
    
    int _number_of_nodes;
    int _number_of_node_types;
    int excelrow_node_information; 
    
    int _number_of_links;
    int _number_of_link_types;    
    int excelrow_link_information;
    
    
    int[][] network_matrix; 
    
    int[][] network_link_type_matrix; 
    String DBname;// = "ADDONAS";
    String Input_file;
    
    String DB_treatment;
    
    // these hash maps store critical networking information
    HashMap<Integer, String> link_adj;
    HashMap<String, Integer> node_types_hash;
    HashMap<String, Integer> link_types_hash;
    
    List<Integer> res_inf_link = new ArrayList<>();
    List<Integer> res_inf_node = new ArrayList<>();;
    
    /*
     * the constructure, gets all the necessary constants from the main file, such
     * as excel important tow numbers, and the DB and inputfile strings
     */
    public InputReader(boolean debug, String dbname, String db_treatment, String input_file, int excel_start_row,int num_node_genInfSlots,
            int num_link_genInfSlots, int num_gen_info_rows,
            int excelRowsPerType, int excelInterInfGap) throws SQLException
    {
        this.Debug = debug;
        this.excelrow_network_information = excel_start_row;
        this.num_network_information_rows = num_gen_info_rows;
        this.DBname = dbname;
        this.excel_types_rows = excelRowsPerType;
        this.excel_interInfo_rows = excelInterInfGap;
        this.Num_general_node_info = num_node_genInfSlots;
        this.Num_general_link_info = num_link_genInfSlots;
        this.Input_file = input_file;
        
        this.node_types_hash = new HashMap();
        this.link_types_hash = new HashMap();
        this.link_adj = new HashMap();
        this.DB_treatment = db_treatment;
        

        //create the auxillary classes
        M = new Maths();
        P = new Print();        
        C = new Converters();
        //this.node_types_hash = new Hashtable
        try {

            S = new CreateSQLStatement(Debug);
            DataBase_initiation(db_treatment);
        }
        catch(SQLException e){
            System.out.println("Failed");
            e.printStackTrace();
        }        
        
    }
    
    public void DataBase_initiation(String db_treatment) throws SQLException
    {
        P.println(" Database access is based on: " + db_treatment); 
        if(db_treatment == "USE")
        {
            S.UseDatabase(DBname);
        }
        else if(db_treatment == "CREATE")
        {
            S.CreateDB(DBname);
        }else 
        {
            P.println(" the database treatment failed");
            System.exit(739);
        }    
    }
    
    
    /*
     * this method uses the available excel constant to give value to the rest,
     * to automate the excel row finding for gathering data info
     */
    public void initialise_network_excel_constants()
    {
        
        //the rows values are being populate in order
        excelrow_nodes_types_dimensions = excelrow_network_information
                +  num_network_information_rows+ excel_interInfo_rows;
        excelrow_links_types_dimensions = excelrow_nodes_types_dimensions 
                + excel_types_rows + excel_interInfo_rows;     
        excelrow_network_topology = excelrow_links_types_dimensions
                + excel_types_rows + excel_interInfo_rows; 
        excelrow_node_information = _number_of_nodes + excelrow_network_topology 
                + excel_interInfo_rows;
        excelrow_link_information = excelrow_node_information + _number_of_nodes 
                + excel_interInfo_rows;
        
        P.println("_number_of_nodes: " + _number_of_nodes);
        P.println("_number_of_node_types: " + _number_of_node_types);
        P.println("number_of_links: " + _number_of_links);
        P.println("_number_of_link_types: " + _number_of_link_types);
        P.println("excelrow_network_topology: " + excelrow_network_topology);
        P.println("excelrow_node_information: " + excelrow_node_information);
        P.println("excelrow_link_information: " + excelrow_link_information);
    }
    
    /*
     * to parse the virtualistion info/ not in use
     */
//    public boolean  virtualisation_excel(String file){
//        /**
//        * Create a new instance for cellDataList
//        */
//        List cellDataList = new ArrayList();
//        try
//        {
//            /**
//            * Create a new instance for FileInputStream class
//            */
//            FileInputStream fileInputVirt = new FileInputStream("C:/Users/Bijan/Dropbox/Under_Work/ADDONAS_1_5_1_5/JAVAEXCEL/virt.xls");//vaio
//            //FileInputStream fileInputVirt = new FileInputStream("C:/Users/br12367/Dropbox/Under_Work/ADDONAS/JAVAEXCEL/virt.xls");//newPC
//
//            // these bits are for importing information from excel
//            
//            /**
//            * Create a new instance for POIFSFileSystem class
//            */
//            POIFSFileSystem fsFileSystem = new POIFSFileSystem(fileInputVirt);
//
//            /*
//            * Create a new instance for HSSFWorkBook Class
//            */
//            HSSFWorkbook workBook = new HSSFWorkbook(fsFileSystem);
//            HSSFSheet hssfSheet = workBook.getSheetAt(0);
//
//            /**
//            * Iterate the rows and cells of the spreadsheet
//            * to get all the datas.
//            */
//            Iterator rowIterator = hssfSheet.rowIterator();
//            
//            while (rowIterator.hasNext())
//            {
//                HSSFRow hssfRow = (HSSFRow) rowIterator.next();
//                Iterator iterator = hssfRow.cellIterator();
//                List cellTempList = new ArrayList();
//                int cell_counter = 0;
//                while (iterator.hasNext())
//                {
//                    HSSFCell hssfCell = (HSSFCell) iterator.next();
//                    cellTempList.add(hssfCell);
//                    cell_counter++;
//                    //System.out.print(cell_counter++);
//                } //System.out.println();
//                cellDataList.add(cellTempList);
//            }            
//
//        }
//        catch (Exception e)
//        {
//            e.printStackTrace();
//        }
//        /**
//        * Call the printToConsole method to print the cell data in the
//        * console.
//        */
//        //printToConsole(cellDataList);
//        System.out.println("***************");
//        List vir =  parseVirtCellData(cellDataList);
//        if(vir!= null){return true;}
//        else{return false;}
//        
//        
//    }
//    
//    /*
//     * this file gathers all the network info from excel file in a cell list
//     */
    public void infrastructure_excel(){
        /**
        * Create a new instance for cellDataList
        */
        List cellDataList = new ArrayList();
        try
        {
            String path = System.getProperty("user.dir");
            /**
            * Create a new instance for FileInputStream class
            */
            //FileInputStream fileInputStream = new FileInputStream("C:/Users/Bijan/Desktop/JAVAEXCEL/first.xls");
           //FileInputStream fileInputStream = new FileInputStream("C:/Users/Bijan/Dropbox/Under_Work/ADDONAS/JAVAEXCEL/first.xls");//vaio and oldPC
           //FileInputStream fileInputStream = new FileInputStream("C:/Users/br12367/Dropbox/Under_Work/ADDONAS/JAVAEXCEL/first.xls");//newPC
           
            //FileInputStream fileInputStream = new FileInputStream("C:/Users/Bijan/Dropbox/Under_Work/ADDONAS_1_5_1_5/JAVAEXCEL/"+ this.Input_file+".xls");//oldPC
           FileInputStream fileInputStream = new FileInputStream(path + "/JAVAEXCEL/"+ this.Input_file+".xls");//oldPC
           //FileInputStream fileInputVirt = new FileInputStream("C:/Users/Bijan/Dropbox/Under_Work/ADDONAS/JAVAEXCEL/virt.xls");//vaio and oldPC
           //FileInputStream fileInputVirt = new FileInputStream("C:/Users/br12367/Dropbox/Under_Work/ADDONAS/JAVAEXCEL/virt.xls");//newPC
           //FileInputStream fileInputVirt = new FileInputStream("C:/Users/Bijan/Dropbox/Under_Work/ADDONAS/JAVAEXCEL/virt.xls");//oldPC
             

            /**
            * Create a new instance for POIFSFileSystem class
            */
            POIFSFileSystem fsFileSystem = new POIFSFileSystem(fileInputStream);

            /*
            * Create a new instance for HSSFWorkBook Class
            */
            HSSFWorkbook workBook = new HSSFWorkbook(fsFileSystem);
            HSSFSheet hssfSheet = workBook.getSheetAt(0);

            /**
            * Iterate the rows and cells of the spreadsheet
            * to get all the datas.
            */
            Iterator rowIterator = hssfSheet.rowIterator();
            
            while (rowIterator.hasNext())
            {
                HSSFRow hssfRow = (HSSFRow) rowIterator.next();
                Iterator iterator = hssfRow.cellIterator();
                List cellTempList = new ArrayList();
                int cell_counter = 0;
                while (iterator.hasNext())
                {
                    HSSFCell hssfCell = (HSSFCell) iterator.next();
                    cellTempList.add(hssfCell);
                    cell_counter++;
                    //System.out.print(cell_counter++);
                } //System.out.println();
                cellDataList.add(cellTempList);
            }            

        }
        catch (Exception e)
        {
            e.printStackTrace();
        }
        /**
        * Call the printToConsole method to print the cell data in the
        * console.
        */
        parseCellData(cellDataList);
        
    }

    /**
    * This method is used to print the cell data to the console.
    * @param cellDataList - List of the data'S in the spreadsheet.
    */
    private void printToConsole(List cellDataList)
    {
        for (int i = 0; i < cellDataList.size(); i++)
        {
            List cellTempList = (List) cellDataList.get(i);
            for (int j = 0; j < cellTempList.size(); j++)
            {
                HSSFCell hssfCell = (HSSFCell) cellTempList.get(j);
                String stringCellValue = hssfCell.toString();
                System.out.print(stringCellValue + "\t");
            }
        System.out.println();
        }        
        
    }
    
    /* 
     * this function, using extracted excel info, populates the sql tables,
     * for either nodes and links.
     * 
     * It can be improved by mroe properly segregating node an dlink info 
     * population
     */
    private void insertDataToDataBase(String tablename, List cellDataList,
            int num_gen_inf_link_or_node, int max_dimension, int start_row, int finish_row, HashMap l_a)
    {

        String column_name = null;
        String[] D1= null;
        int counter = 0;
        if(Debug)
        P.println(" inserting data in to DB, table name : " + tablename + 
                " , start row: " + start_row + " finish row: " + finish_row);
        for (int i = start_row; i < finish_row; i++)
        {
            
            counter++;
            List cellTempList = (List) cellDataList.get(i);
            for (int j = 0; j <  num_gen_inf_link_or_node + max_dimension; j++)
            {
                //get the cell string
                HSSFCell hssfCell = (HSSFCell) cellTempList.get(j);
                if(Debug)
                P.print(" entering cell info nito tables cell number is j = " + j);
                
                //the j==0 has been separated,since it has differnt type, of int
                // and is the ID of the nodess and links
                if(j==0)
                {
                    // identical to all nodes and links
                    column_name = "id";
                    
                    //get the numberic (int) value of the cell
                    int key = (int)(hssfCell.getNumericCellValue());
                    if(Debug)
                    P.println("   in  j = " + j + ", key is -> " + key);
                    
                    try {
                        S.insert_ID_into_tables_(tablename, column_name, key);
                    } catch (SQLException ex) {
                        Logger.getLogger(InputReader.class.getName()).log(Level.SEVERE, null, ex);

                    }
                }
                else
                {
                    /*
                     * Here is generate teh dimension columns for nodes an dlinks
                     * these are called D0, D1 and so on,
                     * it can be differnet for ndoes and links
                     */
                    //get the string value of the cell
                    String stringCellValue = hssfCell.toString();
                    if(Debug)
                    System.out.print("   in  j = " + j + ", values is -> " + stringCellValue + "  the row is i : " + i);

                    if(j>num_gen_inf_link_or_node-1)
                    {
                        if(stringCellValue.isEmpty()){stringCellValue =""+0;}
                        stringCellValue = serialise_int(stringCellValue); 
                    }
                    //determine the column name
                    //since the links and node do not neccessarrily 
                    //have the same amount of general info,
                    //some later cases might be used only by one of them
                    if(j<num_gen_inf_link_or_node && j>0)
                    {
                        switch (j)
                        {
                            case 1:column_name = "type";break;
                            case 2:column_name = "cost";break;
                            case 3:column_name = "delay";;break;
                            
                                //from 4 onwards, only Node has info
                            case 4:column_name = "X";break;
                            case 5:column_name = "Y";break;
                            case 6:column_name = "Z";;break;
                        }
                    }
                    
                    // here the D cells are genrated
                    else if (j>=num_gen_inf_link_or_node && max_dimension>0) 
                    {
                        column_name = "D"+ (j - num_gen_inf_link_or_node);
                    }
                    else P.print(" bad column naming");
                    
                    //here I insert the information into sql table. I am 
                    // calling a query using this method
                    try {
                        if(Debug)
                        P.print(" $$stringCellValue " + stringCellValue);
                        S.insert_Into_tables_(DBname,tablename, column_name, counter, stringCellValue);
                    } catch (SQLException ex) {
                        Logger.getLogger(InputReader.class.getName()).log(Level.SEVERE, null, ex);
                    }  
                }                  
            }

        //System.out.println();
        }                
    }    
    
    
    /*
     * This function, processes all the data of the excel files, which 
     */
    private void parseCellData(List cellDataList)
    {

        //first, I need to give value to all the constants in this class
        int row_counter = 0;
        int[] admin_info = new int [num_network_information_rows];
        for (int i = excelrow_network_information; i < num_network_information_rows 
                + excelrow_network_information; i++)
        {
            List cellTempList = (List) cellDataList.get(i);
            int j= 1;
            {
                HSSFCell hssfCell = (HSSFCell) cellTempList.get(j);
                admin_info[row_counter] = (int)(hssfCell.getNumericCellValue()); 

            }
            row_counter++;
        
        }
        // in this bit the genral network info are gahered in an array
        _number_of_nodes = admin_info[0];
        _number_of_links = admin_info[1];
        _number_of_node_types = admin_info[2];
        _number_of_link_types = admin_info[3];        
        
        
        //now the rest of the variable can be assigned
        //this method initialises all the constants
        initialise_network_excel_constants();
        
        //get the node type information
        node_types_hash = new HashMap();
        List cellTempList = (List) cellDataList.get(excelrow_nodes_types_dimensions);//types: this is the key to HM
        List cellTempList_1 = (List) cellDataList.get(excelrow_nodes_types_dimensions+1);//numbers
        
        ArrayList<String> _types = new ArrayList();
        ArrayList<Integer> _type_dimension = new ArrayList();
        //here I read cell by cell from lefft to right, the node informations
        for (int j = 0; j < _number_of_node_types; j++)
        {
            HSSFCell hssfCell = (HSSFCell) cellTempList.get(j);
            HSSFCell hssfCell_1 = (HSSFCell) cellTempList_1.get(j);
            try{
                String type = hssfCell.getStringCellValue();
                int key = (int)(hssfCell_1.getNumericCellValue());
                _types.add(type);
                _type_dimension.add(key);
                node_types_hash.put(type, key);
                //node_types[i][j] = (int)(hssfCell.getNumericCellValue());
            }
            catch(IllegalStateException e){
                System.err.println("Failed   hssfCell." + hssfCell.toString());
                e.printStackTrace();
                System.exit(1);
            }  
            String stringCellValue = hssfCell.toString();
            String stringCellValue_1 = hssfCell_1.toString();
            System.out.print("\t" + stringCellValue + " : " + stringCellValue_1 + " ");
        }
        /*
        Collection<Integer> Collect_values =  node_types_hash.values();
        Object[] Obj_colec_values =  Collect_values.toArray();
        */
        int [] Int_node_colec_values = new int[_type_dimension.size()];
        for (int j = 0; j < _type_dimension.size(); j++)
        {        
           Int_node_colec_values[j] = _type_dimension.get(j);
        }

        //if(Debug)
        P.print_1D("\nnode collection converted to int ", Int_node_colec_values);
        
        max_node_dimension = M.find_max(Int_node_colec_values);     
        
        
        
        
        
        
        ///for l types
        //get the Linsk type information
        link_types_hash = new HashMap();
        
        _types = new ArrayList();
        _type_dimension = new ArrayList();        
        cellTempList = (List) cellDataList.get(excelrow_links_types_dimensions);//types: this is the key to HM
        cellTempList_1 = (List) cellDataList.get(excelrow_links_types_dimensions+1);//numbers
        for (int j = 0; j < _number_of_link_types; j++)
        {
            HSSFCell hssfCell = (HSSFCell) cellTempList.get(j);
            HSSFCell hssfCell_1 = (HSSFCell) cellTempList_1.get(j);
            try{
                String type = hssfCell.getStringCellValue();
                int key = (int)(hssfCell_1.getNumericCellValue());
                _types.add(type);
                if(Debug)
                P.println("\t  _types -> " + type + "  --");
                _type_dimension.add(key);                
                link_types_hash.put(type, key);
                //node_types[i][j] = (int)(hssfCell.getNumericCellValue());
            }
            catch(IllegalStateException e){
                System.err.println("Failed   hssfCell." + hssfCell.toString());
                e.printStackTrace();
                System.exit(1);
            }  
            String stringCellValue = hssfCell.toString();
            String stringCellValue_1 = hssfCell_1.toString();
            
            if(Debug)
            P.println(stringCellValue + "\t" + stringCellValue_1 + " ");
        }
        ++row_counter;
        
        int [] Int_link_colec_values = new int[_type_dimension.size()];
        for (int j = 0; j < _type_dimension.size(); j++)
        {        
           Int_link_colec_values[j] = _type_dimension.get(j);
        }        
        
        //if(Debug)
        P.print_1D("\nlink collection converted to int ", Int_link_colec_values);
        
        max_link_dimension = M.find_max(Int_link_colec_values);    
        
        
        //get the matrix
        link_adj = new HashMap();
        network_matrix = new int[_number_of_nodes][_number_of_nodes];
        row_counter = excelrow_network_topology;
        for (int i = 0; i < _number_of_nodes; i++)
        { 
            
            //System.out.println("Row counter:  "+ row_counter + " i : " + i + "   ");
            cellTempList = (List) cellDataList.get(row_counter);
            
            for (int j = 0; j < _number_of_nodes; j++)
            {
                HSSFCell hssfCell = (HSSFCell) cellTempList.get(j+1);
                try{
                network_matrix[i][j] = (int)(hssfCell.getNumericCellValue());
                }
                catch(IllegalStateException e){
                    System.err.println("Failed   hssfCell." + hssfCell.toString());
                    e.printStackTrace();
                    System.exit(6);
                }
                
                if(network_matrix[i][j]>0){
                    //P.println("\n");
                    int link_number=network_matrix[i][j] - 1;// I cam converting tthe ids from 1, to from 0.
                    
                    if(Debug)
                    P.println(" database link_number: " + link_number + " (in excel) is : " + (link_number +1) + " the link_adj to be put is  " + (i)+":"+(j) );
                    
                    link_adj.put(link_number,(i)+":"+(j));}
                String stringCellValue = hssfCell.toString();
                //System.out.print(stringCellValue + "\t");
            }
            
        ++row_counter;
        }     
        //if(Debug)
        P.print_2D("\nthe network matrix", network_matrix);
        
        //if to use the previous setup, do not try to recreate database stuff
        if(DB_treatment.equalsIgnoreCase("CREATE")
            )
        {
            try {
                create_nodetable(max_node_dimension);
            } catch (SQLException ex) {
                Logger.getLogger(InputReader.class.getName()).log(Level.SEVERE, null, ex);
            }
            try {
                create_linktable(max_link_dimension);
            } catch (SQLException ex) {
                Logger.getLogger(InputReader.class.getName()).log(Level.SEVERE, null, ex);
            }


            //if(Debug)
            P.print(" max_node_dimension: " + max_node_dimension +" \tmax_link_dimension: " +  max_link_dimension);

            insertDataToDataBase("nodes",cellDataList,Num_general_node_info,max_node_dimension, 
                    excelrow_node_information,excelrow_node_information + _number_of_nodes , link_adj );

            insertDataToDataBase("links",cellDataList,Num_general_link_info,max_link_dimension, 
                    excelrow_link_information,excelrow_link_information + _number_of_links, link_adj );
            String result = null;
            
            if(Debug)
            P.print(" now reading back a cel... ");


            try {
                result = S.read_cell("cost", "nodes", 3);
            } catch (SQLException ex) {
                Logger.getLogger(InputReader.class.getName()).log(Level.SEVERE, null, ex);
            }
            
            if(Debug)
            P.print("restuls " + result);
        }
            
    }       
    
    
    /*
     * parse virtualisation excel file
     */
    private List<VirInstnace>  parseVirtCellData(List cellDataList)
    {
        List<VirInstnace> virt = new ArrayList();
        
        int row_counter = 0;
        int number_of_vir_nets = 0;
        int number_of_links;
        int number_of_domains;

        List cellTempList = (List) cellDataList.get(0);
        
        if(Debug)
        System.out.println( "row_counter: " + row_counter);
        
        HSSFCell hssfCell = (HSSFCell) cellTempList.get(1);
        switch ((int)(hssfCell.getNumericCellValue())){
            case 1:virt= null;break;
            
                
        }


        return virt;
    }    
    
    /*
     * This function gets the dimension of resrouces, and based on that 
     * attemps for sql node table generation
     */

    public void create_nodetable(int dimension) throws SQLException{

        String NodesQueryString = "CREATE TABLE "+ "nodes "
                    + "(id INT NOT NULL AUTO_INCREMENT PRIMARY KEY, TYPE text, COST TEXT,"
                    + " DELAY TEXT ,X TEXT, Y TEXT, Z TEXT";
        if (dimension>0){
            for(int i = 0; i < dimension; i++)
            {
                NodesQueryString = NodesQueryString + ",D"+i+" TEXT";
            }
            NodesQueryString = NodesQueryString + ", D"+dimension+" TEXT);";
        }
        else NodesQueryString = NodesQueryString + ");";
        
        if(Debug)
        P.print(NodesQueryString);
        
        S.createTable(NodesQueryString);        
      
    }
    
    
    /*
     * This function gets the dimension of resrouces, and based on that 
     * attemps for sql link table generation
     */    
    
    public void create_linktable(int dimension) throws SQLException{
        String NodesQueryString = "CREATE TABLE "+ "links "
                    + "(id INT NOT NULL AUTO_INCREMENT PRIMARY KEY, TYPE TEXT, COST TEXT,"
                    + " DELAY TEXT";
        if (dimension>0){
            for(int i = 0; i < dimension; i++)
            {
                NodesQueryString = NodesQueryString + ", D"+i+" TEXT";
            }
            NodesQueryString = NodesQueryString + ", D"+dimension+" TEXT);";
        }
        else NodesQueryString = NodesQueryString + ");";
        S.createTable(NodesQueryString);
    }  
    
    
    public String serialise_int(String input){
        String String_value ="";
        double value = Double.parseDouble(input);
        for(int i = 0; i< value; i++){
            String_value= String_value+0;
        }
        return String_value;
    }
    public int[] convert_string_to_2D(String chain)
    {
        int[] intArray = new int[chain.length()];

        for (int i = 0; i <chain.length(); i++) {
                intArray[i] = Character.digit(chain.charAt(i), 10);
        }
        if(Debug)
        P.print_1D(" the chain is converted-->", intArray);
        
    return intArray;    
    }

    public  List<Node> createNodeList() throws SQLException
    {
        S.UseDatabase(DBname);
        
        
        List<Node> nodes = new ArrayList();

     
        ResultSet rs = null;
        try {
            // read the table row by row
             //first get the size
             rs = S.get_ResultSet_("nodes");
             
             
             
        } catch (SQLException ex) {
            Logger.getLogger(InputReader.class.getName()).log(Level.SEVERE, null, ex);
        }

        P.print(" **************create node list***************  ");
        for(int i = 0; i< _number_of_nodes;i++)
        {
            try {
                int row = i+1;
                rs.absolute(row);
                //print(" rs.getCursorName() " + rs.getCursorName());
            } catch (SQLException ex) {
                Logger.getLogger(InputReader.class.getName()).log(Level.SEVERE, null, ex);
                System.exit(2);
            }
            
            //get the resoruces from the database
            HashMap<String, String> Node_Resources_hash = new HashMap();
            int dimension_size;
            
            //retrive the dimenstion of the node type
            String type = rs.getString("Type");
            dimension_size = 0;
            
//            if(Debug)
            P.println(" node type in nodelist creat method: " + type);
            
            try{
                dimension_size = (int) node_types_hash.get(type);
            }catch(NullPointerException e)
            {
                P.println(" error  ##");
                System.exit(3443);
            }
            //after gettingthe dimension of resrouces, insert them in the hash
            for(int j = 0; j < dimension_size ; j++)
            {
                //note: in data base, Ds start from 0, unlike the excel
                Node_Resources_hash.put("D"+j, rs.getString("D"+j));
            }

            //here I make an iterative resrouce object for each node
            //List<Integer> res_inf = C.convert_hash2List_int(Resources_hash);
            //ParentClass noderes_obj = new ParentClass(res_inf);
            P.println(" \nin the node creating list in inoutreader" + Node_Resources_hash.toString());
            
            res_inf_node = C.convert_hash2List_int(Node_Resources_hash);            
            List<String> res_inf_node_str = C.convert_hash2List_str(Node_Resources_hash);
            
            P.println(" \nin the node creating list in inputreader: res_inf_node_str" + res_inf_node_str.toString());
//            P.println(" \nin the node creating list in inputreader: res_inf_node_str" + res_inf_node_str.toString());
            
            Parent noderes_obj = new Parent(Debug, res_inf_node_str);
            //lr_obj.printResHash();
            //lr_obj.printLayers_List_all_layered_resources();
            //lr_obj.getm_Agg_layers_resources_demonstrator();
            if(Debug)
            noderes_obj.getm_detailed_resources_availabiilties_demonstrator();            

            //make the nodes
            Node node = new Node(Debug,Num_general_node_info,max_node_dimension, i,
                    rs.getString("TYPE"),rs.getString("COST"),rs.getString("DELAY"),
                    rs.getString("X"),rs.getString("Y"),rs.getString("Z"),Node_Resources_hash
                    ,noderes_obj);
            
            //if(Debug)
            P.print(" nodes id =   " + node.getmID()+" \n\n");
            nodes.add(node);

        }
        return nodes;
    }
    
    
    public  List<Link> createLinkList() throws SQLException
    {
        S.UseDatabase(DBname);
        
        
        List<Link> links = new ArrayList();

     
        ResultSet rs = null;
        try {
            // read the table row by row
             //first get the size
             rs = S.get_ResultSet_("links");
             
             
             
        } catch (SQLException ex) {
            Logger.getLogger(InputReader.class.getName()).log(Level.SEVERE, null, ex);
        }

        P.println(" **************create link list***************  ");
        for(int i = 0; i< _number_of_links;i++)
        {
            int row = i+1;
            try {
                
                rs.absolute(row);
                //print(" rs.getCursorName() " + rs.getCursorName());
            } catch (SQLException ex) {
                Logger.getLogger(InputReader.class.getName()).log(Level.SEVERE, null, ex);
                System.exit(2);
            }
            
            //get the link-adj for routing information in the link
            //P.println("the link id from database " + rs.getString("id") + " ,compared with the counter " + i);
            String l_a = (String)link_adj.get(i);
            
            if(l_a!=null){
                //P.println(" the link_adj WITH ID " + i + " IS  " + l_a);
            }
            
            //link adj is converted to src and dest
            int[] src0_dest1 = change_string_to_intArray(l_a);
            
            
            //get the resoruces from the database
            HashMap<String, String> Link_Resources_hash = new HashMap();
            int dimension_size;

            String type = rs.getString("Type");
            
            if(Debug)
            P.println(" \t link type  :: " + type);
            dimension_size = (int) link_types_hash.get(type);

            
            //after gettingthe dimension of resrouces, insert them in the hash
            for(int j = 0; j < dimension_size ; j++)
            {
                 //note: in data base, Ds start from 0, unlike the excel
                 Link_Resources_hash.put("D"+j, rs.getString("D"+j));
                 if(Debug)
                 P.println(" link resouces : D" + j+ "  = " + rs.getString("D"+j));
            }
            //P.println(" \nin the link creating list in inoutreader" + Resources_hash.toString());
            
            //here I make an iterative resrouce object for each link
            res_inf_link = C.convert_hash2List_int(Link_Resources_hash);
            //ParentClass noderes_obj = new ParentClass(res_inf);
            List<String> res_inf_link_str = C.convert_hash2List_str(Link_Resources_hash);
            Parent linkres_obj = new Parent(Debug, res_inf_link_str);
            //lr_obj.printResHash();
            //lr_obj.printLayers_List_all_layered_resources();
            //lr_obj.getm_Agg_layers_resources_demonstrator();
            if(Debug)
            linkres_obj.getm_detailed_resources_availabiilties_demonstrator();
            //P.println(" link resouces tree  size " + noderes_obj.getmeResHash().size());

            
            //make the link
            Link l = new Link(Debug, Num_general_link_info,max_link_dimension, i,
                    rs.getString("TYPE"),rs.getString("COST"),rs.getString("Delay"),
                    l_a, src0_dest1[0], src0_dest1[1],Link_Resources_hash,
                    linkres_obj);
            //l.get_ParentClass_resrouces().explainHeirarchicalResoruces();
            //l.get_ParentClass_resrouces().get_me_heirarcical_reshash_size();
            //P.println(" links id =   " + l.getmID());
            links.add(l);

        }
        return links;
        
        
        

    }

    
    //I need this function to convert the string of link adj to src and dest array
    public int[] change_string_to_intArray(String TBC)
    {
        
        //TBC = TBC.replace(":"," "); 
        String[] items =TBC.split(":");
        int arr[] = new int[items.length];
        for (int i = 0; i < items.length; i ++)
        {
            arr[i] = Integer.parseInt(items[i]);
        }
        
        if(Debug)
        P.print_1D(" converted '"+ TBC +"' to array", arr);
        
        return arr;
    }
    
    public HashMap get_node_types_hash()
    {
        return this.node_types_hash;
    }
    public HashMap get_link_types_hash()
    {
        return this.link_types_hash;
    }
    public HashMap get_link_adj_hash()
    {
        return this.link_adj;
    }
    public List get_res_inf_link()
    {
        return this.res_inf_link;
    }
    public List get_res_inf_node()
    {
        return this.res_inf_node;
    }

}
