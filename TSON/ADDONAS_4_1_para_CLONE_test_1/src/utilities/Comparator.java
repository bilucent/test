package utilities;

/**
 * Objects that need to be compared, need to derive
 * from this class.
 * 
 * @author Joan Triay
 *
 */
public class Comparator {
	/** The value used to compare objects. */
	public double weight;
}
