/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package Record;

import clone.Clone;
import com.rits.cloning.Cloner;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import org.omg.CORBA.Request;
import routing.Route;
import utilities.Converters;
import utilities.Print;

/**
 *
 * @author Bijan
 */
public class Link_Rec_Obj {
    //these probably be used for tracking inter layer allocations...
    private String Link_id;
    private Route Route;
    
    //private Request Req;
    private String req_type;
    private int Id;
    private int InputTime;
    private int Duration;
    private int StopTime;
    
    //to speed up look up and deletions
    private List<Integer> Cell_positions;
    private int Depth_of_alloc =  0;
    //HashMap<String, int[]> Res_allocated;
    private List<List> Alloc_linkres_family;
    
    public Link_Rec_Obj(String req_type, int id, int start, int hold,
            List<List> alloc_res_, int depth_of_allocation, Route route,String link_id, List cell_position){
        this.Link_id =  link_id;
        this.Route = route;
        this.req_type = req_type;
        this.Id = id;
        this.InputTime = start;
        this.Duration = hold;
        this.StopTime = start+hold;
        //this.Res_allocated = res_allocation;
        this.Alloc_linkres_family = alloc_res_;
        this.Depth_of_alloc = depth_of_allocation;
        
        this.Cell_positions = cell_position;
    }
    public Link_Rec_Obj(Link_Rec_Obj copy){
        Cloner cloner = new Cloner();
        this.Link_id =  cloner.deepClone(copy.Link_id);
        this.Route = (Route) copy.Route.clone();
        this.req_type = cloner.deepClone(copy.req_type);
        this.Id = cloner.deepClone(copy.Id);
        this.InputTime =  cloner.deepClone(copy.InputTime);
        this.Duration =  cloner.deepClone(copy.Duration);
        this.StopTime = this.InputTime+this.Duration;
        //this.Res_allocated = res_allocation;

        
        //this block makes a clone of list of lists
//        this.Alloc_linkres_family =new ArrayList<>();
////        for(List l: copy.Alloc_linkres_family)
////        {
////            this.Alloc_linkres_family.add(new ArrayList(l));
////        }
//        for(List l: copy.Alloc_linkres_family)
//        {
//            this.Alloc_linkres_family.add(cloner.deepClone(l));
//        }
   
          this.Alloc_linkres_family = Clone.CloneListList(copy.Alloc_linkres_family);
        
        
//        System.out.println(" cloning the list of list ion node rec");
//        System.out.println(" original: " + copy.return_linkres_req_type().toString());
//        System.out.println(" cloned: " +  Alloc_linkres_family.toString());
        
        this.Depth_of_alloc = cloner.deepClone(copy.Depth_of_alloc);
        
        this.Cell_positions = cloner.deepClone(copy.Cell_positions);
        //this.Res_allocated = res_allocation;

    }
    
//    public HashMap<String, int[]> return_allocresrouces()
//    {
//        return this.Res_allocated;
//    }
   
    public List<Integer> get_link_cell_positions()
    {
        return this.Cell_positions;
    }
    public int return_linkres_stop_time()
    {
        return this.StopTime;
    }    
    public int return_linkres_id()
    {
        return this.Id;
    }
    
    public String return_linkres_req_type()
    {
        return this.req_type;
    }       
    public int return_linkres_req_depth()
    {
        return this.Depth_of_alloc;
    }           
    
    public List<List> return_linkres_allocresrouces_family()
    {
        return this.Alloc_linkres_family;
    }    
    public void display()
    {
        Print.println("\nreq_type   " + req_type);
        Print.println("Id         " + Id);
        Print.println("InputTime  " + InputTime);
        Print.println("Duration   " + Duration);        
        Print.println("StopTime   " + StopTime);
        Print.println("Resources in tree   " + Alloc_linkres_family.toString()+ " \n");
//        String[] keys = Converters.key_set_obj_to_str_array(Res_allocated);
//        for(int i = 0; i <keys.length; i++ )
//        {
//            Print.print_1D("ARRAY?   " , Res_allocated.get(keys[i]));
//        }
        
    }
    
}
