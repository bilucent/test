/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package Infrastructure;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Set;
import routing.Route;
import org.apache.commons.lang3.*;
import utilities.Converters;
import utilities.Maths;
import utilities.Print;

/**
 *
 * @author Bijan
 * this class has been generated to play with the resources in links and nodes
 */
public class ResourceOperations {
    
    Network mNetwork;
    HashMap Link_Adg;
    static boolean Debug;
    Print P = new Print();
    Converters C = new Converters();
   
    public ResourceOperations(boolean debug,Network network, HashMap link_adg)
    {
        this.mNetwork = network;
        this.Link_Adg = link_adg;
        this.Debug = debug;
    }

    /*
     *  the method returns the presumably the BW made of the finest granularity
     * this wont work since some l ink might have differnet number of resrouce 
     * layers
     */
    public int route_BW_availablity(Route route, int level_of_detail)
    {
        int BW = -1;
        List links = mNetwork.getmListLinks();
        List<String> hops = route.return_hops_in_route();     

        for(int i  = 0; i < hops.size();i++)
        {
            int link_number = (int) Link_Adg.get(hops.get(i));
            Link link = (Link) links.get(link_number);
            ArrayList avail_across_layers =  link_BW_availablity(link);
            if(i == 0){
                BW = (int) avail_across_layers.get(i);
            }
            else
            {
                BW = BW * (int) avail_across_layers.get(i);
            }
            if(Debug)
            P.println(" BW in layer: " + i);
        }
        if(Debug)
        P.println(" final aggregate bandwidth " + BW);
        return BW;
    }

    /*
     * this method returns a list of all the zeros per each layer
     * this can be useful later on
     */
    public ArrayList link_BW_availablity(Link l)
    {
        ArrayList<Integer> avail_across_layers = new ArrayList();
        HashMap res = l.get_resrouces();
        
        for(int i =0; i <res.size(); i++ )
        {
            String str_resource = (String) res.get(i);
            int bw = count_zeros(str_resource);
            avail_across_layers.add(bw);
        }
        
        return avail_across_layers;
    }
    
    /*
     * this method returns the number of zeros in a string. perfect method
     * for calculating the remaining bandwidth
     */
    public static int count_zeros(String str)
    {
        int count = StringUtils.countMatches(str, "0");
        return count;
    }
    /*
     * this method returns the number of zeros in a string. perfect method
     * for calculating the remaining bandwidth
     */
//    public static int count_zeros(String str)
//    {
//        int counter = 0;
//        String[] arr_str = str.split("");
//        if(Debug)
//        Print.print_1D(" string : " +  str +
//                "  in count zeros with size + " + arr_str.length, arr_str);
//        for(int i = 0; i < arr_str.length; i++)
//        {
//            if(arr_str[i].matches("0"))
//            {
//                counter++;
//            }
//        }
//        return counter;
//    }
    /*
     * this method returns the number of ones in a string. perfect method
     * for calculating the used bandwidth/resources
     */
    public static int count_ones(String str)
    {
        int count = StringUtils.countMatches(str, "1");
        return count;
    }    
    /*
     * this method returns the number of ones in a string. perfect method
     * for calculating the used bandwidth/resources
     */
//    public static int count_ones(String str)
//    {
//        int counter = 0;
//        String[] arr_str = str.split("");
//        //Print.print_1D(" string in count zeros ", arr_str);
//        for(int i = 0; i < arr_str.length; i++)
//        {
//            if(arr_str[i].matches("1"))
//            {
//                counter++;
//            }
//        }
//        return counter;
//    }     
    public int dimensions_availablity()
    {
        int BW = -1;
        
        
        return BW;
    }
    
    /*
     * Just get the max number of layers available along the route
     */
    public int find_max_link_layers_of_resrouces_on_the_route(List links, List hops)
    {
        int[] res_sizes_per_link = new int [links.size()];
        for(int i  = 0; i < hops.size();i++)
        {
            int link_number = (int) Link_Adg.get(hops.get(i));
            Link link = (Link) links.get(link_number);
            int size = link.get_resrouces().size();
            res_sizes_per_link[i] = size;
        }
      
        return Maths.find_max(res_sizes_per_link);
    }
      
    /*Check ou t later: 1- how layers are added, if there is no such layer in
     * some links, you should get the exception
     * 
     * this class enables allocating resrouces in a synchronous mode
     * across all the hops.
     * for this purpose, the resources of multiple layers, in multiple
     * hops sjhould be added up 
     * 
     * 
     * the level of details, determines how deep we want to operate, based on 
     * the request. for example, if 3 layers is wanted, this is the maximum 
     * layer we are going to investigate the availabilities. now a question,
     * what happens if links with less and more layers of resoruces are in the 
     * route???
     * 
     */
//
//    public List<int[]> route_link_resource_combiner(Route route, 
//            int max_requested_layer_for_allocation,List Delays_perhop, List Delay_if_apply)
//    {       
//        List classified_resrouces = new ArrayList();
//        List collected_resources = new ArrayList();
//        List links = mNetwork.getmListLinks();
//        List<String> hops = route.return_hops_in_route();
//
//        //first make a list, of all the hops resrouces
//        //it is a list of hashmaps
//        collected_resources = res_collector(links,hops);
//        
//        //find how many layers of resrouces are actually available
//        int max_layer_avilable = find_max_link_layers_of_resrouces_on_the_route(links,hops);
//
//        if(Debug)
//        {
//            P.println(" % ResourceOperation%\t max_layer_avilable " + max_layer_avilable);
//            P.println(" % ResourceOperation%\t max_requested_layer_for_allocation " 
//                    + max_requested_layer_for_allocation);
//        }
//        
//        //now, aggregate all the same layers of all the available 
//        //resource hashmaps. so now, I have alist of the same layer lists
//        // it is kind of trans of previous matrix lets say
//        
//        int[] two_level_of_delays = {max_layer_avilable,
//            max_requested_layer_for_allocation};
//        classified_resrouces =res_classifier(collected_resources, 
//                Maths.find_min(two_level_of_delays));
//        
//        if(Debug){
//            P.println(" % ResourceOperation%\t Caution Classified_resrouces.size() " + classified_resrouces.size());
//            P.println(" % ResourceOperation%\t Delay_if_apply.size() " + Delay_if_apply.size());
//            P.println(" % ResourceOperation%\t Delays_perhop.size() " + Delays_perhop.size());
//        }
//        //here, I will add up all the resrouces in each layer, to have a sum for
//        // each layer. this infomation is then fed in to the algorithm,
//        // and wil lbe treated as one.
//        List<int[]> res_arrays = res_combiner(classified_resrouces, 
//                Delays_perhop, Delay_if_apply);
//        return res_arrays;
//    }    
//    
    /* a list of hash map: links are added to the list with hash maps of reousces
     * here I collect all the resrouces across all the route
     * hops.
     */
//    private List<HashMap> res_collector(List links, List hops)
//    {
//        
//        List<HashMap> resoruces_per_hop = new ArrayList();
//        //P.println(" Caution hops.size() = " +hops.size());
//        for(int i  = 0; i < hops.size();i++)
//        {
//            int link_number = (int) Link_Adg.get(hops.get(i));
//            Link link = (Link) links.get(link_number);
//            HashMap res_hash = link.get_resrouces();
//            if(Debug){
//                P.println(" link.getmID() " +link.getmID());
//                P.println(" res_hash.size() " +res_hash.size());
//                P.println(" res_hash print " +res_hash);
//            }
//            resoruces_per_hop.add(res_hash);
//        }
//        if(Debug)
//        P.println(" % ResourceOperation%\t Important: "
//                + "How many HOPS are on the route???" + resoruces_per_hop.size());        
//        return resoruces_per_hop;
//    }
//    
    /* gets a list of hashes, gives back a list of lists
     * =>resroucess per hops of routs in the list, are converted to layered lists on lists.
     * something like a transpose of link resrouce matrices
     * this class process the list of hash resrouces, and gathers all
     * the resrouces of identical layer id in same lists
     */
//    private List res_classifier(List<HashMap> HopList_ResHash, int max_layer)
//    {        
//        List<List> res_per_layers = new ArrayList();
//
//        //get a string of the hop res
//        // her eI mention maximum layer of intended alocation,
//        //because, come links light not have it
//
//        // this for is for the max layer. however I wil take care of the 
//        //hops without that layer
//        
//        //so, the first member of the list is all layer D0 , second all layer D1 and so on...
//        for(int i  = 0; i < max_layer;i++)
//        {
//            List layer_res = new ArrayList();
//            //so in layer i, get the resoruces of all the lins/hops
//            for(int j  = 0; j < HopList_ResHash.size();j++)
//            {
//                // this resrouces per hop
//                HashMap hop_res =  HopList_ResHash.get(j);
//                //P.println("  hop: " + j +"  layer "+  i +"  res is: " + hop_res.get("D"+i));
//                
//                //here, I try to modify to access to layers if there is any of i level
//                if(!(i>hop_res.size()))
//                {
//                    boolean add = false;
//                    try{
//                        //this is a lyaer of the resrouces from a hop
//                        add = layer_res.add(hop_res.get("D"+i));
//                    }catch(NullPointerException e)
//                    {
//                        System.exit(234);
//                        //P.print("  the layers of resource does not exist i = " + i + add);
//                    }
//                }
//                else {
//                    if(Debug)
//                    P.print("  the requested layer of the resource does "
//                            + "not exist i = " + i + "  hop :" + j);
//                }
//            }
//            // after each layer voyeur across layers, I add the resultd list to 
//            //the mother list
//            // I have added this bit, in case all the routes did not support
//            //the layer, there wont be any memebr of the list.
//            if(layer_res!= null)
//            {
//            res_per_layers.add(layer_res);
//            }
//        }
//        if(Debug){    
//            //This is important information, SO I can 
//            //see how many layers existed on a route
//            P.println(" Important: How many layers are on the route???" + res_per_layers.size());        
//            P.println(" res_per_layers size " + res_per_layers.size() + " =?  max_layer = " + max_layer);
//        }
//        return res_per_layers;
//    }   
    
    /* this method, converts the list of list, tp a list, of aggregate resources
     * in this bit I combine all the hop/layer/ resoruces in to one 
     * array set
     */
//    private List<int[]> res_combiner(List<List> layerd_resorucesof_hops,
//            List Delays_per_hop, List<Boolean> Delays_if_apply)
//    {
//        //final output of this method
//        List<int[]> res_per_layers = new ArrayList();
//
//        //Here I should actually get the number of layers...
//        for(int i  = 0; i < layerd_resorucesof_hops.size();i++)
//        {
//            List layer_res = layerd_resorucesof_hops.get(i);
//            if(Debug)
//            P.println(" Important: number of hops is here believe me "+ layer_res.size());
//            
//            //all the res objects, are turned in to array and added up here
//            int[] differentiate = null;
//            
//            //here, in each list of same id layers, I add them up
//            for(int j  = 0; j < layer_res.size();j++)
//            {
//                
//                // this layers resrouces
//                String res_str = (String) layer_res.get(j);
//                int[] res_arr =  C.convert_string_to_1Darray(res_str);
//                if(Debug)
//                P.println(" Important:  The resrouce length "+ 
//                        res_arr.length + "\tof hop: " + j + "\tin layer: " + i);
//                
//                
//                
//                /*differentiate = new int[res_arr.length];in this bit, I have to cosider the effect of delays
//                if I have one like, or equivalently no delay to be considered
//                I have to skip this bit
//                * 
//                * Now, if one hop, so basicallly no delays are there.
//                */
//                int tempdelay = 0;
//                if(j==0)
//                {
//                    
//                    /*
//                     * I have brought the initialisation here, rather than out side.
//                     * since I want to see if ther are no resources, I wont add it 
//                    */
//                    differentiate = new int[res_arr.length];
//                    for(int z = 0; z < res_arr.length; z++ )
//                    {
//                        differentiate[z] = res_arr[z];
//                    }
//                    if(Debug)
//                    P.println(" differentiate.length: "+ differentiate.length); 
//                    
//                    //no delay
//                    tempdelay = 0;
//                }
//                
//                /*
//                 * if the hops are more than one, I have to consider the links delays, 
//                 * and the delay is cused with the preceding one--> j-1
//                 */
//                else if(j>0)
//                {
//                    tempdelay = (int) Delays_per_hop.get(j-1);
//                    
//                    /*
//                     * Delays_if_apply is consiered for maximum number of layers, 
//                     * so might not necessarily apply to all the layers.
//                     * 
//                     * note, delays if apply is per layer, so it goes with i,
//                     * while delay per hop is per hop, this means, these two are
//                     * not neccessarily of the same length
//                     * 
//                     */
//                    if(!Delays_if_apply.get(i))
//                    {
//                        tempdelay = 0;
//                    }  
//                    differentiate = combine_arrays(differentiate,res_arr,tempdelay);                 
//                }
//
//            }
//            if(differentiate!= null){
//                res_per_layers.add(differentiate);
//            }
//            else{
//                    P.println(" differentiate array is nulll... "); 
//                    System.exit(963);
//            }
//            
//       
//        }
//        /*
//         * here, the aggregation is finished, what I have, is a list, of aggregated
//         * resources of differet layers. So the size of the list gives me the 
//         * number of layers, but I can no longer extract the number of hops from this list
//         */
//        if(Debug)
//        P.println(" number of  layers : "+res_per_layers.size()); 
//
//        if(Debug)
//        for(int i =0; i <res_per_layers.size(); i++ )
//        {
//            res_per_layers.get(i);
//                    P.println(" Important: check each layers resource length: "+ 
//                        res_per_layers.get(i).length);               
//        
//        }
//        return res_per_layers;
//    } 
    
    /*
     * this class is used to add two arrays, considering the time differnce 
     * between them
     */
    public static int[] combine_arrays(int[] first, int [] second, int delay)
    {
        int combined[] = first;
        if(first.length != second.length)
        {
            Print.println("  Problem un equal arrays first array length: " 
                    + first.length  + " &&  second array length " + second.length);
            System.exit(5346);
        }
        int i =0;
        while((i+delay)<first.length)
        {
            if(second[i+delay]==1 || first[i] == 1)
            {
                combined[i] = 1;
            }
            i++;
        }
        return combined;
    }
    
    /*
     * this class is used to subtract two arrays, considering the time differnce 
     * between them
     */
    public static int[] differntiate_arrays(int[] first, int [] second, int delay)
    {
        int combined[] = new int[first.length];
        combined = first;
        if(first.length != second.length)
        {
            Print.println("  Problem un equal arrays first array length: " 
                    + first.length  + " &&  second array length " + second.length);
            System.exit(5346);
        }
        int i =0;
        while((i+delay)<first.length)
        {
            if(second[i+delay]==1 && first[i] == 1)
            {
                combined[i] = 0;
            }
            else if(second[i+delay]==1 && first[i] == 0)
            {
                if(Debug)
                Print.println("  !!!!!!!!!!!!!!!!!!!warning in resoperation "
                        + "subtracting the arrays, no 1 to be set to 0 ...." );
            }
                
            i++;
        }
        return combined;
    }
         
     /*
      * remove an allocation, called from anywhere, this method
      * will subtract the given arrays from the arg list from the existing ones,
      * and replaces it.
      */
     public static boolean removeTheAllocation_resFamily_link(Link l,
             List<List> remove_allocations_across_layers)
     {

         List<List> existing_resources = l.get_ParentClass_resrouces().getme_List_all_layered_resources();
         List<List> com_link_resources = new ArrayList<>();
         if(Debug)
         {
            Print.println(" in removal existing_resources  " + existing_resources.toString());
            Print.println(" in removal new_linkResources  " + remove_allocations_across_layers.toString());
         }
         // go through the layers
                 for(int i = 0; i < remove_allocations_across_layers.size(); i++ )
                 {
                     List<String> ex_layers_list = existing_resources.get(i);
                     List<String> new_layers_list = remove_allocations_across_layers.get(i);
                     List<String> com_layer_resources = new ArrayList<>();
                     for(int j = 0; j < ex_layers_list.size(); j++ )
                     {
                         String ex_cell_str = ex_layers_list.get(j);
                         String new_cell_str = new_layers_list.get(j);
                         int [] ex_cell_arr = Converters.convert_string_to_1Darray(ex_cell_str);
                         int [] new_cell_arr = Converters.convert_string_to_1Darray(new_cell_str);
                         int delay = 0;
                         int []combined  = ResourceOperations.
                                 differntiate_arrays(ex_cell_arr, new_cell_arr, delay);
//                         Print.print_1D("  the ex_cell_arr in combining procedure to checked out ", ex_cell_arr);
//                         Print.print_1D("  the new_cell_arr in combining procedure to checked out ", new_cell_arr);
                         
                         String com = Converters.convert_array_to_string(combined);
                         com_layer_resources.add(com);
                     }
                     com_link_resources.add(com_layer_resources);
                 }
                 l.get_ParentClass_resrouces().set_List_all_layered_resources(com_link_resources);
                 //Print.println(" in removal com_node_resources  " + com_node_resources.toString());
                // Print.println(" in removal link resources  " + l.get_ParentClass_resrouces().getm_detailed_resources_().toString());
                 
        return true;
    
     } 
     
     /* The updated version, with looking only at the layers and cells to be deleted, not the whole thing
      * 
      * remove an allocation, called from anywhere, this method
      * will subtract the given arrays from the arg list from the existing ones,
      * and replaces it.
      */
     public static boolean removeTheAllocation_resFamily_link_2(Link l,
             List<List> remove_allocations_across_layers, int layer, List<Integer> cells)
     {

         List<List> existing_resources = l.get_ParentClass_resrouces().getme_List_all_layered_resources();
         List<List> com_link_resources = new ArrayList<>();
         com_link_resources = existing_resources;
         if(Debug)
         {
            Print.println(" in removal existing_resources  " + existing_resources.toString());
            Print.println(" in removal new_linkResources  " + remove_allocations_across_layers.toString());
         }
         // go through the layers
                 //for(int i = 0; i < remove_allocations_across_layers.size(); i++ )
                 {
                     List<String> ex_layers_list = existing_resources.get(layer);
                     List<String> new_layers_list = remove_allocations_across_layers.get(layer);
                     if(ex_layers_list.size()!= new_layers_list.size())
                     {
                         Print.println("  un equaly layers from link and to be deleted in rec");
                         System.exit(2761);
                     }
                     List<String> com_layer_resources = new ArrayList<>();
                     com_layer_resources = ex_layers_list;
                     for(int j = 0; j < cells.size(); j++ )
                     {
                         String ex_cell_str = ex_layers_list.get(cells.get(j));
                         String new_cell_str = new_layers_list.get(cells.get(j));
                         int [] ex_cell_arr = Converters.convert_string_to_1Darray(ex_cell_str);
                         int [] new_cell_arr = Converters.convert_string_to_1Darray(new_cell_str);
                         int delay = 0;
                         int []differentiate  = ResourceOperations.
                                 differntiate_arrays(ex_cell_arr, new_cell_arr, delay);
//                         Print.print_1D("  the ex_cell_arr in combining procedure to checked out ", ex_cell_arr);
//                         Print.print_1D("  the new_cell_arr in combining procedure to checked out ", new_cell_arr);
                         
                         String com = Converters.convert_array_to_string(differentiate);
                         
                         com_layer_resources.remove(j);
                         com_layer_resources.add(j,com);
                     }
                     com_link_resources.remove(layer);
                     com_link_resources.add(layer,com_layer_resources);
                 }
                 l.get_ParentClass_resrouces().set_List_all_layered_resources(com_link_resources);
                 //Print.println(" in removal com_node_resources  " + com_node_resources.toString());
                 if(Debug)
                 Print.println(" after removal, link resources  " + l.get_ParentClass_resrouces().getm_detailed_resources_().toString());
                 
        return true;
    
     }      
     
 /*
      * remove an allocation, called from anywhere, this method
      * will subtract the given arrays from the arg list from the existing ones,
      * and replaces it.
      */
     public static boolean removeTheAllocation_resFamily_node(Node n,
             List<List> remove_allocations_across_layers)
     {

         List<List> existing_resources = n.get_ParentClass_resrouces().getme_List_all_layered_resources();
         List<List> com_node_resources = new ArrayList<>();
         if(Debug)
         {
            Print.println(" in removal existing_resources  " + existing_resources.toString());
            Print.println(" in removal new_linkResources  " + remove_allocations_across_layers.toString());
         }
         // go through the layers
                 for(int i = 0; i < remove_allocations_across_layers.size(); i++ )
                 {
                     List<String> ex_layers_list = existing_resources.get(i);
                     List<String> new_layers_list = remove_allocations_across_layers.get(i);
                     List<String> com_layer_resources = new ArrayList<>();
                     for(int j = 0; j < ex_layers_list.size(); j++ )
                     {
                         String ex_cell_str = ex_layers_list.get(j);
                         String new_cell_str = new_layers_list.get(j);
                         int [] ex_cell_arr = Converters.convert_string_to_1Darray(ex_cell_str);
                         int [] new_cell_arr = Converters.convert_string_to_1Darray(new_cell_str);
                         int delay = 0;
                         int []combined  = ResourceOperations.
                                 differntiate_arrays(ex_cell_arr, new_cell_arr, delay);
//                         Print.print_1D("  the ex_cell_arr in combining procedure to checked out ", ex_cell_arr);
//                         Print.print_1D("  the new_cell_arr in combining procedure to checked out ", new_cell_arr);
                         
                         String com = Converters.convert_array_to_string(combined);
                         com_layer_resources.add(com);
                     }
                     com_node_resources.add(com_layer_resources);
                 }
                 n.get_ParentClass_resrouces().set_List_all_layered_resources(com_node_resources);
                 //Print.println(" in removal com_node_resources  " + com_node_resources.toString());
                 if(Debug)
                 Print.println(" in removal link resources  " + n.get_ParentClass_resrouces().getm_detailed_resources_().toString());
                 
        return true;
    
     }         
     /*
      * remove an allocation, called from anywhere, this method
      * will subtract the given arrays from the arg list from the existing ones,
      * and replaces it.
      */
//     public static boolean removeTheAllocation(Link link,
//             HashMap<String,int[]> remove_allocations_across_layers)
//     {
//        Converters c = new Converters();
//
//            //P.println(" link delay: " + delay);
//            HashMap<String,String> resources = link.get_resrouces();
//            //P.println(" link resources size: " + resources.size());
//            
//            HashMap<String,String> linkResources = resources;
// 
//            //now process differnet layers
//            for(int u =0; u<remove_allocations_across_layers.size();u++)
//            {
//                Print.println("  \tRemoval in link :" + link.getmID() +"  \t\tRemoval in layer   :" + u);
//                //this is the array to be removed
//                int [] remove_res= remove_allocations_across_layers.get("D"+u);
//                String new_res = c.convert_array_to_string(remove_res);
//                
//                //this is what was there, 
//                String old_res = resources.get("D"+u);                
//                int [] existing_alloc = c.convert_string_to_1Darray(old_res);
//                
//                //here is the real operation, on taking out the expired connection
//                int[] allocted_subtractedfrom_previous = 
//                        allocated_delFrom_existing(existing_alloc, remove_res);           
//                //this one replaces the previous link resource
//                String final_res = 
//                        c.convert_array_to_string(allocted_subtractedfrom_previous);
//                
//                /*
//                Print.println(" \tDeleted: Link old_res---> existing:          " + old_res);
//                Print.println(" \tDeleted: Link res---> To be deleted:               " + new_res);
//                Print.println(" \tDeleted: Link final_res---> FINAL:         " + final_res);
//                */ 
//                linkResources.put("D"+u, final_res);
//                
//            }
//            link.set_resrouces(linkResources);
//        
//       return true;
//     } 
     
     public static int [] allocated_delFrom_existing(int [] existing_array, int [] new_array)
     {
         if(Debug){
            Print.println("  Here, the resrouce replacement per layer happens");
            Print.print_1D("\n existing array:          " , existing_array);
            Print.print_1D(" array To be deleted:               " , new_array);
         }
         //int [] final_array = existing_array;
         int i = 0;
         while((i )<existing_array.length)
         {
            if(new_array[i]>0){
                
                if(existing_array[i]==0){
                    Print.println(" !!!!!! warning no delete, the slot"
                            + " was not allocated before!!!! --> i is: "+ i);
                }
                existing_array[i] = 0;
            }
            i++;  
         }
         if(Debug)
         Print.print_1D(" updated array after deletion          " , existing_array);

         return existing_array;
     }    

     public static String create_resource_string_zeroes(int size)
     {
         if(Debug)
         Print.println(" %resourceOperations% create_resource_string with size: " + size);
         String res_str = "";
         for (int i = 0; i < size; i ++)
         {
             res_str = res_str + 0;
         }
         return res_str;
     }

     public static String create_resource_string_ones(int size)
     {
         if(Debug)
         Print.println(" %resourceOperations% create_resource_string with size: " + size);
         String res_str = "";
         for (int i = 0; i < size; i ++)
         {
             res_str = res_str + 1;
         }
         return res_str;
     }     
     
    public static List<Integer> count_ones_in_res_family(List<List>  ex_resources)
    {

        List<Integer> utilised_in_layers = new ArrayList<>();

        if(Debug)
        {
            Print.println(" in removal existing_resources  " + ex_resources.toString());
        }
        // go through the layers
        for(int i = 0; i < ex_resources.size(); i++ )
        {
            List<String> ex_layers_list = ex_resources.get(i);
            String layer_in_str = list_to_string(ex_layers_list);
            int ones = count_ones(layer_in_str);
            utilised_in_layers.add(ones);
        } 
        return utilised_in_layers;
         
    }
    
    /*
     * optimised methods
     */
    public static String list_to_string(List<String> list)
    {
        Object[] objectArray = list.toArray();
        String[] stringArray = Arrays.copyOf(objectArray, objectArray.length, String[].class);
        
        StringBuilder builder = new StringBuilder();
        for (String s : stringArray) {
            builder.append(s);
        }
        return builder.toString();
    }     
    public static String strArr_to_string(String[] arr)
    {

        StringBuilder builder = new StringBuilder();
        for (String s : arr) {
            builder.append(s);
        }
        return builder.toString();
    }  
    public static String intArr_to_string(int[] arr)
    {

        StringBuilder builder = new StringBuilder();
        for (int s : arr) {
            builder.append(""+s);
        }
        return builder.toString();
    }    
    
//    public static String list_to_string(List<String> list)
//    {
//        
//        String all = "";
//        for(int i = 0; i < list.size(); i++)
//        {
//            all = all + list.get(i);
//        }        
//        
//        
//        return all;
//    }     
    
    public static int aggre_listInts_to_oneInt(List<Integer> list)
    {
        int count_all =0;
        for(int i = 0; i < list.size(); i++)
        {
            count_all += list.get(i);
        }
        return count_all;
    }
    
    public static int[] return_rec_keys_int(HashMap hm)
    {
            Set<Integer> keySet = hm.keySet();
            Object[] toArray = keySet.toArray();
            int [] intarr = new int [toArray.length];
            for (int i = 0; i < intarr.length; i++){
                intarr[i] = (int) toArray[i];
            }
            return intarr;
        
    }     
    public static String[] return_rec_keys_str(HashMap hm)
    {
            Set<String> keySet = hm.keySet();
            Object[] objectArray = keySet.toArray();
            String[] stringArray = Arrays.copyOf(objectArray, objectArray.length, String[].class);

            return stringArray;
        
    }       
    
    public static List<Integer> generate_list_zeros(int size)
    {
        List<Integer> l = new ArrayList<>();
        for(int i = 0 ; i < size; i ++)
        {
            l.add(0);
        }
        return l;
    }
}
