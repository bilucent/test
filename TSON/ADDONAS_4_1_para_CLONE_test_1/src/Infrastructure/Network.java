package Infrastructure;

import com.rits.cloning.Cloner;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import org.jgrapht.DirectedGraph;
import org.jgrapht.graph.DefaultDirectedGraph;
import org.jgrapht.graph.DefaultEdge;
import routing.Route;
import utilities.Converters;
import utilities.Maths;

import utilities.Matrix;
import utilities.Print;

/**
 * The Network class allows the definition of the
 * network topology and keeps in memory the nodes
 * and links that belong and conform to such network.
 * 
 * @author Joan Triay
 *
 */
public class Network implements Cloneable{
	/** The number of wavelengths per link. */
	private int mNumLambdas;
	/** List of nodes on the network. */
	private List<Node> mListNodes;
	/** List of links on the network. */
	private List<Link> mListLinks;
	/** List of edges (same as links) on the network. */
	private List<DefaultEdge> mListEdges;
	/** Simple connectivity matrix. */
	private Matrix mNetMatrix;
	/** The graph. */
	protected DirectedGraph<Node, DefaultEdge> mNetworkGraph;
//	protected DirectedGraph<Node, DefaultEdge> mNetworkGraph_flex;
//	protected DirectedGraph<Node, DefaultEdge> mNetworkGraph_grid;
        
        //this hashamp, gives the link id back, using the adjacency string
        private HashMap<String,Integer> Links_str2num_hash;
        
        private HashMap<Integer,String> Links_num2str_hash;
        
        private List<Integer> nodes_ids;
        
        private List<Integer> links_ids;
               
        Print P;

        HashMap<Integer, String> Link_adj_hash;
        HashMap<String, Integer> Node_types_dimension_hash;
        HashMap<String, Integer> Link_types_dimension_hash;
	
	/**
	 * Public constructor.
	 * @param numNodes The number of nodes on the network.
	 * @param numLinks The number of links on the network.
	 * @param numLambdas The number of wavelengths per link.
	 */

        //this is my new constructure, since I want to pass node and linklist all together
	public Network(List Nodes, List Links, HashMap nodes_types_hash,
                HashMap links_types_hash, HashMap link_adj_hash){
		this.mListLinks = Links;
		this.mListNodes = Nodes;
		this.mNetMatrix = new Matrix(Nodes.size(), Links.size());
		this.mNetworkGraph = new DefaultDirectedGraph<Node, DefaultEdge>(DefaultEdge.class);
//		this.mNetworkGraph_flex = new DefaultDirectedGraph<Node, DefaultEdge>(DefaultEdge.class);
//		this.mNetworkGraph_grid = new DefaultDirectedGraph<Node, DefaultEdge>(DefaultEdge.class);
                this.Links_str2num_hash = new HashMap();
                this.Links_num2str_hash = new HashMap();
                
                P = new Print(); 
                nodes_ids = new ArrayList<>();
                links_ids = new ArrayList<>();
                
                this.Node_types_dimension_hash = nodes_types_hash;
                this.Link_adj_hash = link_adj_hash;
                this.Link_types_dimension_hash = links_types_hash;

                //I just moved it in to see the efect, so no need for external calling
                //createNetworkGraph();
	}
	public Network(Network another){
            
                Cloner cloner=new Cloner();
//		this.mListLinks = Converters.deepcloneLinkList(another.mListLinks);
//		this.mListNodes = Converters.deepcloneNodeList(another.mListNodes);
		this.mListLinks = new ArrayList<>();
                for(Link l :another.mListLinks) this.mListLinks.add(new Link(l));
                
		this.mListNodes = new ArrayList<>();
                for(Node n :another.mListNodes) this.mListNodes.add(new Node(n));
//                P.println("  number of nodes when copying " + mListNodes.size());
//                P.println("  number of links when copying " + mListLinks.size());
//                
		this.mNetMatrix = new Matrix(this.mListNodes.size(), this.mListLinks.size());
                this.mNumLambdas = another.mNumLambdas;
                this.mListEdges = cloner.deepClone(another.mListEdges);
//		this.mNetworkGraph = new DefaultDirectedGraph<Node, DefaultEdge>(DefaultEdge.class);
//                this.Links_str2num_hash = new HashMap();
//                this.Links_num2str_hash = new HashMap();
//                
//                P = new Print(); 
//                nodes_ids = new ArrayList<>();
//                links_ids = new ArrayList<>();
//                
//                this.Node_types_dimension_hash = 
//                        cloner.deepClone(another.Node_types_dimension_hash);
//                this.Link_adj_hash = 
//                        cloner.deepClone(another.Link_adj_hash);
//                this.Link_types_dimension_hash = 
//                        cloner.deepClone(another.Link_types_dimension_hash);
		this.mNetworkGraph = new DefaultDirectedGraph<Node, DefaultEdge>(DefaultEdge.class);
                this.Links_str2num_hash = cloner.deepClone(another.Links_str2num_hash);
                this.Links_num2str_hash = cloner.deepClone(another.Links_num2str_hash);
                
                P = new Print(); 
                this.nodes_ids = another.nodes_ids;
                this.links_ids = another.links_ids;
                
                this.Node_types_dimension_hash =another.Node_types_dimension_hash;
                this.Link_adj_hash = another.Link_adj_hash;
                this.Link_types_dimension_hash = another.Link_types_dimension_hash;

                //I just moved it in to see the efect, so no need for external calling
                //createNetworkGraph();
	}

        //******************************************************whole network operations
	/**
	 * This function is called once all the nodes and links
	 * have been added to their corresponding lists. The function
	 * creates the network graph which can then be used to find
	 * the routes between origins and destinations.
	 */
	public void createNetworkGraph(){
            this.mNetworkGraph = new DefaultDirectedGraph<Node, DefaultEdge>(DefaultEdge.class);
            //note, I have node and links id beginning from 1, messed up but it is how it is
		
//            Print.print(" \nthe number of nodes in the new network " + this.mListNodes.size());
//            Print.print(" \nthe number of links in the new network" + this.mListLinks.size());
            for(int i=0; i< this.mListNodes.size(); i++){
			Node node = this.mListNodes.get(i);
                        
                        this.nodes_ids.add(node.getmID());
                        
			this.mNetworkGraph.addVertex(node);
		}
		
		for(int i=0; i< this.mListLinks.size(); i++){
			Link link = this.mListLinks.get(i);
                        
                        this.links_ids.add(link.getmNumber());
                        
			DefaultEdge _edge = this.mNetworkGraph.addEdge(this.mListNodes
                                .get(link.getmOrigin()), this.mListNodes
                                .get(link.getmDest()));
			link.setmEdge(_edge);
		}
                
                //Print.println("  network graph just created "+mNetworkGraph.toString());
                
                //I added this bit for ease of link selection
                //create_link_hash();
	}
	/**
	 * This function is called once all the nodes and links
	 * have been added to their corresponding lists. The function
	 * creates the network graph which can then be used to find
	 * the routes between origins and destinations.
	 */
//	public DirectedGraph<Node, DefaultEdge> createNetworkGraph_filteredNodes(List<Integer> node_ids){
            
	public void createNetworkGraph_filteredNodesOut(List<Integer> node_ids){
            
            this.mNetworkGraph = new DefaultDirectedGraph<Node, DefaultEdge>(DefaultEdge.class);
            
            List<Link> new_links_list = new ArrayList();
            int[][] new_matrix = new int[mListNodes.size()][mListNodes.size()];
//            Print.print(" \nthe number of nodes in the new network " + mListNodes.size());
//            Print.print(" \nthe number of links in the new network" + mListLinks.size());            
            //DirectedGraph<Node, DefaultEdge> NetworkGraph = new DefaultDirectedGraph<>(DefaultEdge.class);
            //note, I have node and links id beginning from 1, messed up but it is how it is
		for(int i=0; i<mListNodes.size(); i++){
			Node node = mListNodes.get(i);
                        
                        nodes_ids.add(node.getmID());
                        P.println("  ");
			mNetworkGraph.addVertex(node);
		}
		
		for(int i=0; i<mListLinks.size(); i++){
			Link link = mListLinks.get(i);
                        
                        if(!node_ids.contains(link.getmOrigin()) && !node_ids.contains(link.getmDest()) )
                        {
                            
                            new_links_list.add(link);
                            links_ids.add(link.getmNumber());

                            DefaultEdge _edge = mNetworkGraph.addEdge(mListNodes
                                    .get(link.getmOrigin()), mListNodes
                                    .get(link.getmDest()));
                            link.setmEdge(_edge);
                            
                            //set the links in the new matrix
                         
                            new_matrix[link.getmOrigin()][link.getmDest()] = link.getmNumber();
                        }
		}
//                Print.print(" \nthe number of nodes in the new network " + mListNodes.size());
//                Print.print(" \nthe number of links in the new network " + new_links_list.size()+"\n");            
//                Print.print_2D(" The new network matrix:  ", new_matrix);
                //Print.print_1D(" mNetworkGraph_GRAPH  " , mNetworkGraph.vertexSet().toArray());
                
                //return NetworkGraph;
                
                //I added this bit for ease of link selection
                //create_link_hash();
	}
//	/**
//	 * This function is called once all the nodes and links
//	 * have been added to their corresponding lists. The function
//	 * creates the network graph which can then be used to find
//	 * the routes between origins and destinations.
//	 */
////	public DirectedGraph<Node, DefaultEdge> createNetworkGraph_filteredNodes(List<Integer> node_ids){
//            
//	public void createNetworkGraph_filteredNodesOut(List<Integer> node_ids){
//            
//            
//            DirectedGraph<Node, DefaultEdge> NetworkGraph = new DefaultDirectedGraph<>(DefaultEdge.class);
//            //note, I have node and links id beginning from 1, messed up but it is how it is
//		for(int i=0; i<mListNodes.size(); i++){
//			Node node = mListNodes.get(i);
//                        
//                        nodes_ids.add(node.getmID());
//                        
//			NetworkGraph.addVertex(node);
//		}
//		
//		for(int i=0; i<mListLinks.size(); i++){
//			Link link = mListLinks.get(i);
//                        
//                        if(!node_ids.contains(link.getmOrigin()) && !node_ids.contains(link.getmDest()) )
//                        {
//                            links_ids.add(link.getmNumber());
//
//                            DefaultEdge _edge = NetworkGraph.addEdge(mListNodes
//                                    .get(link.getmOrigin()), mListNodes
//                                    .get(link.getmDest()));
//                            link.setmEdge(_edge);
//                        }
//		}
//                //return NetworkGraph;
//                
//                //I added this bit for ease of link selection
//                //create_link_hash();
//	}
        
        /* 
         * This method make s hash, for mutual finsing of link id and link
         * number from each other. This method is run in following of
         * create network. 
         */
	public void create_link_hash()
        {
            
            for(int i = 0; i < this.mListLinks.size(); i++)
            {
               
                Link l = this.mListLinks.get(i);
                String str_id = l.getmID();
                this.Links_str2num_hash.put(str_id,i);
                this.Links_num2str_hash.put(i, str_id);
            }

        }

	public int getmNumLambdas() {
		return mNumLambdas;
	}
        
        /*
         * this method will filter the paths, to the requested
         */
	public void filter_routes(List<Node> nodelist, List<Link> linklist,
                List<String> keep_node_types, List<String> keep_link_types )
        {
            for(int i =0; i < nodelist.size(); i++)
            {
                Node n = nodelist.get(i);
                n.getmRouting();
            }
            
        }
	/**
	 * This method can be used when loading the network
	 * matrix topology and connectivity from other means.
	 * @param index The index of the row in the matrix.
	 * @param row The values of the row.
	 */
	public void addRowMatrix(int index, double row[]){
		this.mNetMatrix.setRow(index, row);
	}
	
	// getters and setters
	public DirectedGraph<Node, DefaultEdge> getmNetworkGraph() {
		return mNetworkGraph;
	}
	        
        //******************************************************link operations
        //get the hash of id--> number
        public HashMap<String, Integer> return_link_str2num_hash_list(){
            return this.Links_str2num_hash;
        }
        
         //get the hash of number--> id
        public HashMap<Integer, String> return_link_num2str_hash_list(){
            return this.Links_num2str_hash;
        }        
        
         //get the specific link number havin the link id
        public int get_link_num_from_id(String str_id){            
            return this.return_link_str2num_hash_list().get(str_id);
        }
        
         //get the specific link id havin the link number        
        public String get_link_id_from_num(int link_num){
            return this.return_link_num2str_hash_list().get(link_num);
        }        

	
	/**
	 * Add a node to the list.
	 * @param node The node to add.
	 */
	public void addNodeToList(Node node){
		this.mListNodes.add(node);
	}
	
	/**
	 * Add a link to the list.
	 * @param link The link to add.
	 */
	public void addLinkToList(Link link){
		this.mListLinks.add(link);
	}

	/**
	 * Get the node using its ID.
	 * @param nodeID The ID of the node to get.
	 * @return The node.
	 */
	public Link getLink(String linkNumber){
		for(int i=0; i<mListLinks.size(); i++){
			Link link = mListLinks.get(i);
			if(link.getmID() == linkNumber)
				return link;
		}
		
		return null;
	}


	public List<Link> getmListLinks() {
		return mListLinks;
	}

	public List<Integer> getmLinkIDS() {
		return links_ids;
	}
	        
        //****************************************************** node operations
        
	/**
	 * This function returns the link using the edge
	 * object used by the network graph.
	 * @param edge The edge to get.
	 * @return The link corresponding to the given edge. If it is
	 * not found, it returns null.
	 */
	public Link getLinkByEdge(DefaultEdge edge){
		for(int i=0; i<mListLinks.size(); i++){
			DefaultEdge _edge = mListLinks.get(i).getmEdge();
			if(edge.equals(_edge))
				return mListLinks.get(i);
		}
		
		return null;
	}
        
	/**
	 * Get the node using its ID.
	 * @param nodeID The ID of the node to get.
	 * @return The node.
	 */
	public Node getNode(int nodeID){
		for(int i=0; i<mListNodes.size(); i++){
			Node node = mListNodes.get(i);
			if(node.getmID() == nodeID)
				return node;
		}
		
		return null;
	}

	public List<Node> getmListNodes() {
		return mListNodes;
	}

	public List<Integer> getmNodesIDs() {
		return nodes_ids;
	}	
        
        public List<Link> return_links_in_route(Route route)
        {
            List<Link> link_list  = new ArrayList();
            List hops = route.return_hops_in_route();
            int links_ids[] = return_linksIDs_in_route(hops);
            for (int i = 0; i < hops.size(); i++){
            
                link_list.add(mListLinks.get(links_ids[i]));
            }
            return link_list;
        }           
        public List<Node> return_nodes_in_route(Route route)
        {
            List<Node> node_list  = new ArrayList();
            int[] node_ids = route.return_nodesIDs_in_route();

            for (int i = 0; i < node_ids.length; i++){
            
                node_list.add(mListNodes.get(node_ids[i]));
            }
            return node_list;
        }           
        
        public int[] return_linksIDs_in_route(List hops)
        {
            int links_ids[] = new int[hops.size()];
            for (int i = 0; i < hops.size(); i++){
            
                int link_number = (int) Links_str2num_hash.get(hops.get(i));
                links_ids[i] = link_number;
            }
            return links_ids;
        }   
        
        /*
         * I will use this method to get all types per link,
         * I will need it for differnet domains and so
         */
        public String[] return_route_links_string_arr(List hops)
        {
            
             
            String str_arr[] = new String[hops.size()];
            for (int i = 0; i < hops.size(); i++){
                int link_number = (int) Links_str2num_hash.get(hops.get(i));
                str_arr[i] =
                        mListLinks.get(link_number).getmType();
            }
            return str_arr;
        }  	
        
   
        
        /*
         * I will use this method to get all types per link,
         * I will need it for differnet domains and so
         */
        public String[] return_route_nodes_string_arr(int path[])
        {
            String str_arr[] = new String[path.length];
            for (int i = 0; i < path.length; i++){
                int node_number = path[i];
                    str_arr[i] = 
                            mListNodes.get(node_number).getmNode_Type();
            }
            return str_arr;
        }          
        
        /*
         * the following methods return the very fundemental
         * node and link types information,
         */
        
    public int return_route_min_link_availabilities(Route route, int layer)
    {
        int min_available = 0;
        List<Link> links = this.return_links_in_route(route);
        
        //Print.println("  \n<<>>rout before processing " + route.toString());
        int links_collect[] = new int[links.size()];
        
        for(int i = 0; i < links.size(); i ++)
        {
            Link l = links.get(i);
            
            //Print.println(" reources on the link for rout processing " + l.get_ParentClass_resrouces().getm_Agg_layers_resources().toString());
            
            links_collect[i] = l.get_ParentClass_resrouces().
                    getm_Agg_layers_resources().get(layer);
            
        }
        
        min_available = Maths.find_min(links_collect);
        return min_available;
    }        
        
    
    
    public HashMap retrun_node_types_dimension_hash()
    {
        return this.Node_types_dimension_hash;
    }
    public HashMap retrun_link_types_dimension_hash()
    {
        return this.Link_types_dimension_hash;
    }
    public HashMap retrun_link_adj_hash()
    {
        return this.Link_adj_hash;
    }        
        
    public void reset_network_resources()
    {
        for(int i = 0; i < mListLinks.size(); i ++)
        {
            mListLinks.get(i).reset_link();
            
        }
        for(int i = 0; i < mListNodes.size(); i ++)
        {
            mListNodes.get(i).reset_node();
            
        }        
    }
    public List<Integer> getNodesIds_of_types(List<String> node_types)
    {
        List<Integer> node_ids = new ArrayList();
        for(int i = 0; i < mListNodes.size(); i ++)
        {
            if(node_types.contains(mListNodes.get(i).getmNode_Type()))
                node_ids.add(mListNodes.get(i).getmID());
            
        }   
        return  node_ids;
    }
    
    public Matrix return_network_matrix()
    {
        return mNetMatrix;
    }
    public  HashMap<String, Integer> return_link_types_hash()
    {
        return Link_types_dimension_hash;
    }
}
