package Infrastructure;

import clone.Clone;
import com.rits.cloning.Cloner;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import org.jgrapht.graph.DefaultEdge;
import resources.Parent;
import statistics.Stats;
import utilities.Print;

/**
 * Implementation of a link on the network.
 * 
 * @author Joan Triay
 *
 */
public class Link implements Cloneable{
        /** total variable per node. */
        private int mLink_dimension;
        /** general variables per node. */
        private int mNoF_Link_general_info;
        /** integer ID of the node. */    
	/** string ID of the link. */
	
        private Parent LR_OBJ;
        
	/** number of lambdas supported on the link. */
        private int mLinknumber;
        
        private String mID;
        
        private String mType;
        
        private String mLength;
        
        private String mCost;
        
	private int mNumLambdas;
	/** origin node of this link. */
	private int mOrigin;
	/** destination node of this link. */
	private int mDest;
	/** weight of this link. */
	//private double mWeight;
            
        /**  link resources, key is dimension, value is serialised resources */
        private HashMap<String,String> mLinkResources;
        
	/** edge for the jgraph network representation. */
	private DefaultEdge mEdge;
        
        boolean Debug;
        
        
        // this needs improvements
        private List<List> List_Resources;
        
        
        //here I have a list for stats objects for differnet types
        List<Stats> All_stats;
        
        Print P = new Print();
	
	/**
	 * Class constructor.
	 * 
	 * @param origin The origin node.
	 * @param dest The destination node.
	 * @param numLambdas The number of lambdas on the link.
	 * @param weight The weight of this link.
	 */
	public Link(boolean debug, int link_dim, int no_of_Link_general_info, int link_number, String type, 
                String length, String cost, String id, int src, int dest, HashMap resources,
                Parent lr_obj){
            this.mLink_dimension = link_dim;
            this.mNoF_Link_general_info = no_of_Link_general_info;
            this.mLength = length;
            this.mCost = cost;
            this.mType = type;
            this.mLinknumber = link_number;
            this.mID = id;
            this.mDest = dest;
            this.mOrigin = src;
            this.mLinkResources = resources;
            this.Debug = debug;
            
            this.All_stats  = new ArrayList<>();
            
            this.LR_OBJ = lr_obj;
            this.List_Resources = lr_obj.getme_List_all_layered_resources();
            
            if(Debug){
                P.println(" in link class, I want to investigate the resrouces heirarchy");
                P.println(" link id : " + mID + "  link type : " + mType);
                P.println(" resrouces in different layer");
                for(int i = 0; i < mLinkResources.size(); i++)
                {
                    P.println(" in layer D" + i + " "+mLinkResources.get("D"+i));
                }
                for(int i = 0; i < mLinkResources.size(); i++)
                {    
                    P.println("layered object size " + i + mLinkResources.get("D"+i));

                }
            }
             
	}
        
        public Link(Link copy)
        {
            Cloner cloner=new Cloner();
            this.All_stats = copy.All_stats;
            this.Debug = copy.Debug;
            this.List_Resources = Clone.CloneListList(copy.List_Resources);
            this.P = copy.P;
            this.mCost = copy.mCost;
            this.mDest = copy.mDest;
            this.mEdge = copy.mEdge;
            this.mID = copy.mID;
            this.mLength = copy.mLength;
            this.mLinkResources = cloner.deepClone(copy.mLinkResources);
            this.mLink_dimension = copy.mLink_dimension;
            this.mLinknumber = copy.mLinknumber;
            this.mNoF_Link_general_info = copy.mNoF_Link_general_info;
            this.mNumLambdas = copy.mNumLambdas;
            this.mOrigin = copy.mOrigin;
            this.mType = copy.mType;
            
            
            //Link  l_clone = cloner.deepClone(l);            
            this.LR_OBJ = new Parent(copy.get_ParentClass_resrouces());
//            this.LR_OBJ =  (copy.get_ParentClass_resrouces());
            
            
        }

        

	// getters and setters of various class members.
        public void set_dest(int dest){
            this.mDest = dest;
        }
        public void set_src(int src){
            this.mDest = src;
        }
        public int get_resrouces_dimension()
        {
            return mLinkResources.size();
        }        
        public Parent get_ParentClass_resrouces()
        {
            return LR_OBJ;
        }          
        
        public HashMap get_resrouces()
        {
            return mLinkResources;
        }            
	public String getmID() {
		return mID;
	}
	public String getmType() {
		return mType;
	}
	public int getmOrigin() {
		return mOrigin;
	}

	public int getmDest() {
		return mDest;
	}
	public int getmNumber() {
		return mLinknumber;
	}
	public String getmCost() {
		return mCost;
	}
	public String getmLength() {
		return mLength;
	}        

	public DefaultEdge getmEdge() {
		return mEdge;
	}

	public void setmEdge(DefaultEdge mEdge) {
		this.mEdge = mEdge;
	}
        
        //Caution check
        //replce the resources by the given one
        public void set_resrouces(HashMap<String,String> nodeResources)
        {
            mLinkResources = new HashMap<>();
            mLinkResources = nodeResources;
        }
        
        //this method displayes the resources per layer
        public void show_resrouces(int vod_id)
        {
            P.println(" link id : " + this.mID);
            
            Parent p = get_ParentClass_resrouces();
            P.println("  the aprent resources: " + p.getme_List_all_layered_resources().toString());
            
            
//            for(int i = 0; i < this.mLinkResources.size(); i++)
//            {
//                P.println("  VON ID : "+ vod_id + " resources of D"+ i + "" + this.mLinkResources);
//            }        
        }
        
        //Method sets resrouces in string fomat in a layer
        public void setLayerResrouce(String res, int layer)
        {
            mLinkResources.put("D"+layer, res);
        }
        
        //Breaking/deviding resrouces to 1/deivde piece
        public void cutResrouces(int divide)
        {
            for(int i =0; i < mLinkResources.size();i++)
            {
                String res = mLinkResources.get("D"+i);
                res = res.substring(0,(int) (res.length()/divide));
                setLayerResrouce(res,i);
            }
        }   
//        
//        public Parent return_Res_family()
//        {
//            return LR_OBJ;
//        }
        
//        public List<List> get_res_in_list_outfamily()
//        {
//            return this.List_Resources;
//        }        
        
//        public void set_res_in_list_out_family(List<List> updated_resources)
//        {
//            this.List_Resources = updated_resources;
//        }
        
        
        /*
         * this class will be used to save the information per link as simulation goes on
         */
        public void update_stats(String req_type, int alloc)
        {
            int checked = check_if_reqType_exists(req_type);
//             Print.println(" LINK chekced if req type is there: " + checked);
//             Print.println(" LINK All_stats.size: " + All_stats.size());
            if(checked>-1)
            {
                this.All_stats.get(checked).add_to_allo_list(alloc);
                this.All_stats.get(checked).incremenet_counter();
            }
            else
            {
                Stats stat = new Stats(req_type);
                stat.add_to_allo_list(alloc);
                stat.incremenet_counter();
                this.All_stats.add(stat);
            }

        }
        
        
        //check if this type of request has been used before
        public int check_if_reqType_exists(String req_type)
        {
            int exists = -1;
            if(this.All_stats.size()>0)
            {
                for(int i  = 0; i < this.All_stats.size();i++)
                {
                    String current_type = this.All_stats.get(i).givemethe_associated_reqtype();
                    if(req_type.matches(current_type))
                    {
                        if(Debug)
                        {
                        Print.println("  in checking req type : " + req_type + "  \tthe current type" +  current_type);
                        }
                        exists = i;
                        
                    }
                    
                }
                return exists;
            }
            else return exists;
            
        }
        
        
        public void reset_link()
        {
           List<String> Res_inf =  this.LR_OBJ.getm_resinfo();
           Parent lr_obj = new Parent(Debug, Res_inf);
           this.LR_OBJ = new Parent(lr_obj);
           
        }
        

        
}
