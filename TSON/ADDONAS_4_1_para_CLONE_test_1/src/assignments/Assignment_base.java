/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package assignments;

import java.util.ArrayList;
import utilities.Matrix;

/**
 *
 * @author Bijan
 */
public interface Assignment_base {
    
    public int[] run_assignment(int value, int[] array);
    public int[] run_assignment(int First_value,int[] First_array ,int First_delay); 
    public int[] run_assignment(ArrayList First_indeces, int First_value,int[] First_array ,int First_delay); 
    public int[] run_assignment(ArrayList First_indeces,ArrayList Second_indeces, int Fisrt_value,int[] First_array ,int First_delay);
    public int[] run_assignment(int First_value,int Second_value,  int[] First_array , int[] Second_array, int First_delay, int Second_delay);
    

}
