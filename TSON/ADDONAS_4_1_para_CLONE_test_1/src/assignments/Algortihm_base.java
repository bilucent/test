/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package assignments;

/**
 *
 * @author Bijan
 */
public interface Algortihm_base {
    
    /*
    the algorithm representation is like this:
    type(type)_(Dim)(dimension) 
    */ 
    public static final int FF_1D = 1;
    public static final int FF_2D = 2;
    public static final int FF_3D = 3;
    public static final int FF_4D = 4;
    public static final int FFC_1D = 5;
    public static final int FFC_2D = 6;
    public static final int FFC_3D = 7;
    public static final int FFC_4D = 8;
    public static final int FFR_1D = 9;
    public static final int FFR_2D = 10;
    public static final int FFR_3D = 11;
    public static final int FFR_4D = 12;    
    

}
