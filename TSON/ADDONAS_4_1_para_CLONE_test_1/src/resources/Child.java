/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package resources;

import Infrastructure.ResourceOperations;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import utilities.Print;

/**
 *
 * @author Bijan
 */
public class Child {
     
    HashMap<String, String> Res_hash;
    List<Child> DOWNclasses; 
    final int V;
    final int H;
    int H_counter  =0;
    String layer_resource;
    variables v;
    HashMap<String, Child> Children;
    List<List>List_all_layered_resources;
    
    public Child(List<List>list_all_layered_resources, HashMap<String, Child> children,
            HashMap<String, String> res_hash,variables va,int horiz)
    {   
       
        this.List_all_layered_resources = list_all_layered_resources;
        this.Children = children;
        this.Res_hash = res_hash;
        this.v = va;
        this.V = v.getm_V();
        //this.H = horiz;
        //Print.println("  vert: " + vert  + " \t horiz: " + horiz );
        this.layer_resource = v.getMResList_in_Layer(V);
        
        
        //Print.println("  in while , \t get horiz all " + v.getm_layered_Horiz_member(V));

        
        this.H = v.getm_layered_Horiz_member(V);
        this.Res_hash.put(V +":" + H, layer_resource);
        this.List_all_layered_resources.get(V).add(layer_resource);
        this.DOWNclasses = new ArrayList<>();
        this.v.increase_layered_Horiz_member(V);
//        Print.println("  V: " + V  + " \t H: " + H + " resource :" + this.layer_resource);
        try {
            run();
        } catch (CloneNotSupportedException ex) {
            Logger.getLogger(Child.class.getName()).log(Level.SEVERE, null, ex);
        }

    }    
    
  
    public void run() throws CloneNotSupportedException
    {   
        
        while((H_counter  < layer_resource.length()))
        { 
            //Print.println("\tintro node V: " + V  + " \t H: " + H + " resource :" + this.layer_resource);

            v.set_V_(this.V);
            while(((v.getm_V() + 1)  < v.max_vertical_levels  ) )
            {  

                v.increase_V();
//                Print.println(" \tCintro node V: " +v.getm_V()  + " \t H_counter: " + H_counter 
//                        +" \tv.max_vertical_levels: "+v.max_vertical_levels);
                Child child = new Child(List_all_layered_resources, Children,Res_hash, v, H_counter);
                DOWNclasses.add(child);
                int H_layered = v.getm_layered_Horiz_member(V);
                Children.put(V +":" + H_layered, child);

            }
            this.H_counter++;
            //Print.println("  in while , \t get horiz all " + v.getm_layered_Horiz_member(V));
            
            
        }
    }

}
