/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package resources;

import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Bijan
 */
public class variables {
    int vertical_counter = 0 ;
    int horizontal_counter= 0;
    List<String> Input_res_info = new ArrayList<>();
    int max_vertical_levels = 0;
    int layer_horizontel_counter[];
    public variables(List<String> input_res_info )
    {
        this.Input_res_info = input_res_info;
        this.max_vertical_levels = input_res_info.size();
        this.layer_horizontel_counter = new int [max_vertical_levels];
    }
    
    public void increase_layered_Horiz_member(int V)
    {
        this.layer_horizontel_counter[V]++;
    }
            
    public int getm_layered_Horiz_member(int V)
    {
        return this.layer_horizontel_counter[V];
    }            
    
    public int increase_V()
    {
        return this.vertical_counter++;
    }
    
    public int decrease_V()
    {
        return this.vertical_counter--;
    }     
    public int increase_H()
    {
        return this.horizontal_counter++;
    }    
    
    public List<String> getMResList()
    {
        return this.Input_res_info;
    }

    public String getMResList_in_Layer(int i)
    {
        return this.Input_res_info.get(i);
    }    
    
    public int getm_V()
    {
        return this.vertical_counter;
    }
    public int getm_H()
    {
        return this.horizontal_counter;
    }  
    public int getm_MAX_V()
    {
        return this.max_vertical_levels;
    } 
    public void set_V_zero()
    {
        this.vertical_counter = 0;
    }    
    public void set_V_(int i)
    {
        this.vertical_counter = i;
    }     
    
}
