/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package addonas;

import clockdomain.VonThread;
import Infrastructure.Link;
import Infrastructure.Network;
import Infrastructure.Node;
import Infrastructure.ResourceOperations;
import RequestTypes.CreateReqList_network;
import RequestTypes.Req_BW;
import RequestTypes.Req_IT_NET_timed;
import RequestTypes.Req_NET;
import RequestTypes.Request_base;
import Vritualisation.VirInstnace;
import static addonas.ADDONAS.Debug;
import clockdomain.VonThreadStar;
import com.rits.cloning.Cloner;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
//import link_alloc.RUN_FF_1D;
//import link_alloc.RUN_FF_2D;
import org.apache.poi.hssf.usermodel.HSSFCellStyle;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.Workbook;
import routing.Route;
import routing.Routing;
import statistics.Stats_collector;
import statistics.Stats_collector_Star;
import utilities.CreateSQLStatement;
import utilities.InputReader;
import utilities.Maths;
import utilities.Print;

/**
 *
 * @author Bijan
 */
public class ADDONAS {
    static Maths M = new Maths();
    static Print P = new Print();
//     List<Link> links_list;
//     List<Node> nodes_list;
//     Network mNetwork;
    static boolean Debug = false;
    static boolean STARNET = true;
    ADDONAS (boolean debug){
        Debug = debug;
        
    }

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) throws FileNotFoundException, SQLException {
        List<Link> links_list = new ArrayList<>();
        List<Node> nodes_list  = new ArrayList<>();
        //Network mNetwork;
        ADDONAS A = new ADDONAS(false);
        // import the general info
        
        String path = System.getProperty("user.dir");

        String bijan_test_case = args[0];
        String topo_file= args[1];        
        String Route_selection_type = args[2];
       
        int Spec_time_map = Integer.parseInt(args[3]);
        int subL_min = Integer.parseInt(args[4]);
        int subL_max = Integer.parseInt(args[5]);
        int Lambda_min = Integer.parseInt(args[6]);
        int Lambda_max = Integer.parseInt(args[7]);
        int Flex_max = Integer.parseInt(args[8]);    
        int Flex_min = Integer.parseInt(args[9]);    
        //int start_poisson_mean = 10
        
        Print.println(" Spectrum to time slices mapping is: 1 to " + Spec_time_map);
        
        Print.println(" sublambda spectrum range: ");
        Print.println("           Min: " + subL_min);
        Print.println("           Max: " + subL_max);

        
        Print.println(" lambda spectrum range: ");
        Print.println("           Min: " + Lambda_min);
        Print.println("           Max: " + Lambda_max);

        
        Print.println(" super lambda spectrum range: ");
        Print.println("           Max: " + Flex_max);
        Print.println("           Min: " + Flex_min);
        
        
        if((subL_max-subL_min)%Spec_time_map!= 0 || 
            (Lambda_max - Lambda_min) %Spec_time_map!= 0 || 
            (Flex_max - Flex_min) % Spec_time_map != 0    
           )
        {
            Print.println(" WARNING: The spectrum ranges allocated do not "
                    + "match the mapping of spectrum to time ");
        }


        
        double exponential_lambda = Double.parseDouble(args[10]);
        
        int start_poisson_mean = Integer.parseInt(args[11]);
        int increased_poisson_mean = Integer.parseInt(args[12]);
        int stop_poisson_mean = Integer.parseInt(args[13]);
        
        int  Runs_per_erlang = Integer.parseInt(args[14]);
        int number_route_try = Integer.parseInt(args[15]);
        
        //this will determine the duration of each req        
        //double exponential_lambda = 0.3;
        //double exponential_lambda = 12;

        
        /*
         * the numbers in this method genrally indicate on which lines onthe 
         * excel file should be read
         * 
         */
        InputReader inputreader = new InputReader(Debug,"addonas","CREATE",topo_file,1,7,4,4,2,2);
        inputreader.infrastructure_excel();        
        //inputreader.virtualisation_excel("");




                
        
        
        try {
             nodes_list = inputreader.createNodeList();
        } catch (SQLException ex) {
            Logger.getLogger(InputReader.class.getName()).log(Level.SEVERE, null, ex);
        }    
        if(Debug)
        P.println("nodes.size()  "+nodes_list.size());

        try {
             links_list = inputreader.createLinkList();
        } catch (SQLException ex) {
            Logger.getLogger(InputReader.class.getName()).log(Level.SEVERE, null, ex);
        }    
        if(Debug)
        Print.println("links.size()  "+links_list.size());
                
        
        
        HashMap node_types_hash = inputreader.get_node_types_hash();
        HashMap link_types_hash = inputreader.get_link_types_hash(); 
        HashMap link_adj_hash = inputreader.get_link_adj_hash();        
        List<Integer> res_inf_link = inputreader.get_res_inf_link();
        List<Integer> res_inf_node = inputreader.get_res_inf_node();
 
        
        if((res_inf_link.get(0)*res_inf_link.get(1))%Spec_time_map!= 0)
        {
            Print.println(" WARNING: Resource in layer0*1 does not"
                    + " divide by Spec_to_time ");
        }        
        /*
         * simulation specificaitons
         */
        int req_duration =2000;
        int stop_sim = 6000;
        
        //number of concurrent reqs for tx from each node
        //int start_poisson_mean = 10;
        
        //this will determine the duration of each req        
        //double exponential_lambda = 0.3;
        //double exponential_lambda = 12;
    

        int initialTIme = 0;      
        
//        int number_of_rounds = 2500;
        
       //int number_of_rounds = 1250;
        
        
        /*
         * for running just ones I need this set.
         * Actulllay 4 ones. number of virt,
         * mnumber ofreqs, number of iteration pernode,
         * erlang,
         */
        
        // this is independant and geneeral info
        int number_of_reqs = 2000;
        
        //roundss + iterations are used to make the request list
        int number_of_rounds =100;
        int iterate_pernode_reqs = 1;
        
        //in excel I put several runs next to each other for averagaing and normalising
//        Runs_per_erlang = 10;        
        
//        Cloner cloner=new Cloner();
//        HashMap Node_types_hash = cloner.deepClone(node_types_hash);        
//        HashMap Link_types_hash = cloner.deepClone(link_types_hash);        
//        HashMap Link_adj_hash = cloner.deepClone(link_adj_hash);        
//        List<Link> Links_list  = cloner.deepClone(links_list);        
//        List<Node> Nodes_list  = cloner.deepClone(nodes_list);        
//        
//        
        // this needs to be fixed at some point
//        int excel_space_between_runs =0;
        
        //make the network, and rung createetwork graoh, as inhertied frmo Joan codes,
        //to make a matrix of stuff in the network class
        
//        
//         Network mNetwork = new Network(nodes_list,links_list,
//                inputreader.retrun_node_types_hash(),
//                inputreader.retrun_link_types_hash(), 
//                inputreader.retrun_link_adj_hash());
        
        
        Network mNetwork = new Network(nodes_list,links_list,
                node_types_hash,
                link_types_hash, 
                link_adj_hash);
         for(int i = 0; i < nodes_list.size(); i++)
         {
             Print.println("network nodes 1st... "+ mNetwork.getmListNodes().get(i).getmID());
         }
         for(int i = 0; i < links_list.size(); i++)
         {
             Print.println("network link 1st... "+  mNetwork.getmListLinks().get(i).getmID());
         }
         
//                mNetwork.createNetworkGraph();        
//                Print.print_2D("network created  1st... ", mNetwork.return_network_matrix().getDataInt());
//                Print.println("network nodes 1st... "+ mNetwork.getmListNodes().toString());        
//        
	
	//A.fillRoutes(mNetwork,number_route_fill);        
        
        if(Debug)
        for (int i = 0; i < nodes_list.size(); i++)
        {
            P.println(" the number of routes from node:" + i + "  is: " + nodes_list.get(i).getmRouting().get_number_of_routes_all());
        }

        if(Debug)
        for (int i = 0; i < nodes_list.size(); i++)
        {
            Node node = nodes_list.get(i);
            
            P.println(" ADDONAS: node id is :" + node.getmID());
            node.getmRouting().get_number_of_routes_all();
            node.getmRouting().print_routes();
        }        
        
        
        //test the routing
//        A.fillRoutes(mNetwork,1); 
//        List<Node> listNodes = mNetwork.getmListNodes();
//        for(int i=0; i<listNodes.size(); i++){
//            Node node = listNodes.get(i);
//            for(int j = 0; j < listNodes.size() && j!= i;j++)
//                System.out.println(" ^^^^ number of routes "+node.getmRouting().get_number_of_routes_dest(j));
//        }
        
        P.println(" Make req list*******************************          ");
        CreateReqList_network net_req_list = new CreateReqList_network(Debug);
//        create_reqs(Network network, int Number_of_reqs
//            , int bitrate, int QoS, int duration, String algorithm_form)
        //HashMap reqs = net_req_list.create_reqs(mNetwork, 1, 10, 2, 200, "MMM");
                
        int Virt_domains = 1;
        //this is a static src dest req maker
        //HashMap reqs = net_req_list.create_Static_reqs(mNetwork, 1, 10, 2, 200, "SSS",1 ,3);
        
        List<VonThreadStar> Clck_list = new ArrayList<>();
        List<HashMap> req_sets = new ArrayList<>();
        //process the VON requests
        //VirInstnace(Network network,int virt_scheme, int num_virt)
//        VirInstnace VONs = new VirInstnace(mNetwork, 1, Virt_domains); 
//        List<Network> VON_list =  VONs.networkCloning();
        

       
        for (int i = 0; i < Virt_domains; i++)
        {
            //HashMap reqs = net_req_list.create_reqs(VON_list.get(i), 1, 10, 2, 200, "MMS");
//            
//            String[] req_types = {"aa","bb","cc","dd","ee"};
            String[] Star_req_types = {"a","b","c","d","e","f"};
//           int[] Star_req_weighs = {1,0,0,1,0,0};            
//           int[] Star_req_weighs = {0,0,1,0,0,1};            
           int[] Star_req_weighs = {1,0,0,0,0,0};            
//           int[] Star_req_weighs = {6,3,1,6,3,1};            
//           int[] Star_req_weighs = {1,1,1,1,1,1};            
            
            
            String[] req_types = {"aa","bb","cc","dd","ee"};
//            int[] req_weighs = {80,8,2,8,2};
            //int[] req_weighs = {30,1,0,0,1};
            int[] req_weighs = {1,1,1,1,1};
//           int[] req_weighs = {0,0,0,1,0};            
//           int[] req_weighs = {1,0,0,0,0};            
//           int[] req_weighs = {1,1,1,1,0};            
            if(req_types.length != req_weighs.length)
            {
                System.exit(8230);
            }
//            String[] Star_req_types = {"a","b","c","d","e"};
//            int[] req_weighs = {80,8,2,8,2};
            //int[] req_weighs = {30,1,0,0,1};
//            int[] req_weighs = {1,1,1,1,1};
//           int[] req_weighs = {0,0,0,1,0};            
//           int[] Star_req_weighsIs = {1,0,0,0,0};            
            if(req_types.length != req_weighs.length)
            {
                System.exit(8230);
            }


            /*
             * make a new excel file to write in to 
             */

            String types_concat = ResourceOperations.strArr_to_string(req_types);
            String typesw_concat = ResourceOperations.intArr_to_string(req_weighs);
            String date_time = A.get_DateToString();
            String excel_file_address = path+ "\\JAVAEXCEL"+"\\"
                    +date_time+bijan_test_case+"_RS"+Route_selection_type+ 
                    "_specTimeMap"+Spec_time_map+
                    "_subLAccess"+subL_min+"to"+subL_max+
                    "_LAccess"+Lambda_min+"to"+Lambda_max+
                    "_FlexAccess"+Flex_max+"to"+Flex_min+
                    "erm"+start_poisson_mean+"_exl"+exponential_lambda+ "_kroutes"
                    +number_route_try +"_reqtypes"+types_concat+"_reqweighs"+typesw_concat
                    +"_topo"+topo_file+".xls";
//            String excel_file_address = "C:\\Users\\Bijan\\Dropbox\\Under_Work\\"
//                    + "ADDONAS_1_5_1_5\\JAVAEXCEL\\"
//                    +date_time+bijan_test_case+"_RS"+Route_selection_type+ 
//                    "_specTimeMap"+Spec_time_map+"_GridAccess"+subL_max+"_FlexAccess"+Flex_min
//                    +"erm"+start_poisson_mean+"_exl"+exponential_lambda+ "_kroutes"
//                    +number_route_try +"_reqtypes"+types_concat+"_reqweighs"+typesw_concat
//                    +"_topo"+topo_file+".xls";
            
            Print.println("  excel_file_address: "+ excel_file_address);
            
            FileOutputStream fos = null;
            try {
                    fos=new FileOutputStream(new File(excel_file_address));
                    HSSFWorkbook workbook = new HSSFWorkbook(); 
                    HSSFSheet sheet= workbook.createSheet("Sample sheet"); 
                    HSSFCellStyle hsfstyle=workbook.createCellStyle();
                    hsfstyle.setBorderBottom((short) 1);
                    hsfstyle.setFillBackgroundColor((short)245);
                    workbook.write(fos);
            } catch (Exception e) {
                    e.printStackTrace();
            }        
            

            
            List<Stats_collector_Star> SC_list = new ArrayList();

            
            /*
             * this causes a nother set of iteraation od the simualtion
             */
//            int interval_index = start_poisson_mean;
            for(int interval_index = start_poisson_mean; interval_index<= stop_poisson_mean;interval_index= interval_index + increased_poisson_mean)
            {            
                
//              int []nodes_tobeused ={0,1,2,3,4,5,6,7,8,9,10,11,12,13,14};     
//              int []nodes_tobeused ={0,1,2,3,4,5,6,7,8,9,10,11,12,13,14,
//              15,16,17,18,19,20,21,22,23};     
                
//              int []nodes_tobeused ={0,1,2,3,4,5,6,9,10,11,12,13,14,15,
//              16,19,20,21,22,23};//for OFC2014     
              int []nodes_tobeused ={0,1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,
              16,17,18,19,20,21,22,23};//for reference testing    
              
//                int []nodes_tobeused ={2,5,9,13,15,20,23};     
//                int[] nodes_tobeused ={0,1,2,3,5};   

//                List reqs = net_req_list.create_reqs_node_erlangs(number_of_rounds, iterate_pernode_reqs, 10, 2, req_duration, "bij", 
//                        req_types, req_weighs, nodes_tobeused, interval_index, exponential_lambda);
    //
    // 
    //            public List<Req_NET_timed> create_Static_reqs(Network network, 
    //            int Number_of_reqs, int bitrate, int QoS, int duration, 
    //            String algorithm_form,String[] req_types, int[] types_weighs, final int src, final int dest)

    //            List   reqs = net_req_list.create_Static_reqs(mNetwork, number_of_rounds, 65, 65, req_duration, "bij", req_types, req_weighs, 0, 1);

                
                for(int j = 0; j< Runs_per_erlang;j ++)
                {
            

                            //Cloner cloner=new Cloner();
//                    Cloner cloner=new Cloner();
//                    HashMap Node_types_hash = cloner.deepClone(node_types_hash);        
//                    HashMap Link_types_hash = cloner.deepClone(link_types_hash);        
//                    HashMap Link_adj_hash = cloner.deepClone(link_adj_hash);        
//                    List<Link> Links_list  = cloner.deepClone(links_list);        
//                    List<Node> Nodes_list  = cloner.deepClone(nodes_list);        
//
//

//                    mNetwork = new Network(Nodes_list,Links_list,
//                    Node_types_hash,
//                    Link_types_hash, 
//                    Link_adj_hash);
//
////

                    mNetwork.reset_network_resources();
                    mNetwork.createNetworkGraph();
                    mNetwork.create_link_hash();
                    //A.fillRoutes(mNetwork,number_route_fill);                     

    //                    List reqs = net_req_list.create_reqs_node_erlangs(number_of_rounds, iterate_pernode_reqs, 10, 2, req_duration, "bij", 
//                        req_types, req_weighs, nodes_tobeused,(int)exponential_lambda, interval_index);
                   
                    List reqs = new ArrayList();
                    if(!STARNET){
                        reqs = net_req_list.create_reqs_node_erlangs_IT_NET(number_of_rounds, 
                                iterate_pernode_reqs, 10, 2, req_duration, "bij", 
                                req_types, req_weighs, nodes_tobeused,
                                (int)exponential_lambda, interval_index);
                    }else{
                        int minnodes = 2;
                        int maxnodes = 5;
                        
                        /* 
                         * randomised req list
                         */
                        reqs = net_req_list.create_reqs_node_erlangs_IT_NET_StarNet_2(number_of_rounds, 
                                iterate_pernode_reqs, 10, 2, req_duration, "bij", 
                                Star_req_types, Star_req_weighs, req_types, req_weighs, nodes_tobeused,
                                (int)exponential_lambda, interval_index,
                                minnodes, maxnodes, number_of_reqs);
                        
                        /*
                         * constant seeded
                         */
//                        reqs = net_req_list.create_reqs_node_erlangs_IT_NET_StarNet_2_badseed(number_of_rounds, 
//                                iterate_pernode_reqs, 10, 2, req_duration, "bij", 
//                                Star_req_types, Star_req_weighs, req_types, req_weighs, nodes_tobeused,
//                                (int)exponential_lambda, interval_index,
//                                minnodes, maxnodes, number_of_reqs,
//                                
//                                //the seed I made
//                                (interval_index*100+j)*j);
                    }
                    VonThreadStar CD = new VonThreadStar(Debug, i, mNetwork, number_route_try, 
                        initialTIme,stop_sim, start_poisson_mean, reqs, interval_index*100+j ,excel_file_address,
                            req_types, Star_req_types
                             ,Spec_time_map, 
                            subL_min, subL_max, Lambda_min, Lambda_max,Flex_max, Flex_min, 
                            Route_selection_type, STARNET);

                    Clck_list.add(CD);

                    //CD.start();
                    CD.run(); 
                    SC_list.add(CD.getm_SatisticsCollection());
                }
                start_poisson_mean = start_poisson_mean + increased_poisson_mean;
            }
            Print.println("  the number of SC collected finally " + SC_list.size());            
            
        }
        
//        for (int i = 0; i < Virt_domains; i++)
//        {
//            Clck_list.get(i).start();
//        }
 
    }
    
    
    	/**
         imported from Joan work
	 * This method fills the routes for each node on the network.
	 * It takes as a parameter, the number of k-shortest paths
	 * that it can use to reach any possible destination node on the network.
	 * @param k The number of k-shortest paths.
	 */
	public static void fillRoutes(Network mNetwork, int k){
            P.println(" number of shortest paths to be allocated to the nodes: " +k);
		List<Node> listNodes = mNetwork.getmListNodes();
		for(int i=0; i<listNodes.size(); i++){
			Node node = listNodes.get(i);
			node.getmRouting().searchKShortestPaths(mNetwork, k);
			node.getmRouting().get_number_of_routes_dest(i);
                        
		}
	}

    
    // a test method for playing with sql commands from java
     public static void testSQL() throws SQLException
     {
        CreateSQLStatement s = new CreateSQLStatement(Debug);
        s.UseDatabase(" addonas");

        ResultSet rs = null;
        try {
             rs = s.get_ResultSet_("nodes");
             
        } catch (SQLException ex) {
            Logger.getLogger(InputReader.class.getName()).log(Level.SEVERE, null, ex);
        }
        System.out.println(" **************create node list***************  ");
        for(int i = 1; i< 7;i++)
        {
            try {
                rs.absolute(i);
                //print(" rs.getCursorName() " + rs.getCursorName());
            } catch (SQLException ex) {
                Logger.getLogger(InputReader.class.getName()).log(Level.SEVERE, null, ex);
                System.exit(2);
            }
            System.out.print(rs.getString("TYPE"));        
            System.out.print(rs.getString("COST"));
            System.out.println(rs.getString("DELAY"));

        }
    }
    public static String get_DateToString()    {
 
        //   Allocates a Date object and initializes it so that it represents the time
        // at which it was allocated, measured to the nearest millisecond.
        Date dateNow = new Date ();
 
        DateFormat dateformatYYYYMMDD = new SimpleDateFormat("yyyyMMdd_HHmmss_");
        //SimpleDateFormat dateformatMMDDYYYY = new SimpleDateFormat("MMddyyyy");
 
        StringBuilder nowYYYYMMDD = new StringBuilder( dateformatYYYYMMDD.format( dateNow ) );
        //StringBuilder nowMMDDYYYY = new StringBuilder( dateformatMMDDYYYY.format( dateNow ) );
 
        System.out.println( "DEBUG: Today in YYYYMMDD: '" + nowYYYYMMDD + "'");
        //System.out.println( "DEBUG: Today in MMDDYYYY: '" + nowMMDDYYYY + "'");
        return nowYYYYMMDD.toString();
 
    }     

}

