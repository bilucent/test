package routing;

import java.io.PrintStream;
import java.util.List;

import org.jgrapht.DirectedGraph;
import org.jgrapht.GraphPath;
import org.jgrapht.alg.KShortestPaths;
import org.jgrapht.graph.DefaultEdge;
import Infrastructure.Network;
import Infrastructure.Node;
import java.util.Collection;
import java.util.Hashtable;
import utilities.Print;

/**
 * This class implements the procedure to find all
 * routes for a given node.
 * 
 * @author Joan Triay
 *
 */
public class Routing {
	/** the source node. */
	private Node mNode;
	/** the routing table to fill in. */
	private RoutingTable mRoutingTable;
        
        private Print P;
	
	
	/**
	 * Class constructor.
	 * @param node The node this routing module
	 * belongs to.
	 */
	public Routing(Node node){
		this.mNode = node;
		this.mRoutingTable = new RoutingTable(node.getmID());
	}
        
        public int get_number_of_routes_dest(int dest)
        {
            return mRoutingTable.get_number_of_routes_to_dest(dest);
        }
        public int get_number_of_routes_all()
        {
            return mRoutingTable.get_number_of_routes_all();
        }
	
        public void print_routes()
        {

            mRoutingTable.printTable();
        }	
	/**
	 * This function searches for the k-shortest paths from the
	 * origin node holding this routing module to every remaining node
	 * on the network.
	 * @param network The network topology.
	 * @param numRoutes Number of routes to find.
	 */
	public void searchKShortestPaths(Network network, int numRoutes){		
		// get the net graph
		DirectedGraph<Node, DefaultEdge> networkGraph = network.getmNetworkGraph();
		
		// create the KShortestPaths object
		KShortestPaths<Node, DefaultEdge> kShortestPaths = new KShortestPaths(networkGraph, mNode, numRoutes);
		
		// Get the list of possible destinations from the network topology
		List<Node> listNodes = network.getmListNodes();
		
		// for each destination, find k-shortest paths
		for(int i=0; i<listNodes.size(); i++){
			Node dest = listNodes.get(i);
			
			// we cannot find a route to the node itself, so only
			// compute those routes to the remaining nodes
			if(dest.equals(mNode) == false){
				// calculate the paths
				List<GraphPath<Node, DefaultEdge>> listKPaths = kShortestPaths.getPaths(dest);
                                
                                //if i dont havbe any connections
                                if(listKPaths!=null)
                                {
                                    // run through the list of paths and store them into the database
                                    int _numComputed = listKPaths.size();
                                    for(int j=0; j<_numComputed; j++){
                                            // get the list of edges/links
                                            List<DefaultEdge> _edges = listKPaths.get(j).getEdgeList();

                                            if(_edges != null){
                                                    int _path[] = new int[_edges.size()+1];
                                                    _path[0] = mNode.getmID();

                                                    // set the path of this route
                                                    for(int k=0; k<_edges.size(); k++){
                                                            DefaultEdge _edge =  _edges.get(k);
                                                            _path[k+1] = network.getLinkByEdge(_edge).getmDest();
                                                    }

                                                    // add the route to the table
                                                    this.mRoutingTable.addRoute(dest.getmID(), _path);
                                            } // end if edges != null
                                    } // end for-loop number of computed paths
                                }// if no connections return shit    
			} // end if for destination node
		} // end for-loop for all destination nodes
	}
	/**
         * Bijan: I have slightly modified the original methodd to only
         * find the K paths for the desired destination
	 * This function searches for the k-shortest paths from the
	 * origin node holding this routing module to every remaining node
	 * on the network.
	 * @param network The network topology.
	 * @param numRoutes Number of routes to find.
	 */
	public void searchKShortestPaths_one_Dest(Network network, int numRoutes, int node_id){		
		// get the net graph
		DirectedGraph<Node, DefaultEdge> networkGraph = network.getmNetworkGraph();
		
		// create the KShortestPaths object
		KShortestPaths<Node, DefaultEdge> kShortestPaths = new KShortestPaths(networkGraph, mNode, numRoutes);
		
		// Get the list of possible destinations from the network topology
		List<Node> listNodes = network.getmListNodes();
		
		// for each destination, find k-shortest paths
                {
			Node dest = listNodes.get(node_id);
			// we cannot find a route to the node itself, so only
			// compute those routes to the remaining nodes
			if(dest.equals(mNode) == false){
				// calculate the paths
				List<GraphPath<Node, DefaultEdge>> listKPaths = kShortestPaths.getPaths(dest);
                                
                                //if i dont havbe any connections
                                if(listKPaths!=null)
                                {
                                    // run through the list of paths and store them into the database
                                    int _numComputed = listKPaths.size();
                                    for(int j=0; j<_numComputed; j++){
                                            // get the list of edges/links
                                            List<DefaultEdge> _edges = listKPaths.get(j).getEdgeList();

                                            if(_edges != null){
                                                    int _path[] = new int[_edges.size()+1];
                                                    _path[0] = mNode.getmID();

                                                    // set the path of this route
                                                    for(int k=0; k<_edges.size(); k++){
                                                            DefaultEdge _edge =  _edges.get(k);
                                                            
                                                            _path[k+1] = network.getLinkByEdge(_edge).getmDest();
                                                    }

                                                    // add the route to the table
                                                    this.mRoutingTable.addRoute(dest.getmID(), _path);
                                            } // end if edges != null
                                    } // end for-loop number of computed paths
                                }// if no connections return shit    
			} // end if for destination node
		} // end for-loop for all destination nodes
	}
	
	
	public void printRoutingTable(PrintStream pStream){
		pStream.println("Table Node " + mNode.getmID());
		pStream.println(mRoutingTable.printTable());
	}
	
	// getters and setters
	public RoutingTable getmRoutingTable() {
		return mRoutingTable;
	}
//        public Route Route_on_demand(int src, int dest,  ){
//            
//        }
}
