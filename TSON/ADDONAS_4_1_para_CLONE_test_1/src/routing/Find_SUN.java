/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package routing;

import Infrastructure.Network;
import Infrastructure.Node;
import RequestTypes.CreateReqList_network;
import RequestTypes.Req_IT_NET_timed;
import RequestTypes.Req_IT_NET_timed_startnet;
import cern.jet.random.Uniform;
import cern.jet.random.engine.DRand;
import cern.jet.random.engine.RandomEngine;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Set;
import utilities.Converters;
import utilities.Maths;
import utilities.Print;
import static utilities.Print.println;

/**
 *
 * @author Bijan
 */
public class Find_SUN {
//    private int []potentialSUNS;
    private Network network;
    boolean Debug;
//    int static_IDI;
    public Find_SUN(boolean debug, Network net , int static_IDI)
    {
        this.network = net;
        this.Debug = debug;
//        this.static_IDI = static_IDI;
//        potentialSUNS = psuns;
    }
    public List<Integer> find_random_SUNs(int potentialSuns[], int sunswanted){

        // to static classes are used to make the random selection of the stars
        List<Integer> potential_sun_array = Converters.convert_int_arr_to_Arr_list(potentialSuns);
        return CreateReqList_network.node_select_fromSet_norepeat(potential_sun_array, sunswanted);
    }
        
    
    public List<Integer> find_Star_from_SUNs_TotalshortestPaths(int potentialSuns[], int sunswanted){

        // to static classes are used to make the random selection of the stars
        List<Integer> potential_sun_array = Converters.convert_int_arr_to_Arr_list(potentialSuns);
        return CreateReqList_network.node_select_fromSet_norepeat(potential_sun_array, sunswanted);
    }
        
        
    public List<Integer> find_Star_from_SUNs_LeastUsed(int potentialSuns[], int sunswanted){

        // to static classes are used to make the random selection of the stars
        List<Integer> potential_sun_array = Converters.convert_int_arr_to_Arr_list(potentialSuns);
        return CreateReqList_network.node_select_fromSet_norepeat(potential_sun_array, sunswanted);
    }
    
        
    public List<Integer> find_Star_from_SUNs_shortestPaths_leastVar(int potentialSuns[], int sunswanted){

        // to static classes are used to make the random selection of the stars
        List<Integer> potential_sun_array = Converters.convert_int_arr_to_Arr_list(potentialSuns);
        return CreateReqList_network.node_select_fromSet_norepeat(potential_sun_array, sunswanted);
    }
        

    
    public List<Req_IT_NET_timed> create_reqs_mesh_stars_suns(Req_IT_NET_timed_startnet starreq, int static_IDI)
    {
//        List<PermutationSrcDest> per = new ArrayList<>();
        List<Integer> l = starreq.returnSrcdest();
        List<Req_IT_NET_timed> singlereqa = new ArrayList<>();
        for (int i = 0; i < l.size(); i++)
        {
            
            for (int j = 0; j < l.size(); j++)
            {
                if(l.get(i) != l.get(j))
                {
//                    PermutationSrcDest psd  = new PermutationSrcDest(l.get(i), l.get(j));
                    //the permutations made ar edirectly filling up the request sets
                    Req_IT_NET_timed RIT = new Req_IT_NET_timed
                            (starreq,static_IDI++,l.get(i), l.get(j));
                    singlereqa.add(RIT); 
                }
            }
            
        }
        return singlereqa;
    }    
    
    
    
    public int[][][] star_sun_distance_availability_matrix(int stars[], int suns[])
    {
        //this is [star][sun][route_matrix] availability
        int [][][]distance_cost_array = new int[stars.length][suns.length][];
        
        
        return distance_cost_array;
    }
    
    public List<Req_IT_NET_timed>  create_star_sun_req_list6( List<Integer> nodesIDs1,
            List<Integer> nodesIDs2,Req_IT_NET_timed_startnet starreq, int combinedstatic_IDI)
    {
        List<Req_IT_NET_timed> singlereqa = new ArrayList<>();
        int number_of_network_nodes = network.getmListNodes().size();

        for (int i = 0; i < nodesIDs1.size(); i++)
        {
            
            for (int j = 0; j < nodesIDs2.size(); j++)
            {
//                if(nodesIDs1.get(i) != nodesIDs1.get(j))
                {
                    Req_IT_NET_timed RIT = new Req_IT_NET_timed
                            (starreq, combinedstatic_IDI++, nodesIDs1.get(i), nodesIDs2.get(j));
                    singlereqa.add(RIT); 
//                    Print.println(" RIT src " + RIT.returnSrc() + "   RIT dest " + RIT.returnsDest()
//                            + " RIT time " + combinedstatic_IDI );

                    
                    RIT = new Req_IT_NET_timed
                            (starreq, combinedstatic_IDI++, nodesIDs2.get(j), nodesIDs1.get(i));
                    singlereqa.add(RIT); 
//                    Print.println(" RIT src " + RIT.returnSrc() + "   RIT dest " + RIT.returnsDest()
//                            + " RIT time " + combinedstatic_IDI );
//                    
                }
            }
            
        }

//        Print.println(" singke reqs between suns and starr " + singlereqa.toString());
        
        return singlereqa;
    }        
    public List<Req_IT_NET_timed>  create_star_sun_req_list_test( List<Integer> nodesIDs1,
            List<Integer> nodesIDs2,Req_IT_NET_timed_startnet starreq, int combinedstatic_IDI)
    {
        List<Req_IT_NET_timed> singlereqa = new ArrayList<>();
        int number_of_network_nodes = network.getmListNodes().size();
        
//        public Req_IT_NET_timed(List NET_ALG_type,List IT_ALG_type, String Algtype, String req_type, int id, int src, int dest, int bw,int QOS,  
//                List Net_req,List IT_req, int start_time, int duration)
        
        Req_IT_NET_timed RIT = new Req_IT_NET_timed(starreq.get_NET_ALG_types(), 
                starreq.get_IT_ALG_types(), 
                starreq.returnAlgorithmType()
                , starreq.returnConnectionReqType(), 
                combinedstatic_IDI++,
                nodesIDs1.get(0), nodesIDs1.get(1), 
                starreq.returnBW(),
                starreq.returnQOS(),
                starreq.returnNETREQ(),
                starreq.returnITREQ(),
                starreq.returnStartTime(),
                starreq.returnDurationTime()
                );
        
       
            
        singlereqa.add(RIT);

//        Print.println(" singke reqs between suns and starr " + singlereqa.toString());
        
        return singlereqa;
    }        
    public List[][] create_routeLIST_matrices_for_star_sun1( List<Integer> nodesIDs1,
            List<Integer> nodesIDs2, int numder_of_routes_cap)
    {
//        List<PermutationSrcDest> per = new ArrayList<>();
        int number_of_network_nodes = network.getmListNodes().size();
        List<Route> [][] route_matrix = new List[number_of_network_nodes][number_of_network_nodes];
        
//        Print.println(" number_of_network_nodes " + number_of_network_nodes);
//        Print.println(" nodesIDs1 " + nodesIDs1.toString());
//        Print.println(" nodesIDs2 " + nodesIDs2.toString());
        
        for(int i =0; i < route_matrix.length; i++)
        {
            for(int j =0; j < route_matrix[0].length; j++)
            {
                route_matrix[i][j] = null;
            }
        
        }
        /*
         * to find the suns, I need half of the matrix, from star to suns
         * the other half jut increases the complexity without helping
         * since the routes are bidirectional
         */
        for (int i = 0; i < nodesIDs1.size(); i++)
        {
            
            for (int j = 0; j < nodesIDs2.size(); j++)
            {
                if(nodesIDs1.get(i) != nodesIDs2.get(j))
                {
                    /*
                     * compute the list of routes between th set of nodes, either nodes1 and nodes2
                     */

                    Node n = network.getNode(nodesIDs1.get(i));                    
//                    Print.println(" bijan nodesIDs1.get(i) " +  nodesIDs1.get(i));
//                    Print.println(" bijan nodesIDs2.get(j) " +  nodesIDs2.get(j));
//                    Print.println(" bijan n.getid " +  n.getmID());
//                    
//                    Print.println(" bijan n.getmRouting().get_number_of_routes_all() " +  n.getmRouting().get_number_of_routes_all());

                    List<Route> lr = n.getmRouting().getmRoutingTable()
                            .get_dest_routes_list_capped(nodesIDs2.get(j),numder_of_routes_cap);
                    route_matrix[nodesIDs1.get(i)][nodesIDs2.get(j)] = lr;

                }
            }
            
        }
//        for (int i = 0; i < nodesIDs2.size(); i++)
//        {
//            
//            for (int j = 0; j < nodesIDs1.size(); j++)
//            {
////                if(nodesIDs1.get(i) != nodesIDs1.get(j))
//                {
//                    /*
//                     * compute the list of routes between th set of nodes, either nodes1 and nodes2
//                     */
//
//                    Node n = network.getNode(nodesIDs2.get(i));
//                    List<Route> lr = n.getmRouting().getmRoutingTable()
//                            .get_dest_routes_list_capped(nodesIDs1.get(j),numder_of_routes_cap);
//                    route_matrix[nodesIDs2.get(i)][nodesIDs1.get(j)] = lr;
//
//                }
//            }
//            
//        }
        
        
        if(Debug){
            for (int i = 0; i < route_matrix.length; i++)
            {

                for (int j = 0; j < route_matrix[0].length; j++)
                {

                    if( route_matrix[i][j] != null)
                    Print.println("Route matrix with i:%s and j:%s " +  i + j +" Route is : " + route_matrix[i][j].toString());

                }

            }
        }
        
        
        
        
        
        return route_matrix;
    }        
    public double[][] star_sun_create_distance_matrix_from_route_matrix2(List<Route>[][] route_matrix)
    {
        double distance_matrix[][] = new double[route_matrix.length][route_matrix[0].length];
        for (int i = 0; i < route_matrix.length; i++)
        {
            
            for (int j = 0; j < route_matrix[0].length; j++)
            {
                double lengthS = 0;
                int routeS = 0;
                List<Route> lr = route_matrix[i][j];
                if(lr!=null)                
                {
                    for(Route r:lr)
                    {
                        lengthS += r.getmLength();
                        routeS ++;
                    }
                    distance_matrix[i][j] = (double)lengthS/routeS;
                }
                else
                    distance_matrix[i][j] = 0;
               
            }
        }
        
        /*
         * test
         */
        if(Debug)
        for (int i = 0; i < route_matrix.length; i++)
        {
            
            for (int j = 0; j < route_matrix[0].length; j++)
            {
                Print.print(" i:" +  i + " j:"+ j +">" + distance_matrix[i][j]+ "   ");
            }
            Print.println("");
        }
        
       return distance_matrix; 
    }
    public double[] create_distanceVector_from_disMatrix3(double[][] dist_arr)
    {

        double distnace_vector[] = new double [dist_arr[0].length];
//        for(int[] array:dist_arr)
//            for(int dist:array)
//                distnace_vector[]++dist;

        for(int i =0; i < dist_arr.length; i++)
            for(int j =0; j < dist_arr[0].length; j++)
                distnace_vector[j]+=dist_arr[i][j];
        
        return distnace_vector;
        
    }
    public HashMap<Double,Integer> hash_the_vector_to_keep_indices4(double[] dist_vector)
    {
        HashMap<Double,Integer> distance_hash = new HashMap<>();
        for(int i = 0; i < dist_vector.length; i ++)
            distance_hash.put(dist_vector[i], i);
        return distance_hash;
    }
    public List<Integer> return_index_list_ofSuns_with_SP_stars5(HashMap<Double,Integer> distance_hash, 
            double[] dist_vector,int number_of_suns)
    {
        List<Integer> suns = new ArrayList();
        Set<Double> keySet = distance_hash.keySet();
        Object[] toArray = keySet.toArray();
        double [] doublearr = new double [toArray.length];
        for (int i = 0; i < doublearr.length; i++){
            doublearr[i] = (Double) toArray[i];
        }
        
        double[] sorted_arr = Maths.bubbleSort(doublearr);
        
        if(Debug){
            Print.println("  the sorted double size " + sorted_arr.length);
            Print.print_1D("  the sorted double vector array in funciton 5 ", sorted_arr);
            Print.println("  the calculated distance vector size "+ dist_vector.length);
            Print.print_1D("  the calculated distance vector ", dist_vector);
        }
        int sun_counter = 0;
        for(int i = 0; i < sorted_arr.length && sun_counter < number_of_suns; i++)
        {
            if(i>0 && sorted_arr[i] != 0&& !suns.contains(sorted_arr[i])){
                sun_counter++;
//                Print.println("  sorted_arr[i]: " + sorted_arr[i]);
                suns.add(distance_hash.get(sorted_arr[i]));
            }
        }
//        Print.println(" SP sun nodes to string: " + suns.toString());
        
        return suns;
    }
        
    public List<Integer> return_index_list_ofSuns_with_RND_distnace_to_stars5_1(HashMap<Double,Integer> distance_hash, 
            double[] dist_vector,int number_of_suns)
    {
        List<Integer> suns = new ArrayList();
        Set<Double> keySet = distance_hash.keySet();
        Object[] toArray = keySet.toArray();
        double [] doublearr = new double [toArray.length];
        for (int i = 0; i < doublearr.length; i++){
            doublearr[i] = (Double) toArray[i];
        }
        
        double[] sorted_arr = Maths.bubbleSort(doublearr);
        if(Debug){
            Print.println("  RND the sorted double size " + sorted_arr.length);
            Print.print_1D("  RND the sorted double vector array in funciton 5 ", sorted_arr);
            Print.println("  RND the calculated distance vector size "+ dist_vector.length);
            Print.print_1D("  RND the calculated distance vector ", dist_vector);
        }
        int sun_counter = 0;
        RandomEngine enginenodesel = new DRand((int)(System.currentTimeMillis()));
        Uniform uniNodes = new Uniform(enginenodesel); 
        while(sun_counter<number_of_suns)
        {
            int i = uniNodes.nextIntFromTo(1, sorted_arr.length - 1);
            if(Debug){
                Print.println("  random : " + i);
                Print.println("  sorted_size: " + sorted_arr.length);
            }
            
            if(i>0 && sorted_arr[i] != 0 && !suns.contains(sorted_arr[i])){
                sun_counter++;
//                Print.println("  sorted_arr[i]: " + sorted_arr[i]);
                suns.add(distance_hash.get(sorted_arr[i]));
            }
            
        }
        if(Debug)
        Print.println(" random  sun nodes to string: " + suns.toString());
        
        return suns;
    }
        
   
}



