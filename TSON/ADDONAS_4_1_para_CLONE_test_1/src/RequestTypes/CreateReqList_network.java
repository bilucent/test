/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package RequestTypes;
//these librares were added for calculating random distributions
import Infrastructure.Network;
import Infrastructure.ResourceOperations;
import cern.jet.random.Exponential;
import cern.jet.random.Poisson;
import cern.jet.random.Uniform;
import cern.jet.random.engine.DRand;
import cern.jet.random.engine.RandomEngine;
import java.sql.Array;
import java.util.*;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.apache.commons.math.random.JDKRandomGenerator;
import org.apache.commons.math.stat.StatUtils;
import utilities.Converters;
import utilities.Maths;
import utilities.Print;
/**
 *
 * @author Bijan
 */
public class CreateReqList_network {
    boolean Debug;
    Print P = new Print();
    
    public CreateReqList_network(boolean debug){
        this.Debug =debug;
    }
    public HashMap<Integer,Req_NET_timed> create_reqs(Network network, int Number_of_reqs
            , int bitrate, int QoS, int duration, String algorithm_form,String[] req_types, int[] types_weighs)
    {
        
        
        int src;
        int dest;
        int bw;
        int qos;
        String ReqType;        
        
        HashMap req_hash = new HashMap();
                
        List net_node_ids = network.getmNodesIDs();
        int[] node_id_array = Converters.convert_Int_List_to_1Darray(net_node_ids);
        
        if(Debug)
        P.print_1D("the nodes IDs are converted to int[]:  ", node_id_array);
        int max_node_id =Maths.find_max(node_id_array); 
        if(Debug)
        P.println(" max node id is:" + max_node_id + " numner of reqs:" + Number_of_reqs);
        
        //these infio are not in use yet, I defined the timings out 
        int interval = 5;
        RandomEngine engine = new DRand();
        
        Uniform uniform = new Uniform(0,max_node_id,(int)(System.currentTimeMillis())); 
        Poisson poisson = new Poisson(interval, engine);
        //List<String>  ordered_weighted_types = make_weighted_req_list(Number_of_reqs, req_types, types_weighs);
        List<String>  ordered_weighted_types = make_weighted_ordered_req_list(Number_of_reqs, req_types, types_weighs);
        
        int[] nodes_tobeused ={0,1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,
                                20,21,22,23}; 
//        int[] nodes_tobeused ={0,1,2,3,4,5};        
//        in+t[] nodes_tobeused ={0,1};         
        List<int[]> all_src_dests = create_uniform_src_dest(nodes_tobeused, Number_of_reqs);
        for (int i =0; i < Number_of_reqs; i ++)
        {    
            if(Debug)
            P.println("\n request number:" + i);
            //get a random number in the range, I have added 1
            //since the random funtion does not give 1, so might not get the max       
            src  = -1;
            while (!check_if_num_is_there(src,node_id_array)){
                int random =uniform.nextInt();
                src = (int)random;
 
            } 
            if(Debug)
            P.println("\n selected src:" + src );
            dest  = src;
            while (src==(dest)){
                dest = (int)uniform.nextInt();
            }     


//          int[] src_dest = all_src_dests.get(i);
//          src = src_dest[0];
//          dest = src_dest[1];
//          
            
            
            
//            
//            Uniform uniform2 = new Uniform(0,weighted_types.size()-1,(int)(System.currentTimeMillis())); 
//
//            String req_type = weighted_types.remove(uniform2.nextInt());
            
            String req_type = ordered_weighted_types.get(i);
            List<Integer> Net_req = new ArrayList();
            //Net_req.add(3);
            //Net_req.add(3);
            //Net_req.add(10);
            if(req_type.matches("aa")){
                Net_req.add(0);
                Net_req.add(0);
                Net_req.add(1);
                //Net_req.add(2);      
            }
            else if(req_type.matches("bb")){
                Net_req.add(1);
                //Net_req.add(5);   
            }     
            else if(req_type.matches("cc")){
                Net_req.add(1);
                //Net_req.add(5);   
            }             
            else {
                P.println("\n no such req type to make the BW level for it ");
                System.exit(9131);
            }
            //Net_req.add(1);
            
            List<String> ALG_req = new ArrayList();
            //Net_req.add(3);
            //Net_req.add(3);
            //Net_req.add(10);
            ALG_req.add("M");
            ALG_req.add("M");
            ALG_req.add("M");              
           // NETALG_req.add("M");
            
            int start_time  =  poisson.nextInt();
            //        public Req_NET_timed(String type,int id, int src, int dest, int bw, int qos,  
            //        List Net_req, int start_time, int duration){        

            if(Debug)
            P.print(" Requested algorithm type:" + algorithm_form + " Request type:" 
                    + req_type + " \tid:" +i+ 
                    " \tsrc:" +src+  " \tdest:" +dest+ " \tbitrate:" +bitrate+ 
                    " \tQoS:" +QoS+ " \tstart :" +start_time+ " \tstart :" +duration);
            if(Debug)
            for(int j = 0; j <Net_req.size();j++ )
            {
                P.print(" request D"+j+  "  " + Net_req.get(j));
            }
            if(Debug)
            P.println("\n request number: hello again" + i);
            Req_NET_timed req = new Req_NET_timed(ALG_req,algorithm_form,req_type, i, src, dest, bitrate,
                   QoS,Net_req, start_time,duration);
            req_hash.put(i, req);
             
        }

        return req_hash;
    }
    public List<Req_NET_timed> create_Static_reqs(Network network, 
            int Number_of_reqs, int bitrate, int QoS, int duration, 
            String algorithm_form,String[] req_types, int[] types_weighs, final int src, final int dest)
    {

        int bw;
        int qos;
                
        
        HashMap req_hash = new HashMap();
        List<Req_NET_timed>  req_list = new ArrayList<>();

                
        List net_node_ids = network.getmNodesIDs();
        int[] node_id_array = Converters.convert_Int_List_to_1Darray(net_node_ids);
        
        if(Debug)
        P.print_1D("the nodes IDs are converted to int[]:  ", node_id_array);
        int max_node_id =Maths.find_max(node_id_array); 
        if(Debug)
        P.println(" max node id is:" + max_node_id + " numner of reqs:" + Number_of_reqs);
        int interval = 5;
        RandomEngine engine = new DRand();
        Poisson poisson = new Poisson(interval, engine);
       
        List<String>  weighted_types = make_weighted_req_list(Number_of_reqs, req_types, types_weighs);
        if(Debug)
         P.println("\n request weighted_types:" + weighted_types.toString());
        
        for (int i =0; i < Number_of_reqs; i ++)
        { 
            if(Debug)
            P.println("\n request number:" + i);
            //get a random number in the range, I have added 1
            //since the random funtion does not give 1, so might not get the max       
 

            Uniform uniform = new Uniform(0,weighted_types.size()-1,(int)(System.currentTimeMillis())); 

            String req_type = weighted_types.remove(uniform.nextInt());
            
            
            
            List<Integer> Net_req = new ArrayList();
            //Net_req.add(3);
            //Net_req.add(3);
            //Net_req.add(10);
                    if(req_type.matches("aa")){
                        Net_req.add(0);
                        Net_req.add(0);
                        Net_req.add(2);
                    }
                    else if(req_type.matches("bb")){
                        Net_req.add(0);
                        Net_req.add(4);
                        //Net_req.add(5);   
                    }     
                    else if(req_type.matches("cc")){
                        Net_req.add(0);
                        Net_req.add(8);
                        //Net_req.add(5);   
                    } 
                    else if(req_type.matches("dd")){
                        Net_req.add(0);
                        Net_req.add(4);
                        //Net_req.add(5);   
                    } 
                    else if(req_type.matches("ee")){
                        Net_req.add(0);
                        Net_req.add(6);
                        //Net_req.add(5);   
                    }            
            else {
                P.println("\n no such req type to make the BW level for it ");
                System.exit(9131);
            }
    //Net_req.add(1);
            
            List<String> ALG_req = new ArrayList();
            //Net_req.add(3);
            //Net_req.add(3);
            //Net_req.add(10);
            ALG_req.add("M");
            ALG_req.add("M");
            ALG_req.add("M");              
           // NETALG_req.add("M");
            if(Debug)
            P.println("\n request number: hello" + i);
            int start_time  =  poisson.nextInt();
            //        public Req_NET_timed(String type,int id, int src, int dest, int bw, int qos,  
            //        List Net_req, int start_time, int duration){        


            if(Debug)
            P.print(" Requested algorithm type:" + algorithm_form + " Request type:" 
                    + req_type + " \tid:" +i+ 
                    " \tsrc:" +src+  " \tdest:" +dest+ " \tbitrate:" +bitrate+ 
                    " \tQoS:" +QoS+ " \tstart :" +start_time+ " \tstart :" +duration);
                        for(int j = 0; j <Net_req.size();j++ )
            {
                if(Debug)
                P.print(" request D"+j+  "  " + Net_req.get(j));
            }
            Req_NET_timed req = new Req_NET_timed(ALG_req,algorithm_form, req_type, i, src, dest, bitrate,
                   QoS,Net_req, start_time,duration);
            req_hash.put(i, req);
            req_list.add(req);
        }

        return req_list;
    }    
    public boolean check_if_num_is_there(int number, int array[]){
        boolean found = false;
        for (int i = 0; i < array.length;i++)
        {
            if(array[i]==number)
            {
                found = true;
                if(Debug)
                    P.println("check_if_num_is_there: " + found);
                break;
            }
        }
    return found;    
    } 
    public List<String> make_weighted_req_list(int total,  String[] req_types, int[] types_weighs)
    {
        if(Debug){
            P.print_1D("  in weghted req types ", req_types);
            P.print_1D("  in weghted types_weighs ", types_weighs);
        }
        int all_weighs = 0;
        for(int i =0; i < types_weighs.length;i++)
        {
            all_weighs += types_weighs[i];
        }
        if(Debug)
        P.println("  in weghted all_weighs "+ all_weighs + "   total reqs number: " + total);
        
        List<Integer> separated_agg = new ArrayList<>();
        List<String> all = new ArrayList<>();
        
        if(total>1)
        {
            for(int i =0; i < types_weighs.length;i++)
            {
                int value = (types_weighs[i]*total)/all_weighs;
                if(Debug){
                    P.println("  in value "+ value );
                }
                separated_agg.add(value);
                
            }
            if(Debug)
            P.println("\n request separated_agg:" + separated_agg.toString());

            
            for(int i =0; i < types_weighs.length;i++)
            {
                for(int j =0; j < separated_agg.get(i);j++)
                {
                    all.add(req_types[i]);
                }
            }            
        }
        else
        {
            separated_agg.add(1);
            all.add(req_types[0]);
        }

        P.println("\n request all:" + all.toString());
        return all;
    }    
    public List<String> make_weighted_ordered_req_list(int total,  String[] req_types, int[] types_weighs)
    {
        List<Integer> stacked_weighs = new ArrayList<>();
        if(Debug)
        {
            P.print_1D("\n  in weghted req types ", req_types);
            P.print_1D("  in weghted types_weighs ", types_weighs);
        }
        int all_weighs = 0;
        HashMap<String , int[]> mapping_traffic = new HashMap<>();
        for(int i =0; i < types_weighs.length;i++)
        {
            int map_interval[];
            if(i == 0)
            {
                stacked_weighs.add(types_weighs[i]);
                map_interval = new int[types_weighs[i]];
                for(int g = 0; g<map_interval.length;g++)
                {
                    map_interval[g] = g;
                }
                mapping_traffic.put(req_types[i], map_interval);
            }
            else
            {
                int existing = stacked_weighs.get(i-1);
                int current = types_weighs[i];
                stacked_weighs.add(existing + current);
                map_interval = new int[types_weighs[i]];
                for(int g = 0; g<map_interval.length;g++)
                {
                    map_interval[g] = g+ existing;
                }                
                mapping_traffic.put( req_types[i],map_interval);
            }
            if(Debug)    
            P.print_1D(" MY NEWEST INVENTION! THE MAP INTERVAL ", map_interval);
            all_weighs += types_weighs[i];
        }
        if(Debug)
        Print.println(" making the request list, all_weighs " + all_weighs);
        
        List<String> req_type_in_scal_and_order = new ArrayList<>();
        int counter = 0;
        while(counter<=total)
        {
            for(int i = 0; i < all_weighs; i++)
            {
                String[] type_keys = ResourceOperations.return_rec_keys_str(mapping_traffic);
                if(Debug){
                    Print.println(" type_keys length   " + type_keys.length);
                    Print.print_1D(" type_keys  ", type_keys);
                }

                boolean check = false;
                for(int j = 0; j < type_keys.length;j++)
                {
                    
                    int[] interval =  mapping_traffic.get(type_keys[j]);
                    if(Debug)
                    Print.print_1D(" interval  ", interval);
                    if(check_if_num_is_there(i, interval))
                    {
                        req_type_in_scal_and_order.add(type_keys[j]);
                        if(Debug)
                        Print.println("  the key :rex type is " + type_keys[j] + "  j is: " + j);
                        check = true;
                        counter++;
                    }

                }
                if(!check)
                {
                    Print.println("  problem in making the request list!! , when i is " + i);
                    System.exit(142);
                }
                
            }
            
        }
        if(Debug){
            P.println("\n size of the req_type_in_scal_and_order: " + req_type_in_scal_and_order.size() );
    //
            P.println("\n the req_type_in_scal_and_order:  " + req_type_in_scal_and_order.toString() );
        }
        
        return req_type_in_scal_and_order;
    }
    
    /*
     * this method gives me a uniform src dest asssignmnet 
     */
    public List<int[]> create_uniform_src_dest(int [] nodes_ids, int total)
    {
        List<int[]> src_dest = new ArrayList<>();
        for(int i = 0; i < nodes_ids.length;i++)
        {
            for(int j = 0; j < nodes_ids.length && j!=i; j++)
            {
                int [] arr = {nodes_ids[i],nodes_ids[j]};
                src_dest.add(arr);
            }
        }
        List<int[]> reqnumber_of_src_dests = new ArrayList();
        int counter= 0;
        while(counter<total)
        {
            if(Debug)
            Print.println(" counter   " + counter);
            for(int i = 0; i < src_dest.size(); i++)
            {

                reqnumber_of_src_dests.add(src_dest.get(i));
                counter++;
                
            }
            
        }        
        return  reqnumber_of_src_dests;
        
        
    }
   
//    public HashMap<Integer,Req_NET_timed> create_reqs_node_erlangs(Network network, int number_node_req_iterate
//            , int bitrate, int QoS, int duration, String algorithm_form,
//            String[] req_types, int[] types_weighs, int[] node_id_array
//            ,int erlang_poisson_mean)
//    {
    public List<Req_NET_timed> create_reqs_node_erlangs( int number_reqs,int number_node_req_iterate
            , int bitrate, int QoS, int duration, String algorithm_form,
            String[] req_types, int[] types_weighs, int[] node_id_array
            ,int erlang_poisson_mean,double erlang_exponential_lambda)
    {
        
        
        int src;
        int dest;
        int bw = 0;
        int qos;
        String ReqType;        
        
        //HashMap<Integer,Req_NET_timed> req_hash = new HashMap();
        List<Req_NET_timed>  req_list = new ArrayList<>();
                
        //List net_node_ids = network.getmNodesIDs();
        //int[] node_id_array = Converters.convert_Int_List_to_1Darray(net_node_ids);
        
        if(Debug)
        P.print_1D("the nodes IDs are converted to int[]:  ", node_id_array);
        int max_node_id =Maths.find_max(node_id_array); 
        if(Debug)
        P.println(" max node id is:" + max_node_id + " iterate per node for reqs:" + number_node_req_iterate+"  number_reqs: "+number_reqs);
        
        //these infio are not in use yet, I defined the timings out 
        int interval = erlang_poisson_mean;

        
//        Uniform uniform = new Uniform(0,max_node_id,(int)(System.currentTimeMillis())); 
//        Poisson poisson = new Poisson(numberNodeSelect, engine);
//        Exponential exponential = new Exponential(erlang_exponential_lambda, engine);
//        Poisson poisson_instead_of_exp = new Poisson(erlang_exponential_lambda, engine);
        
        //List<String>  ordered_weighted_types = make_weighted_req_list(number_node_req_iterate, req_types, types_weighs);
        
        
        // this method makes a fair distbution of differn ttypes, considering the allocated weighs
        List<String>  ordered_weighted_types = make_weighted_ordered_req_list(number_reqs, req_types, types_weighs);
        Print.println(" Theordered_weighted_types : "  + ordered_weighted_types);
        Collections.shuffle(ordered_weighted_types, new Random((int)(System.currentTimeMillis())));
        Print.println(" shuffled Theordered_weighted_types : "  + ordered_weighted_types);
//        int []nodes_tobeused2 ={0,1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,
//                                20,21,22,23}; 
//        int[] nodes_tobeused ={0,1,2,3,4,5};        
//        in+t[] nodes_tobeused ={0,1};   
        
        int req_counter = 0;           
//        for(int h =  0; h <number_node_req_iterate  ; h++)
//        {
        for(int h =  0; h <number_reqs  ; h++)
        {
            
            // the time of the requests
           
            
           
            for (int i =0; i < node_id_array.length; i ++)
            {    
                //RandomEngine engine = new DRand(h+i);
                RandomEngine engine = new DRand((int)(System.currentTimeMillis()));
                Poisson poisson = new Poisson(interval, engine);
                Poisson poisson_instead_of_exp = new Poisson(erlang_exponential_lambda, engine);   
                //Uniform uniform = new Uniform(0,max_node_id,(int)(System.currentTimeMillis())); 
                //Random rnd = new Random((int)(System.currentTimeMillis()));
                Random rnd = new Random();
                Uniform uniform = new Uniform(0,max_node_id,rnd.nextInt());  
                // src node, should be,ong to the list, 
                src = node_id_array[i];

                // replacing the distribution with the fixed poisson mean, 
                //to get less randomness
                
                //int srcNode_erlang = poisson.nextInt();
                int srcNode_erlang = interval;
                for(int j = 0; j < srcNode_erlang; j++)
                {
                    dest  = src;
                    boolean node_is_in_array = false;
                    
                    //Exponential exponential = new Exponential(erlang_exponential_lambda, engine);
                                     
                    while (src==(dest)||!node_is_in_array)
                    {
                        dest = (int)uniform.nextInt();
                        node_is_in_array = is_node_in_array(node_id_array, dest);
                    }     

                    //duration = exponential.nextInt();
                    //duration = poisson_instead_of_exp.nextInt();
                    duration = (int)erlang_exponential_lambda;
                    //duration = 100000;
                    String req_type = ordered_weighted_types.get(req_counter);
                    //if(Debug)
                    Print.println(" The reqquest id: "  + req_counter + " The reqquest type: "  + req_type+ " with src: "
                            + src+ " and dest: " + dest + " and starting time: " 
                            + h + "  exponential next " + duration);
                    
                    List<Integer> Net_req = new ArrayList();
                    //Net_req.add(3);
                    //Net_req.add(3);
                    //Net_req.add(10);
                    if(req_type.matches("aa")){
                        Net_req.add(0);
                        Net_req.add(0);
                        Net_req.add(1);
                    }
                    else if(req_type.matches("bb")){
                        Net_req.add(0);
                        Net_req.add(4);
                        //Net_req.add(5);   
                    }     
                    else if(req_type.matches("cc")){
                        Net_req.add(0);
                        Net_req.add(8);
                        //Net_req.add(5);   
                    } 
                    else if(req_type.matches("dd")){
                        Net_req.add(0);
                        Net_req.add(4);
                        //Net_req.add(5);   
                    } 
                    else if(req_type.matches("ee")){
                        Net_req.add(0);
                        Net_req.add(6);
                        //Net_req.add(5);   
                    }                         
                    else {
                        P.println("\n no such req type to make the BW level for it ");
                        System.exit(9131);
                    }
                    //Net_req.add(1);

                    List<String> ALG_req = new ArrayList();
                    ALG_req.add("M");
                    ALG_req.add("M");
                    ALG_req.add("M");              

                    

//                      public Req_NET_timed(List ALG_type, String Algtype, 
//                      String req_type, int id, int src, int dest, int bw,int QOS,  
//                      List Net_req, int start_time, int duration
                    Req_NET_timed req = new Req_NET_timed(ALG_req,algorithm_form,
                            req_type, req_counter, src, dest, bitrate,
                           QoS,Net_req, h,duration);
                    req_list.add(req);
                    //req_hash.put(req_counter, req_NET_IT_starnet);   
                    req_counter++;
                    
                    if(number_reqs < req_counter + 1)
                        return req_list;

                }
             
            }
                
                


            
        }

        for(int i = 0; i < req_list.size(); i ++)
        {
            Print.println("   the new req list: " + req_list.get(i).returnId());
            
        }
        
//        return req_hash;
        return req_list;
    }
   
//    public HashMap<Integer,Req_NET_timed> create_reqs_node_erlangs(Network network, int number_node_req_iterate
//            , int bitrate, int QoS, int duration, String algorithm_form,
//            String[] req_types, int[] types_weighs, int[] node_id_array
//            ,int erlang_poisson_mean)
//    {
    public List<Req_IT_NET_timed> create_reqs_node_erlangs_IT_NET( int number_reqs,
            int number_node_req_iterate
            , int bitrate, int QoS, int duration, String algorithm_form,
            String[] req_types, int[] types_weighs, int[] node_id_array
            ,int erlang_poisson_mean,double erlang_exponential_lambda)
    {
        
        
        int src;
        int dest;
        int bw = 0;
        int qos;
        String ReqType;        
        
        //HashMap<Integer,Req_NET_timed> req_hash = new HashMap();
        List<Req_IT_NET_timed>  req_list = new ArrayList<>();
                
        //List net_node_ids = network.getmNodesIDs();
        //int[] node_id_array = Converters.convert_Int_List_to_1Darray(net_node_ids);
        
        if(Debug)
        P.print_1D("the nodes IDs are converted to int[]:  ", node_id_array);
        int max_node_id =Maths.find_max(node_id_array); 
        if(Debug)
        P.println(" max node id is:" + max_node_id + " iterate per node for reqs:" + number_node_req_iterate+"  number_reqs: "+number_reqs);
        
        //these infio are not in use yet, I defined the timings out 
        int numberNodeSelect = erlang_poisson_mean;

        
//        Uniform uniform = new Uniform(0,max_node_id,(int)(System.currentTimeMillis())); 
//        Poisson poisson = new Poisson(numberNodeSelect, engine);
//        Exponential exponential = new Exponential(erlang_exponential_lambda, engine);
//        Poisson poisson_instead_of_exp = new Poisson(erlang_exponential_lambda, engine);
        
        //List<String>  ordered_weighted_types = make_weighted_req_list(number_node_req_iterate, req_types, types_weighs);
        
        
        // this method makes a fair distbution of differn ttypes, considering the allocated weighs
        List<String>  ordered_weighted_types = make_weighted_ordered_req_list(number_reqs, req_types, types_weighs);
        Print.println(" Theordered_weighted_types : "  + ordered_weighted_types);
        Collections.shuffle(ordered_weighted_types, new Random((int)(System.currentTimeMillis())));
        Print.println(" shuffled Theordered_weighted_types : "  + ordered_weighted_types);
//        int []nodes_tobeused2 ={0,1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,
//                                20,21,22,23}; 
//        int[] nodes_tobeused ={0,1,2,3,4,5};        
//        in+t[] nodes_tobeused ={0,1};   
        
        int req_counter = 0;           
//        for(int h =  0; h <number_node_req_iterate  ; h++)
//        {
        for(int h =  0; h <number_reqs  ; h++)
        {
            
            // the time of the requests
           
            
           
            for (int i =0; i < node_id_array.length; i ++)
            {    
                //RandomEngine engine = new DRand(h+i);
                RandomEngine engine = new DRand((int)(System.currentTimeMillis()));
//                Poisson poisson = new Poisson(numberNodeSelect, engine);
                Poisson poisson_instead_of_exp = new Poisson(erlang_exponential_lambda, engine);   
                //Uniform uniform = new Uniform(0,max_node_id,(int)(System.currentTimeMillis())); 
                //Random rnd = new Random((int)(System.currentTimeMillis()));
                Random rnd = new Random();
                Uniform uniform = new Uniform(0,max_node_id,rnd.nextInt());  
                // src node, should be,ong to the list, 
                src = node_id_array[i];

                // replacing the distribution with the fixed poisson mean, 
                //to get less randomness
                
                //int srcNode_erlang = poisson.nextInt();
                int srcNode_erlang = numberNodeSelect;
                for(int j = 0; j < srcNode_erlang; j++)
                {
                    dest  = src;
                    boolean node_is_in_array = false;
                    
                    //Exponential exponential = new Exponential(erlang_exponential_lambda, engine);
                                     
                    while (src==(dest)||!node_is_in_array)
                    {
                        dest = (int)uniform.nextInt();
                        node_is_in_array = is_node_in_array(node_id_array, dest);
                    }     

                    //duration = exponential.nextInt();
                    //duration = poisson_instead_of_exp.nextInt();
                    duration = (int)erlang_exponential_lambda;
                    //duration = 100000;
                    String req_type = ordered_weighted_types.get(req_counter);
                    //if(Debug)
                    Print.println(" The reqquest id: "  + req_counter + " The reqquest type: "  + req_type+ " with src: "
                            + src+ " and dest: " + dest + " and starting time: " 
                            + h + "  exponential next " + duration);
                    
                    List<Integer> Net_req = new ArrayList();
                    List<Integer> IT_req = new ArrayList();
                    //Net_req.add(3);
                    //Net_req.add(3);
                    //Net_req.add(10);
                    if(req_type.matches("aa")){
                        Net_req.add(0);
                        Net_req.add(0);
                        Net_req.add(1);
                        IT_req.add(1);
                    }
                    else if(req_type.matches("bb")){
                        Net_req.add(0);
                        Net_req.add(4);
                        IT_req.add(1);
                        //Net_req.add(5);   
                    }     
                    else if(req_type.matches("cc")){
                        Net_req.add(0);
                        Net_req.add(8);
                        IT_req.add(1);
                        //Net_req.add(5);   
                    } 
                    else if(req_type.matches("dd")){
                        Net_req.add(0);
                        Net_req.add(4);
                        IT_req.add(1);
                        //Net_req.add(5);   
                    } 
                    else if(req_type.matches("ee")){
                        Net_req.add(0);
                        Net_req.add(6);
                        IT_req.add(1);
                        //Net_req.add(5);   
                    }                         
                    else {
                        P.println("\n no such req type to make the BW level for it ");
                        System.exit(9131);
                    }
                    //Net_req.add(1);

                    List<String> NETALG_req = new ArrayList();
                    NETALG_req.add("M");
                    NETALG_req.add("M");
                    NETALG_req.add("M");              
                    List<String> ITALG_req = new ArrayList();
                    ITALG_req.add("M");
          

                    

//                      public Req_NET_timed(List ALG_type, String Algtype, 
//                      String req_type, int id, int src, int dest, int bw,int QOS,  
//                      List Net_req, int start_time, int duration
                    Req_IT_NET_timed req_NET_IT = new Req_IT_NET_timed(NETALG_req,ITALG_req, algorithm_form,
                            req_type, req_counter, src, dest, bitrate,
                            QoS,
                            Net_req,IT_req, 
                            h,duration);
                    
                    Print.print(" netREQ  " + req_NET_IT.returnNETREQ());
                    Print.print(" ITREQ  " + req_NET_IT.returnITREQ());

                    req_list.add(req_NET_IT);
                    //req_hash.put(req_counter, req_NET_IT_starnet);   
                    req_counter++;
                    
                    if(number_reqs < req_counter + 1)
                return req_list;

                }
             
            }
                
                


            
        }

        for(int i = 0; i < req_list.size(); i ++)
        {
            Print.println("   the new req list: " + req_list.get(i).returnId());
            
        }
        
//        return req_hash;
        return req_list;
    }
    
    /*
     * this is the twin method of create_reqs_node_erlangs_IT_NET
     * now I want to create the requests for a set of nodes. lets say a set of src/dest nodes
     * since the connections needs to be bidirectional
     * 
     */
    public List<Req_IT_NET_timed_startnet> create_reqs_node_erlangs_IT_NET_StarNet( int rounds_of_reqs,
            int number_node_req_iterate, int bitrate, int QoS, int duration, String algorithm_form, 
            String[] req_types, int[] types_weighs, int[] node_id_array, int erlang_poisson_mean, 
            double erlang_exponential_lambda, int minnodes, int maxnodes, int req_number)
    {
        
       
        Random rnd = new Random();

        // Now select how many nodes you wnat in the starnet
 
        int qos;
        String ReqType;        
        
        
        //HashMap<Integer,Req_NET_timed> req_hash = new HashMap();
        List<Req_IT_NET_timed_startnet>  req_list = new ArrayList<>();
                
        //List net_node_ids = network.getmNodesIDs();
        //int[] node_id_array = Converters.convert_Int_List_to_1Darray(net_node_ids);
        
//        if(Debug)
        P.print_1D("the nodes IDs are converted to int[]:  ", node_id_array);
        int max_node_id =Maths.find_max(node_id_array); 
//        if(Debug)
        P.println(" max node id is:" + max_node_id + " iterate per node for reqs:" + number_node_req_iterate+"  number_reqs: "+rounds_of_reqs);
        
        //these infio are not in use yet, I defined the timings out 
        int interval = erlang_poisson_mean;

        
//        Uniform uniform = new Uniform(0,max_node_id,(int)(System.currentTimeMillis())); 
//        Poisson poisson = new Poisson(numberNodeSelect, engine);
//        Exponential exponential = new Exponential(erlang_exponential_lambda, engine);
//        Poisson poisson_instead_of_exp = new Poisson(erlang_exponential_lambda, engine);
        
        //List<String>  ordered_weighted_types = make_weighted_req_list(number_node_req_iterate, req_types, types_weighs);
        
        
        // this method makes a fair distbution of differn ttypes, considering the allocated weighs
        List<String>  ordered_weighted_types = make_weighted_ordered_req_list(req_number, req_types, types_weighs);
        Print.println(" Theordered_weighted_types : "  + ordered_weighted_types);
        
        
        /*
         * no sheffeling, some uncertainty wanted, so later can get the observed data
         */
        
//        Collections.shuffle(ordered_weighted_types, new Random((int)(System.currentTimeMillis())));
        Print.println(" shuffled Theordered_weighted_types : "  + ordered_weighted_types);
//        int []nodes_tobeused2 ={0,1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,
//                                20,21,22,23}; 
//        int[] nodes_tobeused ={0,1,2,3,4,5};        
//        in+t[] nodes_tobeused ={0,1};   
        
        int req_counter = 0;      
        int big_counter = 0;
//        for(int h =  0; h <number_node_req_iterate  ; h++)
//        {
        for(int h =  0; h <rounds_of_reqs && req_counter<req_number ; h+=1)
        {
            
            // the time of the requests
           
            
           
            for (int i =0; i < node_id_array.length && req_counter<req_number; i ++)
            {    

                // replacing the distribution with the fixed poisson mean, 
                //to get less randomness
                
                //int srcNode_erlang = poisson.nextInt();
//                int srcNode_erlang = numberNodeSelect;
                
                
                /*
                 * 
                 * I need the number of stars, so I can add it up
                 * to the already made pool of resources
                 */
                
                    Print.println("big_counter is : " + big_counter++);                
                List starnetnodes = new ArrayList();
                for(int j = 0; j < number_node_req_iterate && req_counter<req_number; j++)
                {
                    

                    Print.println("req_counter is : " + req_counter);
                    RandomEngine enginenodesel = new DRand((int)(System.currentTimeMillis()));
                    Uniform uniNodes = new Uniform(enginenodesel);
                    
                    /*
                     * select a number of start
                     */
                    int numberofstarnetnodes = uniNodes.nextIntFromTo(minnodes, maxnodes);
                           
                    // I am definng the type of the star req using number stars generated
                    String star_req_type = Star_req_types_enum.lookupType(numberofstarnetnodes);
                    
                    int maxnodeid = Maths.find_max(node_id_array);
                    int minnodeid = Maths.find_min(node_id_array);
                    
                    starnetnodes= 
                    node_select_fromSet_norepeat_minmax(Converters.convert_int_arr_to_Arr_list(node_id_array), 
                    numberofstarnetnodes,maxnodeid,minnodeid);

                    Print.println(" starnet nodes selected : "   + starnetnodes.toString());
                    //duration = exponential.nextInt();
                    //duration = poisson_instead_of_exp.nextInt();
                    duration = (int)erlang_exponential_lambda;
                    duration = 1111;
//                    duration = 5;
                    
                    String req_type;
                    try{
                        req_type = ordered_weighted_types.get(req_counter);
                        
                    }
                    catch(IndexOutOfBoundsException e)
                    {
                        break;
                    }
                    //if(Debug)
                    Print.println(" The reqquest id: "  + req_counter + " The reqquest type: "  + req_type+ " with src: "
                            + h + "  exponential next " + duration);
                    
                    List<Integer> Net_req = new ArrayList();
                    List<Integer> IT_req = new ArrayList();
                    //Net_req.add(3);
                    //Net_req.add(3);
                    //Net_req.add(10);
                    if(req_type.matches("aa")){
                        Net_req.add(0);
                        Net_req.add(0);
                        Net_req.add(1);
                        IT_req.add(1);
                    }
                    else if(req_type.matches("bb")){
                        Net_req.add(0);
                        Net_req.add(0);
                        Net_req.add(2);
                        IT_req.add(1);
                        //Net_req.add(5);   
                    }     
                    else if(req_type.matches("cc")){
                        Net_req.add(0);
                        Net_req.add(1);
                        Net_req.add(2);
                        IT_req.add(4);
                        //Net_req.add(5);   
                    } 
                    else if(req_type.matches("dd")){
                        Net_req.add(0);
                        Net_req.add(1);
//                        Net_req.add(4);
                        IT_req.add(1);
                        //Net_req.add(5);   
                    } 
                    else if(req_type.matches("ee")){
                        Net_req.add(0);
                        Net_req.add(1);
                        IT_req.add(1);
                        //Net_req.add(5);   
                    }                         
                    else {
                        P.println("\n no such req type to make the BW level for it ");
                        System.exit(9131);
                    }
                    //Net_req.add(1);

                    List<String> NETALG_req = new ArrayList();
                    NETALG_req.add("M");
                    NETALG_req.add("M");
                    NETALG_req.add("M");              
                    List<String> ITALG_req = new ArrayList();
                    ITALG_req.add("M");
          

                    

//                      public Req_NET_timed(List ALG_type, String Algtype, 
//                      String req_type, int id, int src, int dest, int bw,int QOS,  
//                      List Net_req, int start_time, int duration
                    Req_IT_NET_timed_startnet req_NET_IT_starnet = new Req_IT_NET_timed_startnet(
                            NETALG_req,ITALG_req, algorithm_form,
                            req_type, 
                            star_req_type, 
                            req_counter, starnetnodes, bitrate,
                            QoS,
                            Net_req,IT_req, 
                            big_counter,duration);
                    
                    Print.print(" netREQ  " + req_NET_IT_starnet.returnNETREQ());
                    Print.print(" ITREQ  " + req_NET_IT_starnet.returnITREQ());

                    req_list.add(req_NET_IT_starnet);
                    //req_hash.put(req_counter, req_NET_IT_starnet);   
                    
                    
//                    if(rounds_of_reqs < req_counter + 1)
                    Print.println("  keep an eye on the numbe rof reqs being biult" 
                            + req_counter + " and the max is: " + req_number);
                    if(req_number < req_counter + 1)
                    return req_list;

                    req_counter++;// = req_number + starnetnodes.size();

//                    h+=5;
                }
//             req_counter = req_counter + starnetnodes.size();
            }
                
                


            
        }

        for(int i = 0; i < req_list.size(); i ++)
        {
            Print.println("   Biulding the req list: " + req_list.get(i).returnId());
            
        }
        
//        return req_hash;
        return req_list;
    }
    /*
     * this is the twin method of create_reqs_node_erlangs_IT_NET
     * now I want to create the requests for a set of nodes. lets say a set of src/dest nodes
     * since the connections needs to be bidirectional
     * 
     */
    public List<Req_IT_NET_timed_startnet> create_reqs_node_erlangs_IT_NET_StarNet_2( int rounds_of_reqs,
            int number_node_req_iterate, int bitrate, int QoS, int duration, String algorithm_form, 
            String[] star_req_types, int[] star_types_weighs,
            String[] req_types, int[] types_weighs,
            int[] node_id_array, int erlang_poisson_mean, 
            double erlang_exponential_lambda, int minnodes, int maxnodes, int req_number)
    {
        
       
        Random rnd = new Random();

        // Now select how many nodes you wnat in the starnet
 
        int qos;
        String ReqType;        
        
        
        //HashMap<Integer,Req_NET_timed> req_hash = new HashMap();
        List<Req_IT_NET_timed_startnet>  req_list = new ArrayList<>();
                
        //List net_node_ids = network.getmNodesIDs();
        //int[] node_id_array = Converters.convert_Int_List_to_1Darray(net_node_ids);
        
        if(Debug)
        P.print_1D("the nodes IDs are converted to int[]:  ", node_id_array);
        int max_node_id =Maths.find_max(node_id_array); 
        if(Debug)
        P.println(" max node id is:" + max_node_id + " iterate per node for reqs:" + number_node_req_iterate+"  number_reqs: "+rounds_of_reqs);
        
        //these infio are not in use yet, I defined the timings out 
        int interval = erlang_poisson_mean;

        
//        Uniform uniform = new Uniform(0,max_node_id,(int)(System.currentTimeMillis())); 
//        Poisson poisson = new Poisson(numberNodeSelect, engine);
//        Exponential exponential = new Exponential(erlang_exponential_lambda, engine);
//        Poisson poisson_instead_of_exp = new Poisson(erlang_exponential_lambda, engine);
        
        //List<String>  ordered_weighted_types = make_weighted_req_list(number_node_req_iterate, req_types, types_weighs);
        
        
        // this method makes a fair distbution of differn ttypes, considering the allocated weighs
//        List<String>  ordered_weighted_types = make_weighted_ordered_req_list(req_number, req_types, types_weighs);
        List<String>  ordered_weighted_types = make_weighted_ordered_req_list(req_number, star_req_types, star_types_weighs);
//        Print.println(" Theordered_weighted_types : "  + ordered_weighted_types);
        Collections.shuffle(ordered_weighted_types, new Random((int)(System.currentTimeMillis())));
//        Print.println(" shuffled Theordered_weighted_types : "  + ordered_weighted_types);
//        int []nodes_tobeused2 ={0,1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,
//                                20,21,22,23}; 
//        int[] nodes_tobeused ={0,1,2,3,4,5};        
//        in+t[] nodes_tobeused ={0,1};   
        
        int req_counter = 0;      
        int big_counter = 0;
//        for(int h =  0; h <number_node_req_iterate  ; h++)
//        {
        for(int h =  0; h <rounds_of_reqs && req_counter<req_number ; h+=1)
        {
            
            // the time of the requests
           
            
                            
            for (int i =0; i < node_id_array.length && req_counter<req_number; i ++)
            {    

                // replacing the distribution with the fixed poisson mean, 
                //to get less randomness
                
                //int srcNode_erlang = poisson.nextInt();
//                int srcNode_erlang = numberNodeSelect;
                
                
                /*
                 * 
                 * I need the number of stars, so I can add it up
                 * to the already made pool of resources
                 */
               
                List starnetnodes = new ArrayList();
                for(int j = 0; j < number_node_req_iterate && req_counter<req_number; j++)
                {
                     

                    Print.println("req_counter is : " + req_counter);
                    RandomEngine enginenodesel = new DRand((int)(System.currentTimeMillis()));
                    Uniform uniNodes = new Uniform(enginenodesel);
                    
                    /*
                     * select a number of start
                     */
//                    int numberofstarnetnodes = uniNodes.nextIntFromTo(minnodes, maxnodes);
                           
                    // I am definng the type of the star req using number stars generated
//                    String star_req_type = Star_req_types_enum_2.lookupLinkType(numberofstarnetnodes);
                    

                    //duration = exponential.nextInt();
                    //duration = poisson_instead_of_exp.nextInt();
                    duration = (int)erlang_exponential_lambda;
//                    duration = 111111;
//                    duration = 1;
                    
                    String star_req_type;
                    try{
                        star_req_type = ordered_weighted_types.get(req_counter);
                        
                    }
                    catch(IndexOutOfBoundsException e)
                    {
                        break;
                    }
                    if(Debug)
                    Print.println(" The reqquest id: "  + req_counter + " The reqquest type: "  + star_req_type+ " with src: "
                            + h + "  exponential duration " + duration);
                    
                    
                    /*
                     * the following lines are created to make a realtion between 
                     * the star type as the general type and then the allocation
                     * algorithm types which are to used for end to end setups
                     */
                    List<Integer> Net_req = new ArrayList();
                    List<Integer> IT_req = new ArrayList();
                    List<String> NETALG_req = new ArrayList();          
                    List<String> ITALG_req = new ArrayList();
                    
                    //Net_req.add(3);
                    //Net_req.add(3);
                    //Net_req.add(10);
                    
                    int numberofstarnetnodes = Star_req_types_enum_2.lookupNumStars(star_req_type);   
                    int maxnodeid = Maths.find_max(node_id_array);
                    int minnodeid = Maths.find_min(node_id_array);
                    
//                    starnetnodes= 
//                    node_select_fromSet_norepeat_minmax(Converters.convert_int_arr_to_Arr_list(node_id_array), 
//                    numberofstarnetnodes,maxnodeid,minnodeid);
                    
                    starnetnodes= 
                    node_select_fromSet_norepeat_minmax(Converters.convert_int_arr_to_Arr_list(node_id_array), 
                    numberofstarnetnodes,maxnodeid,minnodeid);

                    
                    
                    Print.println(" I CARE: starnet nodes selected : "   + starnetnodes.toString() + " and star_req_type is: " + star_req_type);                    
                    
                    String req_type = Star_req_types_enum_2.lookupLinkType(star_req_type);
                    if(req_type.equalsIgnoreCase("aa"))
                    {
                        Net_req.add(0);
                        Net_req.add(0);
                        Net_req.add(Star_req_types_enum_2.lookupNetReq(star_req_type));  
                        NETALG_req.add("M");
                        NETALG_req.add("M");
                        NETALG_req.add("M");    
                        ITALG_req.add("M");                    
                    }
                    else if(req_type.equalsIgnoreCase("bb"))
                    {
                        Net_req.add(0);
                        Net_req.add(Star_req_types_enum_2.lookupNetReq(star_req_type));  
                        NETALG_req.add("M");
                        NETALG_req.add("M");        
                        ITALG_req.add("M");                    
                    }
                    else 
                    {
                        P.println("  problem in finding link types ");
                        System.exit(113434);
                    }

                    IT_req.add(Star_req_types_enum_2.lookupITReq(star_req_type));
                    
//                    Print.println(" (Star_req_types_enum_2.lookupNetReq(star_req_type)) : "   + (Star_req_types_enum_2.lookupNetReq(star_req_type)));                    
//                    Print.println(" (Star_req_types_enum_2.lookupITReq(star_req_type)) : "   + (Star_req_types_enum_2.lookupITReq(star_req_type)));                    
//                    
//                    if(star_req_type.matches("a")){
//                        Net_req.add(0);
//                        Net_req.add(0);
//                        Net_req.add(1);
//                        IT_req.add(1);
//                    }
//                    else if(star_req_type.matches("bb")){
//                        Net_req.add(0);
//                        Net_req.add(0);
//                        Net_req.add(2);
//                        IT_req.add(1);
//                        //Net_req.add(5);   
//                    }     
//                    else if(req_type.matches("cc")){
//                        Net_req.add(0);
//                        Net_req.add(1);
//                        Net_req.add(2);
//                        IT_req.add(4);
//                        //Net_req.add(5);   
//                    } 
//                    else if(req_type.matches("dd")){
//                        Net_req.add(0);
//                        Net_req.add(1);
////                        Net_req.add(4);
//                        IT_req.add(1);
//                        //Net_req.add(5);   
//                    } 
//                    else if(req_type.matches("ee")){
//                        Net_req.add(0);
//                        Net_req.add(1);
//                        IT_req.add(1);
//                        //Net_req.add(5);   
//                    }                         
//                    else {
//                        P.println("\n no such req type to make the BW level for it ");
//                        System.exit(9131);
//                    }
//                    //Net_req.add(1);


          

                    

//                      public Req_NET_timed(List ALG_type, String Algtype, 
//                      String req_type, int id, int src, int dest, int bw,int QOS,  
//                      List Net_req, int start_time, int duration
                    Req_IT_NET_timed_startnet req_NET_IT_starnet = new Req_IT_NET_timed_startnet(
                            NETALG_req,ITALG_req, algorithm_form,
                            req_type, 
                            star_req_type, 
                            req_counter, starnetnodes, bitrate,
                            QoS,
                            Net_req,IT_req, 
                            big_counter,duration);
                    
//                    Print.print(" netREQ  " + req_NET_IT_starnet.returnNETREQ());
//                    Print.print(" ITREQ  " + req_NET_IT_starnet.returnITREQ());

                    req_list.add(req_NET_IT_starnet);
                    //req_hash.put(req_counter, req_NET_IT_starnet);   
                    
                    
//                    if(rounds_of_reqs < req_counter + 1)
//                    Print.println("  keep an eye on the numbe rof reqs being biult" 
//                            + req_counter + " and the max is: " + req_number);
                    if(req_number < req_counter + 1)
                    return req_list;

                    req_counter++;// = req_number + starnetnodes.size();
//                    h+=5;
                }
                Print.println("big_counter is : " + big_counter++);                 

//             req_counter = req_counter + starnetnodes.size();
            }
              
                
                


            
        }

        for(int i = 0; i < req_list.size(); i ++)
        {
            Print.println("   Biulding the req list: " + req_list.get(i).returnId());
            
        }
        
//        return req_hash;
        return req_list;
    }
    /*
     * this is the twin method of create_reqs_node_erlangs_IT_NET
     * now I want to create the requests for a set of nodes. lets say a set of src/dest nodes
     * since the connections needs to be bidirectional
     * 
     */
    public List<Req_IT_NET_timed_startnet> create_reqs_node_erlangs_IT_NET_StarNet_2_badseed( int rounds_of_reqs,
            int number_node_req_iterate, int bitrate, int QoS, int duration, String algorithm_form, 
            String[] star_req_types, int[] star_types_weighs,
            String[] req_types, int[] types_weighs,
            int[] node_id_array, int erlang_poisson_mean, 
            double erlang_exponential_lambda, int minnodes, int maxnodes, int req_number,
            
            
            //this seed changes per each iterate of same erlang. but the order and
            //repeats will be identical everytime
            int badseed)
    {
        
       
        Random rnd = new Random();

        // Now select how many nodes you wnat in the starnet
 
        int qos;
        String ReqType;        
        
        
        //HashMap<Integer,Req_NET_timed> req_hash = new HashMap();
        List<Req_IT_NET_timed_startnet>  req_list = new ArrayList<>();
                
        //List net_node_ids = network.getmNodesIDs();
        //int[] node_id_array = Converters.convert_Int_List_to_1Darray(net_node_ids);
        
        if(Debug)
        P.print_1D("the nodes IDs are converted to int[]:  ", node_id_array);
        int max_node_id =Maths.find_max(node_id_array); 
        if(Debug)
        P.println(" max node id is:" + max_node_id + " iterate per node for reqs:" + number_node_req_iterate+"  number_reqs: "+rounds_of_reqs);
        
        //these infio are not in use yet, I defined the timings out 
        int interval = erlang_poisson_mean;

        
//        Uniform uniform = new Uniform(0,max_node_id,(int)(System.currentTimeMillis())); 
//        Poisson poisson = new Poisson(numberNodeSelect, engine);
//        Exponential exponential = new Exponential(erlang_exponential_lambda, engine);
//        Poisson poisson_instead_of_exp = new Poisson(erlang_exponential_lambda, engine);
        
        //List<String>  ordered_weighted_types = make_weighted_req_list(number_node_req_iterate, req_types, types_weighs);
        
        
        // this method makes a fair distbution of differn ttypes, considering the allocated weighs
//        List<String>  ordered_weighted_types = make_weighted_ordered_req_list(req_number, req_types, types_weighs);
        List<String>  ordered_weighted_types = make_weighted_ordered_req_list(req_number, star_req_types, star_types_weighs);
//        Print.println(" Theordered_weighted_types : "  + ordered_weighted_types);
//        Collections.shuffle(ordered_weighted_types, new Random((int)(System.currentTimeMillis())));
//        Print.println(" shuffled Theordered_weighted_types : "  + ordered_weighted_types);
//        int []nodes_tobeused2 ={0,1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,
//                                20,21,22,23}; 
//        int[] nodes_tobeused ={0,1,2,3,4,5};        
//        in+t[] nodes_tobeused ={0,1};   
        
        int req_counter = 0;      
        int big_counter = 0;
//        for(int h =  0; h <number_node_req_iterate  ; h++)
//        {
        for(int h =  0; h <rounds_of_reqs && req_counter<req_number ; h+=1)
        {
            
            // the time of the requests
           
            
                            
            for (int i =0; i < node_id_array.length && req_counter<req_number; i ++)
            {    

                // replacing the distribution with the fixed poisson mean, 
                //to get less randomness
                
                //int srcNode_erlang = poisson.nextInt();
//                int srcNode_erlang = numberNodeSelect;
                
                
                /*
                 * 
                 * I need the number of stars, so I can add it up
                 * to the already made pool of resources
                 */
               
                List starnetnodes = new ArrayList();
                for(int j = 0; j < number_node_req_iterate && req_counter<req_number; j++)
                {
                     

                    Print.println("req_counter is : " + req_counter);
                    RandomEngine enginenodesel = new DRand((int)(System.currentTimeMillis()));
                    Uniform uniNodes = new Uniform(enginenodesel);
                    
                    /*
                     * select a number of start
                     */
//                    int numberofstarnetnodes = uniNodes.nextIntFromTo(minnodes, maxnodes);
                           
                    // I am definng the type of the star req using number stars generated
//                    String star_req_type = Star_req_types_enum_2.lookupLinkType(numberofstarnetnodes);
                    

                    //duration = exponential.nextInt();
                    //duration = poisson_instead_of_exp.nextInt();
                    duration = (int)erlang_exponential_lambda;
//                    duration = 111111;
//                    duration = 1;
                    
                    String star_req_type;
                    try{
                        star_req_type = ordered_weighted_types.get(req_counter);
                        
                    }
                    catch(IndexOutOfBoundsException e)
                    {
                        break;
                    }
                    if(Debug)
                    Print.println(" The reqquest id: "  + req_counter + " The reqquest type: "  + star_req_type+ " with src: "
                            + h + "  exponential duration " + duration);
                    
                    
                    /*
                     * the following lines are created to make a realtion between 
                     * the star type as the general type and then the allocation
                     * algorithm types which are to used for end to end setups
                     */
                    List<Integer> Net_req = new ArrayList();
                    List<Integer> IT_req = new ArrayList();
                    List<String> NETALG_req = new ArrayList();          
                    List<String> ITALG_req = new ArrayList();
                    
                    //Net_req.add(3);
                    //Net_req.add(3);
                    //Net_req.add(10);
                    
                    int numberofstarnetnodes = Star_req_types_enum_2.lookupNumStars(star_req_type);   
                    int maxnodeid = Maths.find_max(node_id_array);
                    int minnodeid = Maths.find_min(node_id_array);
                    
//                    starnetnodes= 
//                    node_select_fromSet_norepeat_minmax(Converters.convert_int_arr_to_Arr_list(node_id_array), 
//                    numberofstarnetnodes,maxnodeid,minnodeid);
                    
                    starnetnodes= 
                    node_select_fromSet_norepeat_minmax_badseed(Converters.convert_int_arr_to_Arr_list(node_id_array), 
                    numberofstarnetnodes,maxnodeid,minnodeid, 
                    
                    //this one goes as the seed
                    badseed);

                    
                    
                    Print.println(" I CARE: starnet nodes selected : "   + starnetnodes.toString() + " and star_req_type is: " + star_req_type);                    
                    
                    String req_type = Star_req_types_enum_2.lookupLinkType(star_req_type);
                    if(req_type.equalsIgnoreCase("aa"))
                    {
                        Net_req.add(0);
                        Net_req.add(0);
                        Net_req.add(Star_req_types_enum_2.lookupNetReq(star_req_type));  
                        NETALG_req.add("M");
                        NETALG_req.add("M");
                        NETALG_req.add("M");    
                        ITALG_req.add("M");                    
                    }
                    else if(req_type.equalsIgnoreCase("bb"))
                    {
                        Net_req.add(0);
                        Net_req.add(Star_req_types_enum_2.lookupNetReq(star_req_type));  
                        NETALG_req.add("M");
                        NETALG_req.add("M");        
                        ITALG_req.add("M");                    
                    }
                    else 
                    {
                        P.println("  problem in finding link types ");
                        System.exit(113434);
                    }

                    IT_req.add(Star_req_types_enum_2.lookupITReq(star_req_type));
                    
//                    Print.println(" (Star_req_types_enum_2.lookupNetReq(star_req_type)) : "   + (Star_req_types_enum_2.lookupNetReq(star_req_type)));                    
//                    Print.println(" (Star_req_types_enum_2.lookupITReq(star_req_type)) : "   + (Star_req_types_enum_2.lookupITReq(star_req_type)));                    
//                    
//                    if(star_req_type.matches("a")){
//                        Net_req.add(0);
//                        Net_req.add(0);
//                        Net_req.add(1);
//                        IT_req.add(1);
//                    }
//                    else if(star_req_type.matches("bb")){
//                        Net_req.add(0);
//                        Net_req.add(0);
//                        Net_req.add(2);
//                        IT_req.add(1);
//                        //Net_req.add(5);   
//                    }     
//                    else if(req_type.matches("cc")){
//                        Net_req.add(0);
//                        Net_req.add(1);
//                        Net_req.add(2);
//                        IT_req.add(4);
//                        //Net_req.add(5);   
//                    } 
//                    else if(req_type.matches("dd")){
//                        Net_req.add(0);
//                        Net_req.add(1);
////                        Net_req.add(4);
//                        IT_req.add(1);
//                        //Net_req.add(5);   
//                    } 
//                    else if(req_type.matches("ee")){
//                        Net_req.add(0);
//                        Net_req.add(1);
//                        IT_req.add(1);
//                        //Net_req.add(5);   
//                    }                         
//                    else {
//                        P.println("\n no such req type to make the BW level for it ");
//                        System.exit(9131);
//                    }
//                    //Net_req.add(1);


          

                    

//                      public Req_NET_timed(List ALG_type, String Algtype, 
//                      String req_type, int id, int src, int dest, int bw,int QOS,  
//                      List Net_req, int start_time, int duration
                    Req_IT_NET_timed_startnet req_NET_IT_starnet = new Req_IT_NET_timed_startnet(
                            NETALG_req,ITALG_req, algorithm_form,
                            req_type, 
                            star_req_type, 
                            req_counter, starnetnodes, bitrate,
                            QoS,
                            Net_req,IT_req, 
                            big_counter,duration);
                    
//                    Print.print(" netREQ  " + req_NET_IT_starnet.returnNETREQ());
//                    Print.print(" ITREQ  " + req_NET_IT_starnet.returnITREQ());

                    req_list.add(req_NET_IT_starnet);
                    //req_hash.put(req_counter, req_NET_IT_starnet);   
                    
                    
//                    if(rounds_of_reqs < req_counter + 1)
//                    Print.println("  keep an eye on the numbe rof reqs being biult" 
//                            + req_counter + " and the max is: " + req_number);
                    if(req_number < req_counter + 1)
                    return req_list;

                    req_counter++;// = req_number + starnetnodes.size();
//                    h+=5;
                }
                                

//             req_counter = req_counter + starnetnodes.size();
            }
            Print.println("big_counter is : " + big_counter++);   
                
                


            
        }

        for(int i = 0; i < req_list.size(); i ++)
        {
            Print.println("   Biulding the req list: " + req_list.get(i).returnId());
            
        }
        
//        return req_hash;
        return req_list;
    }
    public boolean is_node_in_array(int[] array, int node_id)
    {
        boolean found = false;
        for(int i = 0;i < array.length; i++)
        {
            if(array[i]==node_id)
            {
                found = true;
                return found;
            }
        }
        return found;
    }
    
//    
//    public boolean Match(int[]array, int number)
//    {
//        boolean found = false;
//        for(int i = 0; i < array.length; i++)
//        {
//            if(array[i]==number)
//            {
//                found = true;
//                break;
//            }
//        }
//        return found;        
//    }
    
    public static List<Integer> node_select_fromSet_norepeat_minmax(List<Integer> setlist,
            int nodeswanted, int max,int min)
    {
        List nodes = new ArrayList();
        List<Integer> setCopy = new ArrayList<>();
        setCopy = setlist;
        System.out.println("\nset of nodes " + setlist.toString());
        System.out.println("\nset min and max nodes  " + min + " "+ max);
        
        RandomEngine enginenodesel = new DRand((int)(System.currentTimeMillis()));
        Uniform uniNodes = new Uniform(enginenodesel);  
        int node_counter = 0;  
        while(node_counter<nodeswanted)
        {
            System.out.println(" setCopy " + setCopy.toString());

            //
            int nextint = uniNodes.nextIntFromTo(min, max);
            if(setCopy.contains(nextint))
            {
                node_counter++;
                System.out.println(" niNodes.nextInt() " +nextint);
                nodes.add(setCopy.remove((setCopy.indexOf(nextint))));
            }
        }
        System.out.println(" the selected starnet nodes are " + nodes.toString());
        return nodes;
    }
    public static List<Integer> node_select_fromSet_norepeat_minmax_badseed(List<Integer> setlist,
            int nodeswanted, int max,int min, int seed)
    {
        List nodes = new ArrayList();
        List<Integer> setCopy = new ArrayList<>();
        setCopy = setlist;
//        System.out.println("\nset of nodes " + setlist.toString());
//        System.out.println("\nset min and max nodes  " + min + " "+ max);
        
        RandomEngine enginenodesel = new DRand(seed);
        Uniform uniNodes = new Uniform(enginenodesel);  
        int node_counter = 0;  
        while(node_counter<nodeswanted)
        {
//            System.out.println(" setCopy " + setCopy.toString());

            //
            int nextint = uniNodes.nextIntFromTo(min, max);
            if(setCopy.contains(nextint))
            {
                node_counter++;
                System.out.println(" niNodes.nextInt() " +nextint);
                nodes.add(setCopy.remove((setCopy.indexOf(nextint))));
            }
        }
//        System.out.println(" the selected starnet nodes are " + nodes.toString());
        return nodes;
    }
    
    
    public static List<Integer> node_select_fromSet_norepeat(List<Integer> setlist,
            int nodeswanted)
    {
        List nodes = new ArrayList();
        List<Integer> setCopy = new ArrayList<>();
        setCopy = setlist;
        System.out.println("\nset of nodes " + setlist.toString());
        System.out.println("\nset min and max nodes are the whole span ");
        
        RandomEngine enginenodesel = new DRand((int)(System.currentTimeMillis()));
        Uniform uniNodes = new Uniform(enginenodesel);        
        for(int i = 0; i < nodeswanted; i++)
        {
            System.out.println(" setCopy " + setCopy.toString());

            //
            int nextint = uniNodes.nextIntFromTo(0, setCopy.size());
            if(setCopy.contains(nextint))
            {
                System.out.println(" niNodes.nextInt() " +nextint);
                nodes.add(setCopy.remove((setCopy.indexOf(nextint))));
            }
        }
        System.out.println(" the selected starnet nodes are " + nodes.toString());
        return nodes;
    }
  
    public enum Star_req_types_enum{
        A (2,"a"),B (3,"b"),C (4,"c"),D (5,"d"),E (6,"e");
        
        private  String type;
        private  int value;
        
        private Star_req_types_enum( int v , String t) { type = t; value = v;  }    
        
        private static final HashMap<Integer, String> lookup = new HashMap();
        static {
            for (Star_req_types_enum s : Star_req_types_enum.values())
                lookup.put( s.getvalue(),s.gettype());
        }        

        public  String gettype() { return type; }
        
        public  int getvalue() { return value; }
        
        public static String lookupType(int value) {
            return lookup.get(value);
        }            
        
    }     
  
    public enum Star_req_types_enum_2{
        A (3,"a","aa",1,1),B (3,"b","aa",4,2),C (3,"c", "bb",1,0),
        D (5,"d","aa",1,1),E (5,"e","aa",4,2),F (5,"f", "bb",1,0);
        
        private  String startype;
        private  String link_type;
        private  int number_of_stars;
        private  int Net_req;
        private  int IT_req;
        
        private Star_req_types_enum_2( int v , String st, String lt, int nreq, int ireq) 
        { startype = st; link_type= lt; number_of_stars = v; Net_req =  nreq; IT_req =  ireq; }    
        
//        private static final HashMap<Integer, String> lookup = new HashMap();
//        static {
//            for (Star_req_types_enum s : Star_req_types_enum.values())
//                lookup.put( s.getvalue(),s.gettype());
//        }        
        
     

        public  String getstartype() { return startype; }
        
        public  String getlinktype() { return link_type; }
        
        public  int getstarNumvalue() { return number_of_stars; }
        
        public  int getitreq() { return IT_req; }
        
        public  int getnetreq() { return Net_req; }
        
        
           
        private static final HashMap<String, String> lookup_starType_linkType = new HashMap();
        public static String lookupLinkType(String startype) {
            return lookup_starType_linkType.get(startype);
        }            
                     
        
        
        private static final HashMap<String, Integer> lookup_starType_numberStars = new HashMap();
        public static int lookupNumStars(String startype) {
            return lookup_starType_numberStars.get(startype);
        }            
             

           
        private static final HashMap<String, Integer> lookup_starType_netreq = new HashMap();
        public static int lookupNetReq(String startype) {
            return lookup_starType_netreq.get(startype);
        }            
             
          
           
        private static final HashMap<String, Integer> lookup_starType_itreq = new HashMap();
        public static int lookupITReq(String startype) {
            return lookup_starType_itreq.get(startype);
        } 
        
        
        static {
            for (Star_req_types_enum_2 s : Star_req_types_enum_2.values())
            {
                lookup_starType_itreq.put( s.getstartype(),s.getitreq());
                lookup_starType_netreq.put( s.getstartype(),s.getnetreq());
                lookup_starType_numberStars.put( s.getstartype(),s.getstarNumvalue());
                lookup_starType_linkType.put( s.getstartype(),s.getlinktype());
            }

        }           
           
        
    }     
 
//  public enum Day { 
//        MONDAY("M"), TUESDAY("T"), WEDNESDAY("W"),
//        THURSDAY("R"), FRIDAY("F"), SATURDAY("Sa"), SUNDAY("Su"), ;
//
//        private final String abbreviation;
//        // Reverse-lookup map for getting a day from an abbreviation
//        private static final Map<String, Day> lookup = new HashMap<String, Day>();
//        static {
//            for (Day d : Day.values())
//                lookup.put(d.getAbbreviation(), d);
//        }
//
//        private Day(String abbreviation) {
//            this.abbreviation = abbreviation;
//        }
//
//        public String getAbbreviation() {
//            return abbreviation;
//        }
//
//        public static Day get(String abbreviation) {
//            return lookup.get(abbreviation);
//        }
//    }
}
