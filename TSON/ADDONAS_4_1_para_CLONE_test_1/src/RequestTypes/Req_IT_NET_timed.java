/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package RequestTypes;

import java.util.List;
import utilities.Print;

/**
 *
 * @author Bijan
 */
public class Req_IT_NET_timed extends Request_base {

        public Req_IT_NET_timed(List NET_ALG_type,List IT_ALG_type, String Algtype, String req_type, int id, int src, int dest, int bw,int QOS,  
                List Net_req,List IT_req, int start_time, int duration){
            
        this.request_class = "Req_IT_NET_timed";    
            
        this.mAlgorithmType = Algtype;
        this.Connection_ReqType = req_type;
        this.mId = id;
        this.mBW = bw;
        this.mDest = dest;
        this.mSrc = src;
        this.mQOS = QOS;
        this.Net_requirements = Net_req;
        this.IT_requirements = IT_req;
        this.Start_time = start_time;
        this.Call_duration = duration;
        this.End_time = this.Start_time + this.Call_duration;
        this.NET_ALG_req =NET_ALG_type;
        this.IT_ALG_req =IT_ALG_type;
                
    }
        public Req_IT_NET_timed(Req_IT_NET_timed_startnet  starreq, int id, int src, int dest){
            
        this.request_class = "Req_IT_NET_timed";    
            
        this.mAlgorithmType = starreq.returnAlgorithmType();
        this.Connection_ReqType = starreq.returnConnectionReqType();
        this.mId = starreq.returnId();
        this.mBW = starreq.returnBW();
        this.mDest = dest;
        this.mSrc = src;
        this.mQOS = starreq.returnQOS();
        this.Net_requirements = starreq.Net_requirements;
        this.IT_requirements = starreq.IT_requirements;
        this.Start_time = starreq.returnStartTime();
        this.Call_duration = starreq.returnDurationTime();
        this.End_time = starreq.returnStartTime() + 
                starreq.returnDurationTime();
        this.NET_ALG_req =starreq.get_NET_ALG_types();
        this.IT_ALG_req =starreq.get_IT_ALG_types();
                
    }
    @Override
    public void displayAttributes(){
        
       Print.println("\n{{{{{{{{{{{  DISPLAY REQUEST ATTRIBUTES }}}}}}}}}}}}}}}}}");
       Print.println("{{{{{{{{{{{  this.mId   "+ this.mId + " }}}}}}}}}}}}}}}}}");
       Print.println("{{{{{{{{{{{  this.mDest   "+ this.mDest + " }}}}}}}}}}}}}}}}}");
       Print.println("{{{{{{{{{{{  this.mSrc   "+ this.mSrc + " }}}}}}}}}}}}}}}}}");
       Print.println("{{{{{{{{{{{  this.Start_time   "+ this.Start_time + " }}}}}}}}}}}}}}}}}");
       Print.println("{{{{{{{{{{{  this.End_time   "+ this.End_time + " }}}}}}}}}}}}}}}}}\n");
       
        
    };//     
    @Override
    public String returnConnectionReqType() {
        return Connection_ReqType; 
    }
    @Override
    public List<String> get_NET_ALG_types() {
        return NET_ALG_req; 
    }
             
    @Override
    public List<String> get_IT_ALG_types() {
        return IT_ALG_req; 
    }
             
        
    @Override
    public String returnAlgorithmType() {
        return mAlgorithmType; 
    }

    @Override
    public int returnId() {
        return mId;
    }

    @Override
    public int returnSrc() {
        return mSrc;
    }

    @Override
    public int returnsDest() {
        return mDest;
    }

    @Override
    public int returnBW() {
        return mBW;
    }
    @Override
    public int returnStartTime() {
        return Start_time;
    }

    @Override
    public int returnStopTime() {
        return End_time;
    }

    @Override
    public int returnDurationTime() {
        return Call_duration;
    }

    @Override
    public List<Integer> returnNETREQ() {
        return Net_requirements;
    }

    @Override
    public List<Integer> returnITREQ() {
        return IT_requirements;
    }

    @Override
    public List returnNETCostREQ() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public List returnNETDelayREQ() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public List returnITDelayREQ() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public List returnITCostREQ() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }    
}
