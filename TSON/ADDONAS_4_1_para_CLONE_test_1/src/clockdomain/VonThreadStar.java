/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package clockdomain;

import Infrastructure.Network;
import Infrastructure.ResourceOperations;
import Record.Rec_Link_Allocs;
import Record.Rec_Node_Allocs;
//import RequestTypes.Req_NET_timed;
import RequestTypes.Request_base;
import RequestTypes.Req_IT_NET_timed_startnet;
import cern.jet.random.Poisson;
import cern.jet.random.engine.DRand;
import cern.jet.random.engine.RandomEngine;
import java.util.HashMap;
import java.util.List;
import java.util.Set;
import alloc.RUN_Heuristic;
import alloc.RUN_Heuristic_Star;
import statistics.Stats_collector;
import statistics.Stats_collector_Star;
import utilities.Converters;
import utilities.Print;

/**
 *
 * @author Bijan
 * this is the class where I simulate a number of clock domains
 * simultaneously
 * 
 * get/set, add/remove, create/destroy, start/stop, insert/delete,
increment/decrement, old/new, begin/end, first/last, up/down, min/max,
next/previous, old/new, open/close, show/hide, suspend/resume, etc.
 */
public class VonThreadStar extends Thread{

    String routeSelect;
    int specToTimeMap;
    int sublMaxSpec;
    int sublMinSpec;
    int lambdaMinSpec;
    int lambdaMaxSpec;
    int flexMinSpec;      
    int flexMaxSpec;      
    //all the required components for running a thread
    //private int global_counter =0;
    
    //the VON info
    private int vonId;
    private Network mNetwork;
    private int nRoutes;
    
    //the timings of start, duration, interval and finish,
    private int startSimTime;           
    //int Req_duration;
    private int stopSimTime;
    private int requestsInterval;
    
    private int excelHoriz = 0;
    
    private boolean isDebug;
    
    //the hashmap of all the requests made for this VON
    private List<Req_IT_NET_timed_startnet> requestsList;    
    
    private String Excel_file_address;
    private Stats_collector_Star SCS;
//    private Stats_collector SCS;
    int printOffPeriod = 100;
    String[] Link_Req_types;
    String[] Star_Req_types;
    private boolean STARNET;
    static int staticIDI;
    
    // randomEngine for generating distributions
    RandomEngine randomEngine = new DRand();
    
    //the constraucture, initialises all the variables upon being called
    public VonThreadStar(boolean debug, int von_ID, Network network, int number_route,
            int initialTIme, int stop_sim, int req_interval, List<Req_IT_NET_timed_startnet> reqs,
            int excel_horiz, String excel_file_address, 
            String[] req_types,
            String[] star_req_types,
            int spec_time_map,
            int subl_min, int subl_max, int lambda_min, int lambda_max, int flex_max, 
            int flex_min, String route_select, boolean Starnet)
    {
        this.STARNET = Starnet;
        this.vonId = von_ID;
        this.startSimTime = initialTIme;
        this.nRoutes = number_route;
        //this.Req_duration = req_duration;
        this.requestsInterval = req_interval;
        this.requestsList = reqs;
        this.stopSimTime = stop_sim;
        this.mNetwork = network;     
        this.isDebug = debug;
        this.excelHoriz = excel_horiz;
        this.Excel_file_address =  excel_file_address;
        this.Link_Req_types = req_types;
        this.Star_Req_types = star_req_types;
        this.flexMinSpec = flex_min;
        this.flexMaxSpec = flex_max;
        this.sublMinSpec = subl_min;
        this.lambdaMaxSpec = lambda_max;
        this.lambdaMinSpec = lambda_min;
        this.sublMaxSpec = subl_max;
        this.specToTimeMap = spec_time_map;    
        this.routeSelect = route_select;
        
        staticIDI = 0;
    }
    
    //the thread run method overridden to perform as expected
    @Override
    public void run(){
        HashMap<String,Rec_Link_Allocs> recordLinkAllocationHashMap = new HashMap<>();
        HashMap<Integer,Rec_Node_Allocs> recordNodeAllocationHashMap = new HashMap<>();
        SCS = new Stats_collector_Star(isDebug, mNetwork,
                recordLinkAllocationHashMap, 
                recordNodeAllocationHashMap, 
                Excel_file_address,
                Link_Req_types,
                Star_Req_types);
        RUN_Heuristic_Star run_heuristic_star = new RUN_Heuristic_Star(isDebug, vonId, mNetwork, nRoutes,SCS,
                recordLinkAllocationHashMap,
                recordNodeAllocationHashMap,
                specToTimeMap,
                sublMinSpec,sublMaxSpec,lambdaMinSpec,lambdaMaxSpec, flexMaxSpec, flexMinSpec,
                routeSelect, STARNET, staticIDI);
        
        //start, simulation of serving requests over th VON, and stop
        Print.println(" %VOD_thread%\t&&&&&&&&&&&&&&&&&&&&&&& START VON_ID  of " + vonId + "  &&&&&&&&&&&&&&&& withe Excel column position " + excelHoriz + "  ******8\n");

        /*
         * the i is very important for excel writing
         */
        for(int i = startSimTime; i < requestsList.size() ; i++)    
        {   
            run_heuristic_star.init_process_request(requestsList, requestsList.get(i).returnId(), requestsList.get(i).returnStartTime());      
            if(i%printOffPeriod==0)
            {
                        //SC.demonstrate_linkRec_objects();
                        run_heuristic_star.cleanUpTheRecords();
                        
        Print.println(" &&&&&&&&&&&&&&&&&&&&&&& show_recordsArbitraryHashNodeRec(recordNodeAllocationHashMap) &&&&&&&&&&&&&&&& ");
                        

//                        run_heuristic_star.show_records(" showing results in VONSTAR   ");
                        SCS.update_rec_hash_link(recordLinkAllocationHashMap);
                        SCS.update_rec_hash_node(recordNodeAllocationHashMap);
                        SCS.process_linkRec_objects(run_heuristic_star.getNetworkObject(), run_heuristic_star.getRecord_hash_link());
                        SCS.process_nodeRec_objects(run_heuristic_star.getNetworkObject(), run_heuristic_star.getRecord_hash_node());
                        SCS.collect_network_info();
//                        SCS.print_accept_nodeid_linkid_hash(run_heuristic_star.getNetworkObject());
                        SCS.print_them_in_excel((i*1)/printOffPeriod,excelHoriz );
//                        SCS.print_network_info();
                        SCS.display_rerouting_info();
            }
        }
        
        //run_heuristic.fin();

        //SC.demonstrate_linkRec_objects();
        //SC.print_network_info();
        
        Print.println(" %VOD_thread%\t&&&&&&&&&&&&&&&&&&&&&&& STOP VON_ID  of " + vonId + "  &&&&&&&&&&&&&&&& ");

    }
    
    
    public Stats_collector_Star getm_SatisticsCollection()
    {
        return this.SCS;
    }
    
    public void show_recordsArbitraryHashNodeRec (HashMap<Integer, Rec_Node_Allocs> hm)
    {
        String [] keys_arr = Converters.key_set_obj_to_str_array(hm);
        for(int i  = 0; i < keys_arr.length; i ++)
        {
            Print.println(" <<<<<<<<< node id  -->  "+ keys_arr[i] );
            Rec_Node_Allocs node_rec  = hm.get(keys_arr[i]);
            node_rec.printResHash();
//            P.println(" <<<<<<<<< node fuckers  -->  "+ node_rec.return_all_layer_util_of_node_allocs().toString() ); 
                        
        }
 

    }   
    public void show_recordsArbitraryHashLinkRec (HashMap<String, Rec_Link_Allocs> hm)
    {
        String [] keys_arr = Converters.key_set_obj_to_str_array(hm);
        for(int i  = 0; i < keys_arr.length; i ++)
        {
            Print.println(" <<<<<<<<< Link id  -->  "+ keys_arr[i] );
            Rec_Link_Allocs link_rec  = hm.get(keys_arr[i]);
            link_rec.printResHash();
//            P.println(" <<<<<<<<< link fuckers  -->  "+ link_rec.return_all_layer_util_of_link_allocs().toString() ); 
                        
        }
  
    }     
    
}
