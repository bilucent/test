/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package clockdomain;

import Infrastructure.Network;
import Infrastructure.ResourceOperations;
import Record.Rec_Link_Allocs;
import Record.Rec_Node_Allocs;
//import RequestTypes.Req_NET_timed;
import RequestTypes.Request_base;
import cern.jet.random.Poisson;
import cern.jet.random.engine.DRand;
import cern.jet.random.engine.RandomEngine;
import java.util.HashMap;
import java.util.List;
import java.util.Set;
import alloc.RUN_Heuristic;
import statistics.Stats_collector;
import utilities.Print;

/**
 *
 * @author Bijan
 * this is the class where I simulate a number of clock domains
 * simultaneously
 * 
 * get/set, add/remove, create/destroy, start/stop, insert/delete,
increment/decrement, old/new, begin/end, first/last, up/down, min/max,
next/previous, old/new, open/close, show/hide, suspend/resume, etc.
 */
public class VonThread extends Thread{

    String routeSelect;
    int specToTimeMap;
    int sublMaxSpec;
    int sublMinSpec;
    int lambdaMinSpec;
    int lambdaMaxSpec;
    int flexMinSpec;      
    int flexMaxSpec;      
    //all the required components for running a thread
    //private int global_counter =0;
    
    //the VON info
    private int vonId;
    private Network mNetwork;
    private int nRoutes;
    
    //the timings of start, duration, interval and finish,
    private int startSimTime;           
    //int Req_duration;
    private int stopSimTime;
    private int requestsInterval;
    
    private int excelHoriz = 0;
    
    private boolean isDebug;
    
    //the hashmap of all the requests made for this VON
    private List<Request_base> requestsList;    
    
    private String Excel_file_address;
    private Stats_collector SC;
    int printOffPeriod = 120;
    String[] Req_types;
    private boolean STARNET;
    
    // randomEngine for generating distributions
    RandomEngine randomEngine = new DRand();
    
    //the constraucture, initialises all the variables upon being called
    public VonThread(boolean debug, int von_ID, Network network, int number_route,
            int initialTIme, int stop_sim, int req_interval, List<Request_base> reqs,
            int excel_horiz, String excel_file_address, String[] req_types, int spec_time_map,
            int subl_min, int subl_max, int lambda_min, int lambda_max, int flex_max, 
            int flex_min, String route_select, boolean Starnet)
    {
        this.STARNET = Starnet;
        this.vonId = von_ID;
        this.startSimTime = initialTIme;
        this.nRoutes = number_route;
        //this.Req_duration = req_duration;
        this.requestsInterval = req_interval;
        this.requestsList = reqs;
        this.stopSimTime = stop_sim;
        this.mNetwork = network;     
        this.isDebug = debug;
        this.excelHoriz = excel_horiz;
        this.Excel_file_address =  excel_file_address;
        this.Req_types = req_types;
        this.flexMinSpec = flex_min;
        this.flexMaxSpec = flex_max;
        this.sublMinSpec = subl_min;
        this.lambdaMaxSpec = lambda_max;
        this.lambdaMinSpec = lambda_min;
        this.sublMaxSpec = subl_max;
        this.specToTimeMap = spec_time_map;    
        this.routeSelect = route_select;
    }
    
    //the thread run method overridden to perform as expected
    @Override
    public void run(){
        HashMap<String,Rec_Link_Allocs> recordLinkAllocationHashMap = new HashMap<>();
        HashMap<Integer,Rec_Node_Allocs> recordNodeAllocationHashMap = new HashMap<>();
        SC = new Stats_collector(isDebug, mNetwork,recordLinkAllocationHashMap, Excel_file_address, Req_types);
        RUN_Heuristic run_heuristic = new RUN_Heuristic(isDebug, vonId, mNetwork, nRoutes,SC,
                recordLinkAllocationHashMap,
                recordNodeAllocationHashMap,
                specToTimeMap,
                sublMinSpec,sublMaxSpec,lambdaMinSpec,lambdaMaxSpec, flexMaxSpec, flexMinSpec,
                routeSelect, STARNET);
        
        //start, simulation of serving requests over th VON, and stop
        Print.println(" %VOD_thread%\t&&&&&&&&&&&&&&&&&&&&&&& START VON_ID  of " + vonId + "  &&&&&&&&&&&&&&&& withe Excel column position " + excelHoriz + "  ******8\n");

        for(int i = startSimTime; i < requestsList.size() ; i++)    
        {   
            run_heuristic.init_process_request(requestsList, requestsList.get(i).returnId(), requestsList.get(i).returnStartTime());      
            if(i%printOffPeriod==0)
            {
                        //SC.demonstrate_linkRec_objects();
                        run_heuristic.cleanUpTheRecords();
                        SC.update_rec_hash(recordLinkAllocationHashMap);
                        SC.process_linkRec_objects(mNetwork);
                        //SC.process_nodeRec_objects();
                        SC.collect_network_info();
                        SC.cal_link_network_cap_in_layers();
                        SC.cal_node_network_cap_in_layers();
                        SC.print_them_in_excel(i/120,excelHoriz );
                        SC.print_network_info();
            }
        }
        
        //run_heuristic.fin();

        //SC.demonstrate_linkRec_objects();
        //SC.print_network_info();
        
        Print.println(" %VOD_thread%\t&&&&&&&&&&&&&&&&&&&&&&& STOP VON_ID  of " + vonId + "  &&&&&&&&&&&&&&&& ");

    }
    
    
    public Stats_collector getm_SatisticsCollection()
    {
        return this.SC;
    }
}
