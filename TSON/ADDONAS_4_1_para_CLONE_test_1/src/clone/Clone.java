/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package clone;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Set;
import static utilities.Print.println;

/**
 *
 * @author Bijan
 */
public class Clone {
    
    public static List<List> CloneListList(List<List> l){
        List lfather = new ArrayList();
        for(int i = 0; i < l.size(); i++)
        {
            List lchild = (List) l.get(i) ;
            List copyChild = new ArrayList();
            for (int j = 0; j < lchild.size(); j ++)
            {
                copyChild.add(lchild.get(j));
            }
            lfather.add(copyChild);
        }
        return lfather;
    }
    
    public static HashMap<String,String> CloneHashStrStr(HashMap<String,String> hm)
    {
        HashMap<String,String> clonehm = new HashMap<>();
            Set<String> keySet = hm.keySet();
            Object[] toArray = keySet.toArray();
            String [] arrstr = new String [toArray.length];
            for (int i = 0; i < arrstr.length; i++){
                arrstr[i] = toArray[i].toString();
 
            }        
            for (int i = 0; i < arrstr.length; i++){
                String value = hm.get(arrstr[i]);
                clonehm.put(arrstr[i],value);
            }        
        return clonehm;
    }
    
}
